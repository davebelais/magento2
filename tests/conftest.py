import os

import pytest

from magento2 import Magento, model

MAGENTO_SERVER = (
    None if 'MAGENTO_SERVER' not in os.environ else os.environ['MAGENTO_SERVER']
)
MAGENTO_LOGIN = (
    None if 'MAGENTO_LOGIN' not in os.environ else os.environ['MAGENTO_LOGIN']
)


def pytest_addoption(parser):
    if MAGENTO_LOGIN:
        user, password = MAGENTO_LOGIN.split(':')
    else:
        user = password = None
    parser.addoption(
        "--server",
        action="store",
        default=MAGENTO_SERVER,
        help="server: The server's IP address or host name"
    )
    parser.addoption(
        "--user",
        action="store",
        default=user,
        help="access-token: A user account"
    )
    parser.addoption(
        "--password",
        action="store",
        default=password,
        help="access-token: The indicated user's account password"
    )


@pytest.fixture
def magento(request):
    server = request.config.getoption("--server")
    login = (
        request.config.getoption("--user"),
        request.config.getoption("--password")
    )
    if None not in login:
        username, password = login
        return Magento(
            server=server,
            authentication=model.PostAdminToken(
                username=username,
                password=password
            ),
            echo=True
        )
    else:
        raise EnvironmentError('\n'.join((
            'In order to perform unit tests for this module, please either set the following environment variables ' +
            'to reflect credentials for a current Magento server and account or integration:',
            '   - "MAGENTO_SERVER" and...',
            '   - "MAGENTO_LOGIN" (in the format "user:password")',
            '...or run pytest with (all of) the following command line options:',
            '   --server',
            '   --user',
            '   --password'
        )))
