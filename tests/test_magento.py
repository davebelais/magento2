"""
This module is intended for use with `pytest`, to test use of this SDK with a Magento (version 2.2) API. These tests can
be used to confirm that:

    - This package, "magento2", is internally consistent.
    - The REST endpoints on the server specified in `Magento().server` can be reached, and will respond in a format
      consistent with expectations (based on the latest Swagger/OpenAPI schema for Magento 2).
    - This package correctly interprets all data structures contained in responses returned by the the server, and
      is therefore presumed to be aligned/up-to-date with the current deployment.
"""
# region Backwards Compatibility
from __future__ import absolute_import, division, generators, nested_scopes, print_function, unicode_literals, \
    with_statement

from future import standard_library
standard_library.install_aliases()
from builtins import *
from future.utils import native_str
# endregion

from copy import copy

from magento2.errors import MagentoError
from base64 import b64encode
from itertools import chain
from time import sleep
from urllib.parse import urljoin

import os
from datetime import datetime, timedelta, date

import serial
from magento2 import Magento, model

try:
    import typing
    from typing import Union, Dict, Any, Tuple
except ImportError:
    typing = Union = Any = None


def test_modules(magento):
    # type: (Magento) -> None
    assert isinstance(magento.modules, serial.model.Array)


DEFAULT_PRODUCT_ATTRIBUTE_SET_ID = {}  # Cached responses


def default_product_attribute_set_id(magento):
    if magento in DEFAULT_PRODUCT_ATTRIBUTE_SET_ID:
        return DEFAULT_PRODUCT_ATTRIBUTE_SET_ID[magento]
    dpasi = None
    for attribute_set in magento.eav_attribute_sets_list(
            search_criteria=model.SearchCriteria(
                filter_groups=[
                    model.SearchFilterGroup(
                        filters=[
                            model.Filter(
                                field='entity_type_id',
                                value='4',
                                condition_type='eq'
                            )
                        ]
                    ),
                    model.SearchFilterGroup(
                        filters=[
                            model.Filter(
                                field='attribute_set_name',
                                value='Default',
                                condition_type='eq'
                            )
                        ]
                    )
                ]
            )
    ).items:
        if (
            attribute_set.attribute_set_name == 'Default' and
            int(attribute_set.entity_type_id) == 4
        ):
            dpasi = int(attribute_set.attribute_set_id)
            break
    DEFAULT_PRODUCT_ATTRIBUTE_SET_ID[magento] = dpasi
    return dpasi


PRODUCT_ATTRIBUTE_GROUP_ID = {}  # Cached responses


def product_attribute_group_id(magento):
    if magento in PRODUCT_ATTRIBUTE_GROUP_ID:
        return PRODUCT_ATTRIBUTE_GROUP_ID[magento]
    pagi = None
    for group in magento.products_attribute_set_groups_list(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='attribute_set_id',
                            value=str(default_product_attribute_set_id(magento)),  # default product attribute set
                            condition_type='eq'
                        )
                    ]
                ),
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='attribute_group_name',
                            value='Product Details',
                            condition_type='eq'
                        )
                    ]
                )
            ]
        )
    ).items:  # type: model.EAVAttributeGroup
        if group.attribute_group_name == 'Product Details':
            pagi = int(group.attribute_group_id)
            break
    PRODUCT_ATTRIBUTE_GROUP_ID[magento] = pagi
    return pagi


def test_transactions(magento):
    # type: (Magento) -> None
    transactions = magento.transactions(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='updated_at',  # created_at ?
                            value=datetime(1999, 1, 1).isoformat(),
                            condition_type='gt'
                        )
                    ]
                )
            ]
        ),
    )
    assert isinstance(transactions, model.SalesTransactionSearchResult)


PRODUCTS_SPECIAL_PRICES = {}


def test_products_special_prices(magento):
    if magento in PRODUCTS_SPECIAL_PRICES:
        return PRODUCTS_SPECIAL_PRICES[magento]
    products = test_products(magento)
    skus = [
        product.sku for product in products if product.price
    ]
    special_prices = magento.products_special_price_information(
        skus
    )
    serial.marshal.validate(special_prices)
    assert bool(special_prices) is True
    price_update_results = magento.products_special_price_delete(
        special_prices
    )
    serial.marshal.validate(price_update_results)
    assert len(price_update_results) == 0
    serial.marshal.validate(price_update_results)
    after_delete_special_prices = magento.products_special_price_information(
        skus
    )
    serial.marshal.validate(after_delete_special_prices)
    products_special_price_response = magento.products_special_price(
        special_prices
    )
    serial.marshal.validate(products_special_price_response)
    after_resurrect_special_prices = magento.products_special_price_information(
        skus
    )
    assert after_resurrect_special_prices == special_prices
    PRODUCTS_SPECIAL_PRICES[magento] = after_resurrect_special_prices
    
    
PRODUCTS_COST = {}


def test_products_cost(magento):
    if magento in PRODUCTS_COST:
        return PRODUCTS_COST[magento]
    cost = magento.products_cost_information(
        CHILD_SKUS
    )
    serial.marshal.validate(cost)
    assert len(cost) > 0
    assert magento.products_cost_delete(
        CHILD_SKUS
    ) is True
    after_delete_cost = magento.products_cost_information(
        CHILD_SKUS
    )
    serial.marshal.validate(after_delete_cost)
    assert len(after_delete_cost) == 0
    products_cost_response = magento.products_cost(
        cost
    )
    serial.marshal.validate(products_cost_response)
    after_resurrect_cost = magento.products_cost_information(
        CHILD_SKUS
    )
    assert after_resurrect_cost == cost
    PRODUCTS_COST[magento] = after_resurrect_cost
    return PRODUCTS_COST[magento]


PRODUCTS_BASE_PRICES = {}


def test_products_base_prices(magento):
    if magento in PRODUCTS_BASE_PRICES:
        return PRODUCTS_BASE_PRICES[magento]
    base_prices = magento.products_base_prices_information(
        CHILD_SKUS
    )
    serial.marshal.validate(base_prices)
    assert bool(base_prices) is True
    new_base_prices = copy(base_prices)
    for new_base_price in new_base_prices:
        new_base_price.price += 1
    products_base_prices_response = magento.products_base_prices(
        new_base_prices
    )
    serial.marshal.validate(products_base_prices_response)
    updated_base_prices = magento.products_base_prices_information(
        CHILD_SKUS
    )
    assert updated_base_prices == new_base_prices
    PRODUCTS_BASE_PRICES[magento] = new_base_prices
    return PRODUCTS_BASE_PRICES[magento]


def cleanup_categories(magento):
    deleted_categories = set()
    for category in magento.categories_list(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='url_key',
                            value='test-category-%',
                            condition_type='like'
                        ),
                        model.Filter(
                            field='url_key',
                            value='test-root-category',
                            condition_type='eq'
                        )
                    ]
                )
            ]
        )
    ).items:  # type: model.Category
        deleted = False
        for c in category.path.split('/'):
            c = int(c)
            if c == category.id_:
                break
            elif c in deleted_categories:
                deleted = True
                break
        if deleted:
            continue
        deleted = magento.categories(
            delete=category.id_
        )
        assert deleted is True
        deleted_categories.add(category.id_)


TEST_CATEGORIES = {}  # Cached responses


def test_categories(magento):
    # type: (Magento) -> Sequence[model.Category]
    if magento in TEST_CATEGORIES:
        return TEST_CATEGORIES[magento]
    test_categories = []
    # Retrieve the default category tree
    cleanup_categories(magento)
    category_tree = magento.categories(root_category_id=2)
    assert isinstance(category_tree, model.CategoryTree)
    serial.test.json(category_tree)
    root_category_id = category_tree.id_
    assert root_category_id == 2
    # Retrieve a category by ID
    root_category = magento.categories(get=root_category_id)
    assert isinstance(root_category, model.Category)
    serial.test.json(root_category)
    test_category_root = magento.categories(
        post=model.Category(
            parent_id=2,
            name='Test Root Category',
            is_active=True,
            custom_attributes=[
                model.Attribute(
                    attribute_code='url_key',
                    value='test-root-category',
                )
            ]
        )
    )
    test_categories.append(test_category_root)
    assert isinstance(test_category_root, model.Category)
    serial.test.json(test_category_root)
    for k in 'abc':
        category = magento.categories(
            post=model.Category(
                parent_id=test_category_root.id_,
                name='Test Category ' + k.upper(),
                is_active=True,
                custom_attributes=[
                    model.Attribute(
                        attribute_code='url_key',
                        value='test-category-' + k,
                    )
                ]
            )
        )
        serial.test.json(category)
        test_categories.append(category)
    TEST_CATEGORIES[magento] = test_categories
    return test_categories


def test_products_attribute_set_groups_list(magento):
    # type: (Magento) -> None
    for group in magento.products_attribute_set_groups_list(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='attribute_set_id',
                            value=str(default_product_attribute_set_id(magento)),  # default product attribute set
                            condition_type='eq'
                        )
                    ]
                )
            ]
        )
    ).items:  # type: model.EAVAttributeGroup
        serial.test.json(group)


def test_eav(magento):
    # type: (Magento) -> None
    for attribute_set in magento.eav_attribute_sets_list(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='entity_type_id',
                            value='0',
                            condition_type='gt'
                        )
                    ]
                )
            ]
        )
    ).items:
        assert isinstance(attribute_set, model.EAVAttributeSet)
        serial.test.json(attribute_set)
        if int(attribute_set.entity_type_id) == 4:  # product
            for attribute in magento.products_attribute_set_attributes(int(attribute_set.attribute_set_id)):
                assert isinstance(attribute, model.ProductAttribute)
                serial.test.json(attribute)


PRODUCTS_ATTRIBUTES = {}  # Cached responses


def test_products_attributes(magento):
    # type: (Magento) -> typing.Sequence[model.EAVAttributeOption]
    # Find the default product attribute set
    if magento in PRODUCTS_ATTRIBUTES:
        return PRODUCTS_ATTRIBUTES[magento]
    for t in magento.products_attributes_types:
        assert isinstance(t, model.ProductAttributeType)
        serial.test.json(t)
    for a in magento.products_attributes(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='attribute_code',
                            value='',
                            condition_type='neq'
                        )
                    ]
                )
            ]
        )
    ).items:
        serial.test.json(a)
        if 'test_attribute' in a.attribute_code:
            magento.products_attributes(
                delete=a.attribute_code
            )
    test_attribute_a = magento.products_attributes(
        post=model.ProductAttribute(
            attribute_code='test_attribute_a',
            default_frontend_label='Test Attribute A',
            frontend_input='select',  # 'select',
            entity_type_id='4',  # product
            is_required=False,
            frontend_labels=serial.properties.NULL
        )
    )
    serial.test.json(test_attribute_a)
    magento.products_attribute_set_attributes(
        post=model.PostProductsAttributeSetsAttributes(
            attribute_set_id=default_product_attribute_set_id(magento),
            attribute_group_id=product_attribute_group_id(magento),
            attribute_code='test_attribute_a',
            sort_order=0
        )
    )
    test_attribute_b = magento.products_attributes(
        put=model.ProductAttribute(
            attribute_code='test_attribute_b',
            default_frontend_label='Test Attribute B',
            frontend_input='select',  # 'select',
            entity_type_id='4',  # product
            is_required=False,
            frontend_labels=serial.properties.NULL
        )
    )
    serial.test.json(test_attribute_b)
    magento.products_attribute_set_attributes(
        post=model.PostProductsAttributeSetsAttributes(
            attribute_set_id=default_product_attribute_set_id(magento),
            attribute_group_id=product_attribute_group_id(magento),
            attribute_code='test_attribute_b',
            sort_order=0
        )
    )
    magento.products_attribute_options(
        'test_attribute_a',
        post=model.EAVAttributeOption(
            label='Test Attribute A Option 1',
            sort_order=99999999999997
        )
    )
    magento.products_attribute_options(
        'test_attribute_a',
        post=model.EAVAttributeOption(
            label='Test Attribute A Option 2',
            sort_order=99999999999998
        )
    )
    magento.products_attribute_options(
        'test_attribute_a',
        post=model.EAVAttributeOption(
            label='Test Attribute A Option 3',
            sort_order=99999999999999
        )
    )
    for option in magento.products_attribute_options(
        'test_attribute_a'
    ):
        assert isinstance(option, model.EAVAttributeOption)
    # B
    magento.products_attribute_options(
        'test_attribute_b',
        post=model.EAVAttributeOption(
            label='Test Attribute B Option 1',
            sort_order=99999999999997
        )
    )
    magento.products_attribute_options(
        'test_attribute_b',
        post=model.EAVAttributeOption(
            label='Test Attribute B Option 2',
            sort_order=99999999999998
        )
    )
    magento.products_attribute_options(
        'test_attribute_b',
        post=model.EAVAttributeOption(
            label='Test Attribute B Option 3',
            sort_order=99999999999999
        )
    )
    PRODUCTS_ATTRIBUTES[magento] = 1
    return 1


PARENT_SKU = 'M900000000000001'

CHILD_SKUS = (
    '900000000000001',
    '900000000000018',
    '900000000000025',
)

PRODUCTS = {}  # Cached responses


def test_products(magento):
    # type: (Magento) -> Dict[str, Sequence[str]]
    if magento in PRODUCTS:
        return PRODUCTS[magento]
    products = []
    test_products_attributes(magento)
    categories = test_categories(magento)
    option_attribute_a = magento.products_attributes(get='test_attribute_a')  # model.ProductAttribute
    assert isinstance(option_attribute_a, model.ProductAttribute)
    serial.test.json(option_attribute_a)
    option_attribute_b = magento.products_attributes(get='test_attribute_b')  # model.ProductAttribute
    assert isinstance(option_attribute_b, model.ProductAttribute)
    serial.test.json(option_attribute_b)
    option_attribute_size = magento.products_attributes(get='size')  # model.ProductAttribute
    assert isinstance(option_attribute_size, model.ProductAttribute)
    serial.test.json(option_attribute_size)
    labels_values_a = {}
    for option in magento.products_attribute_options('test_attribute_a'):
        if option.value != '':
            labels_values_a[option.label] = int(option.value)
    labels_values_b = {}
    for option in magento.products_attribute_options('test_attribute_b'):
        if option.value != '':
            labels_values_b[option.label] = int(option.value)
    labels_values_size = {}
    for option in magento.products_attribute_options('size'):
        if option.value != '':
            labels_values_size[option.label] = int(option.value)
    for sku in chain(CHILD_SKUS, [PARENT_SKU]):
        try:
            assert magento.products(delete=sku) is True
        except MagentoError as e:
            if not (e.http_error and e.http_error.code == 404):
                raise e
    rainbows = []
    for i in range(1, 5):
        with open(urljoin(str(__file__), 'data/rainbow%s.png' % str(i)), 'rb') as f:
            rainbows.append(str(b64encode(f.read()), 'ascii'))
    media_gallery_entries = [
        model.ProductAttributeMediaGalleryEntry(
            media_type='image',
            label='rainbow%s.png' % str(i + 1),
            position=i,
            disabled=False,
            types=[
                'image',
                'small_image',
                'thumbnail',
                'swatch_image',
            ],
            content=model.Image(
                name='rainbow%s.png' % str(i + 1),
                base_64_encoded_data=rainbows[i],
                type_='image/png'
            )
        ) for i in range(len(rainbows))
    ]
    size_values = tuple(labels_values_size.keys())[1:4]
    products.append(magento.products(
        post=model.PostProducts(
            product=model.Product(
                sku=PARENT_SKU,
                name='Brand Sub-Brand Functional Name',attribute_set_id=default_product_attribute_set_id(magento),
                status=1,
                visibility=4,
                type_id='configurable',
                extension_attributes=(
                    model.ProductExtension(
                        category_links=[
                            model.CategoryLink(
                                position=0,
                                category_id=str(c.id_),
                            ) for c in categories
                        ],
                        stock_item=model.InventoryStockItem(
                            qty=0,
                            is_in_stock=True,
                            notify_stock_qty=0,
                            stock_status_changed_auto=1
                        )
                    )
                ),
                custom_attributes=(
                    [
                        model.Attribute(
                            attribute_code='url_key',
                            value=PARENT_SKU
                        ),
                        model.Attribute(
                            attribute_code='description',
                            value=(
                                '<p>This is the romance copy%s.</p>' +
                                '<ul>' +
                                '<li>These</li>' +
                                '<li>are</li>' +
                                '<li>the</li>' +
                                '<li>features</li>' +
                                '</ul>' +
                                '<p>...+ etc.</p>'
                            ) % (
                                ''
                                if sku[0] == 'M' else
                                ' for ' + sku
                            )
                        ),
                        model.Attribute(
                            attribute_code='test_attribute_a',
                            value=str(labels_values_a['Test Attribute A Option 1'])
                        ),
                        model.Attribute(
                            attribute_code='test_attribute_b',
                            value=str(labels_values_b['Test Attribute B Option 1'])
                        ),
                        model.Attribute(
                            attribute_code='size',
                            value=str(labels_values_size[size_values[0]])
                        )
                    ]
                )
            ),
            save_options=False
        )
    ))
    # magento.configurable_products_options(
    #     parent_sku,
    #     post=model.ConfigurableProductOption(
    #         attribute_id=str(option_attribute_a.attribute_id),
    #         label=option_attribute_a.default_frontend_label,
    #         values=[
    #             model.ConfigurableProductOptionValue(
    #                 value_index=value
    #             ) for value in labels_values_a.values()
    #         ],
    #     )
    # )
    # magento.configurable_products_options(
    #     parent_sku,
    #     post=model.ConfigurableProductOption(
    #         attribute_id=str(option_attribute_b.attribute_id),
    #         label=option_attribute_b.default_frontend_label,
    #         values=[
    #             model.ConfigurableProductOptionValue(
    #                 value_index=v
    #             ) for v in labels_values_b.values()
    #         ],
    #     )
    # )
    magento.configurable_products_options(
        PARENT_SKU,
        post=model.ConfigurableProductOption(
            attribute_id=str(option_attribute_size.attribute_id),
            label=option_attribute_size.default_frontend_label,
            values=[
                model.ConfigurableProductOptionValue(
                    value_index=labels_values_size[v]
                ) for v in size_values
            ]
        )
    )

    n = 1
    for sku in CHILD_SKUS:
        label_a = 'Test Attribute A Option ' + str(n)
        label_b = 'Test Attribute B Option ' + str(n)
        products.append(magento.products(
            post=model.PostProducts(
                product=model.Product(
                    sku=sku,
                    name='Brand Sub-Brand Functional Name - ' + sku,
                    attribute_set_id=default_product_attribute_set_id(magento),
                    price=9.99,
                    status=1,
                    visibility=1,
                    type_id='simple',
                    weight=50,
                    extension_attributes=(
                        model.ProductExtension(
                            stock_item=model.InventoryStockItem(
                                qty=999,
                                is_in_stock=True,
                                notify_stock_qty=0,
                                stock_status_changed_auto=1
                            )
                        )
                    ),
                    custom_attributes=(
                        [
                            model.Attribute(
                                attribute_code='url_key',
                                value=sku
                            ),
                            model.Attribute(
                                attribute_code='special_price',
                                value='6.66'
                            ),
                            model.Attribute(
                                attribute_code='tax_class_id',
                                value='2'  # taxable
                            ),
                            model.Attribute(
                                attribute_code='country_of_manufacture',
                                value='US',
                            ),
                            model.Attribute(
                                attribute_code='cost',
                                value='2.99',
                            ),
                            model.Attribute(
                                attribute_code='test_attribute_a',
                                value=str(labels_values_a[label_a])
                            ),
                            model.Attribute(
                                attribute_code='test_attribute_b',
                                value=str(labels_values_b[label_b])
                            ),
                            model.Attribute(
                                attribute_code='size',
                                value=str(labels_values_size[size_values[n-1]])
                            )
                        ]
                    )
                ),
                save_options=False
            )
        ))
        magento.configurable_products_child(PARENT_SKU, sku)
        n += 1
    for mge in reversed(media_gallery_entries):
        magento.products_media(PARENT_SKU, post=mge)
    for child in magento.configurable_products_children(
        PARENT_SKU
    ):
        assert isinstance(child, model.Product)
        serial.test.json(child)
    products = tuple(products)
    PRODUCTS[magento] = products
    return products


PRODUCT_CATEGORIES = {}  # Cached responses


def test_product_categories(magento):
    # type: (Magento) -> None
    if magento in PRODUCT_CATEGORIES:
        return PRODUCT_CATEGORIES[magento]
    categories = test_categories(magento)  # type: Sequence[model.Category]
    sku = test_products(magento)[0].sku  # type: str
    product_categories = []
    for category in categories:
        skus = set()
        for cpl in magento.categories_products(category.id_):
            assert isinstance(
                cpl,
                model.CategoryProductLink
            )
            if int(cpl.category_id) == category.id_:
                skus.add(cpl.sku)
        assert sku in skus
        magento.categories_products(category.id_, delete=sku)
        skus = set()
        categories_products = magento.categories_products(category.id_)
        serial.test.json(categories_products)
        for cpl in categories_products:
            assert isinstance(
                cpl,
                model.CategoryProductLink
            )
            if int(cpl.category_id) == category.id_:
                skus.add(cpl.sku)
        assert magento.categories_products(
            category.id_,
            post=model.CategoryProductLink(
                sku=sku,
                position=0,
                category_id=str(category.id_)
            )
        ) is True
        assert magento.categories_products(
            category.id_,
            put=model.CategoryProductLink(
                sku=sku,
                position=1,
                category_id=str(category.id_)
            )
        ) is True
        skus = set()
        categories_products = magento.categories_products(category.id_)
        serial.test.json(categories_products)
        for cpl in categories_products:
            assert isinstance(
                cpl,
                model.CategoryProductLink
            )
            if int(cpl.category_id) == category.id_:
                product_categories.append(cpl)
                skus.add(cpl.sku)
                if sku == cpl.sku:
                    assert cpl.position == 1
        assert sku in skus
    product_categories = tuple(product_categories)
    PRODUCT_CATEGORIES[magento] = product_categories
    return product_categories


def cleanup_tax_classes(magento):
    for tax_class in magento.tax_classes(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='class_type',
                            value='PRODUCT',
                            condition_type='eq'
                        )
                    ]
                ),
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='class_name',
                            value='TEST-CLASS-%',
                            condition_type='like'
                        )
                    ]
                )
            ]
        )
    ).items:
        assert magento.tax_classes(delete=tax_class.class_id) is True


TAX_CLASSES = {}  # Cached responses


def test_tax_classes(magento):
    # type: (Magento) -> Tuple[model.TaxClass]
    if magento in TAX_CLASSES:
        return TAX_CLASSES[magento]
    cleanup_tax_classes(magento)
    search_results = magento.tax_classes(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='class_type',
                            value='PRODUCT',
                            condition_type='eq'
                        )
                    ]
                )
            ]
        )
    )  # type: model.TaxClassSearchResults
    serial.test.json(search_results)
    # Identify pre-existing tax classes, so we only cleanup tax classes created for the purpose of testing
    existing_product_tax_classes = {}
    assert len(search_results.items) > 0
    for tax_class in search_results.items:  # type: model.TaxClass
        tax_class_id = int(tax_class.class_id)
        existing_product_tax_classes[tax_class_id] = tax_class
        serial.test.json(magento.tax_classes(
            get=tax_class_id
        ))
    new_tax_classes = {}
    for i in range(3):
        tax_class = model.TaxClass(
            class_name='TEST-CLASS-' + str(i),
            class_type='PRODUCT'
        )
        tax_class_id = magento.tax_classes(
            post=tax_class
        )
        assert isinstance(tax_class_id, int)
        tax_class.class_id = tax_class_id
        new_tax_classes[tax_class_id] = tax_class
    for tax_class_id, tax_class in new_tax_classes.items():
        tax_class.class_name += '-UPDATED'
        updated_tax_class_id = magento.tax_classes(
            put=tax_class
        )
        assert updated_tax_class_id == tax_class_id
    new_tax_classes = tuple(new_tax_classes.values())
    TAX_CLASSES[magento] = new_tax_classes
    return new_tax_classes


def cleanup_sales_rules(magento):
    sales_rules_search_result = magento.sales_rules(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='name',
                            value='TEST-%',
                            condition_type='like'
                        )
                        # model.Filter(
                        #     field='rule_id',
                        #     value='-1',
                        #     condition_type='gt'
                        # )
                    ]
                )
            ]
        )
    )
    assert isinstance(sales_rules_search_result, model.SalesRuleSearchResult)
    serial.test.json(sales_rules_search_result)
    for sales_rule in sales_rules_search_result.items:
        try:
            response = magento.sales_rules(delete=int(sales_rule.rule_id))
            assert response is True
        except MagentoError as e:
            if not (e.http_error and e.http_error.code == 404):
                raise e


SALES_RULES = {}  # Cached responses


def test_sales_rules(magento):
    # type: (Magento) -> Tuple[model.SalesRule]
    if magento in SALES_RULES:
        return SALES_RULES[magento]
    cleanup_sales_rules(magento)
    sales_rules = []
    categories = test_categories(magento)
    store_views = magento.store_views
    website_ids = tuple(set(
        int(store.website_id)
        for store in
        store_views
    ))
    customer_group_ids = tuple(sorted(set(
        customer_group.id_ for customer_group in
        magento.customer_groups(
            search_criteria=model.SearchCriteria(
                filter_groups=[
                    model.SearchFilterGroup(
                        filters=[
                            model.Filter(
                                field='code',
                                value='%',
                                condition_type='like'
                            )
                        ]
                    )
                ]
            )
        ).items
    )))
    for category in categories[1:]:  # type: model.Category
        sales_rule_name = 'TEST-SALES-RULE-' + category.name.replace(' ', '-')
        condition = model.SalesRuleCondition(
            condition_type='Magento\\SalesRule\\Model\\Rule\\Condition\\Combine',
            conditions=[
                model.SalesRuleCondition(
                    condition_type='Magento\\SalesRule\\Model\\Rule\\Condition\\Category',
                    operator='==',
                    attribute_name='category_ids',
                    value=str(category.id_),
                ),
            ],
            aggregator_type='any',
            operator=serial.properties.NULL,
            value='1',
        )
        sales_rule = magento.sales_rules(
            post=model.SalesRule(
                name=sales_rule_name,
                store_labels=[],  # type: Sequence[model.SalesRuleLabel]
                description='%s Description' % sales_rule_name,
                website_ids=website_ids,
                customer_group_ids=customer_group_ids,
                from_date=(date.today() - timedelta(days=1)).isoformat(),
                to_date=(date.today() + timedelta(days=30)).isoformat(),
                uses_per_customer=666,
                is_active=True,
                condition=condition,
                action_condition=condition,
                stop_rules_processing=False,
                is_advanced=True,
                sort_order=1,
                simple_action='by_fixed',
                discount_amount=9.99,
                discount_qty=10,
                discount_step=0,
                apply_to_shipping=False,
                times_used=0,
                is_rss=True,
                coupon_type='SPECIFIC_COUPON',
                use_auto_generation=False,
                uses_per_coupon=999,
                simple_free_shipping='0',
            )
        )
        serial.test.json(sales_rule)
        sales_rules += [
            sales_rule
        ]
    sales_rules = tuple(sales_rules)
    SALES_RULES[magento] = sales_rules
    return sales_rules


def cleanup_coupons(magento):
    sales_rule_coupon_search_results = magento.coupons(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='code',
                            value='TEST-%',
                            condition_type='like'
                        )
                    ]
                )
            ]
        )
    )
    serial.test.json(sales_rule_coupon_search_results)
    for sales_rule_coupon in sales_rule_coupon_search_results.items:
        try:
            assert magento.coupons(delete=sales_rule_coupon.coupon_id) is True
        except MagentoError as e:
            if not (e.http_error and e.http_error.code == 404):
                raise e


COUPONS = {}  # Cached responses


def test_coupons(magento):
    if magento in COUPONS:
        return COUPONS[magento]
    sales_rules = test_sales_rules(magento)
    coupons = []
    i = 0
    for sales_rule in sales_rules:
        i += 1
        sales_rule_coupon = magento.coupons(
            post=model.SalesRuleCoupon(
                rule_id=int(sales_rule.rule_id),
                code='TEST-COUPON-' + str(i),
                usage_limit=999,
                usage_per_customer=666,
                times_used=0,
                expiration_date=(date.today() + timedelta(days=30)).isoformat(),
                is_primary=True,
                type_=0,
            )
        )
        assert isinstance(sales_rule_coupon, model.SalesRuleCoupon)
        coupons.append(sales_rule_coupon)
    coupons = tuple(coupons)
    COUPONS[magento] = coupons
    return coupons


def test_store_websites(magento):
    store_websites = magento.store_websites
    serial.test.json(store_websites)
    for store_website in store_websites:
        assert isinstance(store_website, model.StoreWebsite)


def test_store_configs(magento):
    store_configs = magento.store_configs
    serial.test.json(store_configs)
    for store_config in store_configs:
        assert isinstance(store_config, model.StoreConfig)


STORE_VIEWS = {}  # Cached result


def test_store_views(magento):
    # type: (Magento) -> Sequence[model.Store]
    if magento in STORE_VIEWS:
        return STORE_VIEWS[magento]
    store_views = magento.store_views
    serial.test.json(store_views)
    for store_view in store_views:
        assert isinstance(store_view, model.Store)
    store_views = tuple(store_views)
    STORE_VIEWS[magento] = store_views
    return store_views


def test_store_groups(magento):
    store_groups = magento.store_groups
    serial.test.json(store_groups)
    for store_group in store_groups:
        assert isinstance(store_group, model.StoreGroup)


CUSTOMER_GROUPS_DEFAULT = {}


def test_customer_groups_default(magento):
    if magento in CUSTOMER_GROUPS_DEFAULT:
        return CUSTOMER_GROUPS_DEFAULT[magento]
    customer_groups_default = {}
    customer_groups_default[-1]= magento.customer_groups_default()
    for store_view in test_store_views(magento):
        store_id = int(store_view.id_)
        customer_group = magento.customer_groups_default(store_id)
        serial.test.json(customer_group)
        assert isinstance(customer_group, model.CustomerGroup)
        customer_groups_default[store_id] = customer_group
    CUSTOMER_GROUPS_DEFAULT[magento] = customer_groups_default
    return customer_groups_default


CUSTOMER_GROUPS = {}


def test_customer_groups(magento):
    if magento in CUSTOMER_GROUPS:
        return CUSTOMER_GROUPS[magento]
    customer_groups = []
    customer_groups_search_results = magento.customer_groups(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='code',
                            value='%',
                            condition_type='like'
                        )
                    ]
                )
            ]
        )
    )
    assert isinstance(customer_groups_search_results, model.CustomerGroupSearchResults)
    serial.test.json(customer_groups_search_results)
    for customer_group in customer_groups_search_results.items:
        assert isinstance(customer_group, model.CustomerGroup)
        customer_groups.append(
            customer_group
        )
    CUSTOMER_GROUPS[magento] = customer_groups
    # TODO: Complete test_customer_groups
    return customer_groups


CARTS = {}


def test_carts(magento):
    # type: (Magento) -> model.QuoteCart
    if magento in CARTS:
        return CARTS[magento]
    coupons = test_coupons(magento)
    sales_rules = test_sales_rules(magento)
    products = test_products(magento)
    cart_id = magento.carts()
    assert isinstance(cart_id, int)
    cart_items = []
    for product in products[1:]:
        cart_item = magento.carts_items(
            post=model.QuoteCartItem(
                quote_id=str(cart_id),
                sku=product.sku,
                qty=3
            )
        )  # type: model.QuoteCartItem
        serial.test.json(cart_item)
        assert isinstance(cart_item, model.QuoteCartItem)
        cart_items.append(cart_item)
    for coupon in coupons:
        coupon_applied_successfully = magento.carts_coupons(
            cart_id,
            put=coupon.code
        )
        assert coupon_applied_successfully is True
    address = model.QuoteAddress(
        region='Oregon',
        region_id=49,
        region_code='OR',
        country_id='US',
        street=['4909 SE 65th Ave'],
        telephone='15032085267',
        postcode='97206',
        city='Portland',
        firstname='David',
        lastname='Belais',
        email='david@belais.me'
    )
    shipping_methods = magento.carts_estimate_shipping_methods(
        cart_id,
        address
    )
    serial.test.json(shipping_methods)
    print(repr(shipping_methods))
    cart_shipping_information = magento.carts_shipping_information(
        cart_id,
        model.CheckoutShippingInformation(
            shipping_address=address,
            billing_address=address,
            shipping_method_code=shipping_methods[0].method_code,
            shipping_carrier_code=shipping_methods[0].carrier_code
        )
    )
    assert isinstance(cart_shipping_information, model.CheckoutPaymentDetails)
    serial.test.json(cart_shipping_information)
    cart_shipping_methods = magento.carts_shipping_methods(cart_id)
    serial.test.json(cart_shipping_methods)
    cart = magento.carts(get=cart_id)
    serial.test.json(cart)
    assert isinstance(cart, model.QuoteCart)
    cart_totals = magento.carts_totals(cart_id)
    serial.test.json(cart_totals)
    assert isinstance(cart_totals, model.QuoteTotals)
    for cart_item in magento.carts_items(cart_id=cart_id):
        serial.test.json(cart_item)
        assert isinstance(cart_item, model.QuoteCartItem)
        successfully_deleted = magento.carts_items(
            cart_id,
            delete=cart_item.item_id
        )  # type: bool
        assert successfully_deleted is True
    CARTS[magento] = cart
    return cart
    # TODO:  Complete `test_carts`


def test_carts_order(magento):
    cart = test_carts(magento)
    checkmo_payment_method = None
    for payment_method in magento.carts_payment_methods(cart.id_):
        if payment_method.code == 'checkmo':
            checkmo_payment_method = payment_method
            break
    magento.carts_order(
        put=checkmo_payment_method
    )


ORDERS = {}


def test_orders(magento):
    # type: (Magento) -> model.SalesOrder
    if magento in ORDERS:
        return ORDERS[magento]
    order = magento.orders_create(
        model.SalesOrder(

        )
    )
    ORDERS[magento] = order
    return order
    # TODO:  Complete `test_orders`


CUSTOMERS = {}


def test_customers(magento):
    if magento in CUSTOMERS:
        return CUSTOMERS[magento]
    customer_search_result = magento.customers_search(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='email',
                            value='david@belais.me',
                            condition_type='='
                        )
                    ]
                )
            ]
        )
    )  # type: model.CustomerSearchResults
    serial.marshal.validate(customer_search_result)
    if len(customer_search_result.items) > 0:  # type: model.Customer
        customer = customer_search_result.items[0]
        magento.customers(delete=int(customer.id_))
    customer = magento.customers(
        post=model.PostCustomers(
            customer=model.Customer(
                group_id=1,
                default_billing='1',
                default_shipping='1',
                created_in='Default Store View',
                dob='1900-01-01',
                email='david@belais.me',
                firstname='David',
                lastname='Belais',
                middlename='Isaac',
                prefix='Mr',
                suffix='David Belais',
                gender=0,
                store_id=1,
                website_id=1,
                addresses=[
                    model.CustomerAddress(
                        region=model.CustomerRegion(
                            region_code='OR',
                            region='Oregon',
                            region_id=49,
                        ),
                        region_id=49,
                        country_id='US',
                        street=[
                            '4909 SE 65th Ave',
                        ],
                        telephone='503-208-5267',
                        postcode='97206',
                        city='Portland',
                        firstname='David',
                        lastname='Belais',
                        middlename='Isaac',
                        prefix='Mr',
                        suffix='',
                        vat_id='4909 SE 65th Ave',
                        default_shipping=True,
                        default_billing=True,
                    ),
                    model.CustomerAddress(
                        region=model.CustomerRegion(
                            region_code='OR',
                            region='Oregon',
                            region_id=49,
                        ),
                        region_id=49,
                        country_id='US',
                        street=[
                            '4909 SE 65th Ave',
                        ],
                        telephone='503-208-5267',
                        postcode='97206',
                        city='Portland',
                        firstname='David',
                        lastname='Belais',
                        middlename='Isaac',
                        prefix='Mr',
                        suffix='',
                    ),
                ],
                disable_auto_group_change=0,
            )
        )
    )
    print(repr(customer))
    CUSTOMERS[magento] = customer
    return customer


def test_cleanup(magento):
    cleanup_categories(magento)
    for sku in (
        'M900000000000001',
        '900000000000001',
        '900000000000018',
        '900000000000025'
    ):
        try:
            assert magento.products(delete=sku) is True
        except MagentoError as e:
            if not (e.http_error and e.http_error.code == 404):
                raise e
    for a in magento.products_attributes(
        search_criteria=model.SearchCriteria(
            filter_groups=[
                model.SearchFilterGroup(
                    filters=[
                        model.Filter(
                            field='attribute_code',
                            value='test_attribute%',
                            condition_type='like'
                        )
                    ]
                )
            ]
        )
    ).items:
        if 'test_attribute' in a.attribute_code:
            try:
                magento.products_attributes(
                    delete=a.attribute_code
                )
            except MagentoError as e:
                if not (e.http_error and e.http_error.code == 404):
                    raise e
    cleanup_tax_classes(magento)
    cleanup_sales_rules(magento)
    cleanup_coupons(magento)


if __name__ == '__main__':
    username, password = os.environ['MAGENTO_LOGIN'].split(':')
    magento = Magento(
        server=os.environ['MAGENTO_SERVER'],
        authentication=model.PostAdminToken(
            username=username,
            password=password
        ),
        echo=True
    )
    for name, function in copy(locals()).items():
        if name[:5] == 'test_' and callable(function): # and name != 'test_cleanup':
            function(magento)
