# region Backwards Compatibility
from __future__ import nested_scopes, generators, division, absolute_import, with_statement, \
   print_function, unicode_literals
from future import standard_library
standard_library.install_aliases()
from builtins import *
# endregion

import serial
try:
    from typing import Union, Dict, Any, Sequence, IO
except ImportError:
    Union = Dict = Any = Sequence = IO = None


class CompanyTeamSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-data-team-search-results-interface
    
    Interface for company team search results
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CompanyTeam], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class CompanyTeam(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-team-interface
    
    Team interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        name=None,  # type: Optional[str]
        description=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.id_ = id_
        self.name = name
        self.description = description
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class Attribute(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-attribute-interface
    
    Interface for custom attribute value.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        value=None,  # type: Optional[Union[Union[str, Sequence[Union[str, bool, int]]], serial.properties.Null]]
    ):
        self.attribute_code = attribute_code
        self.value = value
        super().__init__(_)


class SearchCriteria(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-criteria-interface
    
    Search criteria interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        filter_groups=None,  # type: Optional[Union[Sequence[SearchFilterGroup], serial.properties.Null]]
        sort_orders=None,  # type: Optional[Sequence[SortOrder]]
        page_size=None,  # type: Optional[int]
        current_page=None,  # type: Optional[int]
    ):
        self.filter_groups = filter_groups
        self.sort_orders = sort_orders
        self.page_size = page_size
        self.current_page = current_page
        super().__init__(_)


class SearchFilterGroup(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-filter-group
    
    Groups two or more filters together using a logical OR
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        filters=None,  # type: Optional[Sequence[Filter]]
    ):
        self.filters = filters
        super().__init__(_)


class Filter(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-filter
    
    Filter which can be used by any methods from service layer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        field=None,  # type: Optional[Union[str, serial.properties.Null]]
        value=None,  # type: Optional[Union[str, serial.properties.Null]]
        condition_type=None,  # type: Optional[str]
    ):
        self.field = field
        self.value = value
        self.condition_type = condition_type
        super().__init__(_)


class SortOrder(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-sort-order
    
    Data object for sort order.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        field=None,  # type: Optional[Union[str, serial.properties.Null]]
        direction=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.field = field
        self.direction = direction
        super().__init__(_)


class ErrorResponse(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/error-response
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        message=None,  # type: Optional[Union[str, serial.properties.Null]]
        errors=None,  # type: Optional[Sequence[ErrorErrorsItem]]
        code=None,  # type: Optional[int]
        parameters=None,  # type: Optional[Union[Union[Sequence[ErrorParametersItem], Sequence[str]], ErrorParametersItem]]
        trace=None,  # type: Optional[str]
        messages=None,  # type: Optional[dict]
    ):
        self.message = message
        self.errors = errors
        self.code = code
        self.parameters = parameters
        self.trace = trace
        self.messages = messages
        super().__init__(_)


class ErrorErrorsItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/error-errors-item
    
    Error details
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        message=None,  # type: Optional[str]
        parameters=None,  # type: Optional[Sequence[ErrorParametersItem]]
    ):
        self.message = message
        self.parameters = parameters
        super().__init__(_)


class ErrorParametersItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/error-parameters-item
    
    Error parameters item
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        resources=None,  # type: Optional[str]
        field_name=None,  # type: Optional[str]
        field_value=None,  # type: Optional[str]
    ):
        self.resources = resources
        self.field_name = field_name
        self.field_value = field_value
        super().__init__(_)


class SearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/framework-search-search-result-interface
    
    Interface SearchResultInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SearchDocument], serial.properties.Null]]
        aggregations=None,  # type: Optional[Union[SearchAggregation, serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[FrameworkSearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.aggregations = aggregations
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class SearchDocument(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-document-interface
    
    Interface \Magento\Framework\Api\Search\DocumentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.id_ = id_
        self.custom_attributes = custom_attributes
        super().__init__(_)


class SearchAggregation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/framework-search-aggregation-interface
    
    Faceted data
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        buckets=None,  # type: Optional[Union[Sequence[SearchBucket], serial.properties.Null]]
        bucket_names=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.buckets = buckets
        self.bucket_names = bucket_names
        super().__init__(_)


class SearchBucket(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-bucket-interface
    
    Facet Bucket
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        values=None,  # type: Optional[Union[Sequence[SearchAggregationValue], serial.properties.Null]]
    ):
        self.name = name
        self.values = values
        super().__init__(_)


class SearchAggregationValue(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/framework-search-aggregation-value-interface
    
    Interface \Magento\Framework\Api\Search\AggregationValueInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        value=None,  # type: Optional[Union[str, serial.properties.Null]]
        metrics=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.value = value
        self.metrics = metrics
        super().__init__(_)


class FrameworkSearchCriteria(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/framework-search-search-criteria-interface
    
    Interface SearchCriteriaInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        request_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        filter_groups=None,  # type: Optional[Union[Sequence[SearchFilterGroup], serial.properties.Null]]
        sort_orders=None,  # type: Optional[Sequence[SortOrder]]
        page_size=None,  # type: Optional[int]
        current_page=None,  # type: Optional[int]
    ):
        self.request_name = request_name
        self.filter_groups = filter_groups
        self.sort_orders = sort_orders
        self.page_size = page_size
        self.current_page = current_page
        super().__init__(_)


class SalesOrderSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-order-search-result-interface
    
    Order search result interface. An order is a document that a web store issues to a customer. Magento generates a 
    sales order that lists the product items, billing and shipping addresses, and shipping and payment methods. A 
    corresponding external document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesOrder], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class SalesOrder(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-interface
    
    Order interface. An order is a document that a web store issues to a customer. Magento generates a sales order that 
    lists the product items, billing and shipping addresses, and shipping and payment methods. A corresponding external 
    document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        adjustment_negative=None,  # type: Optional[numbers.Number]
        adjustment_positive=None,  # type: Optional[numbers.Number]
        applied_rule_ids=None,  # type: Optional[str]
        base_adjustment_negative=None,  # type: Optional[numbers.Number]
        base_adjustment_positive=None,  # type: Optional[numbers.Number]
        base_currency_code=None,  # type: Optional[str]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        base_discount_canceled=None,  # type: Optional[numbers.Number]
        base_discount_invoiced=None,  # type: Optional[numbers.Number]
        base_discount_refunded=None,  # type: Optional[numbers.Number]
        base_grand_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_invoiced=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_refunded=None,  # type: Optional[numbers.Number]
        base_shipping_amount=None,  # type: Optional[numbers.Number]
        base_shipping_canceled=None,  # type: Optional[numbers.Number]
        base_shipping_discount_amount=None,  # type: Optional[numbers.Number]
        base_shipping_discount_tax_compensation_amnt=None,  # type: Optional[numbers.Number]
        base_shipping_incl_tax=None,  # type: Optional[numbers.Number]
        base_shipping_invoiced=None,  # type: Optional[numbers.Number]
        base_shipping_refunded=None,  # type: Optional[numbers.Number]
        base_shipping_tax_amount=None,  # type: Optional[numbers.Number]
        base_shipping_tax_refunded=None,  # type: Optional[numbers.Number]
        base_subtotal=None,  # type: Optional[numbers.Number]
        base_subtotal_canceled=None,  # type: Optional[numbers.Number]
        base_subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        base_subtotal_invoiced=None,  # type: Optional[numbers.Number]
        base_subtotal_refunded=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        base_tax_canceled=None,  # type: Optional[numbers.Number]
        base_tax_invoiced=None,  # type: Optional[numbers.Number]
        base_tax_refunded=None,  # type: Optional[numbers.Number]
        base_total_canceled=None,  # type: Optional[numbers.Number]
        base_total_due=None,  # type: Optional[numbers.Number]
        base_total_invoiced=None,  # type: Optional[numbers.Number]
        base_total_invoiced_cost=None,  # type: Optional[numbers.Number]
        base_total_offline_refunded=None,  # type: Optional[numbers.Number]
        base_total_online_refunded=None,  # type: Optional[numbers.Number]
        base_total_paid=None,  # type: Optional[numbers.Number]
        base_total_qty_ordered=None,  # type: Optional[numbers.Number]
        base_total_refunded=None,  # type: Optional[numbers.Number]
        base_to_global_rate=None,  # type: Optional[numbers.Number]
        base_to_order_rate=None,  # type: Optional[numbers.Number]
        billing_address_id=None,  # type: Optional[int]
        can_ship_partially=None,  # type: Optional[int]
        can_ship_partially_item=None,  # type: Optional[int]
        coupon_code=None,  # type: Optional[str]
        created_at=None,  # type: Optional[str]
        customer_dob=None,  # type: Optional[str]
        customer_email=None,  # type: Optional[Union[str, serial.properties.Null]]
        customer_firstname=None,  # type: Optional[str]
        customer_gender=None,  # type: Optional[int]
        customer_group_id=None,  # type: Optional[int]
        customer_id=None,  # type: Optional[int]
        customer_is_guest=None,  # type: Optional[int]
        customer_lastname=None,  # type: Optional[str]
        customer_middlename=None,  # type: Optional[str]
        customer_note=None,  # type: Optional[str]
        customer_note_notify=None,  # type: Optional[int]
        customer_prefix=None,  # type: Optional[str]
        customer_suffix=None,  # type: Optional[str]
        customer_taxvat=None,  # type: Optional[str]
        discount_amount=None,  # type: Optional[numbers.Number]
        discount_canceled=None,  # type: Optional[numbers.Number]
        discount_description=None,  # type: Optional[str]
        discount_invoiced=None,  # type: Optional[numbers.Number]
        discount_refunded=None,  # type: Optional[numbers.Number]
        edit_increment=None,  # type: Optional[int]
        email_sent=None,  # type: Optional[int]
        entity_id=None,  # type: Optional[int]
        ext_customer_id=None,  # type: Optional[str]
        ext_order_id=None,  # type: Optional[str]
        forced_shipment_with_invoice=None,  # type: Optional[int]
        global_currency_code=None,  # type: Optional[str]
        grand_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_invoiced=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_refunded=None,  # type: Optional[numbers.Number]
        hold_before_state=None,  # type: Optional[str]
        hold_before_status=None,  # type: Optional[str]
        increment_id=None,  # type: Optional[str]
        is_virtual=None,  # type: Optional[int]
        order_currency_code=None,  # type: Optional[str]
        original_increment_id=None,  # type: Optional[str]
        payment_authorization_amount=None,  # type: Optional[numbers.Number]
        payment_auth_expiration=None,  # type: Optional[int]
        protect_code=None,  # type: Optional[str]
        quote_address_id=None,  # type: Optional[int]
        quote_id=None,  # type: Optional[int]
        relation_child_id=None,  # type: Optional[str]
        relation_child_real_id=None,  # type: Optional[str]
        relation_parent_id=None,  # type: Optional[str]
        relation_parent_real_id=None,  # type: Optional[str]
        remote_ip=None,  # type: Optional[str]
        shipping_amount=None,  # type: Optional[numbers.Number]
        shipping_canceled=None,  # type: Optional[numbers.Number]
        shipping_description=None,  # type: Optional[str]
        shipping_discount_amount=None,  # type: Optional[numbers.Number]
        shipping_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        shipping_incl_tax=None,  # type: Optional[numbers.Number]
        shipping_invoiced=None,  # type: Optional[numbers.Number]
        shipping_refunded=None,  # type: Optional[numbers.Number]
        shipping_tax_amount=None,  # type: Optional[numbers.Number]
        shipping_tax_refunded=None,  # type: Optional[numbers.Number]
        state=None,  # type: Optional[str]
        status=None,  # type: Optional[str]
        store_currency_code=None,  # type: Optional[str]
        store_id=None,  # type: Optional[int]
        store_name=None,  # type: Optional[str]
        store_to_base_rate=None,  # type: Optional[numbers.Number]
        store_to_order_rate=None,  # type: Optional[numbers.Number]
        subtotal=None,  # type: Optional[numbers.Number]
        subtotal_canceled=None,  # type: Optional[numbers.Number]
        subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        subtotal_invoiced=None,  # type: Optional[numbers.Number]
        subtotal_refunded=None,  # type: Optional[numbers.Number]
        tax_amount=None,  # type: Optional[numbers.Number]
        tax_canceled=None,  # type: Optional[numbers.Number]
        tax_invoiced=None,  # type: Optional[numbers.Number]
        tax_refunded=None,  # type: Optional[numbers.Number]
        total_canceled=None,  # type: Optional[numbers.Number]
        total_due=None,  # type: Optional[numbers.Number]
        total_invoiced=None,  # type: Optional[numbers.Number]
        total_item_count=None,  # type: Optional[int]
        total_offline_refunded=None,  # type: Optional[numbers.Number]
        total_online_refunded=None,  # type: Optional[numbers.Number]
        total_paid=None,  # type: Optional[numbers.Number]
        total_qty_ordered=None,  # type: Optional[numbers.Number]
        total_refunded=None,  # type: Optional[numbers.Number]
        updated_at=None,  # type: Optional[str]
        weight=None,  # type: Optional[numbers.Number]
        x_forwarded_for=None,  # type: Optional[str]
        items=None,  # type: Optional[Union[Sequence[SalesOrderItem], serial.properties.Null]]
        billing_address=None,  # type: Optional[SalesOrderAddress]
        payment=None,  # type: Optional[SalesOrderPayment]
        status_histories=None,  # type: Optional[Sequence[SalesOrderStatusHistory]]
        extension_attributes=None,  # type: Optional[SalesOrderExtension]
    ):
        self.adjustment_negative = adjustment_negative
        self.adjustment_positive = adjustment_positive
        self.applied_rule_ids = applied_rule_ids
        self.base_adjustment_negative = base_adjustment_negative
        self.base_adjustment_positive = base_adjustment_positive
        self.base_currency_code = base_currency_code
        self.base_discount_amount = base_discount_amount
        self.base_discount_canceled = base_discount_canceled
        self.base_discount_invoiced = base_discount_invoiced
        self.base_discount_refunded = base_discount_refunded
        self.base_grand_total = base_grand_total
        self.base_discount_tax_compensation_amount = base_discount_tax_compensation_amount
        self.base_discount_tax_compensation_invoiced = base_discount_tax_compensation_invoiced
        self.base_discount_tax_compensation_refunded = base_discount_tax_compensation_refunded
        self.base_shipping_amount = base_shipping_amount
        self.base_shipping_canceled = base_shipping_canceled
        self.base_shipping_discount_amount = base_shipping_discount_amount
        self.base_shipping_discount_tax_compensation_amnt = base_shipping_discount_tax_compensation_amnt
        self.base_shipping_incl_tax = base_shipping_incl_tax
        self.base_shipping_invoiced = base_shipping_invoiced
        self.base_shipping_refunded = base_shipping_refunded
        self.base_shipping_tax_amount = base_shipping_tax_amount
        self.base_shipping_tax_refunded = base_shipping_tax_refunded
        self.base_subtotal = base_subtotal
        self.base_subtotal_canceled = base_subtotal_canceled
        self.base_subtotal_incl_tax = base_subtotal_incl_tax
        self.base_subtotal_invoiced = base_subtotal_invoiced
        self.base_subtotal_refunded = base_subtotal_refunded
        self.base_tax_amount = base_tax_amount
        self.base_tax_canceled = base_tax_canceled
        self.base_tax_invoiced = base_tax_invoiced
        self.base_tax_refunded = base_tax_refunded
        self.base_total_canceled = base_total_canceled
        self.base_total_due = base_total_due
        self.base_total_invoiced = base_total_invoiced
        self.base_total_invoiced_cost = base_total_invoiced_cost
        self.base_total_offline_refunded = base_total_offline_refunded
        self.base_total_online_refunded = base_total_online_refunded
        self.base_total_paid = base_total_paid
        self.base_total_qty_ordered = base_total_qty_ordered
        self.base_total_refunded = base_total_refunded
        self.base_to_global_rate = base_to_global_rate
        self.base_to_order_rate = base_to_order_rate
        self.billing_address_id = billing_address_id
        self.can_ship_partially = can_ship_partially
        self.can_ship_partially_item = can_ship_partially_item
        self.coupon_code = coupon_code
        self.created_at = created_at
        self.customer_dob = customer_dob
        self.customer_email = customer_email
        self.customer_firstname = customer_firstname
        self.customer_gender = customer_gender
        self.customer_group_id = customer_group_id
        self.customer_id = customer_id
        self.customer_is_guest = customer_is_guest
        self.customer_lastname = customer_lastname
        self.customer_middlename = customer_middlename
        self.customer_note = customer_note
        self.customer_note_notify = customer_note_notify
        self.customer_prefix = customer_prefix
        self.customer_suffix = customer_suffix
        self.customer_taxvat = customer_taxvat
        self.discount_amount = discount_amount
        self.discount_canceled = discount_canceled
        self.discount_description = discount_description
        self.discount_invoiced = discount_invoiced
        self.discount_refunded = discount_refunded
        self.edit_increment = edit_increment
        self.email_sent = email_sent
        self.entity_id = entity_id
        self.ext_customer_id = ext_customer_id
        self.ext_order_id = ext_order_id
        self.forced_shipment_with_invoice = forced_shipment_with_invoice
        self.global_currency_code = global_currency_code
        self.grand_total = grand_total
        self.discount_tax_compensation_amount = discount_tax_compensation_amount
        self.discount_tax_compensation_invoiced = discount_tax_compensation_invoiced
        self.discount_tax_compensation_refunded = discount_tax_compensation_refunded
        self.hold_before_state = hold_before_state
        self.hold_before_status = hold_before_status
        self.increment_id = increment_id
        self.is_virtual = is_virtual
        self.order_currency_code = order_currency_code
        self.original_increment_id = original_increment_id
        self.payment_authorization_amount = payment_authorization_amount
        self.payment_auth_expiration = payment_auth_expiration
        self.protect_code = protect_code
        self.quote_address_id = quote_address_id
        self.quote_id = quote_id
        self.relation_child_id = relation_child_id
        self.relation_child_real_id = relation_child_real_id
        self.relation_parent_id = relation_parent_id
        self.relation_parent_real_id = relation_parent_real_id
        self.remote_ip = remote_ip
        self.shipping_amount = shipping_amount
        self.shipping_canceled = shipping_canceled
        self.shipping_description = shipping_description
        self.shipping_discount_amount = shipping_discount_amount
        self.shipping_discount_tax_compensation_amount = shipping_discount_tax_compensation_amount
        self.shipping_incl_tax = shipping_incl_tax
        self.shipping_invoiced = shipping_invoiced
        self.shipping_refunded = shipping_refunded
        self.shipping_tax_amount = shipping_tax_amount
        self.shipping_tax_refunded = shipping_tax_refunded
        self.state = state
        self.status = status
        self.store_currency_code = store_currency_code
        self.store_id = store_id
        self.store_name = store_name
        self.store_to_base_rate = store_to_base_rate
        self.store_to_order_rate = store_to_order_rate
        self.subtotal = subtotal
        self.subtotal_canceled = subtotal_canceled
        self.subtotal_incl_tax = subtotal_incl_tax
        self.subtotal_invoiced = subtotal_invoiced
        self.subtotal_refunded = subtotal_refunded
        self.tax_amount = tax_amount
        self.tax_canceled = tax_canceled
        self.tax_invoiced = tax_invoiced
        self.tax_refunded = tax_refunded
        self.total_canceled = total_canceled
        self.total_due = total_due
        self.total_invoiced = total_invoiced
        self.total_item_count = total_item_count
        self.total_offline_refunded = total_offline_refunded
        self.total_online_refunded = total_online_refunded
        self.total_paid = total_paid
        self.total_qty_ordered = total_qty_ordered
        self.total_refunded = total_refunded
        self.updated_at = updated_at
        self.weight = weight
        self.x_forwarded_for = x_forwarded_for
        self.items = items
        self.billing_address = billing_address
        self.payment = payment
        self.status_histories = status_histories
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesOrderItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-item-interface
    
    Order item interface. An order is a document that a web store issues to a customer. Magento generates a sales order 
    that lists the product items, billing and shipping addresses, and shipping and payment methods. A corresponding 
    external document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        additional_data=None,  # type: Optional[str]
        amount_refunded=None,  # type: Optional[numbers.Number]
        applied_rule_ids=None,  # type: Optional[str]
        base_amount_refunded=None,  # type: Optional[numbers.Number]
        base_cost=None,  # type: Optional[numbers.Number]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        base_discount_invoiced=None,  # type: Optional[numbers.Number]
        base_discount_refunded=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_invoiced=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_refunded=None,  # type: Optional[numbers.Number]
        base_original_price=None,  # type: Optional[numbers.Number]
        base_price=None,  # type: Optional[numbers.Number]
        base_price_incl_tax=None,  # type: Optional[numbers.Number]
        base_row_invoiced=None,  # type: Optional[numbers.Number]
        base_row_total=None,  # type: Optional[numbers.Number]
        base_row_total_incl_tax=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        base_tax_before_discount=None,  # type: Optional[numbers.Number]
        base_tax_invoiced=None,  # type: Optional[numbers.Number]
        base_tax_refunded=None,  # type: Optional[numbers.Number]
        base_weee_tax_applied_amount=None,  # type: Optional[numbers.Number]
        base_weee_tax_applied_row_amnt=None,  # type: Optional[numbers.Number]
        base_weee_tax_disposition=None,  # type: Optional[numbers.Number]
        base_weee_tax_row_disposition=None,  # type: Optional[numbers.Number]
        created_at=None,  # type: Optional[str]
        description=None,  # type: Optional[str]
        discount_amount=None,  # type: Optional[numbers.Number]
        discount_invoiced=None,  # type: Optional[numbers.Number]
        discount_percent=None,  # type: Optional[numbers.Number]
        discount_refunded=None,  # type: Optional[numbers.Number]
        event_id=None,  # type: Optional[int]
        ext_order_item_id=None,  # type: Optional[str]
        free_shipping=None,  # type: Optional[int]
        gw_base_price=None,  # type: Optional[numbers.Number]
        gw_base_price_invoiced=None,  # type: Optional[numbers.Number]
        gw_base_price_refunded=None,  # type: Optional[numbers.Number]
        gw_base_tax_amount=None,  # type: Optional[numbers.Number]
        gw_base_tax_amount_invoiced=None,  # type: Optional[numbers.Number]
        gw_base_tax_amount_refunded=None,  # type: Optional[numbers.Number]
        gw_id=None,  # type: Optional[int]
        gw_price=None,  # type: Optional[numbers.Number]
        gw_price_invoiced=None,  # type: Optional[numbers.Number]
        gw_price_refunded=None,  # type: Optional[numbers.Number]
        gw_tax_amount=None,  # type: Optional[numbers.Number]
        gw_tax_amount_invoiced=None,  # type: Optional[numbers.Number]
        gw_tax_amount_refunded=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_canceled=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_invoiced=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_refunded=None,  # type: Optional[numbers.Number]
        is_qty_decimal=None,  # type: Optional[int]
        is_virtual=None,  # type: Optional[int]
        item_id=None,  # type: Optional[int]
        locked_do_invoice=None,  # type: Optional[int]
        locked_do_ship=None,  # type: Optional[int]
        name=None,  # type: Optional[str]
        no_discount=None,  # type: Optional[int]
        order_id=None,  # type: Optional[int]
        original_price=None,  # type: Optional[numbers.Number]
        parent_item_id=None,  # type: Optional[int]
        price=None,  # type: Optional[numbers.Number]
        price_incl_tax=None,  # type: Optional[numbers.Number]
        product_id=None,  # type: Optional[int]
        product_type=None,  # type: Optional[str]
        qty_backordered=None,  # type: Optional[numbers.Number]
        qty_canceled=None,  # type: Optional[numbers.Number]
        qty_invoiced=None,  # type: Optional[numbers.Number]
        qty_ordered=None,  # type: Optional[numbers.Number]
        qty_refunded=None,  # type: Optional[numbers.Number]
        qty_returned=None,  # type: Optional[numbers.Number]
        qty_shipped=None,  # type: Optional[numbers.Number]
        quote_item_id=None,  # type: Optional[int]
        row_invoiced=None,  # type: Optional[numbers.Number]
        row_total=None,  # type: Optional[numbers.Number]
        row_total_incl_tax=None,  # type: Optional[numbers.Number]
        row_weight=None,  # type: Optional[numbers.Number]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        store_id=None,  # type: Optional[int]
        tax_amount=None,  # type: Optional[numbers.Number]
        tax_before_discount=None,  # type: Optional[numbers.Number]
        tax_canceled=None,  # type: Optional[numbers.Number]
        tax_invoiced=None,  # type: Optional[numbers.Number]
        tax_percent=None,  # type: Optional[numbers.Number]
        tax_refunded=None,  # type: Optional[numbers.Number]
        updated_at=None,  # type: Optional[str]
        weee_tax_applied=None,  # type: Optional[str]
        weee_tax_applied_amount=None,  # type: Optional[numbers.Number]
        weee_tax_applied_row_amount=None,  # type: Optional[numbers.Number]
        weee_tax_disposition=None,  # type: Optional[numbers.Number]
        weee_tax_row_disposition=None,  # type: Optional[numbers.Number]
        weight=None,  # type: Optional[numbers.Number]
        parent_item=None,  # type: Optional[SalesOrderItem]
        product_option=None,  # type: Optional[ProductOption]
        extension_attributes=None,  # type: Optional[SalesOrderItemExtension]
    ):
        self.additional_data = additional_data
        self.amount_refunded = amount_refunded
        self.applied_rule_ids = applied_rule_ids
        self.base_amount_refunded = base_amount_refunded
        self.base_cost = base_cost
        self.base_discount_amount = base_discount_amount
        self.base_discount_invoiced = base_discount_invoiced
        self.base_discount_refunded = base_discount_refunded
        self.base_discount_tax_compensation_amount = base_discount_tax_compensation_amount
        self.base_discount_tax_compensation_invoiced = base_discount_tax_compensation_invoiced
        self.base_discount_tax_compensation_refunded = base_discount_tax_compensation_refunded
        self.base_original_price = base_original_price
        self.base_price = base_price
        self.base_price_incl_tax = base_price_incl_tax
        self.base_row_invoiced = base_row_invoiced
        self.base_row_total = base_row_total
        self.base_row_total_incl_tax = base_row_total_incl_tax
        self.base_tax_amount = base_tax_amount
        self.base_tax_before_discount = base_tax_before_discount
        self.base_tax_invoiced = base_tax_invoiced
        self.base_tax_refunded = base_tax_refunded
        self.base_weee_tax_applied_amount = base_weee_tax_applied_amount
        self.base_weee_tax_applied_row_amnt = base_weee_tax_applied_row_amnt
        self.base_weee_tax_disposition = base_weee_tax_disposition
        self.base_weee_tax_row_disposition = base_weee_tax_row_disposition
        self.created_at = created_at
        self.description = description
        self.discount_amount = discount_amount
        self.discount_invoiced = discount_invoiced
        self.discount_percent = discount_percent
        self.discount_refunded = discount_refunded
        self.event_id = event_id
        self.ext_order_item_id = ext_order_item_id
        self.free_shipping = free_shipping
        self.gw_base_price = gw_base_price
        self.gw_base_price_invoiced = gw_base_price_invoiced
        self.gw_base_price_refunded = gw_base_price_refunded
        self.gw_base_tax_amount = gw_base_tax_amount
        self.gw_base_tax_amount_invoiced = gw_base_tax_amount_invoiced
        self.gw_base_tax_amount_refunded = gw_base_tax_amount_refunded
        self.gw_id = gw_id
        self.gw_price = gw_price
        self.gw_price_invoiced = gw_price_invoiced
        self.gw_price_refunded = gw_price_refunded
        self.gw_tax_amount = gw_tax_amount
        self.gw_tax_amount_invoiced = gw_tax_amount_invoiced
        self.gw_tax_amount_refunded = gw_tax_amount_refunded
        self.discount_tax_compensation_amount = discount_tax_compensation_amount
        self.discount_tax_compensation_canceled = discount_tax_compensation_canceled
        self.discount_tax_compensation_invoiced = discount_tax_compensation_invoiced
        self.discount_tax_compensation_refunded = discount_tax_compensation_refunded
        self.is_qty_decimal = is_qty_decimal
        self.is_virtual = is_virtual
        self.item_id = item_id
        self.locked_do_invoice = locked_do_invoice
        self.locked_do_ship = locked_do_ship
        self.name = name
        self.no_discount = no_discount
        self.order_id = order_id
        self.original_price = original_price
        self.parent_item_id = parent_item_id
        self.price = price
        self.price_incl_tax = price_incl_tax
        self.product_id = product_id
        self.product_type = product_type
        self.qty_backordered = qty_backordered
        self.qty_canceled = qty_canceled
        self.qty_invoiced = qty_invoiced
        self.qty_ordered = qty_ordered
        self.qty_refunded = qty_refunded
        self.qty_returned = qty_returned
        self.qty_shipped = qty_shipped
        self.quote_item_id = quote_item_id
        self.row_invoiced = row_invoiced
        self.row_total = row_total
        self.row_total_incl_tax = row_total_incl_tax
        self.row_weight = row_weight
        self.sku = sku
        self.store_id = store_id
        self.tax_amount = tax_amount
        self.tax_before_discount = tax_before_discount
        self.tax_canceled = tax_canceled
        self.tax_invoiced = tax_invoiced
        self.tax_percent = tax_percent
        self.tax_refunded = tax_refunded
        self.updated_at = updated_at
        self.weee_tax_applied = weee_tax_applied
        self.weee_tax_applied_amount = weee_tax_applied_amount
        self.weee_tax_applied_row_amount = weee_tax_applied_row_amount
        self.weee_tax_disposition = weee_tax_disposition
        self.weee_tax_row_disposition = weee_tax_row_disposition
        self.weight = weight
        self.parent_item = parent_item
        self.product_option = product_option
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-option-interface
    
    Product option interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[ProductOptionExtension]
    ):
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductOptionExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-option-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\ProductOptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        custom_options=None,  # type: Optional[Sequence[CustomOption]]
        bundle_options=None,  # type: Optional[Sequence[BundleOption]]
        configurable_item_options=None,  # type: Optional[Sequence[ConfigurableProductItemOptionValue]]
        downloadable_option=None,  # type: Optional[DownloadableOption]
        giftcard_item_option=None,  # type: Optional[GiftCardOption]
    ):
        self.custom_options = custom_options
        self.bundle_options = bundle_options
        self.configurable_item_options = configurable_item_options
        self.downloadable_option = downloadable_option
        self.giftcard_item_option = giftcard_item_option
        super().__init__(_)


class CustomOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-custom-option-interface
    
    Interface CustomOptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        option_value=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[CustomOptionExtension]
    ):
        self.option_id = option_id
        self.option_value = option_value
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CustomOptionExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-custom-option-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\CustomOptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        file_info=None,  # type: Optional[Image]
    ):
        self.file_info = file_info
        super().__init__(_)


class Image(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/framework-data-image-content-interface
    
    Image Content data interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        base_64_encoded_data=None,  # type: Optional[Union[str, serial.properties.Null]]
        type_=None,  # type: Optional[Union[str, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.base_64_encoded_data = base_64_encoded_data
        self.type_ = type_
        self.name = name
        super().__init__(_)


class BundleOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-bundle-option-interface
    
    Interface BundleOptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        option_qty=None,  # type: Optional[Union[int, serial.properties.Null]]
        option_selections=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.option_id = option_id
        self.option_qty = option_qty
        self.option_selections = option_selections
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ConfigurableProductItemOptionValue(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/configurable-product-data-configurable-item-option-value-interface
    
    Interface ConfigurableItemOptionValueInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        option_value=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.option_id = option_id
        self.option_value = option_value
        self.extension_attributes = extension_attributes
        super().__init__(_)


class DownloadableOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/downloadable-data-downloadable-option-interface
    
    Downloadable Option
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        downloadable_links=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
    ):
        self.downloadable_links = downloadable_links
        super().__init__(_)


class GiftCardOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/gift-card-data-gift-card-option-interface
    
    Interface GiftCardOptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        giftcard_amount=None,  # type: Optional[Union[str, serial.properties.Null]]
        custom_giftcard_amount=None,  # type: Optional[numbers.Number]
        giftcard_sender_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        giftcard_recipient_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        giftcard_sender_email=None,  # type: Optional[Union[str, serial.properties.Null]]
        giftcard_recipient_email=None,  # type: Optional[Union[str, serial.properties.Null]]
        giftcard_message=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.giftcard_amount = giftcard_amount
        self.custom_giftcard_amount = custom_giftcard_amount
        self.giftcard_sender_name = giftcard_sender_name
        self.giftcard_recipient_name = giftcard_recipient_name
        self.giftcard_sender_email = giftcard_sender_email
        self.giftcard_recipient_email = giftcard_recipient_email
        self.giftcard_message = giftcard_message
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesOrderItemExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-order-item-extension-interface
    
    ExtensionInterface class for @see \Magento\Sales\Api\Data\OrderItemInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message=None,  # type: Optional[GiftMessage]
        gw_id=None,  # type: Optional[str]
        gw_base_price=None,  # type: Optional[str]
        gw_price=None,  # type: Optional[str]
        gw_base_tax_amount=None,  # type: Optional[str]
        gw_tax_amount=None,  # type: Optional[str]
        gw_base_price_invoiced=None,  # type: Optional[str]
        gw_price_invoiced=None,  # type: Optional[str]
        gw_base_tax_amount_invoiced=None,  # type: Optional[str]
        gw_tax_amount_invoiced=None,  # type: Optional[str]
        gw_base_price_refunded=None,  # type: Optional[str]
        gw_price_refunded=None,  # type: Optional[str]
        gw_base_tax_amount_refunded=None,  # type: Optional[str]
        gw_tax_amount_refunded=None,  # type: Optional[str]
    ):
        self.gift_message = gift_message
        self.gw_id = gw_id
        self.gw_base_price = gw_base_price
        self.gw_price = gw_price
        self.gw_base_tax_amount = gw_base_tax_amount
        self.gw_tax_amount = gw_tax_amount
        self.gw_base_price_invoiced = gw_base_price_invoiced
        self.gw_price_invoiced = gw_price_invoiced
        self.gw_base_tax_amount_invoiced = gw_base_tax_amount_invoiced
        self.gw_tax_amount_invoiced = gw_tax_amount_invoiced
        self.gw_base_price_refunded = gw_base_price_refunded
        self.gw_price_refunded = gw_price_refunded
        self.gw_base_tax_amount_refunded = gw_base_tax_amount_refunded
        self.gw_tax_amount_refunded = gw_tax_amount_refunded
        super().__init__(_)


class GiftMessage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/gift-message-data-message-interface
    
    Interface MessageInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message_id=None,  # type: Optional[int]
        customer_id=None,  # type: Optional[int]
        sender=None,  # type: Optional[Union[str, serial.properties.Null]]
        recipient=None,  # type: Optional[Union[str, serial.properties.Null]]
        message=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[GiftMessageExtension]
    ):
        self.gift_message_id = gift_message_id
        self.customer_id = customer_id
        self.sender = sender
        self.recipient = recipient
        self.message = message
        self.extension_attributes = extension_attributes
        super().__init__(_)


class GiftMessageExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/gift-message-data-message-extension-interface
    
    ExtensionInterface class for @see \Magento\GiftMessage\Api\Data\MessageInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity_id=None,  # type: Optional[str]
        entity_type=None,  # type: Optional[str]
        wrapping_id=None,  # type: Optional[int]
        wrapping_allow_gift_receipt=None,  # type: Optional[bool]
        wrapping_add_printed_card=None,  # type: Optional[bool]
    ):
        self.entity_id = entity_id
        self.entity_type = entity_type
        self.wrapping_id = wrapping_id
        self.wrapping_allow_gift_receipt = wrapping_allow_gift_receipt
        self.wrapping_add_printed_card = wrapping_add_printed_card
        super().__init__(_)


class SalesOrderAddress(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-address-interface
    
    Order address interface. An order is a document that a web store issues to a customer. Magento generates a sales 
    order that lists the product items, billing and shipping addresses, and shipping and payment methods. A 
    corresponding external document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        city=None,  # type: Optional[Union[str, serial.properties.Null]]
        company=None,  # type: Optional[str]
        country_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        customer_address_id=None,  # type: Optional[int]
        customer_id=None,  # type: Optional[int]
        email=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
        fax=None,  # type: Optional[str]
        firstname=None,  # type: Optional[Union[str, serial.properties.Null]]
        lastname=None,  # type: Optional[Union[str, serial.properties.Null]]
        middlename=None,  # type: Optional[str]
        parent_id=None,  # type: Optional[int]
        postcode=None,  # type: Optional[Union[str, serial.properties.Null]]
        prefix=None,  # type: Optional[str]
        region=None,  # type: Optional[str]
        region_code=None,  # type: Optional[str]
        region_id=None,  # type: Optional[int]
        street=None,  # type: Optional[Sequence[str]]
        suffix=None,  # type: Optional[str]
        telephone=None,  # type: Optional[Union[str, serial.properties.Null]]
        vat_id=None,  # type: Optional[str]
        vat_is_valid=None,  # type: Optional[int]
        vat_request_date=None,  # type: Optional[str]
        vat_request_id=None,  # type: Optional[str]
        vat_request_success=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.address_type = address_type
        self.city = city
        self.company = company
        self.country_id = country_id
        self.customer_address_id = customer_address_id
        self.customer_id = customer_id
        self.email = email
        self.entity_id = entity_id
        self.fax = fax
        self.firstname = firstname
        self.lastname = lastname
        self.middlename = middlename
        self.parent_id = parent_id
        self.postcode = postcode
        self.prefix = prefix
        self.region = region
        self.region_code = region_code
        self.region_id = region_id
        self.street = street
        self.suffix = suffix
        self.telephone = telephone
        self.vat_id = vat_id
        self.vat_is_valid = vat_is_valid
        self.vat_request_date = vat_request_date
        self.vat_request_id = vat_request_id
        self.vat_request_success = vat_request_success
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesOrderPayment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-payment-interface
    
    Order payment interface. An order is a document that a web store issues to a customer. Magento generates a sales 
    order that lists the product items, billing and shipping addresses, and shipping and payment methods. A 
    corresponding external document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        account_status=None,  # type: Optional[Union[str, serial.properties.Null]]
        additional_data=None,  # type: Optional[str]
        additional_information=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        address_status=None,  # type: Optional[str]
        amount_authorized=None,  # type: Optional[numbers.Number]
        amount_canceled=None,  # type: Optional[numbers.Number]
        amount_ordered=None,  # type: Optional[numbers.Number]
        amount_paid=None,  # type: Optional[numbers.Number]
        amount_refunded=None,  # type: Optional[numbers.Number]
        anet_trans_method=None,  # type: Optional[str]
        base_amount_authorized=None,  # type: Optional[numbers.Number]
        base_amount_canceled=None,  # type: Optional[numbers.Number]
        base_amount_ordered=None,  # type: Optional[numbers.Number]
        base_amount_paid=None,  # type: Optional[numbers.Number]
        base_amount_paid_online=None,  # type: Optional[numbers.Number]
        base_amount_refunded=None,  # type: Optional[numbers.Number]
        base_amount_refunded_online=None,  # type: Optional[numbers.Number]
        base_shipping_amount=None,  # type: Optional[numbers.Number]
        base_shipping_captured=None,  # type: Optional[numbers.Number]
        base_shipping_refunded=None,  # type: Optional[numbers.Number]
        cc_approval=None,  # type: Optional[str]
        cc_avs_status=None,  # type: Optional[str]
        cc_cid_status=None,  # type: Optional[str]
        cc_debug_request_body=None,  # type: Optional[str]
        cc_debug_response_body=None,  # type: Optional[str]
        cc_debug_response_serialized=None,  # type: Optional[str]
        cc_exp_month=None,  # type: Optional[str]
        cc_exp_year=None,  # type: Optional[str]
        cc_last_4=None,  # type: Optional[Union[str, serial.properties.Null]]
        cc_number_enc=None,  # type: Optional[str]
        cc_owner=None,  # type: Optional[str]
        cc_secure_verify=None,  # type: Optional[str]
        cc_ss_issue=None,  # type: Optional[str]
        cc_ss_start_month=None,  # type: Optional[str]
        cc_ss_start_year=None,  # type: Optional[str]
        cc_status=None,  # type: Optional[str]
        cc_status_description=None,  # type: Optional[str]
        cc_trans_id=None,  # type: Optional[str]
        cc_type=None,  # type: Optional[str]
        echeck_account_name=None,  # type: Optional[str]
        echeck_account_type=None,  # type: Optional[str]
        echeck_bank_name=None,  # type: Optional[str]
        echeck_routing_number=None,  # type: Optional[str]
        echeck_type=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
        last_trans_id=None,  # type: Optional[str]
        method=None,  # type: Optional[Union[str, serial.properties.Null]]
        parent_id=None,  # type: Optional[int]
        po_number=None,  # type: Optional[str]
        protection_eligibility=None,  # type: Optional[str]
        quote_payment_id=None,  # type: Optional[int]
        shipping_amount=None,  # type: Optional[numbers.Number]
        shipping_captured=None,  # type: Optional[numbers.Number]
        shipping_refunded=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[SalesOrderPaymentExtension]
    ):
        self.account_status = account_status
        self.additional_data = additional_data
        self.additional_information = additional_information
        self.address_status = address_status
        self.amount_authorized = amount_authorized
        self.amount_canceled = amount_canceled
        self.amount_ordered = amount_ordered
        self.amount_paid = amount_paid
        self.amount_refunded = amount_refunded
        self.anet_trans_method = anet_trans_method
        self.base_amount_authorized = base_amount_authorized
        self.base_amount_canceled = base_amount_canceled
        self.base_amount_ordered = base_amount_ordered
        self.base_amount_paid = base_amount_paid
        self.base_amount_paid_online = base_amount_paid_online
        self.base_amount_refunded = base_amount_refunded
        self.base_amount_refunded_online = base_amount_refunded_online
        self.base_shipping_amount = base_shipping_amount
        self.base_shipping_captured = base_shipping_captured
        self.base_shipping_refunded = base_shipping_refunded
        self.cc_approval = cc_approval
        self.cc_avs_status = cc_avs_status
        self.cc_cid_status = cc_cid_status
        self.cc_debug_request_body = cc_debug_request_body
        self.cc_debug_response_body = cc_debug_response_body
        self.cc_debug_response_serialized = cc_debug_response_serialized
        self.cc_exp_month = cc_exp_month
        self.cc_exp_year = cc_exp_year
        self.cc_last_4 = cc_last_4
        self.cc_number_enc = cc_number_enc
        self.cc_owner = cc_owner
        self.cc_secure_verify = cc_secure_verify
        self.cc_ss_issue = cc_ss_issue
        self.cc_ss_start_month = cc_ss_start_month
        self.cc_ss_start_year = cc_ss_start_year
        self.cc_status = cc_status
        self.cc_status_description = cc_status_description
        self.cc_trans_id = cc_trans_id
        self.cc_type = cc_type
        self.echeck_account_name = echeck_account_name
        self.echeck_account_type = echeck_account_type
        self.echeck_bank_name = echeck_bank_name
        self.echeck_routing_number = echeck_routing_number
        self.echeck_type = echeck_type
        self.entity_id = entity_id
        self.last_trans_id = last_trans_id
        self.method = method
        self.parent_id = parent_id
        self.po_number = po_number
        self.protection_eligibility = protection_eligibility
        self.quote_payment_id = quote_payment_id
        self.shipping_amount = shipping_amount
        self.shipping_captured = shipping_captured
        self.shipping_refunded = shipping_refunded
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesOrderPaymentExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-order-payment-extension-interface
    
    ExtensionInterface class for @see \Magento\Sales\Api\Data\OrderPaymentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        vault_payment_token=None,  # type: Optional[VaultPaymentToken]
    ):
        self.vault_payment_token = vault_payment_token
        super().__init__(_)


class VaultPaymentToken(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/vault-data-payment-token-interface
    
    Gateway vault payment token interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity_id=None,  # type: Optional[int]
        customer_id=None,  # type: Optional[int]
        public_hash=None,  # type: Optional[Union[str, serial.properties.Null]]
        payment_method_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        type_=None,  # type: Optional[Union[str, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        expires_at=None,  # type: Optional[str]
        gateway_token=None,  # type: Optional[Union[str, serial.properties.Null]]
        token_details=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_active=None,  # type: Optional[Union[bool, serial.properties.Null]]
        is_visible=None,  # type: Optional[Union[bool, serial.properties.Null]]
    ):
        self.entity_id = entity_id
        self.customer_id = customer_id
        self.public_hash = public_hash
        self.payment_method_code = payment_method_code
        self.type_ = type_
        self.created_at = created_at
        self.expires_at = expires_at
        self.gateway_token = gateway_token
        self.token_details = token_details
        self.is_active = is_active
        self.is_visible = is_visible
        super().__init__(_)


class SalesOrderStatusHistory(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-order-status-history-interface
    
    Order status history interface. An order is a document that a web store issues to a customer. Magento generates a 
    sales order that lists the product items, billing and shipping addresses, and shipping and payment methods. A 
    corresponding external document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
        entity_name=None,  # type: Optional[str]
        is_customer_notified=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_visible_on_front=None,  # type: Optional[Union[int, serial.properties.Null]]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        status=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.comment = comment
        self.created_at = created_at
        self.entity_id = entity_id
        self.entity_name = entity_name
        self.is_customer_notified = is_customer_notified
        self.is_visible_on_front = is_visible_on_front
        self.parent_id = parent_id
        self.status = status
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesOrderExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-extension-interface
    
    ExtensionInterface class for @see \Magento\Sales\Api\Data\OrderInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shipping_assignments=None,  # type: Optional[Sequence[SalesShippingAssignment]]
        applied_taxes=None,  # type: Optional[Sequence[TaxOrderDetailsApplied]]
        item_applied_taxes=None,  # type: Optional[Sequence[TaxOrderDetailsItem]]
        converting_from_quote=None,  # type: Optional[bool]
        company_order_attributes=None,  # type: Optional[CompanyOrder]
        base_customer_balance_amount=None,  # type: Optional[numbers.Number]
        customer_balance_amount=None,  # type: Optional[numbers.Number]
        base_customer_balance_invoiced=None,  # type: Optional[numbers.Number]
        customer_balance_invoiced=None,  # type: Optional[numbers.Number]
        base_customer_balance_refunded=None,  # type: Optional[numbers.Number]
        customer_balance_refunded=None,  # type: Optional[numbers.Number]
        base_customer_balance_total_refunded=None,  # type: Optional[numbers.Number]
        customer_balance_total_refunded=None,  # type: Optional[numbers.Number]
        gift_cards=None,  # type: Optional[Sequence[GiftCardAccount]]
        base_gift_cards_amount=None,  # type: Optional[numbers.Number]
        gift_cards_amount=None,  # type: Optional[numbers.Number]
        base_gift_cards_invoiced=None,  # type: Optional[numbers.Number]
        gift_cards_invoiced=None,  # type: Optional[numbers.Number]
        base_gift_cards_refunded=None,  # type: Optional[numbers.Number]
        gift_cards_refunded=None,  # type: Optional[numbers.Number]
        gift_message=None,  # type: Optional[GiftMessage]
        gw_id=None,  # type: Optional[str]
        gw_allow_gift_receipt=None,  # type: Optional[str]
        gw_add_card=None,  # type: Optional[str]
        gw_base_price=None,  # type: Optional[str]
        gw_price=None,  # type: Optional[str]
        gw_items_base_price=None,  # type: Optional[str]
        gw_items_price=None,  # type: Optional[str]
        gw_card_base_price=None,  # type: Optional[str]
        gw_card_price=None,  # type: Optional[str]
        gw_base_tax_amount=None,  # type: Optional[str]
        gw_tax_amount=None,  # type: Optional[str]
        gw_items_base_tax_amount=None,  # type: Optional[str]
        gw_items_tax_amount=None,  # type: Optional[str]
        gw_card_base_tax_amount=None,  # type: Optional[str]
        gw_card_tax_amount=None,  # type: Optional[str]
        gw_base_price_incl_tax=None,  # type: Optional[str]
        gw_price_incl_tax=None,  # type: Optional[str]
        gw_items_base_price_incl_tax=None,  # type: Optional[str]
        gw_items_price_incl_tax=None,  # type: Optional[str]
        gw_card_base_price_incl_tax=None,  # type: Optional[str]
        gw_card_price_incl_tax=None,  # type: Optional[str]
        gw_base_price_invoiced=None,  # type: Optional[str]
        gw_price_invoiced=None,  # type: Optional[str]
        gw_items_base_price_invoiced=None,  # type: Optional[str]
        gw_items_price_invoiced=None,  # type: Optional[str]
        gw_card_base_price_invoiced=None,  # type: Optional[str]
        gw_card_price_invoiced=None,  # type: Optional[str]
        gw_base_tax_amount_invoiced=None,  # type: Optional[str]
        gw_tax_amount_invoiced=None,  # type: Optional[str]
        gw_items_base_tax_invoiced=None,  # type: Optional[str]
        gw_items_tax_invoiced=None,  # type: Optional[str]
        gw_card_base_tax_invoiced=None,  # type: Optional[str]
        gw_card_tax_invoiced=None,  # type: Optional[str]
        gw_base_price_refunded=None,  # type: Optional[str]
        gw_price_refunded=None,  # type: Optional[str]
        gw_items_base_price_refunded=None,  # type: Optional[str]
        gw_items_price_refunded=None,  # type: Optional[str]
        gw_card_base_price_refunded=None,  # type: Optional[str]
        gw_card_price_refunded=None,  # type: Optional[str]
        gw_base_tax_amount_refunded=None,  # type: Optional[str]
        gw_tax_amount_refunded=None,  # type: Optional[str]
        gw_items_base_tax_refunded=None,  # type: Optional[str]
        gw_items_tax_refunded=None,  # type: Optional[str]
        gw_card_base_tax_refunded=None,  # type: Optional[str]
        gw_card_tax_refunded=None,  # type: Optional[str]
    ):
        self.shipping_assignments = shipping_assignments
        self.applied_taxes = applied_taxes
        self.item_applied_taxes = item_applied_taxes
        self.converting_from_quote = converting_from_quote
        self.company_order_attributes = company_order_attributes
        self.base_customer_balance_amount = base_customer_balance_amount
        self.customer_balance_amount = customer_balance_amount
        self.base_customer_balance_invoiced = base_customer_balance_invoiced
        self.customer_balance_invoiced = customer_balance_invoiced
        self.base_customer_balance_refunded = base_customer_balance_refunded
        self.customer_balance_refunded = customer_balance_refunded
        self.base_customer_balance_total_refunded = base_customer_balance_total_refunded
        self.customer_balance_total_refunded = customer_balance_total_refunded
        self.gift_cards = gift_cards
        self.base_gift_cards_amount = base_gift_cards_amount
        self.gift_cards_amount = gift_cards_amount
        self.base_gift_cards_invoiced = base_gift_cards_invoiced
        self.gift_cards_invoiced = gift_cards_invoiced
        self.base_gift_cards_refunded = base_gift_cards_refunded
        self.gift_cards_refunded = gift_cards_refunded
        self.gift_message = gift_message
        self.gw_id = gw_id
        self.gw_allow_gift_receipt = gw_allow_gift_receipt
        self.gw_add_card = gw_add_card
        self.gw_base_price = gw_base_price
        self.gw_price = gw_price
        self.gw_items_base_price = gw_items_base_price
        self.gw_items_price = gw_items_price
        self.gw_card_base_price = gw_card_base_price
        self.gw_card_price = gw_card_price
        self.gw_base_tax_amount = gw_base_tax_amount
        self.gw_tax_amount = gw_tax_amount
        self.gw_items_base_tax_amount = gw_items_base_tax_amount
        self.gw_items_tax_amount = gw_items_tax_amount
        self.gw_card_base_tax_amount = gw_card_base_tax_amount
        self.gw_card_tax_amount = gw_card_tax_amount
        self.gw_base_price_incl_tax = gw_base_price_incl_tax
        self.gw_price_incl_tax = gw_price_incl_tax
        self.gw_items_base_price_incl_tax = gw_items_base_price_incl_tax
        self.gw_items_price_incl_tax = gw_items_price_incl_tax
        self.gw_card_base_price_incl_tax = gw_card_base_price_incl_tax
        self.gw_card_price_incl_tax = gw_card_price_incl_tax
        self.gw_base_price_invoiced = gw_base_price_invoiced
        self.gw_price_invoiced = gw_price_invoiced
        self.gw_items_base_price_invoiced = gw_items_base_price_invoiced
        self.gw_items_price_invoiced = gw_items_price_invoiced
        self.gw_card_base_price_invoiced = gw_card_base_price_invoiced
        self.gw_card_price_invoiced = gw_card_price_invoiced
        self.gw_base_tax_amount_invoiced = gw_base_tax_amount_invoiced
        self.gw_tax_amount_invoiced = gw_tax_amount_invoiced
        self.gw_items_base_tax_invoiced = gw_items_base_tax_invoiced
        self.gw_items_tax_invoiced = gw_items_tax_invoiced
        self.gw_card_base_tax_invoiced = gw_card_base_tax_invoiced
        self.gw_card_tax_invoiced = gw_card_tax_invoiced
        self.gw_base_price_refunded = gw_base_price_refunded
        self.gw_price_refunded = gw_price_refunded
        self.gw_items_base_price_refunded = gw_items_base_price_refunded
        self.gw_items_price_refunded = gw_items_price_refunded
        self.gw_card_base_price_refunded = gw_card_base_price_refunded
        self.gw_card_price_refunded = gw_card_price_refunded
        self.gw_base_tax_amount_refunded = gw_base_tax_amount_refunded
        self.gw_tax_amount_refunded = gw_tax_amount_refunded
        self.gw_items_base_tax_refunded = gw_items_base_tax_refunded
        self.gw_items_tax_refunded = gw_items_tax_refunded
        self.gw_card_base_tax_refunded = gw_card_base_tax_refunded
        self.gw_card_tax_refunded = gw_card_tax_refunded
        super().__init__(_)


class SalesShippingAssignment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipping-assignment-interface
    
    Interface ShippingAssignmentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shipping=None,  # type: Optional[Union[SalesShipping, serial.properties.Null]]
        items=None,  # type: Optional[Union[Sequence[SalesOrderItem], serial.properties.Null]]
        stock_id=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.shipping = shipping
        self.items = items
        self.stock_id = stock_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesShipping(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipping-interface
    
    Interface ShippingInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[SalesOrderAddress]
        method=None,  # type: Optional[str]
        total=None,  # type: Optional[SalesTotal]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.address = address
        self.method = method
        self.total = total
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesTotal(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-total-interface
    
    Interface TotalInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        base_shipping_amount=None,  # type: Optional[numbers.Number]
        base_shipping_canceled=None,  # type: Optional[numbers.Number]
        base_shipping_discount_amount=None,  # type: Optional[numbers.Number]
        base_shipping_discount_tax_compensation_amnt=None,  # type: Optional[numbers.Number]
        base_shipping_incl_tax=None,  # type: Optional[numbers.Number]
        base_shipping_invoiced=None,  # type: Optional[numbers.Number]
        base_shipping_refunded=None,  # type: Optional[numbers.Number]
        base_shipping_tax_amount=None,  # type: Optional[numbers.Number]
        base_shipping_tax_refunded=None,  # type: Optional[numbers.Number]
        shipping_amount=None,  # type: Optional[numbers.Number]
        shipping_canceled=None,  # type: Optional[numbers.Number]
        shipping_discount_amount=None,  # type: Optional[numbers.Number]
        shipping_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        shipping_incl_tax=None,  # type: Optional[numbers.Number]
        shipping_invoiced=None,  # type: Optional[numbers.Number]
        shipping_refunded=None,  # type: Optional[numbers.Number]
        shipping_tax_amount=None,  # type: Optional[numbers.Number]
        shipping_tax_refunded=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.base_shipping_amount = base_shipping_amount
        self.base_shipping_canceled = base_shipping_canceled
        self.base_shipping_discount_amount = base_shipping_discount_amount
        self.base_shipping_discount_tax_compensation_amnt = base_shipping_discount_tax_compensation_amnt
        self.base_shipping_incl_tax = base_shipping_incl_tax
        self.base_shipping_invoiced = base_shipping_invoiced
        self.base_shipping_refunded = base_shipping_refunded
        self.base_shipping_tax_amount = base_shipping_tax_amount
        self.base_shipping_tax_refunded = base_shipping_tax_refunded
        self.shipping_amount = shipping_amount
        self.shipping_canceled = shipping_canceled
        self.shipping_discount_amount = shipping_discount_amount
        self.shipping_discount_tax_compensation_amount = shipping_discount_tax_compensation_amount
        self.shipping_incl_tax = shipping_incl_tax
        self.shipping_invoiced = shipping_invoiced
        self.shipping_refunded = shipping_refunded
        self.shipping_tax_amount = shipping_tax_amount
        self.shipping_tax_refunded = shipping_tax_refunded
        self.extension_attributes = extension_attributes
        super().__init__(_)


class TaxOrderDetailsApplied(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/tax-data-order-tax-details-applied-tax-interface
    
    Interface OrderTaxDetailsAppliedTaxInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        code=None,  # type: Optional[str]
        title=None,  # type: Optional[str]
        percent=None,  # type: Optional[numbers.Number]
        amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[TaxOrderDetailsAppliedExtension]
    ):
        self.code = code
        self.title = title
        self.percent = percent
        self.amount = amount
        self.base_amount = base_amount
        self.extension_attributes = extension_attributes
        super().__init__(_)


class TaxOrderDetailsAppliedExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/tax-data-order-tax-details-applied-tax-extension-interface
    
    ExtensionInterface class for @see \Magento\Tax\Api\Data\OrderTaxDetailsAppliedTaxInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rates=None,  # type: Optional[Sequence[TaxAppliedRate]]
    ):
        self.rates = rates
        super().__init__(_)


class TaxAppliedRate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-applied-tax-rate-interface
    
    Applied tax rate interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        code=None,  # type: Optional[str]
        title=None,  # type: Optional[str]
        percent=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.code = code
        self.title = title
        self.percent = percent
        self.extension_attributes = extension_attributes
        super().__init__(_)


class TaxOrderDetailsItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/tax-data-order-tax-details-item-interface
    
    Interface OrderTaxDetailsItemInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        type_=None,  # type: Optional[str]
        item_id=None,  # type: Optional[int]
        associated_item_id=None,  # type: Optional[int]
        applied_taxes=None,  # type: Optional[Sequence[TaxOrderDetailsApplied]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.type_ = type_
        self.item_id = item_id
        self.associated_item_id = associated_item_id
        self.applied_taxes = applied_taxes
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CompanyOrder(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-company-order-interface
    
    Order company extension attributes interface. Adds new company attributes to orders.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        order_id=None,  # type: Optional[int]
        company_id=None,  # type: Optional[int]
        company_name=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.order_id = order_id
        self.company_id = company_id
        self.company_name = company_name
        self.extension_attributes = extension_attributes
        super().__init__(_)


class GiftCardAccount(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/gift-card-account-data-gift-card-interface
    
    Gift Card data
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.id_ = id_
        self.code = code
        self.amount = amount
        self.base_amount = base_amount
        super().__init__(_)


class PostCMSPage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1cmsPage/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        page=None,  # type: Optional[Union[CMSPage, serial.properties.Null]]
    ):
        self.page = page
        super().__init__(_)


class CMSPage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/cms-data-page-interface
    
    CMS page interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        identifier=None,  # type: Optional[Union[str, serial.properties.Null]]
        title=None,  # type: Optional[str]
        page_layout=None,  # type: Optional[str]
        meta_title=None,  # type: Optional[str]
        meta_keywords=None,  # type: Optional[str]
        meta_description=None,  # type: Optional[str]
        content_heading=None,  # type: Optional[str]
        content=None,  # type: Optional[str]
        creation_time=None,  # type: Optional[str]
        update_time=None,  # type: Optional[str]
        sort_order=None,  # type: Optional[str]
        layout_update_xml=None,  # type: Optional[str]
        custom_theme=None,  # type: Optional[str]
        custom_root_template=None,  # type: Optional[str]
        custom_layout_update_xml=None,  # type: Optional[str]
        custom_theme_from=None,  # type: Optional[str]
        custom_theme_to=None,  # type: Optional[str]
        active=None,  # type: Optional[bool]
    ):
        self.id_ = id_
        self.identifier = identifier
        self.title = title
        self.page_layout = page_layout
        self.meta_title = meta_title
        self.meta_keywords = meta_keywords
        self.meta_description = meta_description
        self.content_heading = content_heading
        self.content = content
        self.creation_time = creation_time
        self.update_time = update_time
        self.sort_order = sort_order
        self.layout_update_xml = layout_update_xml
        self.custom_theme = custom_theme
        self.custom_root_template = custom_root_template
        self.custom_layout_update_xml = custom_layout_update_xml
        self.custom_theme_from = custom_theme_from
        self.custom_theme_to = custom_theme_to
        self.active = active
        super().__init__(_)


class PostOrders(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1orders~1/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesOrder, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class PostCoupons(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1coupons/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        coupon=None,  # type: Optional[Union[SalesRuleCoupon, serial.properties.Null]]
    ):
        self.coupon = coupon
        super().__init__(_)


class SalesRuleCoupon(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-coupon-interface
    
    Interface CouponInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        coupon_id=None,  # type: Optional[int]
        rule_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        code=None,  # type: Optional[str]
        usage_limit=None,  # type: Optional[int]
        usage_per_customer=None,  # type: Optional[int]
        times_used=None,  # type: Optional[Union[int, serial.properties.Null]]
        expiration_date=None,  # type: Optional[str]
        is_primary=None,  # type: Optional[Union[bool, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        type_=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.coupon_id = coupon_id
        self.rule_id = rule_id
        self.code = code
        self.usage_limit = usage_limit
        self.usage_per_customer = usage_per_customer
        self.times_used = times_used
        self.expiration_date = expiration_date
        self.is_primary = is_primary
        self.created_at = created_at
        self.type_ = type_
        self.extension_attributes = extension_attributes
        super().__init__(_)


class RMASearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-rma-search-result-interface
    
    Interface RmaSearchResultInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[RMA], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class RMA(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-rma-interface
    
    Interface RmaInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        increment_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        order_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        order_increment_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        customer_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        date_requested=None,  # type: Optional[Union[str, serial.properties.Null]]
        customer_custom_email=None,  # type: Optional[Union[str, serial.properties.Null]]
        items=None,  # type: Optional[Union[Sequence[RMAItem], serial.properties.Null]]
        status=None,  # type: Optional[Union[str, serial.properties.Null]]
        comments=None,  # type: Optional[Union[Sequence[RMAComment], serial.properties.Null]]
        tracks=None,  # type: Optional[Union[Sequence[RMATrack], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.increment_id = increment_id
        self.entity_id = entity_id
        self.order_id = order_id
        self.order_increment_id = order_increment_id
        self.store_id = store_id
        self.customer_id = customer_id
        self.date_requested = date_requested
        self.customer_custom_email = customer_custom_email
        self.items = items
        self.status = status
        self.comments = comments
        self.tracks = tracks
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class RMAItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-item-interface
    
    Interface CategoryInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        rma_entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        order_item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty_requested=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty_authorized=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty_approved=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty_returned=None,  # type: Optional[Union[int, serial.properties.Null]]
        reason=None,  # type: Optional[Union[str, serial.properties.Null]]
        condition=None,  # type: Optional[Union[str, serial.properties.Null]]
        resolution=None,  # type: Optional[Union[str, serial.properties.Null]]
        status=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.entity_id = entity_id
        self.rma_entity_id = rma_entity_id
        self.order_item_id = order_item_id
        self.qty_requested = qty_requested
        self.qty_authorized = qty_authorized
        self.qty_approved = qty_approved
        self.qty_returned = qty_returned
        self.reason = reason
        self.condition = condition
        self.resolution = resolution
        self.status = status
        self.extension_attributes = extension_attributes
        super().__init__(_)


class RMAComment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-comment-interface
    
    Interface CommentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        rma_entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        created_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        customer_notified=None,  # type: Optional[Union[bool, serial.properties.Null]]
        visible_on_front=None,  # type: Optional[Union[bool, serial.properties.Null]]
        status=None,  # type: Optional[Union[str, serial.properties.Null]]
        admin=None,  # type: Optional[Union[bool, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.comment = comment
        self.rma_entity_id = rma_entity_id
        self.created_at = created_at
        self.entity_id = entity_id
        self.customer_notified = customer_notified
        self.visible_on_front = visible_on_front
        self.status = status
        self.admin = admin
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class RMATrack(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-track-interface
    
    Interface TrackInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        rma_entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        track_number=None,  # type: Optional[Union[str, serial.properties.Null]]
        carrier_title=None,  # type: Optional[Union[str, serial.properties.Null]]
        carrier_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.entity_id = entity_id
        self.rma_entity_id = rma_entity_id
        self.track_number = track_number
        self.carrier_title = carrier_title
        self.carrier_code = carrier_code
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostReturns(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1returns/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rma_data_object=None,  # type: Optional[Union[RMA, serial.properties.Null]]
    ):
        self.rma_data_object = rma_data_object
        super().__init__(_)


class PostCMSBlock(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1cmsBlock/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        block=None,  # type: Optional[Union[CMSBlock, serial.properties.Null]]
    ):
        self.block = block
        super().__init__(_)


class CMSBlock(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/cms-data-block-interface
    
    CMS block interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        identifier=None,  # type: Optional[Union[str, serial.properties.Null]]
        title=None,  # type: Optional[str]
        content=None,  # type: Optional[str]
        creation_time=None,  # type: Optional[str]
        update_time=None,  # type: Optional[str]
        active=None,  # type: Optional[bool]
    ):
        self.id_ = id_
        self.identifier = identifier
        self.title = title
        self.content = content
        self.creation_time = creation_time
        self.update_time = update_time
        self.active = active
        super().__init__(_)


class ProductSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-search-results-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[Product], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class Product(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        name=None,  # type: Optional[str]
        attribute_set_id=None,  # type: Optional[int]
        price=None,  # type: Optional[numbers.Number]
        status=None,  # type: Optional[int]
        visibility=None,  # type: Optional[int]
        type_id=None,  # type: Optional[str]
        created_at=None,  # type: Optional[str]
        updated_at=None,  # type: Optional[str]
        weight=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[ProductExtension]
        product_links=None,  # type: Optional[Sequence[ProductLink]]
        options=None,  # type: Optional[Sequence[ProductCustomOption]]
        media_gallery_entries=None,  # type: Optional[Sequence[ProductAttributeMediaGalleryEntry]]
        tier_prices=None,  # type: Optional[Sequence[ProductTierPrice]]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.id_ = id_
        self.sku = sku
        self.name = name
        self.attribute_set_id = attribute_set_id
        self.price = price
        self.status = status
        self.visibility = visibility
        self.type_id = type_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.weight = weight
        self.extension_attributes = extension_attributes
        self.product_links = product_links
        self.options = options
        self.media_gallery_entries = media_gallery_entries
        self.tier_prices = tier_prices
        self.custom_attributes = custom_attributes
        super().__init__(_)


class ProductExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\ProductInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        website_ids=None,  # type: Optional[Sequence[int]]
        category_links=None,  # type: Optional[Sequence[CategoryLink]]
        stock_item=None,  # type: Optional[InventoryStockItem]
        bundle_product_options=None,  # type: Optional[Sequence[BundleOptionInterface]]
        configurable_product_options=None,  # type: Optional[Sequence[ConfigurableProductOption]]
        configurable_product_links=None,  # type: Optional[Sequence[int]]
        downloadable_product_links=None,  # type: Optional[Sequence[DownloadableLink]]
        downloadable_product_samples=None,  # type: Optional[Sequence[DownloadableSample]]
        giftcard_amounts=None,  # type: Optional[Sequence[GiftCardGiftcardAmount]]
    ):
        self.website_ids = website_ids
        self.category_links = category_links
        self.stock_item = stock_item
        self.bundle_product_options = bundle_product_options
        self.configurable_product_options = configurable_product_options
        self.configurable_product_links = configurable_product_links
        self.downloadable_product_links = downloadable_product_links
        self.downloadable_product_samples = downloadable_product_samples
        self.giftcard_amounts = giftcard_amounts
        super().__init__(_)


class CategoryLink(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-category-link-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        position=None,  # type: Optional[int]
        category_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.position = position
        self.category_id = category_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class InventoryStockItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-inventory-data-stock-item-interface
    
    Interface StockItem
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        item_id=None,  # type: Optional[int]
        product_id=None,  # type: Optional[int]
        stock_id=None,  # type: Optional[int]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        is_in_stock=None,  # type: Optional[Union[bool, serial.properties.Null]]
        is_qty_decimal=None,  # type: Optional[Union[bool, serial.properties.Null]]
        show_default_notification_message=None,  # type: Optional[Union[bool, serial.properties.Null]]
        use_config_min_qty=None,  # type: Optional[Union[bool, serial.properties.Null]]
        min_qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        use_config_min_sale_qty=None,  # type: Optional[Union[int, serial.properties.Null]]
        min_sale_qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        use_config_max_sale_qty=None,  # type: Optional[Union[bool, serial.properties.Null]]
        max_sale_qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        use_config_backorders=None,  # type: Optional[Union[bool, serial.properties.Null]]
        backorders=None,  # type: Optional[Union[int, serial.properties.Null]]
        use_config_notify_stock_qty=None,  # type: Optional[Union[bool, serial.properties.Null]]
        notify_stock_qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        use_config_qty_increments=None,  # type: Optional[Union[bool, serial.properties.Null]]
        qty_increments=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        use_config_enable_qty_inc=None,  # type: Optional[Union[bool, serial.properties.Null]]
        enable_qty_increments=None,  # type: Optional[Union[bool, serial.properties.Null]]
        use_config_manage_stock=None,  # type: Optional[Union[bool, serial.properties.Null]]
        manage_stock=None,  # type: Optional[Union[bool, serial.properties.Null]]
        low_stock_date=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_decimal_divided=None,  # type: Optional[Union[bool, serial.properties.Null]]
        stock_status_changed_auto=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.item_id = item_id
        self.product_id = product_id
        self.stock_id = stock_id
        self.qty = qty
        self.is_in_stock = is_in_stock
        self.is_qty_decimal = is_qty_decimal
        self.show_default_notification_message = show_default_notification_message
        self.use_config_min_qty = use_config_min_qty
        self.min_qty = min_qty
        self.use_config_min_sale_qty = use_config_min_sale_qty
        self.min_sale_qty = min_sale_qty
        self.use_config_max_sale_qty = use_config_max_sale_qty
        self.max_sale_qty = max_sale_qty
        self.use_config_backorders = use_config_backorders
        self.backorders = backorders
        self.use_config_notify_stock_qty = use_config_notify_stock_qty
        self.notify_stock_qty = notify_stock_qty
        self.use_config_qty_increments = use_config_qty_increments
        self.qty_increments = qty_increments
        self.use_config_enable_qty_inc = use_config_enable_qty_inc
        self.enable_qty_increments = enable_qty_increments
        self.use_config_manage_stock = use_config_manage_stock
        self.manage_stock = manage_stock
        self.low_stock_date = low_stock_date
        self.is_decimal_divided = is_decimal_divided
        self.stock_status_changed_auto = stock_status_changed_auto
        self.extension_attributes = extension_attributes
        super().__init__(_)


class BundleOptionInterface(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-option-interface
    
    Interface OptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option_id=None,  # type: Optional[int]
        title=None,  # type: Optional[str]
        required=None,  # type: Optional[bool]
        type_=None,  # type: Optional[str]
        position=None,  # type: Optional[int]
        sku=None,  # type: Optional[str]
        product_links=None,  # type: Optional[Sequence[BundleLink]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.option_id = option_id
        self.title = title
        self.required = required
        self.type_ = type_
        self.position = position
        self.sku = sku
        self.product_links = product_links
        self.extension_attributes = extension_attributes
        super().__init__(_)


class BundleLink(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-link-interface
    
    Interface LinkInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[str]
        sku=None,  # type: Optional[str]
        option_id=None,  # type: Optional[int]
        qty=None,  # type: Optional[numbers.Number]
        position=None,  # type: Optional[int]
        is_default=None,  # type: Optional[Union[bool, serial.properties.Null]]
        price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        price_type=None,  # type: Optional[Union[int, serial.properties.Null]]
        can_change_quantity=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.sku = sku
        self.option_id = option_id
        self.qty = qty
        self.position = position
        self.is_default = is_default
        self.price = price
        self.price_type = price_type
        self.can_change_quantity = can_change_quantity
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ConfigurableProductOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/configurable-product-data-option-interface
    
    Interface OptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        attribute_id=None,  # type: Optional[str]
        label=None,  # type: Optional[str]
        position=None,  # type: Optional[int]
        is_use_default=None,  # type: Optional[bool]
        values=None,  # type: Optional[Sequence[ConfigurableProductOptionValue]]
        extension_attributes=None,  # type: Optional[dict]
        product_id=None,  # type: Optional[int]
    ):
        self.id_ = id_
        self.attribute_id = attribute_id
        self.label = label
        self.position = position
        self.is_use_default = is_use_default
        self.values = values
        self.extension_attributes = extension_attributes
        self.product_id = product_id
        super().__init__(_)


class ConfigurableProductOptionValue(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/configurable-product-data-option-value-interface
    
    Interface OptionValueInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        value_index=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.value_index = value_index
        self.extension_attributes = extension_attributes
        super().__init__(_)


class DownloadableLink(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/downloadable-data-link-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        title=None,  # type: Optional[str]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_shareable=None,  # type: Optional[Union[int, serial.properties.Null]]
        price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        number_of_downloads=None,  # type: Optional[int]
        link_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        link_file=None,  # type: Optional[str]
        link_file_content=None,  # type: Optional[DownloadableFile]
        link_url=None,  # type: Optional[str]
        sample_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        sample_file=None,  # type: Optional[str]
        sample_file_content=None,  # type: Optional[DownloadableFile]
        sample_url=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.title = title
        self.sort_order = sort_order
        self.is_shareable = is_shareable
        self.price = price
        self.number_of_downloads = number_of_downloads
        self.link_type = link_type
        self.link_file = link_file
        self.link_file_content = link_file_content
        self.link_url = link_url
        self.sample_type = sample_type
        self.sample_file = sample_file
        self.sample_file_content = sample_file_content
        self.sample_url = sample_url
        self.extension_attributes = extension_attributes
        super().__init__(_)


class DownloadableFile(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/downloadable-data-file-content-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        file_data=None,  # type: Optional[Union[str, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.file_data = file_data
        self.name = name
        self.extension_attributes = extension_attributes
        super().__init__(_)


class DownloadableSample(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/downloadable-data-sample-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        title=None,  # type: Optional[Union[str, serial.properties.Null]]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
        sample_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        sample_file=None,  # type: Optional[str]
        sample_file_content=None,  # type: Optional[DownloadableFile]
        sample_url=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.title = title
        self.sort_order = sort_order
        self.sample_type = sample_type
        self.sample_file = sample_file
        self.sample_file_content = sample_file_content
        self.sample_url = sample_url
        self.extension_attributes = extension_attributes
        super().__init__(_)


class GiftCardGiftcardAmount(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/gift-card-data-giftcard-amount-interface
    
    Interface GiftcardAmountInterface: this interface is used to serialize and deserialize EAV attribute 
    giftcard_amounts
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        website_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        website_value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.attribute_id = attribute_id
        self.website_id = website_id
        self.value = value
        self.website_value = website_value
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductLink(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-link-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        link_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        linked_product_sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        linked_product_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        position=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[ProductLinkExtension]
    ):
        self.sku = sku
        self.link_type = link_type
        self.linked_product_sku = linked_product_sku
        self.linked_product_type = linked_product_type
        self.position = position
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductLinkExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-link-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\ProductLinkInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        qty=None,  # type: Optional[numbers.Number]
    ):
        self.qty = qty
        super().__init__(_)


class ProductCustomOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-custom-option-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product_sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        option_id=None,  # type: Optional[int]
        title=None,  # type: Optional[Union[str, serial.properties.Null]]
        type_=None,  # type: Optional[Union[str, serial.properties.Null]]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_require=None,  # type: Optional[Union[bool, serial.properties.Null]]
        price=None,  # type: Optional[numbers.Number]
        price_type=None,  # type: Optional[str]
        sku=None,  # type: Optional[str]
        file_extension=None,  # type: Optional[str]
        max_characters=None,  # type: Optional[int]
        image_size_x=None,  # type: Optional[int]
        image_size_y=None,  # type: Optional[int]
        values=None,  # type: Optional[Sequence[ProductCustomOptionValuesInterface]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.product_sku = product_sku
        self.option_id = option_id
        self.title = title
        self.type_ = type_
        self.sort_order = sort_order
        self.is_require = is_require
        self.price = price
        self.price_type = price_type
        self.sku = sku
        self.file_extension = file_extension
        self.max_characters = max_characters
        self.image_size_x = image_size_x
        self.image_size_y = image_size_y
        self.values = values
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductCustomOptionValuesInterface(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-custom-option-values-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        title=None,  # type: Optional[Union[str, serial.properties.Null]]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
        price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        price_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        sku=None,  # type: Optional[str]
        option_type_id=None,  # type: Optional[int]
    ):
        self.title = title
        self.sort_order = sort_order
        self.price = price
        self.price_type = price_type
        self.sku = sku
        self.option_type_id = option_type_id
        super().__init__(_)


class ProductAttributeMediaGalleryEntry(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-attribute-media-gallery-entry-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        media_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        position=None,  # type: Optional[Union[int, serial.properties.Null]]
        disabled=None,  # type: Optional[Union[bool, serial.properties.Null]]
        types=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        file=None,  # type: Optional[str]
        content=None,  # type: Optional[Image]
        extension_attributes=None,  # type: Optional[ProductAttributeMediaGalleryEntryExtension]
    ):
        self.id_ = id_
        self.media_type = media_type
        self.label = label
        self.position = position
        self.disabled = disabled
        self.types = types
        self.file = file
        self.content = content
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductAttributeMediaGalleryEntryExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-attribute-media-gallery-entry-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        video_content=None,  # type: Optional[Video]
    ):
        self.video_content = video_content
        super().__init__(_)


class Video(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/framework-data-video-content-interface
    
    Video Content data interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        media_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        video_provider=None,  # type: Optional[Union[str, serial.properties.Null]]
        video_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        video_title=None,  # type: Optional[Union[str, serial.properties.Null]]
        video_description=None,  # type: Optional[Union[str, serial.properties.Null]]
        video_metadata=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.media_type = media_type
        self.video_provider = video_provider
        self.video_url = video_url
        self.video_title = video_title
        self.video_description = video_description
        self.video_metadata = video_metadata
        super().__init__(_)


class ProductTierPrice(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-tier-price-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer_group_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[ProductTierPriceExtension]
    ):
        self.customer_group_id = customer_group_id
        self.qty = qty
        self.value = value
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductTierPriceExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-tier-price-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\ProductTierPriceInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        percentage_value=None,  # type: Optional[numbers.Number]
        website_id=None,  # type: Optional[int]
    ):
        self.percentage_value = percentage_value
        self.website_id = website_id
        super().__init__(_)


class PostProducts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1products/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product=None,  # type: Optional[Union[Product, serial.properties.Null]]
        save_options=None,  # type: Optional[bool]
    ):
        self.product = product
        self.save_options = save_options
        super().__init__(_)


class SalesInvoiceSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-invoice-search-result-interface
    
    Invoice search result interface. An invoice is a record of the receipt of payment for an order.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesInvoice], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class SalesInvoice(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-invoice-interface
    
    Invoice interface. An invoice is a record of the receipt of payment for an order.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        base_currency_code=None,  # type: Optional[str]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        base_grand_total=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        base_shipping_amount=None,  # type: Optional[numbers.Number]
        base_shipping_discount_tax_compensation_amnt=None,  # type: Optional[numbers.Number]
        base_shipping_incl_tax=None,  # type: Optional[numbers.Number]
        base_shipping_tax_amount=None,  # type: Optional[numbers.Number]
        base_subtotal=None,  # type: Optional[numbers.Number]
        base_subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        base_total_refunded=None,  # type: Optional[numbers.Number]
        base_to_global_rate=None,  # type: Optional[numbers.Number]
        base_to_order_rate=None,  # type: Optional[numbers.Number]
        billing_address_id=None,  # type: Optional[int]
        can_void_flag=None,  # type: Optional[int]
        created_at=None,  # type: Optional[str]
        discount_amount=None,  # type: Optional[numbers.Number]
        discount_description=None,  # type: Optional[str]
        email_sent=None,  # type: Optional[int]
        entity_id=None,  # type: Optional[int]
        global_currency_code=None,  # type: Optional[str]
        grand_total=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        increment_id=None,  # type: Optional[str]
        is_used_for_refund=None,  # type: Optional[int]
        order_currency_code=None,  # type: Optional[str]
        order_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        shipping_address_id=None,  # type: Optional[int]
        shipping_amount=None,  # type: Optional[numbers.Number]
        shipping_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        shipping_incl_tax=None,  # type: Optional[numbers.Number]
        shipping_tax_amount=None,  # type: Optional[numbers.Number]
        state=None,  # type: Optional[int]
        store_currency_code=None,  # type: Optional[str]
        store_id=None,  # type: Optional[int]
        store_to_base_rate=None,  # type: Optional[numbers.Number]
        store_to_order_rate=None,  # type: Optional[numbers.Number]
        subtotal=None,  # type: Optional[numbers.Number]
        subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        tax_amount=None,  # type: Optional[numbers.Number]
        total_qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        transaction_id=None,  # type: Optional[str]
        updated_at=None,  # type: Optional[str]
        items=None,  # type: Optional[Union[Sequence[SalesInvoiceItem], serial.properties.Null]]
        comments=None,  # type: Optional[Sequence[SalesInvoiceComment]]
        extension_attributes=None,  # type: Optional[SalesInvoiceExtension]
    ):
        self.base_currency_code = base_currency_code
        self.base_discount_amount = base_discount_amount
        self.base_grand_total = base_grand_total
        self.base_discount_tax_compensation_amount = base_discount_tax_compensation_amount
        self.base_shipping_amount = base_shipping_amount
        self.base_shipping_discount_tax_compensation_amnt = base_shipping_discount_tax_compensation_amnt
        self.base_shipping_incl_tax = base_shipping_incl_tax
        self.base_shipping_tax_amount = base_shipping_tax_amount
        self.base_subtotal = base_subtotal
        self.base_subtotal_incl_tax = base_subtotal_incl_tax
        self.base_tax_amount = base_tax_amount
        self.base_total_refunded = base_total_refunded
        self.base_to_global_rate = base_to_global_rate
        self.base_to_order_rate = base_to_order_rate
        self.billing_address_id = billing_address_id
        self.can_void_flag = can_void_flag
        self.created_at = created_at
        self.discount_amount = discount_amount
        self.discount_description = discount_description
        self.email_sent = email_sent
        self.entity_id = entity_id
        self.global_currency_code = global_currency_code
        self.grand_total = grand_total
        self.discount_tax_compensation_amount = discount_tax_compensation_amount
        self.increment_id = increment_id
        self.is_used_for_refund = is_used_for_refund
        self.order_currency_code = order_currency_code
        self.order_id = order_id
        self.shipping_address_id = shipping_address_id
        self.shipping_amount = shipping_amount
        self.shipping_discount_tax_compensation_amount = shipping_discount_tax_compensation_amount
        self.shipping_incl_tax = shipping_incl_tax
        self.shipping_tax_amount = shipping_tax_amount
        self.state = state
        self.store_currency_code = store_currency_code
        self.store_id = store_id
        self.store_to_base_rate = store_to_base_rate
        self.store_to_order_rate = store_to_order_rate
        self.subtotal = subtotal
        self.subtotal_incl_tax = subtotal_incl_tax
        self.tax_amount = tax_amount
        self.total_qty = total_qty
        self.transaction_id = transaction_id
        self.updated_at = updated_at
        self.items = items
        self.comments = comments
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesInvoiceItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-invoice-item-interface
    
    Invoice item interface. An invoice is a record of the receipt of payment for an order. An invoice item is a 
    purchased item in an invoice.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        additional_data=None,  # type: Optional[str]
        base_cost=None,  # type: Optional[numbers.Number]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        base_price=None,  # type: Optional[numbers.Number]
        base_price_incl_tax=None,  # type: Optional[numbers.Number]
        base_row_total=None,  # type: Optional[numbers.Number]
        base_row_total_incl_tax=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        description=None,  # type: Optional[str]
        discount_amount=None,  # type: Optional[numbers.Number]
        entity_id=None,  # type: Optional[int]
        discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        name=None,  # type: Optional[str]
        parent_id=None,  # type: Optional[int]
        price=None,  # type: Optional[numbers.Number]
        price_incl_tax=None,  # type: Optional[numbers.Number]
        product_id=None,  # type: Optional[int]
        row_total=None,  # type: Optional[numbers.Number]
        row_total_incl_tax=None,  # type: Optional[numbers.Number]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        tax_amount=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[dict]
        order_item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.additional_data = additional_data
        self.base_cost = base_cost
        self.base_discount_amount = base_discount_amount
        self.base_discount_tax_compensation_amount = base_discount_tax_compensation_amount
        self.base_price = base_price
        self.base_price_incl_tax = base_price_incl_tax
        self.base_row_total = base_row_total
        self.base_row_total_incl_tax = base_row_total_incl_tax
        self.base_tax_amount = base_tax_amount
        self.description = description
        self.discount_amount = discount_amount
        self.entity_id = entity_id
        self.discount_tax_compensation_amount = discount_tax_compensation_amount
        self.name = name
        self.parent_id = parent_id
        self.price = price
        self.price_incl_tax = price_incl_tax
        self.product_id = product_id
        self.row_total = row_total
        self.row_total_incl_tax = row_total_incl_tax
        self.sku = sku
        self.tax_amount = tax_amount
        self.extension_attributes = extension_attributes
        self.order_item_id = order_item_id
        self.qty = qty
        super().__init__(_)


class SalesInvoiceComment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-invoice-comment-interface
    
    Invoice comment interface. An invoice is a record of the receipt of payment for an order. An invoice can include 
    comments that detail the invoice history.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        is_customer_notified=None,  # type: Optional[Union[int, serial.properties.Null]]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_visible_on_front=None,  # type: Optional[Union[int, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
    ):
        self.is_customer_notified = is_customer_notified
        self.parent_id = parent_id
        self.extension_attributes = extension_attributes
        self.comment = comment
        self.is_visible_on_front = is_visible_on_front
        self.created_at = created_at
        self.entity_id = entity_id
        super().__init__(_)


class SalesInvoiceExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-invoice-extension-interface
    
    ExtensionInterface class for @see \Magento\Sales\Api\Data\InvoiceInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        base_customer_balance_amount=None,  # type: Optional[numbers.Number]
        customer_balance_amount=None,  # type: Optional[numbers.Number]
        base_gift_cards_amount=None,  # type: Optional[numbers.Number]
        gift_cards_amount=None,  # type: Optional[numbers.Number]
        gw_base_price=None,  # type: Optional[str]
        gw_price=None,  # type: Optional[str]
        gw_items_base_price=None,  # type: Optional[str]
        gw_items_price=None,  # type: Optional[str]
        gw_card_base_price=None,  # type: Optional[str]
        gw_card_price=None,  # type: Optional[str]
        gw_base_tax_amount=None,  # type: Optional[str]
        gw_tax_amount=None,  # type: Optional[str]
        gw_items_base_tax_amount=None,  # type: Optional[str]
        gw_items_tax_amount=None,  # type: Optional[str]
        gw_card_base_tax_amount=None,  # type: Optional[str]
        gw_card_tax_amount=None,  # type: Optional[str]
    ):
        self.base_customer_balance_amount = base_customer_balance_amount
        self.customer_balance_amount = customer_balance_amount
        self.base_gift_cards_amount = base_gift_cards_amount
        self.gift_cards_amount = gift_cards_amount
        self.gw_base_price = gw_base_price
        self.gw_price = gw_price
        self.gw_items_base_price = gw_items_base_price
        self.gw_items_price = gw_items_price
        self.gw_card_base_price = gw_card_base_price
        self.gw_card_price = gw_card_price
        self.gw_base_tax_amount = gw_base_tax_amount
        self.gw_tax_amount = gw_tax_amount
        self.gw_items_base_tax_amount = gw_items_base_tax_amount
        self.gw_items_tax_amount = gw_items_tax_amount
        self.gw_card_base_tax_amount = gw_card_base_tax_amount
        self.gw_card_tax_amount = gw_card_tax_amount
        super().__init__(_)


class PutTaxRates(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRates/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        tax_rate=None,  # type: Optional[Union[TaxRate, serial.properties.Null]]
    ):
        self.tax_rate = tax_rate
        super().__init__(_)


class TaxRate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-rate-interface
    
    Tax rate interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        tax_country_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        tax_region_id=None,  # type: Optional[int]
        region_name=None,  # type: Optional[str]
        tax_postcode=None,  # type: Optional[str]
        zip_is_range=None,  # type: Optional[int]
        zip_from=None,  # type: Optional[int]
        zip_to=None,  # type: Optional[int]
        rate=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        titles=None,  # type: Optional[Sequence[TaxRateTitle]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.tax_country_id = tax_country_id
        self.tax_region_id = tax_region_id
        self.region_name = region_name
        self.tax_postcode = tax_postcode
        self.zip_is_range = zip_is_range
        self.zip_from = zip_from
        self.zip_to = zip_to
        self.rate = rate
        self.code = code
        self.titles = titles
        self.extension_attributes = extension_attributes
        super().__init__(_)


class TaxRateTitle(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-rate-title-interface
    
    Tax rate title interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        store_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        value=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.store_id = store_id
        self.value = value
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostTaxRates(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRates/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        tax_rate=None,  # type: Optional[Union[TaxRate, serial.properties.Null]]
    ):
        self.tax_rate = tax_rate
        super().__init__(_)


class PutTaxRules(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRules/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rule=None,  # type: Optional[Union[TaxRule, serial.properties.Null]]
    ):
        self.rule = rule
        super().__init__(_)


class TaxRule(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-rule-interface
    
    Tax rule interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        priority=None,  # type: Optional[Union[int, serial.properties.Null]]
        position=None,  # type: Optional[Union[int, serial.properties.Null]]
        customer_tax_class_ids=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
        product_tax_class_ids=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
        tax_rate_ids=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
        calculate_subtotal=None,  # type: Optional[bool]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.code = code
        self.priority = priority
        self.position = position
        self.customer_tax_class_ids = customer_tax_class_ids
        self.product_tax_class_ids = product_tax_class_ids
        self.tax_rate_ids = tax_rate_ids
        self.calculate_subtotal = calculate_subtotal
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostTaxRules(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRules/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rule=None,  # type: Optional[Union[TaxRule, serial.properties.Null]]
    ):
        self.rule = rule
        super().__init__(_)


class CompanySearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-data-company-search-results-interface
    
    Interface for company search results
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[Company], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class Company(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-company-interface
    
    Interface for Company entity.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        status=None,  # type: Optional[int]
        company_name=None,  # type: Optional[str]
        legal_name=None,  # type: Optional[str]
        company_email=None,  # type: Optional[str]
        vat_tax_id=None,  # type: Optional[str]
        reseller_id=None,  # type: Optional[str]
        comment=None,  # type: Optional[str]
        street=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        city=None,  # type: Optional[str]
        country_id=None,  # type: Optional[str]
        region=None,  # type: Optional[str]
        region_id=None,  # type: Optional[str]
        postcode=None,  # type: Optional[str]
        telephone=None,  # type: Optional[str]
        customer_group_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        sales_representative_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        reject_reason=None,  # type: Optional[Union[str, serial.properties.Null]]
        rejected_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        super_user_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[CompanyExtension]
    ):
        self.id_ = id_
        self.status = status
        self.company_name = company_name
        self.legal_name = legal_name
        self.company_email = company_email
        self.vat_tax_id = vat_tax_id
        self.reseller_id = reseller_id
        self.comment = comment
        self.street = street
        self.city = city
        self.country_id = country_id
        self.region = region
        self.region_id = region_id
        self.postcode = postcode
        self.telephone = telephone
        self.customer_group_id = customer_group_id
        self.sales_representative_id = sales_representative_id
        self.reject_reason = reject_reason
        self.rejected_at = rejected_at
        self.super_user_id = super_user_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CompanyExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-data-company-extension-interface
    
    ExtensionInterface class for @see \Magento\Company\Api\Data\CompanyInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        applicable_payment_method=None,  # type: Optional[int]
        available_payment_methods=None,  # type: Optional[str]
        use_config_settings=None,  # type: Optional[int]
        quote_config=None,  # type: Optional[NegotiableQuoteCompanyConfig]
    ):
        self.applicable_payment_method = applicable_payment_method
        self.available_payment_methods = available_payment_methods
        self.use_config_settings = use_config_settings
        self.quote_config = quote_config
        super().__init__(_)


class NegotiableQuoteCompanyConfig(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-company-quote-config-interface
    
    Interface CompanyQuoteConfigInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        company_id=None,  # type: Optional[str]
        is_quote_enabled=None,  # type: Optional[Union[bool, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.company_id = company_id
        self.is_quote_enabled = is_quote_enabled
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostCompany(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1company~1/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        company=None,  # type: Optional[Union[Company, serial.properties.Null]]
    ):
        self.company = company
        super().__init__(_)


class PostCustomers(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1customers/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer=None,  # type: Optional[Union[Customer, serial.properties.Null]]
        password=None,  # type: Optional[str]
        redirect_url=None,  # type: Optional[str]
    ):
        self.customer = customer
        self.password = password
        self.redirect_url = redirect_url
        super().__init__(_)


class Customer(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-customer-interface
    
    Customer interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        group_id=None,  # type: Optional[int]
        default_billing=None,  # type: Optional[str]
        default_shipping=None,  # type: Optional[str]
        confirmation=None,  # type: Optional[str]
        created_at=None,  # type: Optional[str]
        updated_at=None,  # type: Optional[str]
        created_in=None,  # type: Optional[str]
        dob=None,  # type: Optional[str]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        firstname=None,  # type: Optional[Union[str, serial.properties.Null]]
        lastname=None,  # type: Optional[Union[str, serial.properties.Null]]
        middlename=None,  # type: Optional[str]
        prefix=None,  # type: Optional[str]
        suffix=None,  # type: Optional[str]
        gender=None,  # type: Optional[int]
        store_id=None,  # type: Optional[int]
        taxvat=None,  # type: Optional[str]
        website_id=None,  # type: Optional[int]
        addresses=None,  # type: Optional[Sequence[CustomerAddress]]
        disable_auto_group_change=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[CustomerExtension]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.id_ = id_
        self.group_id = group_id
        self.default_billing = default_billing
        self.default_shipping = default_shipping
        self.confirmation = confirmation
        self.created_at = created_at
        self.updated_at = updated_at
        self.created_in = created_in
        self.dob = dob
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.middlename = middlename
        self.prefix = prefix
        self.suffix = suffix
        self.gender = gender
        self.store_id = store_id
        self.taxvat = taxvat
        self.website_id = website_id
        self.addresses = addresses
        self.disable_auto_group_change = disable_auto_group_change
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class CustomerAddress(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-address-interface
    
    Customer address interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        customer_id=None,  # type: Optional[int]
        region=None,  # type: Optional[CustomerRegion]
        region_id=None,  # type: Optional[int]
        country_id=None,  # type: Optional[str]
        street=None,  # type: Optional[Sequence[str]]
        company=None,  # type: Optional[str]
        telephone=None,  # type: Optional[str]
        fax=None,  # type: Optional[str]
        postcode=None,  # type: Optional[str]
        city=None,  # type: Optional[str]
        firstname=None,  # type: Optional[str]
        lastname=None,  # type: Optional[str]
        middlename=None,  # type: Optional[str]
        prefix=None,  # type: Optional[str]
        suffix=None,  # type: Optional[str]
        vat_id=None,  # type: Optional[str]
        default_shipping=None,  # type: Optional[bool]
        default_billing=None,  # type: Optional[bool]
        extension_attributes=None,  # type: Optional[dict]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.id_ = id_
        self.customer_id = customer_id
        self.region = region
        self.region_id = region_id
        self.country_id = country_id
        self.street = street
        self.company = company
        self.telephone = telephone
        self.fax = fax
        self.postcode = postcode
        self.city = city
        self.firstname = firstname
        self.lastname = lastname
        self.middlename = middlename
        self.prefix = prefix
        self.suffix = suffix
        self.vat_id = vat_id
        self.default_shipping = default_shipping
        self.default_billing = default_billing
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class CustomerRegion(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-region-interface
    
    Customer address region interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        region_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        region=None,  # type: Optional[Union[str, serial.properties.Null]]
        region_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.region_code = region_code
        self.region = region
        self.region_id = region_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CustomerExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/customer-data-customer-extension-interface
    
    ExtensionInterface class for @see \Magento\Customer\Api\Data\CustomerInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        company_attributes=None,  # type: Optional[CompanyCustomer]
        is_subscribed=None,  # type: Optional[bool]
    ):
        self.company_attributes = company_attributes
        self.is_subscribed = is_subscribed
        super().__init__(_)


class CompanyCustomer(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-data-company-customer-interface
    
    Extended customer custom attributes interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer_id=None,  # type: Optional[int]
        company_id=None,  # type: Optional[int]
        job_title=None,  # type: Optional[str]
        status=None,  # type: Optional[int]
        telephone=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.customer_id = customer_id
        self.company_id = company_id
        self.job_title = job_title
        self.status = status
        self.telephone = telephone
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostInvoices(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1invoices~1/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesInvoice, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class SalesShipmentSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-search-result-interface
    
    Shipment search result interface. A shipment is a delivery package that contains products. A shipment document 
    accompanies the shipment. This document lists the products and their quantities in the delivery package.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesShipment], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class SalesShipment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-interface
    
    Shipment interface. A shipment is a delivery package that contains products. A shipment document accompanies the 
    shipment. This document lists the products and their quantities in the delivery package.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        billing_address_id=None,  # type: Optional[int]
        created_at=None,  # type: Optional[str]
        customer_id=None,  # type: Optional[int]
        email_sent=None,  # type: Optional[int]
        entity_id=None,  # type: Optional[int]
        increment_id=None,  # type: Optional[str]
        order_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        packages=None,  # type: Optional[Sequence[SalesShipmentPackage]]
        shipment_status=None,  # type: Optional[int]
        shipping_address_id=None,  # type: Optional[int]
        shipping_label=None,  # type: Optional[str]
        store_id=None,  # type: Optional[int]
        total_qty=None,  # type: Optional[numbers.Number]
        total_weight=None,  # type: Optional[numbers.Number]
        updated_at=None,  # type: Optional[str]
        items=None,  # type: Optional[Union[Sequence[SalesShipmentItem], serial.properties.Null]]
        tracks=None,  # type: Optional[Union[Sequence[SalesShipmentTrack], serial.properties.Null]]
        comments=None,  # type: Optional[Union[Sequence[SalesShipmentComment], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.billing_address_id = billing_address_id
        self.created_at = created_at
        self.customer_id = customer_id
        self.email_sent = email_sent
        self.entity_id = entity_id
        self.increment_id = increment_id
        self.order_id = order_id
        self.packages = packages
        self.shipment_status = shipment_status
        self.shipping_address_id = shipping_address_id
        self.shipping_label = shipping_label
        self.store_id = store_id
        self.total_qty = total_qty
        self.total_weight = total_weight
        self.updated_at = updated_at
        self.items = items
        self.tracks = tracks
        self.comments = comments
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesShipmentPackage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-package-interface
    
    Shipment package interface. A shipment is a delivery package that contains products. A shipment document accompanies
    the shipment. This document lists the products and their quantities in the delivery package.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesShipmentItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-item-interface
    
    Shipment item interface. A shipment is a delivery package that contains products. A shipment document accompanies 
    the shipment. This document lists the products and their quantities in the delivery package. A product is an item in
    a shipment.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        additional_data=None,  # type: Optional[str]
        description=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
        name=None,  # type: Optional[str]
        parent_id=None,  # type: Optional[int]
        price=None,  # type: Optional[numbers.Number]
        product_id=None,  # type: Optional[int]
        row_total=None,  # type: Optional[numbers.Number]
        sku=None,  # type: Optional[str]
        weight=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[dict]
        order_item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.additional_data = additional_data
        self.description = description
        self.entity_id = entity_id
        self.name = name
        self.parent_id = parent_id
        self.price = price
        self.product_id = product_id
        self.row_total = row_total
        self.sku = sku
        self.weight = weight
        self.extension_attributes = extension_attributes
        self.order_item_id = order_item_id
        self.qty = qty
        super().__init__(_)


class SalesShipmentTrack(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-track-interface
    
    Shipment track interface. A shipment is a delivery package that contains products. A shipment document accompanies 
    the shipment. This document lists the products and their quantities in the delivery package. Merchants and customers
    can track shipments.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        order_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        updated_at=None,  # type: Optional[str]
        weight=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        description=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        track_number=None,  # type: Optional[Union[str, serial.properties.Null]]
        title=None,  # type: Optional[Union[str, serial.properties.Null]]
        carrier_code=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.order_id = order_id
        self.created_at = created_at
        self.entity_id = entity_id
        self.parent_id = parent_id
        self.updated_at = updated_at
        self.weight = weight
        self.qty = qty
        self.description = description
        self.extension_attributes = extension_attributes
        self.track_number = track_number
        self.title = title
        self.carrier_code = carrier_code
        super().__init__(_)


class SalesShipmentComment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-comment-interface
    
    Shipment comment interface. A shipment is a delivery package that contains products. A shipment document accompanies
    the shipment. This document lists the products and their quantities in the delivery package. A shipment document can
    contain comments.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        is_customer_notified=None,  # type: Optional[Union[int, serial.properties.Null]]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_visible_on_front=None,  # type: Optional[Union[int, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
    ):
        self.is_customer_notified = is_customer_notified
        self.parent_id = parent_id
        self.extension_attributes = extension_attributes
        self.comment = comment
        self.is_visible_on_front = is_visible_on_front
        self.created_at = created_at
        self.entity_id = entity_id
        super().__init__(_)


class PostShipment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1shipment~1/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesShipment, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class CategoryTree(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-category-tree-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_active=None,  # type: Optional[Union[bool, serial.properties.Null]]
        position=None,  # type: Optional[Union[int, serial.properties.Null]]
        level=None,  # type: Optional[Union[int, serial.properties.Null]]
        product_count=None,  # type: Optional[Union[int, serial.properties.Null]]
        children_data=None,  # type: Optional[Union[Sequence[CategoryTree], serial.properties.Null]]
    ):
        self.id_ = id_
        self.parent_id = parent_id
        self.name = name
        self.is_active = is_active
        self.position = position
        self.level = level
        self.product_count = product_count
        self.children_data = children_data
        super().__init__(_)


class PostCategories(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1categories/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        category=None,  # type: Optional[Union[Category, serial.properties.Null]]
    ):
        self.category = category
        super().__init__(_)


class Category(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-category-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        parent_id=None,  # type: Optional[int]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_active=None,  # type: Optional[bool]
        position=None,  # type: Optional[int]
        level=None,  # type: Optional[int]
        children=None,  # type: Optional[str]
        created_at=None,  # type: Optional[str]
        updated_at=None,  # type: Optional[str]
        path=None,  # type: Optional[str]
        available_sort_by=None,  # type: Optional[Sequence[str]]
        include_in_menu=None,  # type: Optional[bool]
        extension_attributes=None,  # type: Optional[dict]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.id_ = id_
        self.parent_id = parent_id
        self.name = name
        self.is_active = is_active
        self.position = position
        self.level = level
        self.children = children
        self.created_at = created_at
        self.updated_at = updated_at
        self.path = path
        self.available_sort_by = available_sort_by
        self.include_in_menu = include_in_menu
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class QuoteCart(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-cart-interface
    
    Interface CartInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        updated_at=None,  # type: Optional[str]
        converted_at=None,  # type: Optional[str]
        is_active=None,  # type: Optional[bool]
        is_virtual=None,  # type: Optional[bool]
        items=None,  # type: Optional[Sequence[QuoteCartItem]]
        items_count=None,  # type: Optional[int]
        items_qty=None,  # type: Optional[numbers.Number]
        customer=None,  # type: Optional[Union[Customer, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
        reserved_order_id=None,  # type: Optional[int]
        orig_order_id=None,  # type: Optional[int]
        currency=None,  # type: Optional[QuoteCurrency]
        customer_is_guest=None,  # type: Optional[bool]
        customer_note=None,  # type: Optional[str]
        customer_note_notify=None,  # type: Optional[bool]
        customer_tax_class_id=None,  # type: Optional[int]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[QuoteCartExtension]
    ):
        self.id_ = id_
        self.created_at = created_at
        self.updated_at = updated_at
        self.converted_at = converted_at
        self.is_active = is_active
        self.is_virtual = is_virtual
        self.items = items
        self.items_count = items_count
        self.items_qty = items_qty
        self.customer = customer
        self.billing_address = billing_address
        self.reserved_order_id = reserved_order_id
        self.orig_order_id = orig_order_id
        self.currency = currency
        self.customer_is_guest = customer_is_guest
        self.customer_note = customer_note
        self.customer_note_notify = customer_note_notify
        self.customer_tax_class_id = customer_tax_class_id
        self.store_id = store_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteCartItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-cart-item-interface
    
    Interface CartItemInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        item_id=None,  # type: Optional[int]
        sku=None,  # type: Optional[str]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        name=None,  # type: Optional[str]
        price=None,  # type: Optional[numbers.Number]
        product_type=None,  # type: Optional[str]
        quote_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        product_option=None,  # type: Optional[QuoteProductOption]
        extension_attributes=None,  # type: Optional[QuoteCartItemExtension]
    ):
        self.item_id = item_id
        self.sku = sku
        self.qty = qty
        self.name = name
        self.price = price
        self.product_type = product_type
        self.quote_id = quote_id
        self.product_option = product_option
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteProductOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-product-option-interface
    
    Product option interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[QuoteProductOptionExtension]
    ):
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteProductOptionExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-product-option-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\ProductOptionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        custom_options=None,  # type: Optional[Sequence[CustomOption]]
        bundle_options=None,  # type: Optional[Sequence[BundleOption]]
        configurable_item_options=None,  # type: Optional[Sequence[ConfigurableProductItemOptionValue]]
        downloadable_option=None,  # type: Optional[DownloadableOption]
        giftcard_item_option=None,  # type: Optional[GiftCardOption]
    ):
        self.custom_options = custom_options
        self.bundle_options = bundle_options
        self.configurable_item_options = configurable_item_options
        self.downloadable_option = downloadable_option
        self.giftcard_item_option = giftcard_item_option
        super().__init__(_)


class QuoteCartItemExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-cart-item-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\CartItemInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        negotiable_quote_item=None,  # type: Optional[NegotiableQuoteItem]
    ):
        self.negotiable_quote_item = negotiable_quote_item
        super().__init__(_)


class NegotiableQuoteItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-negotiable-quote-item-interface
    
    Interface CompanyQuoteConfigInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        original_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        original_tax_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        original_discount_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.item_id = item_id
        self.original_price = original_price
        self.original_tax_amount = original_tax_amount
        self.original_discount_amount = original_discount_amount
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteAddress(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-address-interface
    
    Interface AddressInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        region=None,  # type: Optional[Union[str, serial.properties.Null]]
        region_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        region_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        country_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        street=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        company=None,  # type: Optional[str]
        telephone=None,  # type: Optional[Union[str, serial.properties.Null]]
        fax=None,  # type: Optional[str]
        postcode=None,  # type: Optional[Union[str, serial.properties.Null]]
        city=None,  # type: Optional[Union[str, serial.properties.Null]]
        firstname=None,  # type: Optional[Union[str, serial.properties.Null]]
        lastname=None,  # type: Optional[Union[str, serial.properties.Null]]
        middlename=None,  # type: Optional[str]
        prefix=None,  # type: Optional[str]
        suffix=None,  # type: Optional[str]
        vat_id=None,  # type: Optional[str]
        customer_id=None,  # type: Optional[int]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        same_as_billing=None,  # type: Optional[int]
        customer_address_id=None,  # type: Optional[int]
        save_in_address_book=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[QuoteAddressExtension]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.id_ = id_
        self.region = region
        self.region_id = region_id
        self.region_code = region_code
        self.country_id = country_id
        self.street = street
        self.company = company
        self.telephone = telephone
        self.fax = fax
        self.postcode = postcode
        self.city = city
        self.firstname = firstname
        self.lastname = lastname
        self.middlename = middlename
        self.prefix = prefix
        self.suffix = suffix
        self.vat_id = vat_id
        self.customer_id = customer_id
        self.email = email
        self.same_as_billing = same_as_billing
        self.customer_address_id = customer_address_id
        self.save_in_address_book = save_in_address_book
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class QuoteAddressExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-address-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\AddressInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_registry_id=None,  # type: Optional[int]
    ):
        self.gift_registry_id = gift_registry_id
        super().__init__(_)


class QuoteCurrency(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-currency-interface
    
    Interface CurrencyInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        global_currency_code=None,  # type: Optional[str]
        base_currency_code=None,  # type: Optional[str]
        store_currency_code=None,  # type: Optional[str]
        quote_currency_code=None,  # type: Optional[str]
        store_to_base_rate=None,  # type: Optional[numbers.Number]
        store_to_quote_rate=None,  # type: Optional[numbers.Number]
        base_to_global_rate=None,  # type: Optional[numbers.Number]
        base_to_quote_rate=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.global_currency_code = global_currency_code
        self.base_currency_code = base_currency_code
        self.store_currency_code = store_currency_code
        self.quote_currency_code = quote_currency_code
        self.store_to_base_rate = store_to_base_rate
        self.store_to_quote_rate = store_to_quote_rate
        self.base_to_global_rate = base_to_global_rate
        self.base_to_quote_rate = base_to_quote_rate
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteCartExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-cart-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\CartInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shipping_assignments=None,  # type: Optional[Sequence[QuoteShippingAssignment]]
        negotiable_quote=None,  # type: Optional[NegotiableQuote]
    ):
        self.shipping_assignments = shipping_assignments
        self.negotiable_quote = negotiable_quote
        super().__init__(_)


class QuoteShippingAssignment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-shipping-assignment-interface
    
    Interface ShippingAssignmentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shipping=None,  # type: Optional[Union[QuoteShipping, serial.properties.Null]]
        items=None,  # type: Optional[Union[Sequence[QuoteCartItem], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.shipping = shipping
        self.items = items
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteShipping(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-shipping-interface
    
    Interface ShippingInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
        method=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.address = address
        self.method = method
        self.extension_attributes = extension_attributes
        super().__init__(_)


class NegotiableQuote(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-negotiable-quote-interface
    
    Interface NegotiableQuoteInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        quote_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_regular_quote=None,  # type: Optional[Union[bool, serial.properties.Null]]
        status=None,  # type: Optional[Union[str, serial.properties.Null]]
        negotiated_price_type=None,  # type: Optional[Union[int, serial.properties.Null]]
        negotiated_price_value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        shipping_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        quote_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        expiration_period=None,  # type: Optional[Union[str, serial.properties.Null]]
        email_notification_status=None,  # type: Optional[Union[int, serial.properties.Null]]
        has_unconfirmed_changes=None,  # type: Optional[Union[bool, serial.properties.Null]]
        is_shipping_tax_changed=None,  # type: Optional[Union[bool, serial.properties.Null]]
        is_customer_price_changed=None,  # type: Optional[Union[bool, serial.properties.Null]]
        notifications=None,  # type: Optional[Union[int, serial.properties.Null]]
        applied_rule_ids=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_address_draft=None,  # type: Optional[Union[bool, serial.properties.Null]]
        deleted_sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        creator_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        creator_type=None,  # type: Optional[Union[int, serial.properties.Null]]
        original_total_price=None,  # type: Optional[numbers.Number]
        base_original_total_price=None,  # type: Optional[numbers.Number]
        negotiated_total_price=None,  # type: Optional[numbers.Number]
        base_negotiated_total_price=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.quote_id = quote_id
        self.is_regular_quote = is_regular_quote
        self.status = status
        self.negotiated_price_type = negotiated_price_type
        self.negotiated_price_value = negotiated_price_value
        self.shipping_price = shipping_price
        self.quote_name = quote_name
        self.expiration_period = expiration_period
        self.email_notification_status = email_notification_status
        self.has_unconfirmed_changes = has_unconfirmed_changes
        self.is_shipping_tax_changed = is_shipping_tax_changed
        self.is_customer_price_changed = is_customer_price_changed
        self.notifications = notifications
        self.applied_rule_ids = applied_rule_ids
        self.is_address_draft = is_address_draft
        self.deleted_sku = deleted_sku
        self.creator_id = creator_id
        self.creator_type = creator_type
        self.original_total_price = original_total_price
        self.base_original_total_price = base_original_total_price
        self.negotiated_total_price = negotiated_total_price
        self.base_negotiated_total_price = base_negotiated_total_price
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutCartsMine(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1carts~1mine/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        quote=None,  # type: Optional[Union[QuoteCart, serial.properties.Null]]
    ):
        self.quote = quote
        super().__init__(_)


class PostCreditMemo(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1creditmemo/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesCreditMemo, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class SalesCreditMemo(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-creditmemo-interface
    
    Credit memo interface. After a customer places and pays for an order and an invoice has been issued, the merchant 
    can create a credit memo to refund all or part of the amount paid for any returned or undelivered items. The memo 
    restores funds to the customer account so that the customer can make future purchases.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        adjustment=None,  # type: Optional[numbers.Number]
        adjustment_negative=None,  # type: Optional[numbers.Number]
        adjustment_positive=None,  # type: Optional[numbers.Number]
        base_adjustment=None,  # type: Optional[numbers.Number]
        base_adjustment_negative=None,  # type: Optional[numbers.Number]
        base_adjustment_positive=None,  # type: Optional[numbers.Number]
        base_currency_code=None,  # type: Optional[str]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        base_grand_total=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        base_shipping_amount=None,  # type: Optional[numbers.Number]
        base_shipping_discount_tax_compensation_amnt=None,  # type: Optional[numbers.Number]
        base_shipping_incl_tax=None,  # type: Optional[numbers.Number]
        base_shipping_tax_amount=None,  # type: Optional[numbers.Number]
        base_subtotal=None,  # type: Optional[numbers.Number]
        base_subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        base_to_global_rate=None,  # type: Optional[numbers.Number]
        base_to_order_rate=None,  # type: Optional[numbers.Number]
        billing_address_id=None,  # type: Optional[int]
        created_at=None,  # type: Optional[str]
        creditmemo_status=None,  # type: Optional[int]
        discount_amount=None,  # type: Optional[numbers.Number]
        discount_description=None,  # type: Optional[str]
        email_sent=None,  # type: Optional[int]
        entity_id=None,  # type: Optional[int]
        global_currency_code=None,  # type: Optional[str]
        grand_total=None,  # type: Optional[numbers.Number]
        discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        increment_id=None,  # type: Optional[str]
        invoice_id=None,  # type: Optional[int]
        order_currency_code=None,  # type: Optional[str]
        order_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        shipping_address_id=None,  # type: Optional[int]
        shipping_amount=None,  # type: Optional[numbers.Number]
        shipping_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        shipping_incl_tax=None,  # type: Optional[numbers.Number]
        shipping_tax_amount=None,  # type: Optional[numbers.Number]
        state=None,  # type: Optional[int]
        store_currency_code=None,  # type: Optional[str]
        store_id=None,  # type: Optional[int]
        store_to_base_rate=None,  # type: Optional[numbers.Number]
        store_to_order_rate=None,  # type: Optional[numbers.Number]
        subtotal=None,  # type: Optional[numbers.Number]
        subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        tax_amount=None,  # type: Optional[numbers.Number]
        transaction_id=None,  # type: Optional[str]
        updated_at=None,  # type: Optional[str]
        items=None,  # type: Optional[Union[Sequence[SalesCreditMemoItem], serial.properties.Null]]
        comments=None,  # type: Optional[Sequence[SalesCreditMemoComment]]
        extension_attributes=None,  # type: Optional[SalesCreditMemoExtension]
    ):
        self.adjustment = adjustment
        self.adjustment_negative = adjustment_negative
        self.adjustment_positive = adjustment_positive
        self.base_adjustment = base_adjustment
        self.base_adjustment_negative = base_adjustment_negative
        self.base_adjustment_positive = base_adjustment_positive
        self.base_currency_code = base_currency_code
        self.base_discount_amount = base_discount_amount
        self.base_grand_total = base_grand_total
        self.base_discount_tax_compensation_amount = base_discount_tax_compensation_amount
        self.base_shipping_amount = base_shipping_amount
        self.base_shipping_discount_tax_compensation_amnt = base_shipping_discount_tax_compensation_amnt
        self.base_shipping_incl_tax = base_shipping_incl_tax
        self.base_shipping_tax_amount = base_shipping_tax_amount
        self.base_subtotal = base_subtotal
        self.base_subtotal_incl_tax = base_subtotal_incl_tax
        self.base_tax_amount = base_tax_amount
        self.base_to_global_rate = base_to_global_rate
        self.base_to_order_rate = base_to_order_rate
        self.billing_address_id = billing_address_id
        self.created_at = created_at
        self.creditmemo_status = creditmemo_status
        self.discount_amount = discount_amount
        self.discount_description = discount_description
        self.email_sent = email_sent
        self.entity_id = entity_id
        self.global_currency_code = global_currency_code
        self.grand_total = grand_total
        self.discount_tax_compensation_amount = discount_tax_compensation_amount
        self.increment_id = increment_id
        self.invoice_id = invoice_id
        self.order_currency_code = order_currency_code
        self.order_id = order_id
        self.shipping_address_id = shipping_address_id
        self.shipping_amount = shipping_amount
        self.shipping_discount_tax_compensation_amount = shipping_discount_tax_compensation_amount
        self.shipping_incl_tax = shipping_incl_tax
        self.shipping_tax_amount = shipping_tax_amount
        self.state = state
        self.store_currency_code = store_currency_code
        self.store_id = store_id
        self.store_to_base_rate = store_to_base_rate
        self.store_to_order_rate = store_to_order_rate
        self.subtotal = subtotal
        self.subtotal_incl_tax = subtotal_incl_tax
        self.tax_amount = tax_amount
        self.transaction_id = transaction_id
        self.updated_at = updated_at
        self.items = items
        self.comments = comments
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesCreditMemoItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-creditmemo-item-interface
    
    Credit memo item interface. After a customer places and pays for an order and an invoice has been issued, the 
    merchant can create a credit memo to refund all or part of the amount paid for any returned or undelivered items. 
    The memo restores funds to the customer account so that the customer can make future purchases. A credit memo item 
    is an invoiced item for which a merchant creates a credit memo.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        additional_data=None,  # type: Optional[str]
        base_cost=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        base_discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        base_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_price_incl_tax=None,  # type: Optional[numbers.Number]
        base_row_total=None,  # type: Optional[numbers.Number]
        base_row_total_incl_tax=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        base_weee_tax_applied_amount=None,  # type: Optional[numbers.Number]
        base_weee_tax_applied_row_amnt=None,  # type: Optional[numbers.Number]
        base_weee_tax_disposition=None,  # type: Optional[numbers.Number]
        base_weee_tax_row_disposition=None,  # type: Optional[numbers.Number]
        description=None,  # type: Optional[str]
        discount_amount=None,  # type: Optional[numbers.Number]
        entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        discount_tax_compensation_amount=None,  # type: Optional[numbers.Number]
        name=None,  # type: Optional[str]
        order_item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        parent_id=None,  # type: Optional[int]
        price=None,  # type: Optional[numbers.Number]
        price_incl_tax=None,  # type: Optional[numbers.Number]
        product_id=None,  # type: Optional[int]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        row_total=None,  # type: Optional[numbers.Number]
        row_total_incl_tax=None,  # type: Optional[numbers.Number]
        sku=None,  # type: Optional[str]
        tax_amount=None,  # type: Optional[numbers.Number]
        weee_tax_applied=None,  # type: Optional[str]
        weee_tax_applied_amount=None,  # type: Optional[numbers.Number]
        weee_tax_applied_row_amount=None,  # type: Optional[numbers.Number]
        weee_tax_disposition=None,  # type: Optional[numbers.Number]
        weee_tax_row_disposition=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.additional_data = additional_data
        self.base_cost = base_cost
        self.base_discount_amount = base_discount_amount
        self.base_discount_tax_compensation_amount = base_discount_tax_compensation_amount
        self.base_price = base_price
        self.base_price_incl_tax = base_price_incl_tax
        self.base_row_total = base_row_total
        self.base_row_total_incl_tax = base_row_total_incl_tax
        self.base_tax_amount = base_tax_amount
        self.base_weee_tax_applied_amount = base_weee_tax_applied_amount
        self.base_weee_tax_applied_row_amnt = base_weee_tax_applied_row_amnt
        self.base_weee_tax_disposition = base_weee_tax_disposition
        self.base_weee_tax_row_disposition = base_weee_tax_row_disposition
        self.description = description
        self.discount_amount = discount_amount
        self.entity_id = entity_id
        self.discount_tax_compensation_amount = discount_tax_compensation_amount
        self.name = name
        self.order_item_id = order_item_id
        self.parent_id = parent_id
        self.price = price
        self.price_incl_tax = price_incl_tax
        self.product_id = product_id
        self.qty = qty
        self.row_total = row_total
        self.row_total_incl_tax = row_total_incl_tax
        self.sku = sku
        self.tax_amount = tax_amount
        self.weee_tax_applied = weee_tax_applied
        self.weee_tax_applied_amount = weee_tax_applied_amount
        self.weee_tax_applied_row_amount = weee_tax_applied_row_amount
        self.weee_tax_disposition = weee_tax_disposition
        self.weee_tax_row_disposition = weee_tax_row_disposition
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesCreditMemoComment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-comment-interface
    
    Credit memo comment interface. After a customer places and pays for an order and an invoice has been issued, the 
    merchant can create a credit memo to refund all or part of the amount paid for any returned or undelivered items. 
    The memo restores funds to the customer account so that the customer can make future purchases. A credit memo 
    usually includes comments that detail why the credit memo amount was credited to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        created_at=None,  # type: Optional[str]
        entity_id=None,  # type: Optional[int]
        is_customer_notified=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_visible_on_front=None,  # type: Optional[Union[int, serial.properties.Null]]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.comment = comment
        self.created_at = created_at
        self.entity_id = entity_id
        self.is_customer_notified = is_customer_notified
        self.is_visible_on_front = is_visible_on_front
        self.parent_id = parent_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesCreditMemoExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-extension-interface
    
    ExtensionInterface class for @see \Magento\Sales\Api\Data\CreditmemoInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        base_customer_balance_amount=None,  # type: Optional[numbers.Number]
        customer_balance_amount=None,  # type: Optional[numbers.Number]
        base_gift_cards_amount=None,  # type: Optional[numbers.Number]
        gift_cards_amount=None,  # type: Optional[numbers.Number]
        gw_base_price=None,  # type: Optional[str]
        gw_price=None,  # type: Optional[str]
        gw_items_base_price=None,  # type: Optional[str]
        gw_items_price=None,  # type: Optional[str]
        gw_card_base_price=None,  # type: Optional[str]
        gw_card_price=None,  # type: Optional[str]
        gw_base_tax_amount=None,  # type: Optional[str]
        gw_tax_amount=None,  # type: Optional[str]
        gw_items_base_tax_amount=None,  # type: Optional[str]
        gw_items_tax_amount=None,  # type: Optional[str]
        gw_card_base_tax_amount=None,  # type: Optional[str]
        gw_card_tax_amount=None,  # type: Optional[str]
    ):
        self.base_customer_balance_amount = base_customer_balance_amount
        self.customer_balance_amount = customer_balance_amount
        self.base_gift_cards_amount = base_gift_cards_amount
        self.gift_cards_amount = gift_cards_amount
        self.gw_base_price = gw_base_price
        self.gw_price = gw_price
        self.gw_items_base_price = gw_items_base_price
        self.gw_items_price = gw_items_price
        self.gw_card_base_price = gw_card_base_price
        self.gw_card_price = gw_card_price
        self.gw_base_tax_amount = gw_base_tax_amount
        self.gw_tax_amount = gw_tax_amount
        self.gw_items_base_tax_amount = gw_items_base_tax_amount
        self.gw_items_tax_amount = gw_items_tax_amount
        self.gw_card_base_tax_amount = gw_card_base_tax_amount
        self.gw_card_tax_amount = gw_card_tax_amount
        super().__init__(_)


class PostSalesRules(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1salesRules/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rule=None,  # type: Optional[Union[SalesRule, serial.properties.Null]]
    ):
        self.rule = rule
        super().__init__(_)


class SalesRule(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-rule-interface
    
    Interface RuleInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rule_id=None,  # type: Optional[int]
        name=None,  # type: Optional[str]
        store_labels=None,  # type: Optional[Sequence[SalesRuleLabel]]
        description=None,  # type: Optional[str]
        website_ids=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
        customer_group_ids=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
        from_date=None,  # type: Optional[str]
        to_date=None,  # type: Optional[str]
        uses_per_customer=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_active=None,  # type: Optional[Union[bool, serial.properties.Null]]
        condition=None,  # type: Optional[SalesRuleCondition]
        action_condition=None,  # type: Optional[SalesRuleCondition]
        stop_rules_processing=None,  # type: Optional[Union[bool, serial.properties.Null]]
        is_advanced=None,  # type: Optional[Union[bool, serial.properties.Null]]
        product_ids=None,  # type: Optional[Sequence[int]]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
        simple_action=None,  # type: Optional[str]
        discount_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        discount_qty=None,  # type: Optional[numbers.Number]
        discount_step=None,  # type: Optional[Union[int, serial.properties.Null]]
        apply_to_shipping=None,  # type: Optional[Union[bool, serial.properties.Null]]
        times_used=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_rss=None,  # type: Optional[Union[bool, serial.properties.Null]]
        coupon_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        use_auto_generation=None,  # type: Optional[Union[bool, serial.properties.Null]]
        uses_per_coupon=None,  # type: Optional[Union[int, serial.properties.Null]]
        simple_free_shipping=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[SalesRuleExtension]
    ):
        self.rule_id = rule_id
        self.name = name
        self.store_labels = store_labels
        self.description = description
        self.website_ids = website_ids
        self.customer_group_ids = customer_group_ids
        self.from_date = from_date
        self.to_date = to_date
        self.uses_per_customer = uses_per_customer
        self.is_active = is_active
        self.condition = condition
        self.action_condition = action_condition
        self.stop_rules_processing = stop_rules_processing
        self.is_advanced = is_advanced
        self.product_ids = product_ids
        self.sort_order = sort_order
        self.simple_action = simple_action
        self.discount_amount = discount_amount
        self.discount_qty = discount_qty
        self.discount_step = discount_step
        self.apply_to_shipping = apply_to_shipping
        self.times_used = times_used
        self.is_rss = is_rss
        self.coupon_type = coupon_type
        self.use_auto_generation = use_auto_generation
        self.uses_per_coupon = uses_per_coupon
        self.simple_free_shipping = simple_free_shipping
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesRuleLabel(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-rule-label-interface
    
    Interface RuleLabelInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        store_label=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.store_id = store_id
        self.store_label = store_label
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesRuleCondition(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-condition-interface
    
    Interface ConditionInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        condition_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        conditions=None,  # type: Optional[Sequence[SalesRuleCondition]]
        aggregator_type=None,  # type: Optional[str]
        operator=None,  # type: Optional[Union[str, serial.properties.Null]]
        attribute_name=None,  # type: Optional[str]
        value=None,  # type: Optional[Union[Union[str, bool, Sequence[str]], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.condition_type = condition_type
        self.conditions = conditions
        self.aggregator_type = aggregator_type
        self.operator = operator
        self.attribute_name = attribute_name
        self.value = value
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesRuleExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-rule-data-rule-extension-interface
    
    ExtensionInterface class for @see \Magento\SalesRule\Api\Data\RuleInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        reward_points_delta=None,  # type: Optional[int]
    ):
        self.reward_points_delta = reward_points_delta
        super().__init__(_)


class PostTaxClasses(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxClasses/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        tax_class=None,  # type: Optional[Union[TaxClass, serial.properties.Null]]
    ):
        self.tax_class = tax_class
        super().__init__(_)


class TaxClass(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-class-interface
    
    Tax class interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        class_id=None,  # type: Optional[int]
        class_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        class_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.class_id = class_id
        self.class_name = class_name
        self.class_type = class_type
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesCreditMemoSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-search-result-interface
    
    Credit memo search result interface. After a customer places and pays for an order and an invoice has been issued, 
    the merchant can create a credit memo to refund all or part of the amount paid for any returned or undelivered 
    items. The memo restores funds to the customer account so that the customer can make future purchases.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesCreditMemo], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PutMe(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1me/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer=None,  # type: Optional[Union[Customer, serial.properties.Null]]
        password_hash=None,  # type: Optional[str]
    ):
        self.customer = customer
        self.password_hash = password_hash
        super().__init__(_)


class PutCMSPage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1cmsPage~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        page=None,  # type: Optional[Union[CMSPage, serial.properties.Null]]
    ):
        self.page = page
        super().__init__(_)


class QuoteCartSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-cart-search-results-interface
    
    Interface CartSearchResultsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[QuoteCart], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class SalesOrderItemSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-order-item-search-result-interface
    
    Order item search result interface. An order is a document that a web store issues to a customer. Magento generates 
    a sales order that lists the product items, billing and shipping addresses, and shipping and payment methods. A 
    corresponding external document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesOrderItem], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class SalesTransactionSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-transaction-search-result-interface
    
    Transaction search result interface. A transaction is an interaction between a merchant and a customer such as a 
    purchase, a credit, a refund, and so on.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesTransaction], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class SalesTransaction(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-transaction-interface
    
    Transaction interface. A transaction is an interaction between a merchant and a customer such as a purchase, a 
    credit, a refund, and so on.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        transaction_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        parent_id=None,  # type: Optional[int]
        order_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        payment_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        txn_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        parent_txn_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        txn_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_closed=None,  # type: Optional[Union[int, serial.properties.Null]]
        additional_information=None,  # type: Optional[Sequence[str]]
        created_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        child_transactions=None,  # type: Optional[Union[Sequence[SalesTransaction], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.transaction_id = transaction_id
        self.parent_id = parent_id
        self.order_id = order_id
        self.payment_id = payment_id
        self.txn_id = txn_id
        self.parent_txn_id = parent_txn_id
        self.txn_type = txn_type
        self.is_closed = is_closed
        self.additional_information = additional_information
        self.created_at = created_at
        self.child_transactions = child_transactions
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutReturns(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1returns~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rma_data_object=None,  # type: Optional[Union[RMA, serial.properties.Null]]
    ):
        self.rma_data_object = rma_data_object
        super().__init__(_)


class DeleteReturns(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1returns~1{id}/delete/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rma_data_object=None,  # type: Optional[Union[RMA, serial.properties.Null]]
    ):
        self.rma_data_object = rma_data_object
        super().__init__(_)


class PutCMSBlock(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1cmsBlock~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        block=None,  # type: Optional[Union[CMSBlock, serial.properties.Null]]
    ):
        self.block = block
        super().__init__(_)


class PostProductsCost(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1cost/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        prices=None,  # type: Optional[Union[Sequence[Cost], serial.properties.Null]]
    ):
        self.prices = prices
        super().__init__(_)


class Cost(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-cost-interface
    
    Cost interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cost=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.cost = cost
        self.store_id = store_id
        self.sku = sku
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PriceUpdateResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-price-update-result-interface
    
    Interface returned in case of incorrect price passed to efficient price API.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        message=None,  # type: Optional[Union[str, serial.properties.Null]]
        parameters=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.message = message
        self.parameters = parameters
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutOrdersCreate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1orders~1create/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesOrder, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class CheckoutAgreementsAgreement(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/checkout-agreements-data-agreement-interface
    
    Interface AgreementInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        agreement_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        content=None,  # type: Optional[Union[str, serial.properties.Null]]
        content_height=None,  # type: Optional[str]
        checkbox_text=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_active=None,  # type: Optional[Union[bool, serial.properties.Null]]
        is_html=None,  # type: Optional[Union[bool, serial.properties.Null]]
        mode=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.agreement_id = agreement_id
        self.name = name
        self.content = content
        self.content_height = content_height
        self.checkbox_text = checkbox_text
        self.is_active = is_active
        self.is_html = is_html
        self.mode = mode
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutTeam(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1team~1{teamId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        team=None,  # type: Optional[Union[CompanyTeam, serial.properties.Null]]
    ):
        self.team = team
        super().__init__(_)


class CompanyRoleSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-data-role-search-results-interface
    
    Interface for role search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CompanyRole], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class CompanyRole(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-role-interface
    
    Role data transfer object interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        role_name=None,  # type: Optional[str]
        permissions=None,  # type: Optional[Union[Sequence[CompanyPermission], serial.properties.Null]]
        company_id=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.role_name = role_name
        self.permissions = permissions
        self.company_id = company_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CompanyPermission(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-permission-interface
    
    Permission interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        role_id=None,  # type: Optional[int]
        resource_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        permission=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.id_ = id_
        self.role_id = role_id
        self.resource_id = resource_id
        self.permission = permission
        super().__init__(_)


class PostCompanyRole(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1company~1role~1/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        role=None,  # type: Optional[Union[CompanyRole, serial.properties.Null]]
    ):
        self.role = role
        super().__init__(_)


class PostSharedCatalog(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shared_catalog=None,  # type: Optional[Union[SharedCatalog, serial.properties.Null]]
    ):
        self.shared_catalog = shared_catalog
        super().__init__(_)


class SharedCatalog(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/shared-catalog-data-shared-catalog-interface
    
    SharedCatalogInterface interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        description=None,  # type: Optional[Union[str, serial.properties.Null]]
        customer_group_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        type_=None,  # type: Optional[Union[int, serial.properties.Null]]
        created_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        created_by=None,  # type: Optional[Union[int, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        tax_class_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.id_ = id_
        self.name = name
        self.description = description
        self.customer_group_id = customer_group_id
        self.type_ = type_
        self.created_at = created_at
        self.created_by = created_by
        self.store_id = store_id
        self.tax_class_id = tax_class_id
        super().__init__(_)


class StoreWebsite(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-website-interface
    
    Website interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        default_group_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.code = code
        self.name = name
        self.default_group_id = default_group_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostCustomerGroups(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customerGroups/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        group=None,  # type: Optional[Union[CustomerGroup, serial.properties.Null]]
    ):
        self.group = group
        super().__init__(_)


class CustomerGroup(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-group-interface
    
    Customer group interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        tax_class_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        tax_class_name=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.code = code
        self.tax_class_id = tax_class_id
        self.tax_class_name = tax_class_name
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CMSPageSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/cms-data-page-search-results-interface
    
    Interface for cms page search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CMSPage], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PutProducts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product=None,  # type: Optional[Union[Product, serial.properties.Null]]
        save_options=None,  # type: Optional[bool]
    ):
        self.product = product
        self.save_options = save_options
        super().__init__(_)


class ProductType(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-type-interface
    
    Product type details
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.name = name
        self.label = label
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutCarts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.customer_id = customer_id
        self.store_id = store_id
        super().__init__(_)


class PostShipmentTrack(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1shipment~1track/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesShipmentTrack, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class SalesRuleCouponSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-rule-data-coupon-search-result-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesRuleCoupon], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class CompanyHierarchy(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-hierarchy-interface
    
    Company hierarchy DTO interface for WebAPI.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        structure_id=None,  # type: Optional[int]
        entity_id=None,  # type: Optional[int]
        entity_type=None,  # type: Optional[str]
        structure_parent_id=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.structure_id = structure_id
        self.entity_id = entity_id
        self.entity_type = entity_type
        self.structure_parent_id = structure_parent_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class AnalyticsLink(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/analytics-data-link-interface
    
    Interface LinkInterface Represents link with collected data and initialized vector for decryption.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        url=None,  # type: Optional[Union[str, serial.properties.Null]]
        initialization_vector=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.url = url
        self.initialization_vector = initialization_vector
        super().__init__(_)


class SearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/shared-catalog-data-search-results-interface
    
    Interface for Shared Catalog search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SharedCatalog], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class GiftWrappingSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/gift-wrapping-data-wrapping-search-results-interface
    
    Interface WrappingSearchResultsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[GiftWrapping], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class GiftWrapping(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/gift-wrapping-data-wrapping-interface
    
    Interface WrappingInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        wrapping_id=None,  # type: Optional[int]
        design=None,  # type: Optional[Union[str, serial.properties.Null]]
        status=None,  # type: Optional[Union[int, serial.properties.Null]]
        base_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        image_name=None,  # type: Optional[str]
        image_base_64_content=None,  # type: Optional[str]
        base_currency_code=None,  # type: Optional[str]
        website_ids=None,  # type: Optional[Sequence[int]]
        image_url=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.wrapping_id = wrapping_id
        self.design = design
        self.status = status
        self.base_price = base_price
        self.image_name = image_name
        self.image_base_64_content = image_base_64_content
        self.base_currency_code = base_currency_code
        self.website_ids = website_ids
        self.image_url = image_url
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostGiftWrappings(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1gift-wrappings/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        data=None,  # type: Optional[Union[GiftWrapping, serial.properties.Null]]
        store_id=None,  # type: Optional[int]
    ):
        self.data = data
        self.store_id = store_id
        super().__init__(_)


class CMSBlockSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/cms-data-block-search-results-interface
    
    Interface for cms block search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CMSBlock], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PutCategories(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1categories~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        category=None,  # type: Optional[Union[Category, serial.properties.Null]]
    ):
        self.category = category
        super().__init__(_)


class CategorySearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-category-search-results-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[Category], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class TaxRateSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/tax-data-tax-rate-search-results-interface
    
    Interface for tax rate search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[TaxRate], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class TaxRuleSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/tax-data-tax-rule-search-results-interface
    
    Interface for tax rule search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[TaxRule], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class CompanyCreditLimitSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-credit-data-credit-limit-search-results-interface
    
    Interface for Credit Limit search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CompanyCredit], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class CompanyCredit(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-credit-data-credit-data-interface
    
    Credit Data interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        company_id=None,  # type: Optional[int]
        credit_limit=None,  # type: Optional[numbers.Number]
        balance=None,  # type: Optional[numbers.Number]
        currency_code=None,  # type: Optional[str]
        exceed_limit=None,  # type: Optional[Union[bool, serial.properties.Null]]
        available_limit=None,  # type: Optional[numbers.Number]
    ):
        self.id_ = id_
        self.company_id = company_id
        self.credit_limit = credit_limit
        self.balance = balance
        self.currency_code = currency_code
        self.exceed_limit = exceed_limit
        self.available_limit = available_limit
        super().__init__(_)


class Store(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-store-interface
    
    Store interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        website_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        store_group_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.code = code
        self.name = name
        self.website_id = website_id
        self.store_group_id = store_group_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CustomerSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/customer-data-customer-search-results-interface
    
    Interface for customer search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[Customer], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostProductsOptions(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1options/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option=None,  # type: Optional[Union[ProductCustomOption, serial.properties.Null]]
    ):
        self.option = option
        super().__init__(_)


class PutCartsMineOrder(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1order/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[QuotePayment]
    ):
        self.payment_method = payment_method
        super().__init__(_)


class QuotePayment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-payment-interface
    
    Interface PaymentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        po_number=None,  # type: Optional[str]
        method=None,  # type: Optional[Union[str, serial.properties.Null]]
        additional_data=None,  # type: Optional[Sequence[str]]
        extension_attributes=None,  # type: Optional[QuotePaymentExtension]
    ):
        self.po_number = po_number
        self.method = method
        self.additional_data = additional_data
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuotePaymentExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-payment-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\PaymentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        agreement_ids=None,  # type: Optional[Sequence[str]]
    ):
        self.agreement_ids = agreement_ids
        super().__init__(_)


class PostCartsMineItems(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1items/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cart_item=None,  # type: Optional[Union[QuoteCartItem, serial.properties.Null]]
    ):
        self.cart_item = cart_item
        super().__init__(_)


class PostCouponsGenerate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1coupons~1generate/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        coupon_spec=None,  # type: Optional[Union[SalesRuleCouponGenerationSpec, serial.properties.Null]]
    ):
        self.coupon_spec = coupon_spec
        super().__init__(_)


class SalesRuleCouponGenerationSpec(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-rule-data-coupon-generation-spec-interface
    
    CouponGenerationSpecInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rule_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        format_=None,  # type: Optional[Union[str, serial.properties.Null]]
        quantity=None,  # type: Optional[Union[int, serial.properties.Null]]
        length=None,  # type: Optional[Union[int, serial.properties.Null]]
        prefix=None,  # type: Optional[str]
        suffix=None,  # type: Optional[str]
        delimiter_at_every=None,  # type: Optional[int]
        delimiter=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.rule_id = rule_id
        self.format_ = format_
        self.quantity = quantity
        self.length = length
        self.prefix = prefix
        self.suffix = suffix
        self.delimiter_at_every = delimiter_at_every
        self.delimiter = delimiter
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostTeam(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1team~1{companyId}/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        team=None,  # type: Optional[Union[CompanyTeam, serial.properties.Null]]
    ):
        self.team = team
        super().__init__(_)


class StoreGroup(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-group-interface
    
    Group interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        website_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        root_category_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        default_store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.website_id = website_id
        self.root_category_id = root_category_id
        self.default_store_id = default_store_id
        self.name = name
        self.code = code
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostCustomersConfirm(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1confirm/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        website_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        redirect_url=None,  # type: Optional[str]
    ):
        self.email = email
        self.website_id = website_id
        self.redirect_url = redirect_url
        super().__init__(_)


class QuoteTotals(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-totals-interface
    
    Interface TotalsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        grand_total=None,  # type: Optional[numbers.Number]
        base_grand_total=None,  # type: Optional[numbers.Number]
        subtotal=None,  # type: Optional[numbers.Number]
        base_subtotal=None,  # type: Optional[numbers.Number]
        discount_amount=None,  # type: Optional[numbers.Number]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        subtotal_with_discount=None,  # type: Optional[numbers.Number]
        base_subtotal_with_discount=None,  # type: Optional[numbers.Number]
        shipping_amount=None,  # type: Optional[numbers.Number]
        base_shipping_amount=None,  # type: Optional[numbers.Number]
        shipping_discount_amount=None,  # type: Optional[numbers.Number]
        base_shipping_discount_amount=None,  # type: Optional[numbers.Number]
        tax_amount=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        weee_tax_applied_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        shipping_tax_amount=None,  # type: Optional[numbers.Number]
        base_shipping_tax_amount=None,  # type: Optional[numbers.Number]
        subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        base_subtotal_incl_tax=None,  # type: Optional[numbers.Number]
        shipping_incl_tax=None,  # type: Optional[numbers.Number]
        base_shipping_incl_tax=None,  # type: Optional[numbers.Number]
        base_currency_code=None,  # type: Optional[str]
        quote_currency_code=None,  # type: Optional[str]
        coupon_code=None,  # type: Optional[str]
        items_qty=None,  # type: Optional[int]
        items=None,  # type: Optional[Sequence[QuoteTotalsItem]]
        total_segments=None,  # type: Optional[Union[Sequence[QuoteTotalSegment], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[QuoteTotalsExtension]
    ):
        self.grand_total = grand_total
        self.base_grand_total = base_grand_total
        self.subtotal = subtotal
        self.base_subtotal = base_subtotal
        self.discount_amount = discount_amount
        self.base_discount_amount = base_discount_amount
        self.subtotal_with_discount = subtotal_with_discount
        self.base_subtotal_with_discount = base_subtotal_with_discount
        self.shipping_amount = shipping_amount
        self.base_shipping_amount = base_shipping_amount
        self.shipping_discount_amount = shipping_discount_amount
        self.base_shipping_discount_amount = base_shipping_discount_amount
        self.tax_amount = tax_amount
        self.base_tax_amount = base_tax_amount
        self.weee_tax_applied_amount = weee_tax_applied_amount
        self.shipping_tax_amount = shipping_tax_amount
        self.base_shipping_tax_amount = base_shipping_tax_amount
        self.subtotal_incl_tax = subtotal_incl_tax
        self.base_subtotal_incl_tax = base_subtotal_incl_tax
        self.shipping_incl_tax = shipping_incl_tax
        self.base_shipping_incl_tax = base_shipping_incl_tax
        self.base_currency_code = base_currency_code
        self.quote_currency_code = quote_currency_code
        self.coupon_code = coupon_code
        self.items_qty = items_qty
        self.items = items
        self.total_segments = total_segments
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteTotalsItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-totals-item-interface
    
    Interface TotalsItemInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        row_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_row_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        row_total_with_discount=None,  # type: Optional[numbers.Number]
        tax_amount=None,  # type: Optional[numbers.Number]
        base_tax_amount=None,  # type: Optional[numbers.Number]
        tax_percent=None,  # type: Optional[numbers.Number]
        discount_amount=None,  # type: Optional[numbers.Number]
        base_discount_amount=None,  # type: Optional[numbers.Number]
        discount_percent=None,  # type: Optional[numbers.Number]
        price_incl_tax=None,  # type: Optional[numbers.Number]
        base_price_incl_tax=None,  # type: Optional[numbers.Number]
        row_total_incl_tax=None,  # type: Optional[numbers.Number]
        base_row_total_incl_tax=None,  # type: Optional[numbers.Number]
        options=None,  # type: Optional[Union[str, serial.properties.Null]]
        weee_tax_applied_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        weee_tax_applied=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[QuoteTotalsItemExtension]
        name=None,  # type: Optional[str]
    ):
        self.item_id = item_id
        self.price = price
        self.base_price = base_price
        self.qty = qty
        self.row_total = row_total
        self.base_row_total = base_row_total
        self.row_total_with_discount = row_total_with_discount
        self.tax_amount = tax_amount
        self.base_tax_amount = base_tax_amount
        self.tax_percent = tax_percent
        self.discount_amount = discount_amount
        self.base_discount_amount = base_discount_amount
        self.discount_percent = discount_percent
        self.price_incl_tax = price_incl_tax
        self.base_price_incl_tax = base_price_incl_tax
        self.row_total_incl_tax = row_total_incl_tax
        self.base_row_total_incl_tax = base_row_total_incl_tax
        self.options = options
        self.weee_tax_applied_amount = weee_tax_applied_amount
        self.weee_tax_applied = weee_tax_applied
        self.extension_attributes = extension_attributes
        self.name = name
        super().__init__(_)


class QuoteTotalsItemExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-totals-item-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\TotalsItemInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        negotiable_quote_item_totals=None,  # type: Optional[NegotiableQuoteItemTotals]
    ):
        self.negotiable_quote_item_totals = negotiable_quote_item_totals
        super().__init__(_)


class NegotiableQuoteItemTotals(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-negotiable-quote-item-totals-interface
    
    Extension attribute for quote item totals model.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cost=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        catalog_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_catalog_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        catalog_price_incl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_catalog_price_incl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        cart_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_cart_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        cart_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_cart_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        cart_price_incl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_cart_price_incl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.cost = cost
        self.catalog_price = catalog_price
        self.base_catalog_price = base_catalog_price
        self.catalog_price_incl_tax = catalog_price_incl_tax
        self.base_catalog_price_incl_tax = base_catalog_price_incl_tax
        self.cart_price = cart_price
        self.base_cart_price = base_cart_price
        self.cart_tax = cart_tax
        self.base_cart_tax = base_cart_tax
        self.cart_price_incl_tax = cart_price_incl_tax
        self.base_cart_price_incl_tax = base_cart_price_incl_tax
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteTotalSegment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-total-segment-interface
    
    Interface TotalsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        title=None,  # type: Optional[str]
        value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        area=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[QuoteTotalSegmentExtension]
    ):
        self.code = code
        self.title = title
        self.value = value
        self.area = area
        self.extension_attributes = extension_attributes
        super().__init__(_)


class QuoteTotalSegmentExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-total-segment-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\TotalSegmentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        tax_grandtotal_details=None,  # type: Optional[Sequence[TaxGrandTotalDetails]]
        gift_cards=None,  # type: Optional[str]
        gw_order_id=None,  # type: Optional[str]
        gw_item_ids=None,  # type: Optional[Sequence[str]]
        gw_allow_gift_receipt=None,  # type: Optional[str]
        gw_add_card=None,  # type: Optional[str]
        gw_price=None,  # type: Optional[str]
        gw_base_price=None,  # type: Optional[str]
        gw_items_price=None,  # type: Optional[str]
        gw_items_base_price=None,  # type: Optional[str]
        gw_card_price=None,  # type: Optional[str]
        gw_card_base_price=None,  # type: Optional[str]
        gw_base_tax_amount=None,  # type: Optional[str]
        gw_tax_amount=None,  # type: Optional[str]
        gw_items_base_tax_amount=None,  # type: Optional[str]
        gw_items_tax_amount=None,  # type: Optional[str]
        gw_card_base_tax_amount=None,  # type: Optional[str]
        gw_card_tax_amount=None,  # type: Optional[str]
        gw_price_incl_tax=None,  # type: Optional[str]
        gw_base_price_incl_tax=None,  # type: Optional[str]
        gw_card_price_incl_tax=None,  # type: Optional[str]
        gw_card_base_price_incl_tax=None,  # type: Optional[str]
        gw_items_price_incl_tax=None,  # type: Optional[str]
        gw_items_base_price_incl_tax=None,  # type: Optional[str]
    ):
        self.tax_grandtotal_details = tax_grandtotal_details
        self.gift_cards = gift_cards
        self.gw_order_id = gw_order_id
        self.gw_item_ids = gw_item_ids
        self.gw_allow_gift_receipt = gw_allow_gift_receipt
        self.gw_add_card = gw_add_card
        self.gw_price = gw_price
        self.gw_base_price = gw_base_price
        self.gw_items_price = gw_items_price
        self.gw_items_base_price = gw_items_base_price
        self.gw_card_price = gw_card_price
        self.gw_card_base_price = gw_card_base_price
        self.gw_base_tax_amount = gw_base_tax_amount
        self.gw_tax_amount = gw_tax_amount
        self.gw_items_base_tax_amount = gw_items_base_tax_amount
        self.gw_items_tax_amount = gw_items_tax_amount
        self.gw_card_base_tax_amount = gw_card_base_tax_amount
        self.gw_card_tax_amount = gw_card_tax_amount
        self.gw_price_incl_tax = gw_price_incl_tax
        self.gw_base_price_incl_tax = gw_base_price_incl_tax
        self.gw_card_price_incl_tax = gw_card_price_incl_tax
        self.gw_card_base_price_incl_tax = gw_card_base_price_incl_tax
        self.gw_items_price_incl_tax = gw_items_price_incl_tax
        self.gw_items_base_price_incl_tax = gw_items_base_price_incl_tax
        super().__init__(_)


class TaxGrandTotalDetails(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/tax-data-grand-total-details-interface
    
    Interface GrandTotalDetailsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        rates=None,  # type: Optional[Union[Sequence[TaxGrandTotalRates], serial.properties.Null]]
        group_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.amount = amount
        self.rates = rates
        self.group_id = group_id
        super().__init__(_)


class TaxGrandTotalRates(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-grand-total-rates-interface
    
    Interface GrandTotalRatesInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        percent=None,  # type: Optional[Union[str, serial.properties.Null]]
        title=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.percent = percent
        self.title = title
        super().__init__(_)


class QuoteTotalsExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-totals-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\TotalsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        coupon_label=None,  # type: Optional[str]
        base_customer_balance_amount=None,  # type: Optional[numbers.Number]
        customer_balance_amount=None,  # type: Optional[numbers.Number]
        negotiable_quote_totals=None,  # type: Optional[NegotiableQuoteTotals]
        reward_points_balance=None,  # type: Optional[numbers.Number]
        reward_currency_amount=None,  # type: Optional[numbers.Number]
        base_reward_currency_amount=None,  # type: Optional[numbers.Number]
    ):
        self.coupon_label = coupon_label
        self.base_customer_balance_amount = base_customer_balance_amount
        self.customer_balance_amount = customer_balance_amount
        self.negotiable_quote_totals = negotiable_quote_totals
        self.reward_points_balance = reward_points_balance
        self.reward_currency_amount = reward_currency_amount
        self.base_reward_currency_amount = base_reward_currency_amount
        super().__init__(_)


class NegotiableQuoteTotals(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-negotiable-quote-totals-interface
    
    Extension attribute for quote totals model.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items_count=None,  # type: Optional[Union[int, serial.properties.Null]]
        quote_status=None,  # type: Optional[Union[str, serial.properties.Null]]
        created_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        updated_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        customer_group=None,  # type: Optional[Union[int, serial.properties.Null]]
        base_to_quote_rate=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        cost_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_cost_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        original_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_original_total=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        original_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_original_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        original_price_incl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_original_price_incl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        negotiated_price_type=None,  # type: Optional[Union[int, serial.properties.Null]]
        negotiated_price_value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.items_count = items_count
        self.quote_status = quote_status
        self.created_at = created_at
        self.updated_at = updated_at
        self.customer_group = customer_group
        self.base_to_quote_rate = base_to_quote_rate
        self.cost_total = cost_total
        self.base_cost_total = base_cost_total
        self.original_total = original_total
        self.base_original_total = base_original_total
        self.original_tax = original_tax
        self.base_original_tax = base_original_tax
        self.original_price_incl_tax = original_price_incl_tax
        self.base_original_price_incl_tax = base_original_price_incl_tax
        self.negotiated_price_type = negotiated_price_type
        self.negotiated_price_value = negotiated_price_value
        super().__init__(_)


class PostRequisitionLists(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1requisition_lists/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        requisition_list=None,  # type: Optional[Union[RequisitionList, serial.properties.Null]]
    ):
        self.requisition_list = requisition_list
        super().__init__(_)


class RequisitionList(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/requisition-list-data-requisition-list-interface
    
    Interface RequisitionListInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        customer_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        updated_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        description=None,  # type: Optional[Union[str, serial.properties.Null]]
        items=None,  # type: Optional[Union[Sequence[RequisitionListItem], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.customer_id = customer_id
        self.name = name
        self.updated_at = updated_at
        self.description = description
        self.items = items
        self.extension_attributes = extension_attributes
        super().__init__(_)


class RequisitionListItem(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/requisition-list-data-requisition-list-item-interface
    
    Interface RequisitionListItemInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        sku=None,  # type: Optional[Union[int, serial.properties.Null]]
        requisition_list_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        options=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        added_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.sku = sku
        self.requisition_list_id = requisition_list_id
        self.qty = qty
        self.options = options
        self.store_id = store_id
        self.added_at = added_at
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostInvoicesComments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1invoices~1comments/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesInvoiceComment, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class PostCreditMemoRefund(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1creditmemo~1refund/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        creditmemo=None,  # type: Optional[Union[SalesCreditMemo, serial.properties.Null]]
        offline_requested=None,  # type: Optional[bool]
    ):
        self.creditmemo = creditmemo
        self.offline_requested = offline_requested
        super().__init__(_)


class SalesRuleSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-rule-data-rule-search-result-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesRule], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class TaxClassSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/tax-data-tax-class-search-results-interface
    
    Interface for tax class search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[TaxClass], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PutCompanyRole(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1company~1role~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        role=None,  # type: Optional[Union[CompanyRole, serial.properties.Null]]
    ):
        self.role = role
        super().__init__(_)


class StoreConfig(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-store-config-interface
    
    StoreConfig interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        website_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        locale=None,  # type: Optional[Union[str, serial.properties.Null]]
        base_currency_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        default_display_currency_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        timezone=None,  # type: Optional[Union[str, serial.properties.Null]]
        weight_unit=None,  # type: Optional[Union[str, serial.properties.Null]]
        base_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        base_link_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        base_static_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        base_media_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        secure_base_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        secure_base_link_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        secure_base_static_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        secure_base_media_url=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.code = code
        self.website_id = website_id
        self.locale = locale
        self.base_currency_code = base_currency_code
        self.default_display_currency_code = default_display_currency_code
        self.timezone = timezone
        self.weight_unit = weight_unit
        self.base_url = base_url
        self.base_link_url = base_link_url
        self.base_static_url = base_static_url
        self.base_media_url = base_media_url
        self.secure_base_url = secure_base_url
        self.secure_base_link_url = secure_base_link_url
        self.secure_base_static_url = secure_base_static_url
        self.secure_base_media_url = secure_base_media_url
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CurrencyInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/directory-data-currency-information-interface
    
    Currency Information interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        base_currency_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        base_currency_symbol=None,  # type: Optional[Union[str, serial.properties.Null]]
        default_display_currency_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        default_display_currency_symbol=None,  # type: Optional[Union[str, serial.properties.Null]]
        available_currency_codes=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        exchange_rates=None,  # type: Optional[Union[Sequence[ExchangeRate], serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.base_currency_code = base_currency_code
        self.base_currency_symbol = base_currency_symbol
        self.default_display_currency_code = default_display_currency_code
        self.default_display_currency_symbol = default_display_currency_symbol
        self.available_currency_codes = available_currency_codes
        self.exchange_rates = exchange_rates
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ExchangeRate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/directory-data-exchange-rate-interface
    
    Exchange Rate interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        currency_to=None,  # type: Optional[Union[str, serial.properties.Null]]
        rate=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.currency_to = currency_to
        self.rate = rate
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostEAVAttributeSets(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1eav~1attribute-sets/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity_type_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        attribute_set=None,  # type: Optional[Union[EAVAttributeSet, serial.properties.Null]]
        skeleton_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.entity_type_code = entity_type_code
        self.attribute_set = attribute_set
        self.skeleton_id = skeleton_id
        super().__init__(_)


class EAVAttributeSet(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/eav-data-attribute-set-interface
    
    Interface AttributeSetInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_set_id=None,  # type: Optional[int]
        attribute_set_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
        entity_type_id=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.attribute_set_id = attribute_set_id
        self.attribute_set_name = attribute_set_name
        self.sort_order = sort_order
        self.entity_type_id = entity_type_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutCustomersPassword(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1password/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        template=None,  # type: Optional[Union[str, serial.properties.Null]]
        website_id=None,  # type: Optional[int]
    ):
        self.email = email
        self.template = template
        self.website_id = website_id
        super().__init__(_)


class PutCustomersValidate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1validate/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer=None,  # type: Optional[Union[Customer, serial.properties.Null]]
    ):
        self.customer = customer
        super().__init__(_)


class CustomerValidationResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/customer-data-validation-results-interface
    
    Validation results interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        valid=None,  # type: Optional[Union[bool, serial.properties.Null]]
        messages=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.valid = valid
        self.messages = messages
        super().__init__(_)


class PutOrders(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1orders~1{parent_id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesOrderAddress, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class PutCoupons(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1coupons~1{couponId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        coupon=None,  # type: Optional[Union[SalesRuleCoupon, serial.properties.Null]]
    ):
        self.coupon = coupon
        super().__init__(_)


class PutSharedCatalog(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shared_catalog=None,  # type: Optional[Union[SharedCatalog, serial.properties.Null]]
    ):
        self.shared_catalog = shared_catalog
        super().__init__(_)


class CountryInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/directory-data-country-information-interface
    
    Country Information interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[str, serial.properties.Null]]
        two_letter_abbreviation=None,  # type: Optional[Union[str, serial.properties.Null]]
        three_letter_abbreviation=None,  # type: Optional[Union[str, serial.properties.Null]]
        full_name_locale=None,  # type: Optional[Union[str, serial.properties.Null]]
        full_name_english=None,  # type: Optional[Union[str, serial.properties.Null]]
        available_regions=None,  # type: Optional[Sequence[RegionInformation]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.two_letter_abbreviation = two_letter_abbreviation
        self.three_letter_abbreviation = three_letter_abbreviation
        self.full_name_locale = full_name_locale
        self.full_name_english = full_name_english
        self.available_regions = available_regions
        self.extension_attributes = extension_attributes
        super().__init__(_)


class RegionInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/directory-data-region-information-interface
    
    Region Information interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[Union[str, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.code = code
        self.name = name
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutCustomerGroups(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customerGroups~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        group=None,  # type: Optional[Union[CustomerGroup, serial.properties.Null]]
    ):
        self.group = group
        super().__init__(_)


class ProductAttributeSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-attribute-search-results-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[ProductAttribute], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class ProductAttribute(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-attribute-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        is_wysiwyg_enabled=None,  # type: Optional[bool]
        is_html_allowed_on_front=None,  # type: Optional[bool]
        used_for_sort_by=None,  # type: Optional[bool]
        is_filterable=None,  # type: Optional[bool]
        is_filterable_in_search=None,  # type: Optional[bool]
        is_used_in_grid=None,  # type: Optional[bool]
        is_visible_in_grid=None,  # type: Optional[bool]
        is_filterable_in_grid=None,  # type: Optional[bool]
        position=None,  # type: Optional[int]
        apply_to=None,  # type: Optional[Sequence[str]]
        is_searchable=None,  # type: Optional[str]
        is_visible_in_advanced_search=None,  # type: Optional[str]
        is_comparable=None,  # type: Optional[str]
        is_used_for_promo_rules=None,  # type: Optional[str]
        is_visible_on_front=None,  # type: Optional[str]
        used_in_product_listing=None,  # type: Optional[str]
        is_visible=None,  # type: Optional[bool]
        scope=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
        attribute_id=None,  # type: Optional[int]
        attribute_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        frontend_input=None,  # type: Optional[Union[str, serial.properties.Null]]
        entity_type_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_required=None,  # type: Optional[Union[bool, serial.properties.Null]]
        options=None,  # type: Optional[Sequence[EAVAttributeOption]]
        is_user_defined=None,  # type: Optional[bool]
        default_frontend_label=None,  # type: Optional[str]
        frontend_labels=None,  # type: Optional[Union[Sequence[EAVAttributeFrontendLabel], serial.properties.Null]]
        note=None,  # type: Optional[str]
        backend_type=None,  # type: Optional[str]
        backend_model=None,  # type: Optional[str]
        source_model=None,  # type: Optional[str]
        default_value=None,  # type: Optional[str]
        is_unique=None,  # type: Optional[str]
        frontend_class=None,  # type: Optional[str]
        validation_rules=None,  # type: Optional[Sequence[EAVAttributeValidationRule]]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.is_wysiwyg_enabled = is_wysiwyg_enabled
        self.is_html_allowed_on_front = is_html_allowed_on_front
        self.used_for_sort_by = used_for_sort_by
        self.is_filterable = is_filterable
        self.is_filterable_in_search = is_filterable_in_search
        self.is_used_in_grid = is_used_in_grid
        self.is_visible_in_grid = is_visible_in_grid
        self.is_filterable_in_grid = is_filterable_in_grid
        self.position = position
        self.apply_to = apply_to
        self.is_searchable = is_searchable
        self.is_visible_in_advanced_search = is_visible_in_advanced_search
        self.is_comparable = is_comparable
        self.is_used_for_promo_rules = is_used_for_promo_rules
        self.is_visible_on_front = is_visible_on_front
        self.used_in_product_listing = used_in_product_listing
        self.is_visible = is_visible
        self.scope = scope
        self.extension_attributes = extension_attributes
        self.attribute_id = attribute_id
        self.attribute_code = attribute_code
        self.frontend_input = frontend_input
        self.entity_type_id = entity_type_id
        self.is_required = is_required
        self.options = options
        self.is_user_defined = is_user_defined
        self.default_frontend_label = default_frontend_label
        self.frontend_labels = frontend_labels
        self.note = note
        self.backend_type = backend_type
        self.backend_model = backend_model
        self.source_model = source_model
        self.default_value = default_value
        self.is_unique = is_unique
        self.frontend_class = frontend_class
        self.validation_rules = validation_rules
        self.custom_attributes = custom_attributes
        super().__init__(_)


class EAVAttributeOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/eav-data-attribute-option-interface
    
    Created from:
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        value=None,  # type: Optional[Union[str, serial.properties.Null]]
        sort_order=None,  # type: Optional[int]
        is_default=None,  # type: Optional[bool]
        store_labels=None,  # type: Optional[Sequence[EAVAttributeOptionLabel]]
    ):
        self.label = label
        self.value = value
        self.sort_order = sort_order
        self.is_default = is_default
        self.store_labels = store_labels
        super().__init__(_)


class EAVAttributeOptionLabel(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/eav-data-attribute-option-label-interface
    
    Interface AttributeOptionLabelInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        store_id=None,  # type: Optional[int]
        label=None,  # type: Optional[str]
    ):
        self.store_id = store_id
        self.label = label
        super().__init__(_)


class EAVAttributeFrontendLabel(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/eav-data-attribute-frontend-label-interface
    
    Interface AttributeFrontendLabelInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        store_id=None,  # type: Optional[int]
        label=None,  # type: Optional[str]
    ):
        self.store_id = store_id
        self.label = label
        super().__init__(_)


class EAVAttributeValidationRule(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/eav-data-attribute-validation-rule-interface
    
    Interface AttributeValidationRuleInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        key=None,  # type: Optional[Union[str, serial.properties.Null]]
        value=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.key = key
        self.value = value
        super().__init__(_)


class PostProductsAttributes(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attributes/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute=None,  # type: Optional[Union[ProductAttribute, serial.properties.Null]]
    ):
        self.attribute = attribute
        super().__init__(_)


class PutSalesRules(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1salesRules~1{ruleId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        rule=None,  # type: Optional[Union[SalesRule, serial.properties.Null]]
    ):
        self.rule = rule
        super().__init__(_)


class PostCouponsDeleteByIds(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1coupons~1deleteByIds/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        ids=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
        ignore_invalid_coupons=None,  # type: Optional[bool]
    ):
        self.ids = ids
        self.ignore_invalid_coupons = ignore_invalid_coupons
        super().__init__(_)


class SalesRuleCouponMassDeleteResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-rule-data-coupon-mass-delete-result-interface
    
    Coupon mass delete results interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        failed_items=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        missing_items=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.failed_items = failed_items
        self.missing_items = missing_items
        super().__init__(_)


class PutCompany(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1company~1{companyId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        company=None,  # type: Optional[Union[Company, serial.properties.Null]]
    ):
        self.company = company
        super().__init__(_)


class PutHierarchyMove(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1hierarchy~1move~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        new_parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.new_parent_id = new_parent_id
        super().__init__(_)


class PutCompanyAssignRoles(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1company~1assignRoles/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        user_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        roles=None,  # type: Optional[Union[Sequence[CompanyRole], serial.properties.Null]]
    ):
        self.user_id = user_id
        self.roles = roles
        super().__init__(_)


class PutCompanyCredits(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1companyCredits~1{id}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        credit_limit=None,  # type: Optional[Union[CompanyCreditLimit, serial.properties.Null]]
    ):
        self.credit_limit = credit_limit
        super().__init__(_)


class CompanyCreditLimit(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-credit-data-credit-limit-interface
    
    Credit Limit data transfer object interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        company_id=None,  # type: Optional[int]
        credit_limit=None,  # type: Optional[numbers.Number]
        balance=None,  # type: Optional[numbers.Number]
        currency_code=None,  # type: Optional[str]
        exceed_limit=None,  # type: Optional[Union[bool, serial.properties.Null]]
        available_limit=None,  # type: Optional[numbers.Number]
        credit_comment=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.id_ = id_
        self.company_id = company_id
        self.credit_limit = credit_limit
        self.balance = balance
        self.currency_code = currency_code
        self.exceed_limit = exceed_limit
        self.available_limit = available_limit
        self.credit_comment = credit_comment
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostProductsMedia(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1media/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entry=None,  # type: Optional[Union[ProductAttributeMediaGalleryEntry, serial.properties.Null]]
    ):
        self.entry = entry
        super().__init__(_)


class PutProductsTierPrices(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1tier-prices/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        prices=None,  # type: Optional[Union[Sequence[TierPrice], serial.properties.Null]]
    ):
        self.prices = prices
        super().__init__(_)


class TierPrice(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-tier-price-interface
    
    Tier price interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        price_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        website_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        customer_group=None,  # type: Optional[Union[str, serial.properties.Null]]
        quantity=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.price = price
        self.price_type = price_type
        self.website_id = website_id
        self.sku = sku
        self.customer_group = customer_group
        self.quantity = quantity
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostProductsTierPrices(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1tier-prices/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        prices=None,  # type: Optional[Union[Sequence[TierPrice], serial.properties.Null]]
    ):
        self.prices = prices
        super().__init__(_)


class PostProductsBasePrices(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1base-prices/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        prices=None,  # type: Optional[Union[Sequence[BasePrice], serial.properties.Null]]
    ):
        self.prices = prices
        super().__init__(_)


class BasePrice(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-base-price-interface
    
    Price interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.price = price
        self.store_id = store_id
        self.sku = sku
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostProductsCostDelete(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1cost-delete/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        skus=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.skus = skus
        super().__init__(_)


class ProductLinkType(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-link-type-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        code=None,  # type: Optional[Union[int, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.code = code
        self.name = name
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutProductsLinks(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1links/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[ProductLink, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class PostProductsLinks(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1links/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[ProductLink], serial.properties.Null]]
    ):
        self.items = items
        super().__init__(_)


class ProductRenderSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-search-results-interface
    
    Dto that holds render information about products
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[ProductRender], serial.properties.Null]]
    ):
        self.items = items
        super().__init__(_)


class ProductRender(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-interface
    
    Represents Data Object which holds enough information to render product This information is put into part as Add To 
    Cart or Add to Compare Data or Price Data
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        add_to_cart_button=None,  # type: Optional[Union[ProductRenderButton, serial.properties.Null]]
        add_to_compare_button=None,  # type: Optional[Union[ProductRenderButton, serial.properties.Null]]
        price_info=None,  # type: Optional[Union[ProductRenderPriceInfo, serial.properties.Null]]
        images=None,  # type: Optional[Union[Sequence[ProductRenderImage], serial.properties.Null]]
        url=None,  # type: Optional[Union[str, serial.properties.Null]]
        id_=None,  # type: Optional[Union[int, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        type_=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_salable=None,  # type: Optional[Union[str, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        currency_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[Union[ProductRenderExtension, serial.properties.Null]]
    ):
        self.add_to_cart_button = add_to_cart_button
        self.add_to_compare_button = add_to_compare_button
        self.price_info = price_info
        self.images = images
        self.url = url
        self.id_ = id_
        self.name = name
        self.type_ = type_
        self.is_salable = is_salable
        self.store_id = store_id
        self.currency_code = currency_code
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductRenderButton(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-button-interface
    
    Button interface. This interface represents all manner of product buttons: add to cart, add to compare, etc... The 
    buttons describes by this interface should have interaction with backend
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        post_data=None,  # type: Optional[Union[str, serial.properties.Null]]
        url=None,  # type: Optional[Union[str, serial.properties.Null]]
        required_options=None,  # type: Optional[Union[bool, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.post_data = post_data
        self.url = url
        self.required_options = required_options
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductRenderPriceInfo(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-price-info-interface
    
    Price interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        final_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        max_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        max_regular_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        minimal_regular_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        special_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        minimal_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        regular_price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        formatted_prices=None,  # type: Optional[Union[ProductRenderFormattedPriceInfo, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[ProductRenderPriceInfoExtension]
    ):
        self.final_price = final_price
        self.max_price = max_price
        self.max_regular_price = max_regular_price
        self.minimal_regular_price = minimal_regular_price
        self.special_price = special_price
        self.minimal_price = minimal_price
        self.regular_price = regular_price
        self.formatted_prices = formatted_prices
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductRenderFormattedPriceInfo(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-formatted-price-info-interface
    
    Formatted Price interface. Aggregate formatted html with price representations. E.g.: <span class="price">$9.00</
    span> Consider currency, rounding and html
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        final_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        max_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        minimal_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        max_regular_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        minimal_regular_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        special_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        regular_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.final_price = final_price
        self.max_price = max_price
        self.minimal_price = minimal_price
        self.max_regular_price = max_regular_price
        self.minimal_regular_price = minimal_regular_price
        self.special_price = special_price
        self.regular_price = regular_price
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductRenderPriceInfoExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-price-info-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\ProductRender\PriceInfoInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        msrp=None,  # type: Optional[MsrpProductRenderPriceInfo]
        tax_adjustments=None,  # type: Optional[ProductRenderPriceInfo]
        weee_attributes=None,  # type: Optional[Sequence[WeeeProductRenderAdjustmentAttribute]]
        weee_adjustment=None,  # type: Optional[str]
    ):
        self.msrp = msrp
        self.tax_adjustments = tax_adjustments
        self.weee_attributes = weee_attributes
        self.weee_adjustment = weee_adjustment
        super().__init__(_)


class MsrpProductRenderPriceInfo(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/msrp-data-product-render-msrp-price-info-interface
    
    Price interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        msrp_price=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_applicable=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_shown_price_on_gesture=None,  # type: Optional[Union[str, serial.properties.Null]]
        msrp_message=None,  # type: Optional[Union[str, serial.properties.Null]]
        explanation_message=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.msrp_price = msrp_price
        self.is_applicable = is_applicable
        self.is_shown_price_on_gesture = is_shown_price_on_gesture
        self.msrp_message = msrp_message
        self.explanation_message = explanation_message
        self.extension_attributes = extension_attributes
        super().__init__(_)


class WeeeProductRenderAdjustmentAttribute(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/weee-data-product-render-weee-adjustment-attribute-interface
    
    List of all weee attributes, their amounts, etc.., that product has
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        amount=None,  # type: Optional[Union[str, serial.properties.Null]]
        tax_amount=None,  # type: Optional[Union[str, serial.properties.Null]]
        tax_amount_incl_tax=None,  # type: Optional[Union[str, serial.properties.Null]]
        amount_excl_tax=None,  # type: Optional[Union[str, serial.properties.Null]]
        attribute_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[Union[dict, serial.properties.Null]]
    ):
        self.amount = amount
        self.tax_amount = tax_amount
        self.tax_amount_incl_tax = tax_amount_incl_tax
        self.amount_excl_tax = amount_excl_tax
        self.attribute_code = attribute_code
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductRenderImage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-image-interface
    
    Product Render image interface. Represents physical characteristics of image, that can be used in product listing or
    product view
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        url=None,  # type: Optional[Union[str, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        height=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        width=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        resized_width=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        resized_height=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.url = url
        self.code = code
        self.height = height
        self.width = width
        self.label = label
        self.resized_width = resized_width
        self.resized_height = resized_height
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductRenderExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-render-extension-interface
    
    ExtensionInterface class for @see \Magento\Catalog\Api\Data\ProductRenderInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        wishlist_button=None,  # type: Optional[ProductRenderButton]
        review_html=None,  # type: Optional[str]
    ):
        self.wishlist_button = wishlist_button
        self.review_html = review_html
        super().__init__(_)


class InventoryStockStatusCollection(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-inventory-data-stock-status-collection-interface
    
    Stock Status collection interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[InventoryStockStatus], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[InventoryStockStatusCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class InventoryStockStatus(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-inventory-data-stock-status-interface
    
    Interface StockStatusInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        stock_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[int, serial.properties.Null]]
        stock_status=None,  # type: Optional[Union[int, serial.properties.Null]]
        stock_item=None,  # type: Optional[Union[InventoryStockItem, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.product_id = product_id
        self.stock_id = stock_id
        self.qty = qty
        self.stock_status = stock_status
        self.stock_item = stock_item
        self.extension_attributes = extension_attributes
        super().__init__(_)


class InventoryStockStatusCriteria(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-inventory-stock-status-criteria-interface
    
    Interface StockStatusCriteriaInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        mapper_interface_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        criteria_list=None,  # type: Optional[Union[Sequence[Criteria], serial.properties.Null]]
        filters=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        orders=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        limit=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.mapper_interface_name = mapper_interface_name
        self.criteria_list = criteria_list
        self.filters = filters
        self.orders = orders
        self.limit = limit
        super().__init__(_)


class Criteria(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-criteria-interface
    
    Interface CriteriaInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        mapper_interface_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        criteria_list=None,  # type: Optional[Union[Sequence[Criteria], serial.properties.Null]]
        filters=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        orders=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        limit=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.mapper_interface_name = mapper_interface_name
        self.criteria_list = criteria_list
        self.filters = filters
        self.orders = orders
        self.limit = limit
        super().__init__(_)


class PutCartsOrder(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1order/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[QuotePayment]
    ):
        self.payment_method = payment_method
        super().__init__(_)


class PutGuestCarts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.customer_id = customer_id
        self.store_id = store_id
        super().__init__(_)


class SalesOrderStatusHistorySearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-order-status-history-search-result-interface
    
    Order status history search result interface. An order is a document that a web store issues to a customer. Magento 
    generates a sales order that lists the product items, billing and shipping addresses, and shipping and payment 
    methods. A corresponding external document, known as a purchase order, is emailed to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesOrderStatusHistory], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostOrdersComments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1orders~1{id}~1comments/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        status_history=None,  # type: Optional[Union[SalesOrderStatusHistory, serial.properties.Null]]
    ):
        self.status_history = status_history
        super().__init__(_)


class PostOrderShip(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1order~1{orderId}~1ship/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Sequence[SalesShipmentItemCreation]]
        notify=None,  # type: Optional[bool]
        append_comment=None,  # type: Optional[bool]
        comment=None,  # type: Optional[SalesShipmentCommentCreation]
        tracks=None,  # type: Optional[Sequence[SalesShipmentTrackCreation]]
        packages=None,  # type: Optional[Sequence[SalesShipmentPackageCreation]]
        arguments=None,  # type: Optional[SalesShipmentCreationArguments]
    ):
        self.items = items
        self.notify = notify
        self.append_comment = append_comment
        self.comment = comment
        self.tracks = tracks
        self.packages = packages
        self.arguments = arguments
        super().__init__(_)


class SalesShipmentItemCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-item-creation-interface
    
    Input argument for shipment item creation Interface ShipmentItemCreationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
        order_item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.extension_attributes = extension_attributes
        self.order_item_id = order_item_id
        self.qty = qty
        super().__init__(_)


class SalesShipmentCommentCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-comment-creation-interface
    
    Interface ShipmentCommentCreationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_visible_on_front=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.extension_attributes = extension_attributes
        self.comment = comment
        self.is_visible_on_front = is_visible_on_front
        super().__init__(_)


class SalesShipmentTrackCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-track-creation-interface
    
    Shipment Track Creation interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
        track_number=None,  # type: Optional[Union[str, serial.properties.Null]]
        title=None,  # type: Optional[Union[str, serial.properties.Null]]
        carrier_code=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.extension_attributes = extension_attributes
        self.track_number = track_number
        self.title = title
        self.carrier_code = carrier_code
        super().__init__(_)


class SalesShipmentPackageCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-package-creation-interface
    
    Shipment package interface. A shipment is a delivery package that contains products. A shipment document accompanies
    the shipment. This document lists the products and their quantities in the delivery package.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesShipmentCreationArguments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-creation-arguments-interface
    
    Interface for creation arguments for Shipment.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutTaxClasses(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1taxClasses~1{classId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        tax_class=None,  # type: Optional[Union[TaxClass, serial.properties.Null]]
    ):
        self.tax_class = tax_class
        super().__init__(_)


class PostCartsMineGiftCards(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1giftCards/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_card_account_data=None,  # type: Optional[Union[GiftCardAccountInterface, serial.properties.Null]]
    ):
        self.gift_card_account_data = gift_card_account_data
        super().__init__(_)


class GiftCardAccountInterface(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/gift-card-account-data-gift-card-account-interface
    
    Gift Card Account data
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_cards=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        gift_cards_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_gift_cards_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        gift_cards_amount_used=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_gift_cards_amount_used=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.gift_cards = gift_cards
        self.gift_cards_amount = gift_cards_amount
        self.base_gift_cards_amount = base_gift_cards_amount
        self.gift_cards_amount_used = gift_cards_amount_used
        self.base_gift_cards_amount_used = base_gift_cards_amount_used
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CustomerGroupSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/customer-data-group-search-results-interface
    
    Interface for customer groups search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CustomerGroup], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PutMeActivate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1me~1activate/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        confirmation_key=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.confirmation_key = confirmation_key
        super().__init__(_)


class PutMePassword(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1me~1password/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        current_password=None,  # type: Optional[Union[str, serial.properties.Null]]
        new_password=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.current_password = current_password
        self.new_password = new_password
        super().__init__(_)


class CategoryAttributeSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-category-attribute-search-results-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CategoryAttribute], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class CategoryAttribute(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-category-attribute-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        is_wysiwyg_enabled=None,  # type: Optional[bool]
        is_html_allowed_on_front=None,  # type: Optional[bool]
        used_for_sort_by=None,  # type: Optional[bool]
        is_filterable=None,  # type: Optional[bool]
        is_filterable_in_search=None,  # type: Optional[bool]
        is_used_in_grid=None,  # type: Optional[bool]
        is_visible_in_grid=None,  # type: Optional[bool]
        is_filterable_in_grid=None,  # type: Optional[bool]
        position=None,  # type: Optional[int]
        apply_to=None,  # type: Optional[Sequence[str]]
        is_searchable=None,  # type: Optional[str]
        is_visible_in_advanced_search=None,  # type: Optional[str]
        is_comparable=None,  # type: Optional[str]
        is_used_for_promo_rules=None,  # type: Optional[str]
        is_visible_on_front=None,  # type: Optional[str]
        used_in_product_listing=None,  # type: Optional[str]
        is_visible=None,  # type: Optional[bool]
        scope=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
        attribute_id=None,  # type: Optional[int]
        attribute_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        frontend_input=None,  # type: Optional[Union[str, serial.properties.Null]]
        entity_type_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_required=None,  # type: Optional[Union[bool, serial.properties.Null]]
        options=None,  # type: Optional[Sequence[EAVAttributeOption]]
        is_user_defined=None,  # type: Optional[bool]
        default_frontend_label=None,  # type: Optional[str]
        frontend_labels=None,  # type: Optional[Union[Sequence[EAVAttributeFrontendLabel], serial.properties.Null]]
        note=None,  # type: Optional[str]
        backend_type=None,  # type: Optional[str]
        backend_model=None,  # type: Optional[str]
        source_model=None,  # type: Optional[str]
        default_value=None,  # type: Optional[str]
        is_unique=None,  # type: Optional[str]
        frontend_class=None,  # type: Optional[str]
        validation_rules=None,  # type: Optional[Sequence[EAVAttributeValidationRule]]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.is_wysiwyg_enabled = is_wysiwyg_enabled
        self.is_html_allowed_on_front = is_html_allowed_on_front
        self.used_for_sort_by = used_for_sort_by
        self.is_filterable = is_filterable
        self.is_filterable_in_search = is_filterable_in_search
        self.is_used_in_grid = is_used_in_grid
        self.is_visible_in_grid = is_visible_in_grid
        self.is_filterable_in_grid = is_filterable_in_grid
        self.position = position
        self.apply_to = apply_to
        self.is_searchable = is_searchable
        self.is_visible_in_advanced_search = is_visible_in_advanced_search
        self.is_comparable = is_comparable
        self.is_used_for_promo_rules = is_used_for_promo_rules
        self.is_visible_on_front = is_visible_on_front
        self.used_in_product_listing = used_in_product_listing
        self.is_visible = is_visible
        self.scope = scope
        self.extension_attributes = extension_attributes
        self.attribute_id = attribute_id
        self.attribute_code = attribute_code
        self.frontend_input = frontend_input
        self.entity_type_id = entity_type_id
        self.is_required = is_required
        self.options = options
        self.is_user_defined = is_user_defined
        self.default_frontend_label = default_frontend_label
        self.frontend_labels = frontend_labels
        self.note = note
        self.backend_type = backend_type
        self.backend_model = backend_model
        self.source_model = source_model
        self.default_value = default_value
        self.is_unique = is_unique
        self.frontend_class = frontend_class
        self.validation_rules = validation_rules
        self.custom_attributes = custom_attributes
        super().__init__(_)


class PostCartsItems(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{quoteId}~1items/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cart_item=None,  # type: Optional[Union[QuoteCartItem, serial.properties.Null]]
    ):
        self.cart_item = cart_item
        super().__init__(_)


class PostCouponsDeleteByCodes(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1coupons~1deleteByCodes/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        codes=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
        ignore_invalid_coupons=None,  # type: Optional[bool]
    ):
        self.codes = codes
        self.ignore_invalid_coupons = ignore_invalid_coupons
        super().__init__(_)


class RMACommentSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/rma-data-comment-search-result-interface
    
    Interface CommentSearchResultInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[RMAComment], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostReturnsComments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1returns~1{id}~1comments/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        data=None,  # type: Optional[Union[RMAComment, serial.properties.Null]]
    ):
        self.data = data
        super().__init__(_)


class PutCustomers(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1{customerId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer=None,  # type: Optional[Union[Customer, serial.properties.Null]]
        password_hash=None,  # type: Optional[str]
    ):
        self.customer = customer
        self.password_hash = password_hash
        super().__init__(_)


class PostProductsSpecialPrice(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1special-price/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        prices=None,  # type: Optional[Union[Sequence[SpecialPrice], serial.properties.Null]]
    ):
        self.prices = prices
        super().__init__(_)


class SpecialPrice(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-special-price-interface
    
    Product Special Price Interface is used to encapsulate data that can be processed by efficient price API.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        price=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        store_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        price_from=None,  # type: Optional[Union[str, serial.properties.Null]]
        price_to=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.price = price
        self.store_id = store_id
        self.sku = sku
        self.price_from = price_from
        self.price_to = price_to
        self.extension_attributes = extension_attributes
        super().__init__(_)


class ProductCustomOptionType(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-custom-option-type-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        group=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.label = label
        self.code = code
        self.group = group
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesInvoiceCommentSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-invoice-comment-search-result-interface
    
    Invoice comment search result interface. An invoice is a record of the receipt of payment for an order. An invoice 
    can include comments that detail the invoice history.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesInvoiceComment], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostOrderRefund(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1order~1{orderId}~1refund/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Sequence[SalesCreditMemoItemCreation]]
        notify=None,  # type: Optional[bool]
        append_comment=None,  # type: Optional[bool]
        comment=None,  # type: Optional[SalesCreditMemoCommentCreation]
        arguments=None,  # type: Optional[SalesCreditMemoCreationArguments]
    ):
        self.items = items
        self.notify = notify
        self.append_comment = append_comment
        self.comment = comment
        self.arguments = arguments
        super().__init__(_)


class SalesCreditMemoItemCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-item-creation-interface
    
    Interface CreditmemoItemCreationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
        order_item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.extension_attributes = extension_attributes
        self.order_item_id = order_item_id
        self.qty = qty
        super().__init__(_)


class SalesCreditMemoCommentCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-comment-creation-interface
    
    Interface CreditmemoCommentCreationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_visible_on_front=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.extension_attributes = extension_attributes
        self.comment = comment
        self.is_visible_on_front = is_visible_on_front
        super().__init__(_)


class SalesCreditMemoCreationArguments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-creation-arguments-interface
    
    Interface CreditmemoCreationArgumentsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shipping_amount=None,  # type: Optional[numbers.Number]
        adjustment_positive=None,  # type: Optional[numbers.Number]
        adjustment_negative=None,  # type: Optional[numbers.Number]
        extension_attributes=None,  # type: Optional[SalesCreditMemoCreationArgumentsExtension]
    ):
        self.shipping_amount = shipping_amount
        self.adjustment_positive = adjustment_positive
        self.adjustment_negative = adjustment_negative
        self.extension_attributes = extension_attributes
        super().__init__(_)


class SalesCreditMemoCreationArgumentsExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-creation-arguments-extension-interface
    
    ExtensionInterface class for @see \Magento\Sales\Api\Data\CreditmemoCreationArgumentsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        return_to_stock_items=None,  # type: Optional[Sequence[int]]
    ):
        self.return_to_stock_items = return_to_stock_items
        super().__init__(_)


class SalesShipmentCommentSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-shipment-comment-search-result-interface
    
    Shipment comment search result interface. A shipment is a delivery package that contains products. A shipment 
    document accompanies the shipment. This document lists the products and their quantities in the delivery package. A 
    shipment document can contain comments.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesShipmentComment], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostShipmentComments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1shipment~1{id}~1comments/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesShipmentComment, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class CompanyCreditHistorySearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-credit-data-history-search-results-interface
    
    Interface for History search results.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[CompanyCreditHistory], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class CompanyCreditHistory(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-credit-data-history-data-interface
    
    History data transfer object interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        id_=None,  # type: Optional[int]
        company_credit_id=None,  # type: Optional[int]
        user_id=None,  # type: Optional[int]
        user_type=None,  # type: Optional[int]
        currency_credit=None,  # type: Optional[str]
        currency_operation=None,  # type: Optional[str]
        rate=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        rate_credit=None,  # type: Optional[numbers.Number]
        amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        balance=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        credit_limit=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        available_limit=None,  # type: Optional[numbers.Number]
        type_=None,  # type: Optional[int]
        datetime=None,  # type: Optional[str]
        purchase_order=None,  # type: Optional[str]
        comment=None,  # type: Optional[str]
    ):
        self.id_ = id_
        self.company_credit_id = company_credit_id
        self.user_id = user_id
        self.user_type = user_type
        self.currency_credit = currency_credit
        self.currency_operation = currency_operation
        self.rate = rate
        self.rate_credit = rate_credit
        self.amount = amount
        self.balance = balance
        self.credit_limit = credit_limit
        self.available_limit = available_limit
        self.type_ = type_
        self.datetime = datetime
        self.purchase_order = purchase_order
        self.comment = comment
        super().__init__(_)


class EAVAttributeSetSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/eav-data-attribute-set-search-results-interface
    
    Interface AttributeSetSearchResultsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[EAVAttributeSet], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostCustomersResetPassword(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1resetPassword/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        reset_token=None,  # type: Optional[Union[str, serial.properties.Null]]
        new_password=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.email = email
        self.reset_token = reset_token
        self.new_password = new_password
        super().__init__(_)


class PostProductsAttributeSets(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attribute-sets/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_set=None,  # type: Optional[Union[EAVAttributeSet, serial.properties.Null]]
        skeleton_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.attribute_set = attribute_set
        self.skeleton_id = skeleton_id
        super().__init__(_)


class PutProductsWebsites(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1websites/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product_website_link=None,  # type: Optional[Union[ProductWebsiteLink, serial.properties.Null]]
    ):
        self.product_website_link = product_website_link
        super().__init__(_)


class ProductWebsiteLink(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-website-link-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        sku=None,  # type: Optional[Union[str, serial.properties.Null]]
        website_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.sku = sku
        self.website_id = website_id
        super().__init__(_)


class PostProductsWebsites(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1websites/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product_website_link=None,  # type: Optional[Union[ProductWebsiteLink, serial.properties.Null]]
    ):
        self.product_website_link = product_website_link
        super().__init__(_)


class PostOrderInvoice(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1order~1{orderId}~1invoice/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        capture=None,  # type: Optional[bool]
        items=None,  # type: Optional[Sequence[SalesInvoiceItemCreation]]
        notify=None,  # type: Optional[bool]
        append_comment=None,  # type: Optional[bool]
        comment=None,  # type: Optional[SalesInvoiceCommentCreation]
        arguments=None,  # type: Optional[SalesInvoiceCreationArguments]
    ):
        self.capture = capture
        self.items = items
        self.notify = notify
        self.append_comment = append_comment
        self.comment = comment
        self.arguments = arguments
        super().__init__(_)


class SalesInvoiceItemCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-invoice-item-creation-interface
    
    Input argument for invoice creation Interface InvoiceItemCreationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
        order_item_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        qty=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.extension_attributes = extension_attributes
        self.order_item_id = order_item_id
        self.qty = qty
        super().__init__(_)


class SalesInvoiceCommentCreation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-invoice-comment-creation-interface
    
    Interface InvoiceCommentCreationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_visible_on_front=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.extension_attributes = extension_attributes
        self.comment = comment
        self.is_visible_on_front = is_visible_on_front
        super().__init__(_)


class SalesInvoiceCreationArguments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-invoice-creation-arguments-interface
    
    Interface for creation arguments for Invoice.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostAdminToken(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1integration~1admin~1token/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        username=None,  # type: Optional[Union[str, serial.properties.Null]]
        password=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.username = username
        self.password = password
        super().__init__(_)


class PostNegotiableQuoteRequest(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiableQuote~1request/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        quote_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        quote_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        comment=None,  # type: Optional[str]
        files=None,  # type: Optional[Sequence[NegotiableQuoteAttachment]]
    ):
        self.quote_id = quote_id
        self.quote_name = quote_name
        self.comment = comment
        self.files = files
        super().__init__(_)


class NegotiableQuoteAttachment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-attachment-content-interface
    
    Attachment files content interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        base_64_encoded_data=None,  # type: Optional[Union[str, serial.properties.Null]]
        type_=None,  # type: Optional[Union[str, serial.properties.Null]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.base_64_encoded_data = base_64_encoded_data
        self.type_ = type_
        self.name = name
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostNegotiableQuoteDecline(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiableQuote~1decline/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        quote_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        reason=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.quote_id = quote_id
        self.reason = reason
        super().__init__(_)


class PostCartsMineGiftMessage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1gift-message/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message=None,  # type: Optional[Union[GiftMessage, serial.properties.Null]]
    ):
        self.gift_message = gift_message
        super().__init__(_)


class SalesCreditMemoCommentSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/sales-data-creditmemo-comment-search-result-interface
    
    Credit memo comment search result interface. After a customer places and pays for an order and an invoice has been 
    issued, the merchant can create a credit memo to refund all or part of the amount paid for any returned or 
    undelivered items. The memo restores funds to the customer account so that the customer can make future purchases. A
    credit memo usually includes comments that detail why the credit memo amount was credited to the customer.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[SalesCreditMemoComment], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostCreditMemoComments(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1creditmemo~1{id}~1comments/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity=None,  # type: Optional[Union[SalesCreditMemoComment, serial.properties.Null]]
    ):
        self.entity = entity
        super().__init__(_)


class PutCartsGiftCards(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1giftCards/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_card_account_data=None,  # type: Optional[Union[GiftCardAccountInterface, serial.properties.Null]]
    ):
        self.gift_card_account_data = gift_card_account_data
        super().__init__(_)


class CustomerAttributeMetadata(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/customer-data-attribute-metadata-interface
    
    Customer attribute metadata interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        frontend_input=None,  # type: Optional[Union[str, serial.properties.Null]]
        input_filter=None,  # type: Optional[Union[str, serial.properties.Null]]
        store_label=None,  # type: Optional[Union[str, serial.properties.Null]]
        validation_rules=None,  # type: Optional[Union[Sequence[CustomerValidationRule], serial.properties.Null]]
        multiline_count=None,  # type: Optional[Union[int, serial.properties.Null]]
        visible=None,  # type: Optional[Union[bool, serial.properties.Null]]
        required=None,  # type: Optional[Union[bool, serial.properties.Null]]
        data_model=None,  # type: Optional[Union[str, serial.properties.Null]]
        options=None,  # type: Optional[Union[Sequence[CustomerOption], serial.properties.Null]]
        frontend_class=None,  # type: Optional[Union[str, serial.properties.Null]]
        user_defined=None,  # type: Optional[Union[bool, serial.properties.Null]]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
        frontend_label=None,  # type: Optional[Union[str, serial.properties.Null]]
        note=None,  # type: Optional[Union[str, serial.properties.Null]]
        system=None,  # type: Optional[Union[bool, serial.properties.Null]]
        backend_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        is_used_in_grid=None,  # type: Optional[bool]
        is_visible_in_grid=None,  # type: Optional[bool]
        is_filterable_in_grid=None,  # type: Optional[bool]
        is_searchable_in_grid=None,  # type: Optional[bool]
        attribute_code=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.frontend_input = frontend_input
        self.input_filter = input_filter
        self.store_label = store_label
        self.validation_rules = validation_rules
        self.multiline_count = multiline_count
        self.visible = visible
        self.required = required
        self.data_model = data_model
        self.options = options
        self.frontend_class = frontend_class
        self.user_defined = user_defined
        self.sort_order = sort_order
        self.frontend_label = frontend_label
        self.note = note
        self.system = system
        self.backend_type = backend_type
        self.is_used_in_grid = is_used_in_grid
        self.is_visible_in_grid = is_visible_in_grid
        self.is_filterable_in_grid = is_filterable_in_grid
        self.is_searchable_in_grid = is_searchable_in_grid
        self.attribute_code = attribute_code
        super().__init__(_)


class CustomerValidationRule(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/customer-data-validation-rule-interface
    
    Validation rule interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        name=None,  # type: Optional[Union[str, serial.properties.Null]]
        value=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.name = name
        self.value = value
        super().__init__(_)


class CustomerOption(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-option-interface
    
    Option interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        value=None,  # type: Optional[str]
        options=None,  # type: Optional[Sequence[CustomerOption]]
    ):
        self.label = label
        self.value = value
        self.options = options
        super().__init__(_)


class ProductAttributeType(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-attribute-type-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        value=None,  # type: Optional[Union[str, serial.properties.Null]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.value = value
        self.label = label
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostProductsCostInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1cost-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        skus=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.skus = skus
        super().__init__(_)


class PutCartsMineItems(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1items~1{itemId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cart_item=None,  # type: Optional[Union[QuoteCartItem, serial.properties.Null]]
    ):
        self.cart_item = cart_item
        super().__init__(_)


class PutCartsMineCollectTotals(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1collect-totals/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        shipping_carrier_code=None,  # type: Optional[str]
        shipping_method_code=None,  # type: Optional[str]
        additional_data=None,  # type: Optional[QuoteTotalsAdditional]
    ):
        self.payment_method = payment_method
        self.shipping_carrier_code = shipping_carrier_code
        self.shipping_method_code = shipping_method_code
        self.additional_data = additional_data
        super().__init__(_)


class QuoteTotalsAdditional(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-totals-additional-data-interface
    
    Additional data for totals collection.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        extension_attributes=None,  # type: Optional[QuoteTotalsAdditionalExtension]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class QuoteTotalsAdditionalExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/quote-data-totals-additional-data-extension-interface
    
    ExtensionInterface class for @see \Magento\Quote\Api\Data\TotalsAdditionalDataInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_messages=None,  # type: Optional[Sequence[GiftMessage]]
    ):
        self.gift_messages = gift_messages
        super().__init__(_)


class PutNegotiableQuote(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiableQuote~1{quoteId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        quote=None,  # type: Optional[Union[QuoteCart, serial.properties.Null]]
    ):
        self.quote = quote
        super().__init__(_)


class PutCustomersActivate(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1{email}~1activate/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        confirmation_key=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.confirmation_key = confirmation_key
        super().__init__(_)


class PostCustomersIsEmailAvailable(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1customers~1isEmailAvailable/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        customer_email=None,  # type: Optional[Union[str, serial.properties.Null]]
        website_id=None,  # type: Optional[int]
    ):
        self.customer_email = customer_email
        self.website_id = website_id
        super().__init__(_)


class PutGuestCartsOrder(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1order/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[QuotePayment]
    ):
        self.payment_method = payment_method
        super().__init__(_)


class PostGuestCartsItems(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1items/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cart_item=None,  # type: Optional[Union[QuoteCartItem, serial.properties.Null]]
    ):
        self.cart_item = cart_item
        super().__init__(_)


class QuotePaymentMethod(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-payment-method-interface
    
    Interface PaymentMethodInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        title=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.code = code
        self.title = title
        super().__init__(_)


class PostCartsMineBillingAddress(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1billing-address/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
        use_for_shipping=None,  # type: Optional[bool]
    ):
        self.address = address
        self.use_for_shipping = use_for_shipping
        super().__init__(_)


class PostInvoiceRefund(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1invoice~1{invoiceId}~1refund/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Sequence[SalesCreditMemoItemCreation]]
        is_online=None,  # type: Optional[bool]
        notify=None,  # type: Optional[bool]
        append_comment=None,  # type: Optional[bool]
        comment=None,  # type: Optional[SalesCreditMemoCommentCreation]
        arguments=None,  # type: Optional[SalesCreditMemoCreationArguments]
    ):
        self.items = items
        self.is_online = is_online
        self.notify = notify
        self.append_comment = append_comment
        self.comment = comment
        self.arguments = arguments
        super().__init__(_)


class PostCustomerToken(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1integration~1customer~1token/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        username=None,  # type: Optional[Union[str, serial.properties.Null]]
        password=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.username = username
        self.password = password
        super().__init__(_)


class PostProductsTierPricesDelete(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1tier-prices-delete/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        prices=None,  # type: Optional[Union[Sequence[TierPrice], serial.properties.Null]]
    ):
        self.prices = prices
        super().__init__(_)


class PutProductsOptions(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1options~1{optionId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option=None,  # type: Optional[Union[ProductCustomOption, serial.properties.Null]]
    ):
        self.option = option
        super().__init__(_)


class PostBundleProductsOptionsAdd(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1bundle-products~1options~1add/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option=None,  # type: Optional[Union[BundleOptionInterface, serial.properties.Null]]
    ):
        self.option = option
        super().__init__(_)


class QuoteShippingMethod(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-shipping-method-interface
    
    Interface ShippingMethodInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        carrier_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        method_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        carrier_title=None,  # type: Optional[str]
        method_title=None,  # type: Optional[str]
        amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        base_amount=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        available=None,  # type: Optional[Union[bool, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        error_message=None,  # type: Optional[Union[str, serial.properties.Null]]
        price_excl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        price_incl_tax=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
    ):
        self.carrier_code = carrier_code
        self.method_code = method_code
        self.carrier_title = carrier_title
        self.method_title = method_title
        self.amount = amount
        self.base_amount = base_amount
        self.available = available
        self.extension_attributes = extension_attributes
        self.error_message = error_message
        self.price_excl_tax = price_excl_tax
        self.price_incl_tax = price_incl_tax
        super().__init__(_)


class PostCartsGiftMessage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1gift-message/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message=None,  # type: Optional[Union[GiftMessage, serial.properties.Null]]
    ):
        self.gift_message = gift_message
        super().__init__(_)


class PutGiftWrappings(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1gift-wrappings~1{wrappingId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        data=None,  # type: Optional[Union[GiftWrapping, serial.properties.Null]]
        store_id=None,  # type: Optional[int]
    ):
        self.data = data
        self.store_id = store_id
        super().__init__(_)


class PutCategoriesMove(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1categories~1{categoryId}~1move/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        after_id=None,  # type: Optional[int]
    ):
        self.parent_id = parent_id
        self.after_id = after_id
        super().__init__(_)


class PostProductsSpecialPriceDelete(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1special-price-delete/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        prices=None,  # type: Optional[Union[Sequence[SpecialPrice], serial.properties.Null]]
    ):
        self.prices = prices
        super().__init__(_)


class BundleOptionType(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-option-type-interface
    
    Interface OptionTypeInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        label=None,  # type: Optional[Union[str, serial.properties.Null]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.label = label
        self.code = code
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutCartsItems(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1items~1{itemId}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cart_item=None,  # type: Optional[Union[QuoteCartItem, serial.properties.Null]]
    ):
        self.cart_item = cart_item
        super().__init__(_)


class PostCartsMineTotalsInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1totals-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_information=None,  # type: Optional[Union[CheckoutTotalsInformation, serial.properties.Null]]
    ):
        self.address_information = address_information
        super().__init__(_)


class CheckoutTotalsInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/checkout-data-totals-information-interface
    
    Interface TotalsInformationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
        shipping_method_code=None,  # type: Optional[str]
        shipping_carrier_code=None,  # type: Optional[str]
        extension_attributes=None,  # type: Optional[dict]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.address = address
        self.shipping_method_code = shipping_method_code
        self.shipping_carrier_code = shipping_carrier_code
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class PostNegotiableQuotePricesUpdated(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiableQuote~1pricesUpdated/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        quote_ids=None,  # type: Optional[Union[Sequence[int], serial.properties.Null]]
    ):
        self.quote_ids = quote_ids
        super().__init__(_)


class RMATrackSearchResult(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/rma-data-track-search-result-interface
    
    Interface TrackSearchResultInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[RMATrack], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PostReturnsTrackingNumbers(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1returns~1{id}~1tracking-numbers/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        track=None,  # type: Optional[Union[RMATrack, serial.properties.Null]]
    ):
        self.track = track
        super().__init__(_)


class PostProductsAttributeSetsGroups(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attribute-sets~1groups/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        group=None,  # type: Optional[Union[EAVAttributeGroup, serial.properties.Null]]
    ):
        self.group = group
        super().__init__(_)


class EAVAttributeGroup(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/eav-data-attribute-group-interface
    
    Interface AttributeGroupInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_group_id=None,  # type: Optional[str]
        attribute_group_name=None,  # type: Optional[str]
        attribute_set_id=None,  # type: Optional[int]
        extension_attributes=None,  # type: Optional[EAVAttributeGroupExtension]
    ):
        self.attribute_group_id = attribute_group_id
        self.attribute_group_name = attribute_group_name
        self.attribute_set_id = attribute_set_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class EAVAttributeGroupExtension(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/eav-data-attribute-group-extension-interface
    
    ExtensionInterface class for @see \Magento\Eav\Api\Data\AttributeGroupInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_group_code=None,  # type: Optional[str]
        sort_order=None,  # type: Optional[str]
    ):
        self.attribute_group_code = attribute_group_code
        self.sort_order = sort_order
        super().__init__(_)


class PutProductsMedia(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1media~1{entryId}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entry=None,  # type: Optional[Union[ProductAttributeMediaGalleryEntry, serial.properties.Null]]
    ):
        self.entry = entry
        super().__init__(_)


class PostCartsBillingAddress(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1billing-address/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
        use_for_shipping=None,  # type: Optional[bool]
    ):
        self.address = address
        self.use_for_shipping = use_for_shipping
        super().__init__(_)


class CheckoutPaymentDetails(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/checkout-data-payment-details-interface
    
    Interface PaymentDetailsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_methods=None,  # type: Optional[Union[Sequence[QuotePaymentMethod], serial.properties.Null]]
        totals=None,  # type: Optional[Union[QuoteTotals, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.payment_methods = payment_methods
        self.totals = totals
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PostCartsMinePaymentInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1payment-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
    ):
        self.payment_method = payment_method
        self.billing_address = billing_address
        super().__init__(_)


class PostCartsMineShippingInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1shipping-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_information=None,  # type: Optional[Union[CheckoutShippingInformation, serial.properties.Null]]
    ):
        self.address_information = address_information
        super().__init__(_)


class CheckoutShippingInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/checkout-data-shipping-information-interface
    
    Interface ShippingInformationInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shipping_address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
        shipping_method_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        shipping_carrier_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        custom_attributes=None,  # type: Optional[Sequence[Attribute]]
    ):
        self.shipping_address = shipping_address
        self.billing_address = billing_address
        self.shipping_method_code = shipping_method_code
        self.shipping_carrier_code = shipping_carrier_code
        self.extension_attributes = extension_attributes
        self.custom_attributes = custom_attributes
        super().__init__(_)


class PutConfigurableProductsVariation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1configurable-products~1variation/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product=None,  # type: Optional[Union[Product, serial.properties.Null]]
        options=None,  # type: Optional[Union[Sequence[ConfigurableProductOption], serial.properties.Null]]
    ):
        self.product = product
        self.options = options
        super().__init__(_)


class Metadata(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-metadata-object-interface
    
    Provides metadata about an attribute.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_code=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.attribute_code = attribute_code
        super().__init__(_)


class PostProductsTierPricesInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1tier-prices-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        skus=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.skus = skus
        super().__init__(_)


class PostProductsBasePricesInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1base-prices-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        skus=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.skus = skus
        super().__init__(_)


class ProductLinkAttribute(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-product-link-attribute-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        code=None,  # type: Optional[Union[str, serial.properties.Null]]
        type_=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.code = code
        self.type_ = type_
        self.extension_attributes = extension_attributes
        super().__init__(_)


class CategoryProductLink(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/catalog-data-category-product-link-interface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        sku=None,  # type: Optional[str]
        position=None,  # type: Optional[int]
        category_id=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.sku = sku
        self.position = position
        self.category_id = category_id
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutCategoriesProducts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1categories~1{categoryId}~1products/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product_link=None,  # type: Optional[Union[CategoryProductLink, serial.properties.Null]]
    ):
        self.product_link = product_link
        super().__init__(_)


class PostCategoriesProducts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1categories~1{categoryId}~1products/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        product_link=None,  # type: Optional[Union[CategoryProductLink, serial.properties.Null]]
    ):
        self.product_link = product_link
        super().__init__(_)


class PutBundleProductsLinks(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1bundle-products~1{sku}~1links~1{id}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        linked_product=None,  # type: Optional[Union[BundleLink, serial.properties.Null]]
    ):
        self.linked_product = linked_product
        super().__init__(_)


class PostNegotiableQuoteSubmitToCustomer(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiableQuote~1submitToCustomer/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        quote_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        comment=None,  # type: Optional[str]
        files=None,  # type: Optional[Sequence[NegotiableQuoteAttachment]]
    ):
        self.quote_id = quote_id
        self.comment = comment
        self.files = files
        super().__init__(_)


class GiftMessageItemRepositorySavePost(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1gift-message~1{itemId}/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message=None,  # type: Optional[Union[GiftMessage, serial.properties.Null]]
    ):
        self.gift_message = gift_message
        super().__init__(_)


class PostCartsTotalsInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1totals-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_information=None,  # type: Optional[Union[CheckoutTotalsInformation, serial.properties.Null]]
    ):
        self.address_information = address_information
        super().__init__(_)


class PostConfigurableProductsChild(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1configurable-products~1{sku}~1child/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        child_sku=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.child_sku = child_sku
        super().__init__(_)


class PostProductsDownloadableLinks(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1downloadable-links/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        link=None,  # type: Optional[Union[DownloadableLink, serial.properties.Null]]
        is_global_scope_content=None,  # type: Optional[bool]
    ):
        self.link = link
        self.is_global_scope_content = is_global_scope_content
        super().__init__(_)


class PostAssignProducts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog~1{id}~1assignProducts/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        products=None,  # type: Optional[Union[Sequence[Product], serial.properties.Null]]
    ):
        self.products = products
        super().__init__(_)


class PostGuestCartsGiftMessage(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1gift-message/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message=None,  # type: Optional[Union[GiftMessage, serial.properties.Null]]
    ):
        self.gift_message = gift_message
        super().__init__(_)


class PostProductsAttributeSetsAttributes(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attribute-sets~1attributes/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_set_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        attribute_group_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        attribute_code=None,  # type: Optional[Union[str, serial.properties.Null]]
        sort_order=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.attribute_set_id = attribute_set_id
        self.attribute_group_id = attribute_group_id
        self.attribute_code = attribute_code
        self.sort_order = sort_order
        super().__init__(_)


class PostProductsSpecialPriceInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1special-price-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        skus=None,  # type: Optional[Union[Sequence[str], serial.properties.Null]]
    ):
        self.skus = skus
        super().__init__(_)


class PutBundleProductsOptions(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1bundle-products~1options~1{optionId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option=None,  # type: Optional[Union[BundleOptionInterface, serial.properties.Null]]
    ):
        self.option = option
        super().__init__(_)


class PutCartsMineSelectedPaymentMethod(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1selected-payment-method/put/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
    ):
        self.method = method
        super().__init__(_)


class PostCartsMineSetPaymentInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1set-payment-information/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
    ):
        self.payment_method = payment_method
        self.billing_address = billing_address
        super().__init__(_)


class NegotiableQuoteComment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-comment-interface
    
    Interface CommentInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        entity_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        parent_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        creator_type=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_decline=None,  # type: Optional[Union[int, serial.properties.Null]]
        is_draft=None,  # type: Optional[Union[int, serial.properties.Null]]
        creator_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        comment=None,  # type: Optional[Union[str, serial.properties.Null]]
        created_at=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
        attachments=None,  # type: Optional[Union[Sequence[NegotiableQuoteCommentAttachment], serial.properties.Null]]
    ):
        self.entity_id = entity_id
        self.parent_id = parent_id
        self.creator_type = creator_type
        self.is_decline = is_decline
        self.is_draft = is_draft
        self.creator_id = creator_id
        self.comment = comment
        self.created_at = created_at
        self.extension_attributes = extension_attributes
        self.attachments = attachments
        super().__init__(_)


class NegotiableQuoteCommentAttachment(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/negotiable-quote-data-comment-attachment-interface
    
    Interface for quote comment attachment.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attachment_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        comment_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        file_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        file_path=None,  # type: Optional[Union[str, serial.properties.Null]]
        file_type=None,  # type: Optional[Union[str, serial.properties.Null]]
        extension_attributes=None,  # type: Optional[dict]
    ):
        self.attachment_id = attachment_id
        self.comment_id = comment_id
        self.file_name = file_name
        self.file_path = file_path
        self.file_type = file_type
        self.extension_attributes = extension_attributes
        super().__init__(_)


class PutCompanyCreditsHistory(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1companyCredits~1history~1{historyId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        purchase_order=None,  # type: Optional[str]
        comment=None,  # type: Optional[str]
    ):
        self.purchase_order = purchase_order
        self.comment = comment
        super().__init__(_)


class PutEAVAttributeSets(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1eav~1attribute-sets~1{attributeSetId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_set=None,  # type: Optional[Union[EAVAttributeSet, serial.properties.Null]]
    ):
        self.attribute_set = attribute_set
        super().__init__(_)


class PutProductsAttributes(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attributes~1{attributeCode}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute=None,  # type: Optional[Union[ProductAttribute, serial.properties.Null]]
    ):
        self.attribute = attribute
        super().__init__(_)


class EAVAttributeGroupSearchResults(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/eav-data-attribute-group-search-results-interface
    
    Interface AttributeGroupSearchResultsInterface
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        items=None,  # type: Optional[Union[Sequence[EAVAttributeGroup], serial.properties.Null]]
        search_criteria=None,  # type: Optional[Union[SearchCriteria, serial.properties.Null]]
        total_count=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.items = items
        self.search_criteria = search_criteria
        self.total_count = total_count
        super().__init__(_)


class PutGuestCartsItems(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1items~1{itemId}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        cart_item=None,  # type: Optional[Union[QuoteCartItem, serial.properties.Null]]
    ):
        self.cart_item = cart_item
        super().__init__(_)


class PutGuestCartsCollectTotals(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1collect-totals/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        shipping_carrier_code=None,  # type: Optional[str]
        shipping_method_code=None,  # type: Optional[str]
        additional_data=None,  # type: Optional[QuoteTotalsAdditional]
    ):
        self.payment_method = payment_method
        self.shipping_carrier_code = shipping_carrier_code
        self.shipping_method_code = shipping_method_code
        self.additional_data = additional_data
        super().__init__(_)


class PostCartsShippingInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1shipping-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_information=None,  # type: Optional[Union[CheckoutShippingInformation, serial.properties.Null]]
    ):
        self.address_information = address_information
        super().__init__(_)


class PostConfigurableProductsOptions(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1configurable-products~1{sku}~1options/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option=None,  # type: Optional[Union[ConfigurableProductOption, serial.properties.Null]]
    ):
        self.option = option
        super().__init__(_)


class PostNegotiableCartsGiftCards(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiable-carts~1{cartId}~1giftCards/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_card_account_data=None,  # type: Optional[Union[GiftCardAccountInterface, serial.properties.Null]]
    ):
        self.gift_card_account_data = gift_card_account_data
        super().__init__(_)


class PostUnassignProducts(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog~1{id}~1unassignProducts/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        products=None,  # type: Optional[Union[Sequence[Product], serial.properties.Null]]
    ):
        self.products = products
        super().__init__(_)


class PostAssignCategories(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog~1{id}~1assignCategories/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        categories=None,  # type: Optional[Union[Sequence[Category], serial.properties.Null]]
    ):
        self.categories = categories
        super().__init__(_)


class PostCartsMineEstimateShippingMethods(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1estimate-shipping-methods/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
    ):
        self.address = address
        super().__init__(_)


class PostGuestCartsBillingAddress(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1billing-address/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
        use_for_shipping=None,  # type: Optional[bool]
    ):
        self.address = address
        self.use_for_shipping = use_for_shipping
        super().__init__(_)


class PostCartsGuestGiftCards(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1guest-carts~1{cartId}~1giftCards/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_card_account_data=None,  # type: Optional[Union[GiftCardAccountInterface, serial.properties.Null]]
    ):
        self.gift_card_account_data = gift_card_account_data
        super().__init__(_)


class GiftMessageItemRepositorySavePost1(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1gift-message~1{itemId}/post/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message=None,  # type: Optional[Union[GiftMessage, serial.properties.Null]]
    ):
        self.gift_message = gift_message
        super().__init__(_)


class PostUnassignCategories(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog~1{id}~1unassignCategories/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        categories=None,  # type: Optional[Union[Sequence[Category], serial.properties.Null]]
    ):
        self.categories = categories
        super().__init__(_)


class PostBundleProductsLinks(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1bundle-products~1{sku}~1links~1{optionId}/post/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        linked_product=None,  # type: Optional[Union[BundleLink, serial.properties.Null]]
    ):
        self.linked_product = linked_product
        super().__init__(_)


class PutCartsSelectedPaymentMethod(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1selected-payment-method/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
    ):
        self.method = method
        super().__init__(_)


class PutProductsDownloadableLinks(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1downloadable-links~1{id}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        link=None,  # type: Optional[Union[DownloadableLink, serial.properties.Null]]
        is_global_scope_content=None,  # type: Optional[bool]
    ):
        self.link = link
        self.is_global_scope_content = is_global_scope_content
        super().__init__(_)


class PostGuestCartsTotalsInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1totals-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_information=None,  # type: Optional[Union[CheckoutTotalsInformation, serial.properties.Null]]
    ):
        self.address_information = address_information
        super().__init__(_)


class PutProductsAttributeSets(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attribute-sets~1{attributeSetId}/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        attribute_set=None,  # type: Optional[Union[EAVAttributeSet, serial.properties.Null]]
    ):
        self.attribute_set = attribute_set
        super().__init__(_)


class PostCartsEstimateShippingMethods(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
    ):
        self.address = address
        super().__init__(_)


class PostGuestCartsPaymentInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1payment-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
    ):
        self.email = email
        self.payment_method = payment_method
        self.billing_address = billing_address
        super().__init__(_)


class PutConfigurableProductsOptions(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1configurable-products~1{sku}~1options~1{id}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option=None,  # type: Optional[Union[ConfigurableProductOption, serial.properties.Null]]
    ):
        self.option = option
        super().__init__(_)


class PutNegotiableQuoteShippingMethod(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiableQuote~1{quoteId}~1shippingMethod/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        shipping_method=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.shipping_method = shipping_method
        super().__init__(_)


class PutProductsStockItems(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{productSku}~1stockItems~1{itemId}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        stock_item=None,  # type: Optional[Union[InventoryStockItem, serial.properties.Null]]
    ):
        self.stock_item = stock_item
        super().__init__(_)


class PostGuestCartsShippingInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1shipping-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_information=None,  # type: Optional[Union[CheckoutShippingInformation, serial.properties.Null]]
    ):
        self.address_information = address_information
        super().__init__(_)


class PostProductsDownloadableLinksSamples(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1downloadable-links~1samples/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        sample=None,  # type: Optional[Union[DownloadableSample, serial.properties.Null]]
        is_global_scope_content=None,  # type: Optional[bool]
    ):
        self.sample = sample
        self.is_global_scope_content = is_global_scope_content
        super().__init__(_)


class PostNegotiableCartsBillingAddress(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiable-carts~1{cartId}~1billing-address/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
        use_for_shipping=None,  # type: Optional[bool]
    ):
        self.address = address
        self.use_for_shipping = use_for_shipping
        super().__init__(_)


class PostCompanyCreditsIncreaseBalance(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1companyCredits~1{creditId}~1increaseBalance/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        currency=None,  # type: Optional[Union[str, serial.properties.Null]]
        operation_type=None,  # type: Optional[Union[int, serial.properties.Null]]
        comment=None,  # type: Optional[str]
        options=None,  # type: Optional[CompanyCreditBalanceOptions]
    ):
        self.value = value
        self.currency = currency
        self.operation_type = operation_type
        self.comment = comment
        self.options = options
        super().__init__(_)


class CompanyCreditBalanceOptions(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/definitions/company-credit-data-credit-balance-options-interface
    
    Credit balance data transfer object interface.
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        purchase_order=None,  # type: Optional[Union[str, serial.properties.Null]]
        order_increment=None,  # type: Optional[Union[str, serial.properties.Null]]
        currency_display=None,  # type: Optional[Union[str, serial.properties.Null]]
        currency_base=None,  # type: Optional[Union[str, serial.properties.Null]]
    ):
        self.purchase_order = purchase_order
        self.order_increment = order_increment
        self.currency_display = currency_display
        self.currency_base = currency_base
        super().__init__(_)


class PostCompanyCreditsDecreaseBalance(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1companyCredits~1{creditId}~1decreaseBalance/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        value=None,  # type: Optional[Union[numbers.Number, serial.properties.Null]]
        currency=None,  # type: Optional[Union[str, serial.properties.Null]]
        operation_type=None,  # type: Optional[Union[int, serial.properties.Null]]
        comment=None,  # type: Optional[str]
        options=None,  # type: Optional[CompanyCreditBalanceOptions]
    ):
        self.value = value
        self.currency = currency
        self.operation_type = operation_type
        self.comment = comment
        self.options = options
        super().__init__(_)


class GiftMessageGuestItemRepositorySavePost(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1gift-message~1{itemId}/post/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        gift_message=None,  # type: Optional[Union[GiftMessage, serial.properties.Null]]
    ):
        self.gift_message = gift_message
        super().__init__(_)


class PostProductsAttributesOptions(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attributes~1{attributeCode}~1options/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        option=None,  # type: Optional[Union[EAVAttributeOption, serial.properties.Null]]
    ):
        self.option = option
        super().__init__(_)


class PostGiftRegistryMineEstimateShippingMethods(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1giftregistry~1mine~1estimate-shipping-methods/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        registry_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.registry_id = registry_id
        super().__init__(_)


class PutGuestCartsSelectedPaymentMethod(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1selected-payment-method/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
    ):
        self.method = method
        super().__init__(_)


class PostGuestCartsSetPaymentInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1set-payment-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
    ):
        self.email = email
        self.payment_method = payment_method
        self.billing_address = billing_address
        super().__init__(_)


class PostNegotiableCartsPaymentInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiable-carts~1{cartId}~1payment-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
    ):
        self.payment_method = payment_method
        self.billing_address = billing_address
        super().__init__(_)


class PostGuestCartsEstimateShippingMethods(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-carts~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
    ):
        self.address = address
        super().__init__(_)


class PutProductsDownloadableLinksSamples(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1{sku}~1downloadable-links~1samples~1{id}/put/parameters[2]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        sample=None,  # type: Optional[Union[DownloadableSample, serial.properties.Null]]
        is_global_scope_content=None,  # type: Optional[bool]
    ):
        self.sample = sample
        self.is_global_scope_content = is_global_scope_content
        super().__init__(_)


class PostNegotiableCartsShippingInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiable-carts~1{cartId}~1shipping-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_information=None,  # type: Optional[Union[CheckoutShippingInformation, serial.properties.Null]]
    ):
        self.address_information = address_information
        super().__init__(_)


class PutProductsAttributeSetsGroups(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1products~1attribute-sets~1{attributeSetId}~1groups/put/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        group=None,  # type: Optional[Union[EAVAttributeGroup, serial.properties.Null]]
    ):
        self.group = group
        super().__init__(_)


class PostAssignCompanies(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog~1{sharedCatalogId}~1assignCompanies/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        companies=None,  # type: Optional[Union[Sequence[Company], serial.properties.Null]]
    ):
        self.companies = companies
        super().__init__(_)


class PostNegotiableCartsSetPaymentInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiable-carts~1{cartId}~1set-payment-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
    ):
        self.payment_method = payment_method
        self.billing_address = billing_address
        super().__init__(_)


class PostUnassignCompanies(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1sharedCatalog~1{sharedCatalogId}~1unassignCompanies/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        companies=None,  # type: Optional[Union[Sequence[Company], serial.properties.Null]]
    ):
        self.companies = companies
        super().__init__(_)


class PostWorldpayGuestCartsPaymentInformation(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1worldpay-guest-carts~1{cartId}~1payment-information/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        email=None,  # type: Optional[Union[str, serial.properties.Null]]
        payment_method=None,  # type: Optional[Union[QuotePayment, serial.properties.Null]]
        billing_address=None,  # type: Optional[QuoteAddress]
    ):
        self.email = email
        self.payment_method = payment_method
        self.billing_address = billing_address
        super().__init__(_)


class PostCartsMineEstimateShippingMethodsByAddressID(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1mine~1estimate-shipping-methods-by-address-id/post/parameters[0]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.address_id = address_id
        super().__init__(_)


class PostNegotiableCartsEstimateShippingMethods(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiable-carts~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address=None,  # type: Optional[Union[QuoteAddress, serial.properties.Null]]
    ):
        self.address = address
        super().__init__(_)


class PostGuestGiftRegistryEstimateShippingMethods(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1guest-giftregistry~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        registry_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.registry_id = registry_id
        super().__init__(_)


class PostCartsEstimateShippingMethodsByAddressID(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1carts~1{cartId}~1estimate-shipping-methods-by-address-id/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.address_id = address_id
        super().__init__(_)


class PostNegotiableCartsEstimateShippingMethodsByAddressID(serial.model.Object):
    """
    https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
    #/paths/~1V1~1negotiable-carts~1{cartId}~1estimate-shipping-methods-by-address-id/post/parameters[1]/schema
    """

    def __init__(
        self,
        _=None,  # type: Optional[Union[str, bytes, dict, Sequence, IO]]
        address_id=None,  # type: Optional[Union[int, serial.properties.Null]]
    ):
        self.address_id = address_id
        super().__init__(_)


# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-data-team-search-results-interface
serial.meta.writable(CompanyTeamSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CompanyTeam,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-team-interface
serial.meta.writable(CompanyTeam).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('name', serial.properties.String()),
    ('description', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-attribute-interface
serial.meta.writable(Attribute).properties = [
    (
        'attribute_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        serial.properties.String(),
                        serial.properties.Array(
                            item_types=(
                                serial.properties.Property(
                                    types=(
                                        serial.properties.String(),
                                        serial.properties.Boolean(),
                                        serial.properties.Integer()
                                    ),
                                ),
                            ),
                        )
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-criteria-interface
serial.meta.writable(SearchCriteria).properties = [
    (
        'filter_groups',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SearchFilterGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_orders',
        serial.properties.Array(
            item_types=(
                SortOrder,
            ),
        )
    ),
    ('page_size', serial.properties.Integer()),
    ('current_page', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-filter-group
serial.meta.writable(SearchFilterGroup).properties = [
    (
        'filters',
        serial.properties.Array(
            item_types=(
                Filter,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-filter
serial.meta.writable(Filter).properties = [
    (
        'field',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('condition_type', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-sort-order
serial.meta.writable(SortOrder).properties = [
    (
        'field',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'direction',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/error-response
serial.meta.writable(ErrorResponse).properties = [
    (
        'message',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'errors',
        serial.properties.Array(
            item_types=(
                ErrorErrorsItem,
            ),
        )
    ),
    ('code', serial.properties.Integer()),
    (
        'parameters',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        serial.properties.Array(
                            item_types=(
                                ErrorParametersItem,
                            ),
                        ),
                        serial.properties.Array(
                            item_types=(
                                str,
                            ),
                        )
                    ),
                ),
                serial.properties.Property(
                    types=(
                        ErrorParametersItem,
                    ),
                )
            ),
        )
    ),
    ('trace', serial.properties.String()),
    ('messages', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/error-errors-item
serial.meta.writable(ErrorErrorsItem).properties = [
    ('message', serial.properties.String()),
    (
        'parameters',
        serial.properties.Array(
            item_types=(
                ErrorParametersItem,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/error-parameters-item
serial.meta.writable(ErrorParametersItem).properties = [
    ('resources', serial.properties.String()),
    (
        'field_name',
        serial.properties.String(
            name='fieldName',
        )
    ),
    (
        'field_value',
        serial.properties.String(
            name='fieldValue',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/framework-search-search-result-interface
serial.meta.writable(SearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SearchDocument,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'aggregations',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchAggregation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        FrameworkSearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-document-interface
serial.meta.writable(SearchDocument).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-aggregation-interface
serial.meta.writable(SearchAggregation).properties = [
    (
        'buckets',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SearchBucket,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'bucket_names',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-search-bucket-interface
serial.meta.writable(SearchBucket).properties = [
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'values',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SearchAggregationValue,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/framework-search-aggregation-value-interface
serial.meta.writable(SearchAggregationValue).properties = [
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'metrics',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/framework-search-search-criteria-interface
serial.meta.writable(FrameworkSearchCriteria).properties = [
    (
        'request_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'filter_groups',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SearchFilterGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_orders',
        serial.properties.Array(
            item_types=(
                SortOrder,
            ),
        )
    ),
    ('page_size', serial.properties.Integer()),
    ('current_page', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-order-search-result-interface
serial.meta.writable(SalesOrderSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesOrder,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-interface
serial.meta.writable(SalesOrder).properties = [
    ('adjustment_negative', serial.properties.Number()),
    ('adjustment_positive', serial.properties.Number()),
    ('applied_rule_ids', serial.properties.String()),
    ('base_adjustment_negative', serial.properties.Number()),
    ('base_adjustment_positive', serial.properties.Number()),
    ('base_currency_code', serial.properties.String()),
    ('base_discount_amount', serial.properties.Number()),
    ('base_discount_canceled', serial.properties.Number()),
    ('base_discount_invoiced', serial.properties.Number()),
    ('base_discount_refunded', serial.properties.Number()),
    (
        'base_grand_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('base_discount_tax_compensation_amount', serial.properties.Number()),
    ('base_discount_tax_compensation_invoiced', serial.properties.Number()),
    ('base_discount_tax_compensation_refunded', serial.properties.Number()),
    ('base_shipping_amount', serial.properties.Number()),
    ('base_shipping_canceled', serial.properties.Number()),
    ('base_shipping_discount_amount', serial.properties.Number()),
    ('base_shipping_discount_tax_compensation_amnt', serial.properties.Number()),
    ('base_shipping_incl_tax', serial.properties.Number()),
    ('base_shipping_invoiced', serial.properties.Number()),
    ('base_shipping_refunded', serial.properties.Number()),
    ('base_shipping_tax_amount', serial.properties.Number()),
    ('base_shipping_tax_refunded', serial.properties.Number()),
    ('base_subtotal', serial.properties.Number()),
    ('base_subtotal_canceled', serial.properties.Number()),
    ('base_subtotal_incl_tax', serial.properties.Number()),
    ('base_subtotal_invoiced', serial.properties.Number()),
    ('base_subtotal_refunded', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    ('base_tax_canceled', serial.properties.Number()),
    ('base_tax_invoiced', serial.properties.Number()),
    ('base_tax_refunded', serial.properties.Number()),
    ('base_total_canceled', serial.properties.Number()),
    ('base_total_due', serial.properties.Number()),
    ('base_total_invoiced', serial.properties.Number()),
    ('base_total_invoiced_cost', serial.properties.Number()),
    ('base_total_offline_refunded', serial.properties.Number()),
    ('base_total_online_refunded', serial.properties.Number()),
    ('base_total_paid', serial.properties.Number()),
    ('base_total_qty_ordered', serial.properties.Number()),
    ('base_total_refunded', serial.properties.Number()),
    ('base_to_global_rate', serial.properties.Number()),
    ('base_to_order_rate', serial.properties.Number()),
    ('billing_address_id', serial.properties.Integer()),
    ('can_ship_partially', serial.properties.Integer()),
    ('can_ship_partially_item', serial.properties.Integer()),
    ('coupon_code', serial.properties.String()),
    ('created_at', serial.properties.String()),
    ('customer_dob', serial.properties.String()),
    (
        'customer_email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('customer_firstname', serial.properties.String()),
    ('customer_gender', serial.properties.Integer()),
    ('customer_group_id', serial.properties.Integer()),
    ('customer_id', serial.properties.Integer()),
    ('customer_is_guest', serial.properties.Integer()),
    ('customer_lastname', serial.properties.String()),
    ('customer_middlename', serial.properties.String()),
    ('customer_note', serial.properties.String()),
    ('customer_note_notify', serial.properties.Integer()),
    ('customer_prefix', serial.properties.String()),
    ('customer_suffix', serial.properties.String()),
    ('customer_taxvat', serial.properties.String()),
    ('discount_amount', serial.properties.Number()),
    ('discount_canceled', serial.properties.Number()),
    ('discount_description', serial.properties.String()),
    ('discount_invoiced', serial.properties.Number()),
    ('discount_refunded', serial.properties.Number()),
    ('edit_increment', serial.properties.Integer()),
    ('email_sent', serial.properties.Integer()),
    ('entity_id', serial.properties.Integer()),
    ('ext_customer_id', serial.properties.String()),
    ('ext_order_id', serial.properties.String()),
    ('forced_shipment_with_invoice', serial.properties.Integer()),
    ('global_currency_code', serial.properties.String()),
    (
        'grand_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('discount_tax_compensation_amount', serial.properties.Number()),
    ('discount_tax_compensation_invoiced', serial.properties.Number()),
    ('discount_tax_compensation_refunded', serial.properties.Number()),
    ('hold_before_state', serial.properties.String()),
    ('hold_before_status', serial.properties.String()),
    ('increment_id', serial.properties.String()),
    ('is_virtual', serial.properties.Integer()),
    ('order_currency_code', serial.properties.String()),
    ('original_increment_id', serial.properties.String()),
    ('payment_authorization_amount', serial.properties.Number()),
    ('payment_auth_expiration', serial.properties.Integer()),
    ('protect_code', serial.properties.String()),
    ('quote_address_id', serial.properties.Integer()),
    ('quote_id', serial.properties.Integer()),
    ('relation_child_id', serial.properties.String()),
    ('relation_child_real_id', serial.properties.String()),
    ('relation_parent_id', serial.properties.String()),
    ('relation_parent_real_id', serial.properties.String()),
    ('remote_ip', serial.properties.String()),
    ('shipping_amount', serial.properties.Number()),
    ('shipping_canceled', serial.properties.Number()),
    ('shipping_description', serial.properties.String()),
    ('shipping_discount_amount', serial.properties.Number()),
    ('shipping_discount_tax_compensation_amount', serial.properties.Number()),
    ('shipping_incl_tax', serial.properties.Number()),
    ('shipping_invoiced', serial.properties.Number()),
    ('shipping_refunded', serial.properties.Number()),
    ('shipping_tax_amount', serial.properties.Number()),
    ('shipping_tax_refunded', serial.properties.Number()),
    ('state', serial.properties.String()),
    ('status', serial.properties.String()),
    ('store_currency_code', serial.properties.String()),
    ('store_id', serial.properties.Integer()),
    ('store_name', serial.properties.String()),
    ('store_to_base_rate', serial.properties.Number()),
    ('store_to_order_rate', serial.properties.Number()),
    ('subtotal', serial.properties.Number()),
    ('subtotal_canceled', serial.properties.Number()),
    ('subtotal_incl_tax', serial.properties.Number()),
    ('subtotal_invoiced', serial.properties.Number()),
    ('subtotal_refunded', serial.properties.Number()),
    ('tax_amount', serial.properties.Number()),
    ('tax_canceled', serial.properties.Number()),
    ('tax_invoiced', serial.properties.Number()),
    ('tax_refunded', serial.properties.Number()),
    ('total_canceled', serial.properties.Number()),
    ('total_due', serial.properties.Number()),
    ('total_invoiced', serial.properties.Number()),
    ('total_item_count', serial.properties.Integer()),
    ('total_offline_refunded', serial.properties.Number()),
    ('total_online_refunded', serial.properties.Number()),
    ('total_paid', serial.properties.Number()),
    ('total_qty_ordered', serial.properties.Number()),
    ('total_refunded', serial.properties.Number()),
    ('updated_at', serial.properties.String()),
    ('weight', serial.properties.Number()),
    ('x_forwarded_for', serial.properties.String()),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesOrderItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            types=(
                SalesOrderAddress,
            ),
        )
    ),
    (
        'payment',
        serial.properties.Property(
            types=(
                SalesOrderPayment,
            ),
        )
    ),
    (
        'status_histories',
        serial.properties.Array(
            item_types=(
                SalesOrderStatusHistory,
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                SalesOrderExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-item-interface
serial.meta.writable(SalesOrderItem).properties = [
    ('additional_data', serial.properties.String()),
    ('amount_refunded', serial.properties.Number()),
    ('applied_rule_ids', serial.properties.String()),
    ('base_amount_refunded', serial.properties.Number()),
    ('base_cost', serial.properties.Number()),
    ('base_discount_amount', serial.properties.Number()),
    ('base_discount_invoiced', serial.properties.Number()),
    ('base_discount_refunded', serial.properties.Number()),
    ('base_discount_tax_compensation_amount', serial.properties.Number()),
    ('base_discount_tax_compensation_invoiced', serial.properties.Number()),
    ('base_discount_tax_compensation_refunded', serial.properties.Number()),
    ('base_original_price', serial.properties.Number()),
    ('base_price', serial.properties.Number()),
    ('base_price_incl_tax', serial.properties.Number()),
    ('base_row_invoiced', serial.properties.Number()),
    ('base_row_total', serial.properties.Number()),
    ('base_row_total_incl_tax', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    ('base_tax_before_discount', serial.properties.Number()),
    ('base_tax_invoiced', serial.properties.Number()),
    ('base_tax_refunded', serial.properties.Number()),
    ('base_weee_tax_applied_amount', serial.properties.Number()),
    ('base_weee_tax_applied_row_amnt', serial.properties.Number()),
    ('base_weee_tax_disposition', serial.properties.Number()),
    ('base_weee_tax_row_disposition', serial.properties.Number()),
    ('created_at', serial.properties.String()),
    ('description', serial.properties.String()),
    ('discount_amount', serial.properties.Number()),
    ('discount_invoiced', serial.properties.Number()),
    ('discount_percent', serial.properties.Number()),
    ('discount_refunded', serial.properties.Number()),
    ('event_id', serial.properties.Integer()),
    ('ext_order_item_id', serial.properties.String()),
    ('free_shipping', serial.properties.Integer()),
    ('gw_base_price', serial.properties.Number()),
    ('gw_base_price_invoiced', serial.properties.Number()),
    ('gw_base_price_refunded', serial.properties.Number()),
    ('gw_base_tax_amount', serial.properties.Number()),
    ('gw_base_tax_amount_invoiced', serial.properties.Number()),
    ('gw_base_tax_amount_refunded', serial.properties.Number()),
    ('gw_id', serial.properties.Integer()),
    ('gw_price', serial.properties.Number()),
    ('gw_price_invoiced', serial.properties.Number()),
    ('gw_price_refunded', serial.properties.Number()),
    ('gw_tax_amount', serial.properties.Number()),
    ('gw_tax_amount_invoiced', serial.properties.Number()),
    ('gw_tax_amount_refunded', serial.properties.Number()),
    ('discount_tax_compensation_amount', serial.properties.Number()),
    ('discount_tax_compensation_canceled', serial.properties.Number()),
    ('discount_tax_compensation_invoiced', serial.properties.Number()),
    ('discount_tax_compensation_refunded', serial.properties.Number()),
    ('is_qty_decimal', serial.properties.Integer()),
    ('is_virtual', serial.properties.Integer()),
    ('item_id', serial.properties.Integer()),
    ('locked_do_invoice', serial.properties.Integer()),
    ('locked_do_ship', serial.properties.Integer()),
    ('name', serial.properties.String()),
    ('no_discount', serial.properties.Integer()),
    ('order_id', serial.properties.Integer()),
    ('original_price', serial.properties.Number()),
    ('parent_item_id', serial.properties.Integer()),
    ('price', serial.properties.Number()),
    ('price_incl_tax', serial.properties.Number()),
    ('product_id', serial.properties.Integer()),
    ('product_type', serial.properties.String()),
    ('qty_backordered', serial.properties.Number()),
    ('qty_canceled', serial.properties.Number()),
    ('qty_invoiced', serial.properties.Number()),
    ('qty_ordered', serial.properties.Number()),
    ('qty_refunded', serial.properties.Number()),
    ('qty_returned', serial.properties.Number()),
    ('qty_shipped', serial.properties.Number()),
    ('quote_item_id', serial.properties.Integer()),
    ('row_invoiced', serial.properties.Number()),
    ('row_total', serial.properties.Number()),
    ('row_total_incl_tax', serial.properties.Number()),
    ('row_weight', serial.properties.Number()),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('store_id', serial.properties.Integer()),
    ('tax_amount', serial.properties.Number()),
    ('tax_before_discount', serial.properties.Number()),
    ('tax_canceled', serial.properties.Number()),
    ('tax_invoiced', serial.properties.Number()),
    ('tax_percent', serial.properties.Number()),
    ('tax_refunded', serial.properties.Number()),
    ('updated_at', serial.properties.String()),
    ('weee_tax_applied', serial.properties.String()),
    ('weee_tax_applied_amount', serial.properties.Number()),
    ('weee_tax_applied_row_amount', serial.properties.Number()),
    ('weee_tax_disposition', serial.properties.Number()),
    ('weee_tax_row_disposition', serial.properties.Number()),
    ('weight', serial.properties.Number()),
    (
        'parent_item',
        serial.properties.Property(
            types=(
                SalesOrderItem,
            ),
        )
    ),
    (
        'product_option',
        serial.properties.Property(
            types=(
                ProductOption,
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                SalesOrderItemExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-option-interface
serial.meta.writable(ProductOption).properties = [
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                ProductOptionExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-option-extension-interface
serial.meta.writable(ProductOptionExtension).properties = [
    (
        'custom_options',
        serial.properties.Array(
            item_types=(
                CustomOption,
            ),
        )
    ),
    (
        'bundle_options',
        serial.properties.Array(
            item_types=(
                BundleOption,
            ),
        )
    ),
    (
        'configurable_item_options',
        serial.properties.Array(
            item_types=(
                ConfigurableProductItemOptionValue,
            ),
        )
    ),
    (
        'downloadable_option',
        serial.properties.Property(
            types=(
                DownloadableOption,
            ),
        )
    ),
    (
        'giftcard_item_option',
        serial.properties.Property(
            types=(
                GiftCardOption,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-custom-option-interface
serial.meta.writable(CustomOption).properties = [
    (
        'option_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'option_value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                CustomOptionExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-custom-option-extension-interface
serial.meta.writable(CustomOptionExtension).properties = [
    (
        'file_info',
        serial.properties.Property(
            types=(
                Image,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-data-image-content-interface
serial.meta.writable(Image).properties = [
    (
        'base_64_encoded_data',
        serial.properties.Property(
            name='base64_encoded_data',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'type_',
        serial.properties.Property(
            name='type',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-bundle-option-interface
serial.meta.writable(BundleOption).properties = [
    (
        'option_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'option_qty',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'option_selections',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/configurable-product-data-configurable-item-option-value-interface
serial.meta.writable(ConfigurableProductItemOptionValue).properties = [
    (
        'option_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('option_value', serial.properties.Integer()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/downloadable-data-downloadable-option-interface
serial.meta.writable(DownloadableOption).properties = [
    (
        'downloadable_links',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/gift-card-data-gift-card-option-interface
serial.meta.writable(GiftCardOption).properties = [
    (
        'giftcard_amount',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('custom_giftcard_amount', serial.properties.Number()),
    (
        'giftcard_sender_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'giftcard_recipient_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'giftcard_sender_email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'giftcard_recipient_email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('giftcard_message', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-order-item-extension-interface
serial.meta.writable(SalesOrderItemExtension).properties = [
    (
        'gift_message',
        serial.properties.Property(
            types=(
                GiftMessage,
            ),
        )
    ),
    ('gw_id', serial.properties.String()),
    ('gw_base_price', serial.properties.String()),
    ('gw_price', serial.properties.String()),
    ('gw_base_tax_amount', serial.properties.String()),
    ('gw_tax_amount', serial.properties.String()),
    ('gw_base_price_invoiced', serial.properties.String()),
    ('gw_price_invoiced', serial.properties.String()),
    ('gw_base_tax_amount_invoiced', serial.properties.String()),
    ('gw_tax_amount_invoiced', serial.properties.String()),
    ('gw_base_price_refunded', serial.properties.String()),
    ('gw_price_refunded', serial.properties.String()),
    ('gw_base_tax_amount_refunded', serial.properties.String()),
    ('gw_tax_amount_refunded', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/gift-message-data-message-interface
serial.meta.writable(GiftMessage).properties = [
    ('gift_message_id', serial.properties.Integer()),
    ('customer_id', serial.properties.Integer()),
    (
        'sender',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'recipient',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'message',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                GiftMessageExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/gift-message-data-message-extension-interface
serial.meta.writable(GiftMessageExtension).properties = [
    ('entity_id', serial.properties.String()),
    ('entity_type', serial.properties.String()),
    ('wrapping_id', serial.properties.Integer()),
    ('wrapping_allow_gift_receipt', serial.properties.Boolean()),
    ('wrapping_add_printed_card', serial.properties.Boolean())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-address-interface
serial.meta.writable(SalesOrderAddress).properties = [
    (
        'address_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'city',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('company', serial.properties.String()),
    (
        'country_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('customer_address_id', serial.properties.Integer()),
    ('customer_id', serial.properties.Integer()),
    ('email', serial.properties.String()),
    ('entity_id', serial.properties.Integer()),
    ('fax', serial.properties.String()),
    (
        'firstname',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'lastname',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('middlename', serial.properties.String()),
    ('parent_id', serial.properties.Integer()),
    (
        'postcode',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('prefix', serial.properties.String()),
    ('region', serial.properties.String()),
    ('region_code', serial.properties.String()),
    ('region_id', serial.properties.Integer()),
    (
        'street',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    ('suffix', serial.properties.String()),
    (
        'telephone',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('vat_id', serial.properties.String()),
    ('vat_is_valid', serial.properties.Integer()),
    ('vat_request_date', serial.properties.String()),
    ('vat_request_id', serial.properties.String()),
    ('vat_request_success', serial.properties.Integer()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-payment-interface
serial.meta.writable(SalesOrderPayment).properties = [
    (
        'account_status',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('additional_data', serial.properties.String()),
    (
        'additional_information',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('address_status', serial.properties.String()),
    ('amount_authorized', serial.properties.Number()),
    ('amount_canceled', serial.properties.Number()),
    ('amount_ordered', serial.properties.Number()),
    ('amount_paid', serial.properties.Number()),
    ('amount_refunded', serial.properties.Number()),
    ('anet_trans_method', serial.properties.String()),
    ('base_amount_authorized', serial.properties.Number()),
    ('base_amount_canceled', serial.properties.Number()),
    ('base_amount_ordered', serial.properties.Number()),
    ('base_amount_paid', serial.properties.Number()),
    ('base_amount_paid_online', serial.properties.Number()),
    ('base_amount_refunded', serial.properties.Number()),
    ('base_amount_refunded_online', serial.properties.Number()),
    ('base_shipping_amount', serial.properties.Number()),
    ('base_shipping_captured', serial.properties.Number()),
    ('base_shipping_refunded', serial.properties.Number()),
    ('cc_approval', serial.properties.String()),
    ('cc_avs_status', serial.properties.String()),
    ('cc_cid_status', serial.properties.String()),
    ('cc_debug_request_body', serial.properties.String()),
    ('cc_debug_response_body', serial.properties.String()),
    ('cc_debug_response_serialized', serial.properties.String()),
    ('cc_exp_month', serial.properties.String()),
    ('cc_exp_year', serial.properties.String()),
    (
        'cc_last_4',
        serial.properties.Property(
            name='cc_last4',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('cc_number_enc', serial.properties.String()),
    ('cc_owner', serial.properties.String()),
    ('cc_secure_verify', serial.properties.String()),
    ('cc_ss_issue', serial.properties.String()),
    ('cc_ss_start_month', serial.properties.String()),
    ('cc_ss_start_year', serial.properties.String()),
    ('cc_status', serial.properties.String()),
    ('cc_status_description', serial.properties.String()),
    ('cc_trans_id', serial.properties.String()),
    ('cc_type', serial.properties.String()),
    ('echeck_account_name', serial.properties.String()),
    ('echeck_account_type', serial.properties.String()),
    ('echeck_bank_name', serial.properties.String()),
    ('echeck_routing_number', serial.properties.String()),
    ('echeck_type', serial.properties.String()),
    ('entity_id', serial.properties.Integer()),
    ('last_trans_id', serial.properties.String()),
    (
        'method',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('parent_id', serial.properties.Integer()),
    ('po_number', serial.properties.String()),
    ('protection_eligibility', serial.properties.String()),
    ('quote_payment_id', serial.properties.Integer()),
    ('shipping_amount', serial.properties.Number()),
    ('shipping_captured', serial.properties.Number()),
    ('shipping_refunded', serial.properties.Number()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                SalesOrderPaymentExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-order-payment-extension-interface
serial.meta.writable(SalesOrderPaymentExtension).properties = [
    (
        'vault_payment_token',
        serial.properties.Property(
            types=(
                VaultPaymentToken,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/vault-data-payment-token-interface
serial.meta.writable(VaultPaymentToken).properties = [
    ('entity_id', serial.properties.Integer()),
    ('customer_id', serial.properties.Integer()),
    (
        'public_hash',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'payment_method_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'type_',
        serial.properties.Property(
            name='type',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    ('expires_at', serial.properties.String()),
    (
        'gateway_token',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'token_details',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_active',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-order-status-history-interface
serial.meta.writable(SalesOrderStatusHistory).properties = [
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    ('entity_id', serial.properties.Integer()),
    ('entity_name', serial.properties.String()),
    (
        'is_customer_notified',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'parent_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('status', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-order-extension-interface
serial.meta.writable(SalesOrderExtension).properties = [
    (
        'shipping_assignments',
        serial.properties.Array(
            item_types=(
                SalesShippingAssignment,
            ),
        )
    ),
    (
        'applied_taxes',
        serial.properties.Array(
            item_types=(
                TaxOrderDetailsApplied,
            ),
        )
    ),
    (
        'item_applied_taxes',
        serial.properties.Array(
            item_types=(
                TaxOrderDetailsItem,
            ),
        )
    ),
    ('converting_from_quote', serial.properties.Boolean()),
    (
        'company_order_attributes',
        serial.properties.Property(
            types=(
                CompanyOrder,
            ),
        )
    ),
    ('base_customer_balance_amount', serial.properties.Number()),
    ('customer_balance_amount', serial.properties.Number()),
    ('base_customer_balance_invoiced', serial.properties.Number()),
    ('customer_balance_invoiced', serial.properties.Number()),
    ('base_customer_balance_refunded', serial.properties.Number()),
    ('customer_balance_refunded', serial.properties.Number()),
    ('base_customer_balance_total_refunded', serial.properties.Number()),
    ('customer_balance_total_refunded', serial.properties.Number()),
    (
        'gift_cards',
        serial.properties.Array(
            item_types=(
                GiftCardAccount,
            ),
        )
    ),
    ('base_gift_cards_amount', serial.properties.Number()),
    ('gift_cards_amount', serial.properties.Number()),
    ('base_gift_cards_invoiced', serial.properties.Number()),
    ('gift_cards_invoiced', serial.properties.Number()),
    ('base_gift_cards_refunded', serial.properties.Number()),
    ('gift_cards_refunded', serial.properties.Number()),
    (
        'gift_message',
        serial.properties.Property(
            types=(
                GiftMessage,
            ),
        )
    ),
    ('gw_id', serial.properties.String()),
    ('gw_allow_gift_receipt', serial.properties.String()),
    ('gw_add_card', serial.properties.String()),
    ('gw_base_price', serial.properties.String()),
    ('gw_price', serial.properties.String()),
    ('gw_items_base_price', serial.properties.String()),
    ('gw_items_price', serial.properties.String()),
    ('gw_card_base_price', serial.properties.String()),
    ('gw_card_price', serial.properties.String()),
    ('gw_base_tax_amount', serial.properties.String()),
    ('gw_tax_amount', serial.properties.String()),
    ('gw_items_base_tax_amount', serial.properties.String()),
    ('gw_items_tax_amount', serial.properties.String()),
    ('gw_card_base_tax_amount', serial.properties.String()),
    ('gw_card_tax_amount', serial.properties.String()),
    ('gw_base_price_incl_tax', serial.properties.String()),
    ('gw_price_incl_tax', serial.properties.String()),
    ('gw_items_base_price_incl_tax', serial.properties.String()),
    ('gw_items_price_incl_tax', serial.properties.String()),
    ('gw_card_base_price_incl_tax', serial.properties.String()),
    ('gw_card_price_incl_tax', serial.properties.String()),
    ('gw_base_price_invoiced', serial.properties.String()),
    ('gw_price_invoiced', serial.properties.String()),
    ('gw_items_base_price_invoiced', serial.properties.String()),
    ('gw_items_price_invoiced', serial.properties.String()),
    ('gw_card_base_price_invoiced', serial.properties.String()),
    ('gw_card_price_invoiced', serial.properties.String()),
    ('gw_base_tax_amount_invoiced', serial.properties.String()),
    ('gw_tax_amount_invoiced', serial.properties.String()),
    ('gw_items_base_tax_invoiced', serial.properties.String()),
    ('gw_items_tax_invoiced', serial.properties.String()),
    ('gw_card_base_tax_invoiced', serial.properties.String()),
    ('gw_card_tax_invoiced', serial.properties.String()),
    ('gw_base_price_refunded', serial.properties.String()),
    ('gw_price_refunded', serial.properties.String()),
    ('gw_items_base_price_refunded', serial.properties.String()),
    ('gw_items_price_refunded', serial.properties.String()),
    ('gw_card_base_price_refunded', serial.properties.String()),
    ('gw_card_price_refunded', serial.properties.String()),
    ('gw_base_tax_amount_refunded', serial.properties.String()),
    ('gw_tax_amount_refunded', serial.properties.String()),
    ('gw_items_base_tax_refunded', serial.properties.String()),
    ('gw_items_tax_refunded', serial.properties.String()),
    ('gw_card_base_tax_refunded', serial.properties.String()),
    ('gw_card_tax_refunded', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipping-assignment-interface
serial.meta.writable(SalesShippingAssignment).properties = [
    (
        'shipping',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesShipping,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesOrderItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('stock_id', serial.properties.Integer()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipping-interface
serial.meta.writable(SalesShipping).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                SalesOrderAddress,
            ),
        )
    ),
    ('method', serial.properties.String()),
    (
        'total',
        serial.properties.Property(
            types=(
                SalesTotal,
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-total-interface
serial.meta.writable(SalesTotal).properties = [
    ('base_shipping_amount', serial.properties.Number()),
    ('base_shipping_canceled', serial.properties.Number()),
    ('base_shipping_discount_amount', serial.properties.Number()),
    ('base_shipping_discount_tax_compensation_amnt', serial.properties.Number()),
    ('base_shipping_incl_tax', serial.properties.Number()),
    ('base_shipping_invoiced', serial.properties.Number()),
    ('base_shipping_refunded', serial.properties.Number()),
    ('base_shipping_tax_amount', serial.properties.Number()),
    ('base_shipping_tax_refunded', serial.properties.Number()),
    ('shipping_amount', serial.properties.Number()),
    ('shipping_canceled', serial.properties.Number()),
    ('shipping_discount_amount', serial.properties.Number()),
    ('shipping_discount_tax_compensation_amount', serial.properties.Number()),
    ('shipping_incl_tax', serial.properties.Number()),
    ('shipping_invoiced', serial.properties.Number()),
    ('shipping_refunded', serial.properties.Number()),
    ('shipping_tax_amount', serial.properties.Number()),
    ('shipping_tax_refunded', serial.properties.Number()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/tax-data-order-tax-details-applied-tax-interface
serial.meta.writable(TaxOrderDetailsApplied).properties = [
    ('code', serial.properties.String()),
    ('title', serial.properties.String()),
    ('percent', serial.properties.Number()),
    (
        'amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                TaxOrderDetailsAppliedExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/tax-data-order-tax-details-applied-tax-extension-interface
serial.meta.writable(TaxOrderDetailsAppliedExtension).properties = [
    (
        'rates',
        serial.properties.Array(
            item_types=(
                TaxAppliedRate,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-applied-tax-rate-interface
serial.meta.writable(TaxAppliedRate).properties = [
    ('code', serial.properties.String()),
    ('title', serial.properties.String()),
    ('percent', serial.properties.Number()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/tax-data-order-tax-details-item-interface
serial.meta.writable(TaxOrderDetailsItem).properties = [
    (
        'type_',
        serial.properties.String(
            name='type',
        )
    ),
    ('item_id', serial.properties.Integer()),
    ('associated_item_id', serial.properties.Integer()),
    (
        'applied_taxes',
        serial.properties.Array(
            item_types=(
                TaxOrderDetailsApplied,
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-company-order-interface
serial.meta.writable(CompanyOrder).properties = [
    ('order_id', serial.properties.Integer()),
    ('company_id', serial.properties.Integer()),
    ('company_name', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/gift-card-account-data-gift-card-interface
serial.meta.writable(GiftCardAccount).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1cmsPage/post/parameters[0]/schema
serial.meta.writable(PostCMSPage).properties = [
    (
        'page',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CMSPage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/cms-data-page-interface
serial.meta.writable(CMSPage).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'identifier',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('title', serial.properties.String()),
    ('page_layout', serial.properties.String()),
    ('meta_title', serial.properties.String()),
    ('meta_keywords', serial.properties.String()),
    ('meta_description', serial.properties.String()),
    ('content_heading', serial.properties.String()),
    ('content', serial.properties.String()),
    ('creation_time', serial.properties.String()),
    ('update_time', serial.properties.String()),
    ('sort_order', serial.properties.String()),
    ('layout_update_xml', serial.properties.String()),
    ('custom_theme', serial.properties.String()),
    ('custom_root_template', serial.properties.String()),
    ('custom_layout_update_xml', serial.properties.String()),
    ('custom_theme_from', serial.properties.String()),
    ('custom_theme_to', serial.properties.String()),
    ('active', serial.properties.Boolean())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1orders~1/post/parameters[0]/schema
serial.meta.writable(PostOrders).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesOrder,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1coupons/post/parameters[0]/schema
serial.meta.writable(PostCoupons).properties = [
    (
        'coupon',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesRuleCoupon,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-coupon-interface
serial.meta.writable(SalesRuleCoupon).properties = [
    ('coupon_id', serial.properties.Integer()),
    (
        'rule_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('code', serial.properties.String()),
    ('usage_limit', serial.properties.Integer()),
    ('usage_per_customer', serial.properties.Integer()),
    (
        'times_used',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('expiration_date', serial.properties.String()),
    (
        'is_primary',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    (
        'type_',
        serial.properties.Integer(
            name='type',
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-rma-search-result-interface
serial.meta.writable(RMASearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        RMA,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-rma-interface
serial.meta.writable(RMA).properties = [
    (
        'increment_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'order_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'order_increment_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'date_requested',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_custom_email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        RMAItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'status',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'comments',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        RMAComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'tracks',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        RMATrack,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-item-interface
serial.meta.writable(RMAItem).properties = [
    (
        'entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'rma_entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'order_item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty_requested',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty_authorized',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty_approved',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty_returned',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'reason',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'condition',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'resolution',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'status',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-comment-interface
serial.meta.writable(RMAComment).properties = [
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'rma_entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'created_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_notified',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'status',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'admin',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-track-interface
serial.meta.writable(RMATrack).properties = [
    (
        'entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'rma_entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'track_number',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'carrier_title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'carrier_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1returns/post/parameters[0]/schema
serial.meta.writable(PostReturns).properties = [
    (
        'rma_data_object',
        serial.properties.Property(
            name='rmaDataObject',
            types=(
                serial.properties.Property(
                    types=(
                        RMA,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1cmsBlock/post/parameters[0]/schema
serial.meta.writable(PostCMSBlock).properties = [
    (
        'block',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CMSBlock,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/cms-data-block-interface
serial.meta.writable(CMSBlock).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'identifier',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('title', serial.properties.String()),
    ('content', serial.properties.String()),
    ('creation_time', serial.properties.String()),
    ('update_time', serial.properties.String()),
    ('active', serial.properties.Boolean())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-search-results-interface
serial.meta.writable(ProductSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Product,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-interface
serial.meta.writable(Product).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('name', serial.properties.String()),
    ('attribute_set_id', serial.properties.Integer()),
    ('price', serial.properties.Number()),
    ('status', serial.properties.Integer()),
    ('visibility', serial.properties.Integer()),
    ('type_id', serial.properties.String()),
    ('created_at', serial.properties.String()),
    ('updated_at', serial.properties.String()),
    ('weight', serial.properties.Number()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                ProductExtension,
            ),
        )
    ),
    (
        'product_links',
        serial.properties.Array(
            item_types=(
                ProductLink,
            ),
        )
    ),
    (
        'options',
        serial.properties.Array(
            item_types=(
                ProductCustomOption,
            ),
        )
    ),
    (
        'media_gallery_entries',
        serial.properties.Array(
            item_types=(
                ProductAttributeMediaGalleryEntry,
            ),
        )
    ),
    (
        'tier_prices',
        serial.properties.Array(
            item_types=(
                ProductTierPrice,
            ),
        )
    ),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-extension-interface
serial.meta.writable(ProductExtension).properties = [
    (
        'website_ids',
        serial.properties.Array(
            item_types=(
                int,
            ),
        )
    ),
    (
        'category_links',
        serial.properties.Array(
            item_types=(
                CategoryLink,
            ),
        )
    ),
    (
        'stock_item',
        serial.properties.Property(
            types=(
                InventoryStockItem,
            ),
        )
    ),
    (
        'bundle_product_options',
        serial.properties.Array(
            item_types=(
                BundleOptionInterface,
            ),
        )
    ),
    (
        'configurable_product_options',
        serial.properties.Array(
            item_types=(
                ConfigurableProductOption,
            ),
        )
    ),
    (
        'configurable_product_links',
        serial.properties.Array(
            item_types=(
                int,
            ),
        )
    ),
    (
        'downloadable_product_links',
        serial.properties.Array(
            item_types=(
                DownloadableLink,
            ),
        )
    ),
    (
        'downloadable_product_samples',
        serial.properties.Array(
            item_types=(
                DownloadableSample,
            ),
        )
    ),
    (
        'giftcard_amounts',
        serial.properties.Array(
            item_types=(
                GiftCardGiftcardAmount,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-category-link-interface
serial.meta.writable(CategoryLink).properties = [
    ('position', serial.properties.Integer()),
    (
        'category_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-inventory-data-stock-item-interface
serial.meta.writable(InventoryStockItem).properties = [
    ('item_id', serial.properties.Integer()),
    ('product_id', serial.properties.Integer()),
    ('stock_id', serial.properties.Integer()),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_in_stock',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_qty_decimal',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'show_default_notification_message',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_min_qty',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'min_qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_min_sale_qty',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'min_sale_qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_max_sale_qty',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'max_sale_qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_backorders',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'backorders',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_notify_stock_qty',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'notify_stock_qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_qty_increments',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty_increments',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_enable_qty_inc',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'enable_qty_increments',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_config_manage_stock',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'manage_stock',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'low_stock_date',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_decimal_divided',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'stock_status_changed_auto',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-option-interface
serial.meta.writable(BundleOptionInterface).properties = [
    ('option_id', serial.properties.Integer()),
    ('title', serial.properties.String()),
    ('required', serial.properties.Boolean()),
    (
        'type_',
        serial.properties.String(
            name='type',
        )
    ),
    ('position', serial.properties.Integer()),
    ('sku', serial.properties.String()),
    (
        'product_links',
        serial.properties.Array(
            item_types=(
                BundleLink,
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-link-interface
serial.meta.writable(BundleLink).properties = [
    (
        'id_',
        serial.properties.String(
            name='id',
        )
    ),
    ('sku', serial.properties.String()),
    ('option_id', serial.properties.Integer()),
    ('qty', serial.properties.Number()),
    ('position', serial.properties.Integer()),
    (
        'is_default',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_type',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('can_change_quantity', serial.properties.Integer()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/configurable-product-data-option-interface
serial.meta.writable(ConfigurableProductOption).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('attribute_id', serial.properties.String()),
    ('label', serial.properties.String()),
    ('position', serial.properties.Integer()),
    ('is_use_default', serial.properties.Boolean()),
    (
        'values',
        serial.properties.Array(
            item_types=(
                ConfigurableProductOptionValue,
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    ('product_id', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/configurable-product-data-option-value-interface
serial.meta.writable(ConfigurableProductOptionValue).properties = [
    (
        'value_index',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/downloadable-data-link-interface
serial.meta.writable(DownloadableLink).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('title', serial.properties.String()),
    (
        'sort_order',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_shareable',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('number_of_downloads', serial.properties.Integer()),
    (
        'link_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('link_file', serial.properties.String()),
    (
        'link_file_content',
        serial.properties.Property(
            types=(
                DownloadableFile,
            ),
        )
    ),
    ('link_url', serial.properties.String()),
    (
        'sample_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('sample_file', serial.properties.String()),
    (
        'sample_file_content',
        serial.properties.Property(
            types=(
                DownloadableFile,
            ),
        )
    ),
    ('sample_url', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/downloadable-data-file-content-interface
serial.meta.writable(DownloadableFile).properties = [
    (
        'file_data',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/downloadable-data-sample-interface
serial.meta.writable(DownloadableSample).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_order',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sample_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('sample_file', serial.properties.String()),
    (
        'sample_file_content',
        serial.properties.Property(
            types=(
                DownloadableFile,
            ),
        )
    ),
    ('sample_url', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/gift-card-data-giftcard-amount-interface
serial.meta.writable(GiftCardGiftcardAmount).properties = [
    (
        'attribute_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-link-interface
serial.meta.writable(ProductLink).properties = [
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'link_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'linked_product_sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'linked_product_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'position',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                ProductLinkExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-link-extension-interface
serial.meta.writable(ProductLinkExtension).properties = [
    ('qty', serial.properties.Number())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-custom-option-interface
serial.meta.writable(ProductCustomOption).properties = [
    (
        'product_sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('option_id', serial.properties.Integer()),
    (
        'title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'type_',
        serial.properties.Property(
            name='type',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_order',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_require',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('price', serial.properties.Number()),
    ('price_type', serial.properties.String()),
    ('sku', serial.properties.String()),
    ('file_extension', serial.properties.String()),
    ('max_characters', serial.properties.Integer()),
    ('image_size_x', serial.properties.Integer()),
    ('image_size_y', serial.properties.Integer()),
    (
        'values',
        serial.properties.Array(
            item_types=(
                ProductCustomOptionValuesInterface,
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-custom-option-values-interface
serial.meta.writable(ProductCustomOptionValuesInterface).properties = [
    (
        'title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_order',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('sku', serial.properties.String()),
    ('option_type_id', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-attribute-media-gallery-entry-interface
serial.meta.writable(ProductAttributeMediaGalleryEntry).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'media_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'position',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'disabled',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'types',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('file', serial.properties.String()),
    (
        'content',
        serial.properties.Property(
            types=(
                Image,
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                ProductAttributeMediaGalleryEntryExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-attribute-media-gallery-entry-extension-interface
serial.meta.writable(ProductAttributeMediaGalleryEntryExtension).properties = [
    (
        'video_content',
        serial.properties.Property(
            types=(
                Video,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-data-video-content-interface
serial.meta.writable(Video).properties = [
    (
        'media_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'video_provider',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'video_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'video_title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'video_description',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'video_metadata',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-tier-price-interface
serial.meta.writable(ProductTierPrice).properties = [
    (
        'customer_group_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                ProductTierPriceExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-tier-price-extension-interface
serial.meta.writable(ProductTierPriceExtension).properties = [
    ('percentage_value', serial.properties.Number()),
    ('website_id', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1products/post/parameters[0]/schema
serial.meta.writable(PostProducts).properties = [
    (
        'product',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Product,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'save_options',
        serial.properties.Boolean(
            name='saveOptions',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-invoice-search-result-interface
serial.meta.writable(SalesInvoiceSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesInvoice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-invoice-interface
serial.meta.writable(SalesInvoice).properties = [
    ('base_currency_code', serial.properties.String()),
    ('base_discount_amount', serial.properties.Number()),
    ('base_grand_total', serial.properties.Number()),
    ('base_discount_tax_compensation_amount', serial.properties.Number()),
    ('base_shipping_amount', serial.properties.Number()),
    ('base_shipping_discount_tax_compensation_amnt', serial.properties.Number()),
    ('base_shipping_incl_tax', serial.properties.Number()),
    ('base_shipping_tax_amount', serial.properties.Number()),
    ('base_subtotal', serial.properties.Number()),
    ('base_subtotal_incl_tax', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    ('base_total_refunded', serial.properties.Number()),
    ('base_to_global_rate', serial.properties.Number()),
    ('base_to_order_rate', serial.properties.Number()),
    ('billing_address_id', serial.properties.Integer()),
    ('can_void_flag', serial.properties.Integer()),
    ('created_at', serial.properties.String()),
    ('discount_amount', serial.properties.Number()),
    ('discount_description', serial.properties.String()),
    ('email_sent', serial.properties.Integer()),
    ('entity_id', serial.properties.Integer()),
    ('global_currency_code', serial.properties.String()),
    ('grand_total', serial.properties.Number()),
    ('discount_tax_compensation_amount', serial.properties.Number()),
    ('increment_id', serial.properties.String()),
    ('is_used_for_refund', serial.properties.Integer()),
    ('order_currency_code', serial.properties.String()),
    (
        'order_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('shipping_address_id', serial.properties.Integer()),
    ('shipping_amount', serial.properties.Number()),
    ('shipping_discount_tax_compensation_amount', serial.properties.Number()),
    ('shipping_incl_tax', serial.properties.Number()),
    ('shipping_tax_amount', serial.properties.Number()),
    ('state', serial.properties.Integer()),
    ('store_currency_code', serial.properties.String()),
    ('store_id', serial.properties.Integer()),
    ('store_to_base_rate', serial.properties.Number()),
    ('store_to_order_rate', serial.properties.Number()),
    ('subtotal', serial.properties.Number()),
    ('subtotal_incl_tax', serial.properties.Number()),
    ('tax_amount', serial.properties.Number()),
    (
        'total_qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('transaction_id', serial.properties.String()),
    ('updated_at', serial.properties.String()),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesInvoiceItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'comments',
        serial.properties.Array(
            item_types=(
                SalesInvoiceComment,
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                SalesInvoiceExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-invoice-item-interface
serial.meta.writable(SalesInvoiceItem).properties = [
    ('additional_data', serial.properties.String()),
    ('base_cost', serial.properties.Number()),
    ('base_discount_amount', serial.properties.Number()),
    ('base_discount_tax_compensation_amount', serial.properties.Number()),
    ('base_price', serial.properties.Number()),
    ('base_price_incl_tax', serial.properties.Number()),
    ('base_row_total', serial.properties.Number()),
    ('base_row_total_incl_tax', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    ('description', serial.properties.String()),
    ('discount_amount', serial.properties.Number()),
    ('entity_id', serial.properties.Integer()),
    ('discount_tax_compensation_amount', serial.properties.Number()),
    ('name', serial.properties.String()),
    ('parent_id', serial.properties.Integer()),
    ('price', serial.properties.Number()),
    ('price_incl_tax', serial.properties.Number()),
    ('product_id', serial.properties.Integer()),
    ('row_total', serial.properties.Number()),
    ('row_total_incl_tax', serial.properties.Number()),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('tax_amount', serial.properties.Number()),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'order_item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-invoice-comment-interface
serial.meta.writable(SalesInvoiceComment).properties = [
    (
        'is_customer_notified',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'parent_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    ('entity_id', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-invoice-extension-interface
serial.meta.writable(SalesInvoiceExtension).properties = [
    ('base_customer_balance_amount', serial.properties.Number()),
    ('customer_balance_amount', serial.properties.Number()),
    ('base_gift_cards_amount', serial.properties.Number()),
    ('gift_cards_amount', serial.properties.Number()),
    ('gw_base_price', serial.properties.String()),
    ('gw_price', serial.properties.String()),
    ('gw_items_base_price', serial.properties.String()),
    ('gw_items_price', serial.properties.String()),
    ('gw_card_base_price', serial.properties.String()),
    ('gw_card_price', serial.properties.String()),
    ('gw_base_tax_amount', serial.properties.String()),
    ('gw_tax_amount', serial.properties.String()),
    ('gw_items_base_tax_amount', serial.properties.String()),
    ('gw_items_tax_amount', serial.properties.String()),
    ('gw_card_base_tax_amount', serial.properties.String()),
    ('gw_card_tax_amount', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRates/put/parameters[0]/schema
serial.meta.writable(PutTaxRates).properties = [
    (
        'tax_rate',
        serial.properties.Property(
            name='taxRate',
            types=(
                serial.properties.Property(
                    types=(
                        TaxRate,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-rate-interface
serial.meta.writable(TaxRate).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'tax_country_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('tax_region_id', serial.properties.Integer()),
    ('region_name', serial.properties.String()),
    ('tax_postcode', serial.properties.String()),
    ('zip_is_range', serial.properties.Integer()),
    ('zip_from', serial.properties.Integer()),
    ('zip_to', serial.properties.Integer()),
    (
        'rate',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'titles',
        serial.properties.Array(
            item_types=(
                TaxRateTitle,
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-rate-title-interface
serial.meta.writable(TaxRateTitle).properties = [
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRates/post/parameters[0]/schema
serial.meta.writable(PostTaxRates).properties = [
    (
        'tax_rate',
        serial.properties.Property(
            name='taxRate',
            types=(
                serial.properties.Property(
                    types=(
                        TaxRate,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRules/put/parameters[0]/schema
serial.meta.writable(PutTaxRules).properties = [
    (
        'rule',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        TaxRule,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-rule-interface
serial.meta.writable(TaxRule).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'priority',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'position',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_tax_class_ids',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'product_tax_class_ids',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'tax_rate_ids',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('calculate_subtotal', serial.properties.Boolean()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxRules/post/parameters[0]/schema
serial.meta.writable(PostTaxRules).properties = [
    (
        'rule',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        TaxRule,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-data-company-search-results-interface
serial.meta.writable(CompanySearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Company,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-company-interface
serial.meta.writable(Company).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('status', serial.properties.Integer()),
    ('company_name', serial.properties.String()),
    ('legal_name', serial.properties.String()),
    ('company_email', serial.properties.String()),
    ('vat_tax_id', serial.properties.String()),
    ('reseller_id', serial.properties.String()),
    ('comment', serial.properties.String()),
    (
        'street',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('city', serial.properties.String()),
    ('country_id', serial.properties.String()),
    ('region', serial.properties.String()),
    ('region_id', serial.properties.String()),
    ('postcode', serial.properties.String()),
    ('telephone', serial.properties.String()),
    (
        'customer_group_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sales_representative_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'reject_reason',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'rejected_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'super_user_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                CompanyExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-data-company-extension-interface
serial.meta.writable(CompanyExtension).properties = [
    ('applicable_payment_method', serial.properties.Integer()),
    ('available_payment_methods', serial.properties.String()),
    ('use_config_settings', serial.properties.Integer()),
    (
        'quote_config',
        serial.properties.Property(
            types=(
                NegotiableQuoteCompanyConfig,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-company-quote-config-interface
serial.meta.writable(NegotiableQuoteCompanyConfig).properties = [
    ('company_id', serial.properties.String()),
    (
        'is_quote_enabled',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1company~1/post/parameters[0]/schema
serial.meta.writable(PostCompany).properties = [
    (
        'company',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Company,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1customers/post/parameters[0]/schema
serial.meta.writable(PostCustomers).properties = [
    (
        'customer',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Customer,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('password', serial.properties.String()),
    (
        'redirect_url',
        serial.properties.String(
            name='redirectUrl',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-customer-interface
serial.meta.writable(Customer).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('group_id', serial.properties.Integer()),
    ('default_billing', serial.properties.String()),
    ('default_shipping', serial.properties.String()),
    ('confirmation', serial.properties.String()),
    ('created_at', serial.properties.String()),
    ('updated_at', serial.properties.String()),
    ('created_in', serial.properties.String()),
    ('dob', serial.properties.String()),
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'firstname',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'lastname',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('middlename', serial.properties.String()),
    ('prefix', serial.properties.String()),
    ('suffix', serial.properties.String()),
    ('gender', serial.properties.Integer()),
    ('store_id', serial.properties.Integer()),
    ('taxvat', serial.properties.String()),
    ('website_id', serial.properties.Integer()),
    (
        'addresses',
        serial.properties.Array(
            item_types=(
                CustomerAddress,
            ),
        )
    ),
    ('disable_auto_group_change', serial.properties.Integer()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                CustomerExtension,
            ),
        )
    ),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-address-interface
serial.meta.writable(CustomerAddress).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('customer_id', serial.properties.Integer()),
    (
        'region',
        serial.properties.Property(
            types=(
                CustomerRegion,
            ),
        )
    ),
    ('region_id', serial.properties.Integer()),
    ('country_id', serial.properties.String()),
    (
        'street',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    ('company', serial.properties.String()),
    ('telephone', serial.properties.String()),
    ('fax', serial.properties.String()),
    ('postcode', serial.properties.String()),
    ('city', serial.properties.String()),
    ('firstname', serial.properties.String()),
    ('lastname', serial.properties.String()),
    ('middlename', serial.properties.String()),
    ('prefix', serial.properties.String()),
    ('suffix', serial.properties.String()),
    ('vat_id', serial.properties.String()),
    ('default_shipping', serial.properties.Boolean()),
    ('default_billing', serial.properties.Boolean()),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-region-interface
serial.meta.writable(CustomerRegion).properties = [
    (
        'region_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'region',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'region_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/customer-data-customer-extension-interface
serial.meta.writable(CustomerExtension).properties = [
    (
        'company_attributes',
        serial.properties.Property(
            types=(
                CompanyCustomer,
            ),
        )
    ),
    ('is_subscribed', serial.properties.Boolean())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-data-company-customer-interface
serial.meta.writable(CompanyCustomer).properties = [
    ('customer_id', serial.properties.Integer()),
    ('company_id', serial.properties.Integer()),
    ('job_title', serial.properties.String()),
    ('status', serial.properties.Integer()),
    ('telephone', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1invoices~1/post/parameters[0]/schema
serial.meta.writable(PostInvoices).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesInvoice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipment-search-result-interface
serial.meta.writable(SalesShipmentSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesShipment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-interface
serial.meta.writable(SalesShipment).properties = [
    ('billing_address_id', serial.properties.Integer()),
    ('created_at', serial.properties.String()),
    ('customer_id', serial.properties.Integer()),
    ('email_sent', serial.properties.Integer()),
    ('entity_id', serial.properties.Integer()),
    ('increment_id', serial.properties.String()),
    (
        'order_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'packages',
        serial.properties.Array(
            item_types=(
                SalesShipmentPackage,
            ),
        )
    ),
    ('shipment_status', serial.properties.Integer()),
    ('shipping_address_id', serial.properties.Integer()),
    ('shipping_label', serial.properties.String()),
    ('store_id', serial.properties.Integer()),
    ('total_qty', serial.properties.Number()),
    ('total_weight', serial.properties.Number()),
    ('updated_at', serial.properties.String()),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesShipmentItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'tracks',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesShipmentTrack,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'comments',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesShipmentComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-package-interface
serial.meta.writable(SalesShipmentPackage).properties = [
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-item-interface
serial.meta.writable(SalesShipmentItem).properties = [
    ('additional_data', serial.properties.String()),
    ('description', serial.properties.String()),
    ('entity_id', serial.properties.Integer()),
    ('name', serial.properties.String()),
    ('parent_id', serial.properties.Integer()),
    ('price', serial.properties.Number()),
    ('product_id', serial.properties.Integer()),
    ('row_total', serial.properties.Number()),
    ('sku', serial.properties.String()),
    ('weight', serial.properties.Number()),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'order_item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-track-interface
serial.meta.writable(SalesShipmentTrack).properties = [
    (
        'order_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    ('entity_id', serial.properties.Integer()),
    (
        'parent_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('updated_at', serial.properties.String()),
    (
        'weight',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'description',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'track_number',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'carrier_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-shipment-comment-interface
serial.meta.writable(SalesShipmentComment).properties = [
    (
        'is_customer_notified',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'parent_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    ('entity_id', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1shipment~1/post/parameters[0]/schema
serial.meta.writable(PostShipment).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesShipment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-category-tree-interface
serial.meta.writable(CategoryTree).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'parent_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_active',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'position',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'level',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'product_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'children_data',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CategoryTree,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1categories/post/parameters[0]/schema
serial.meta.writable(PostCategories).properties = [
    (
        'category',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Category,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-category-interface
serial.meta.writable(Category).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('parent_id', serial.properties.Integer()),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('is_active', serial.properties.Boolean()),
    ('position', serial.properties.Integer()),
    ('level', serial.properties.Integer()),
    ('children', serial.properties.String()),
    ('created_at', serial.properties.String()),
    ('updated_at', serial.properties.String()),
    ('path', serial.properties.String()),
    (
        'available_sort_by',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    ('include_in_menu', serial.properties.Boolean()),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-cart-interface
serial.meta.writable(QuoteCart).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    ('updated_at', serial.properties.String()),
    ('converted_at', serial.properties.String()),
    ('is_active', serial.properties.Boolean()),
    ('is_virtual', serial.properties.Boolean()),
    (
        'items',
        serial.properties.Array(
            item_types=(
                QuoteCartItem,
            ),
        )
    ),
    ('items_count', serial.properties.Integer()),
    ('items_qty', serial.properties.Number()),
    (
        'customer',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Customer,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            types=(
                QuoteAddress,
            ),
        )
    ),
    ('reserved_order_id', serial.properties.Integer()),
    ('orig_order_id', serial.properties.Integer()),
    (
        'currency',
        serial.properties.Property(
            types=(
                QuoteCurrency,
            ),
        )
    ),
    ('customer_is_guest', serial.properties.Boolean()),
    ('customer_note', serial.properties.String()),
    ('customer_note_notify', serial.properties.Boolean()),
    ('customer_tax_class_id', serial.properties.Integer()),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteCartExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-cart-item-interface
serial.meta.writable(QuoteCartItem).properties = [
    ('item_id', serial.properties.Integer()),
    ('sku', serial.properties.String()),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('name', serial.properties.String()),
    ('price', serial.properties.Number()),
    ('product_type', serial.properties.String()),
    (
        'quote_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'product_option',
        serial.properties.Property(
            types=(
                QuoteProductOption,
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteCartItemExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-product-option-interface
serial.meta.writable(QuoteProductOption).properties = [
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteProductOptionExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-product-option-extension-interface
serial.meta.writable(QuoteProductOptionExtension).properties = [
    (
        'custom_options',
        serial.properties.Array(
            item_types=(
                CustomOption,
            ),
        )
    ),
    (
        'bundle_options',
        serial.properties.Array(
            item_types=(
                BundleOption,
            ),
        )
    ),
    (
        'configurable_item_options',
        serial.properties.Array(
            item_types=(
                ConfigurableProductItemOptionValue,
            ),
        )
    ),
    (
        'downloadable_option',
        serial.properties.Property(
            types=(
                DownloadableOption,
            ),
        )
    ),
    (
        'giftcard_item_option',
        serial.properties.Property(
            types=(
                GiftCardOption,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-cart-item-extension-interface
serial.meta.writable(QuoteCartItemExtension).properties = [
    (
        'negotiable_quote_item',
        serial.properties.Property(
            types=(
                NegotiableQuoteItem,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-negotiable-quote-item-interface
serial.meta.writable(NegotiableQuoteItem).properties = [
    (
        'item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'original_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'original_tax_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'original_discount_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-address-interface
serial.meta.writable(QuoteAddress).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'region',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'region_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'region_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'country_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'street',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('company', serial.properties.String()),
    (
        'telephone',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('fax', serial.properties.String()),
    (
        'postcode',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'city',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'firstname',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'lastname',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('middlename', serial.properties.String()),
    ('prefix', serial.properties.String()),
    ('suffix', serial.properties.String()),
    ('vat_id', serial.properties.String()),
    ('customer_id', serial.properties.Integer()),
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('same_as_billing', serial.properties.Integer()),
    ('customer_address_id', serial.properties.Integer()),
    ('save_in_address_book', serial.properties.Integer()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteAddressExtension,
            ),
        )
    ),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-address-extension-interface
serial.meta.writable(QuoteAddressExtension).properties = [
    ('gift_registry_id', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-currency-interface
serial.meta.writable(QuoteCurrency).properties = [
    ('global_currency_code', serial.properties.String()),
    ('base_currency_code', serial.properties.String()),
    ('store_currency_code', serial.properties.String()),
    ('quote_currency_code', serial.properties.String()),
    ('store_to_base_rate', serial.properties.Number()),
    ('store_to_quote_rate', serial.properties.Number()),
    ('base_to_global_rate', serial.properties.Number()),
    ('base_to_quote_rate', serial.properties.Number()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-cart-extension-interface
serial.meta.writable(QuoteCartExtension).properties = [
    (
        'shipping_assignments',
        serial.properties.Array(
            item_types=(
                QuoteShippingAssignment,
            ),
        )
    ),
    (
        'negotiable_quote',
        serial.properties.Property(
            types=(
                NegotiableQuote,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-shipping-assignment-interface
serial.meta.writable(QuoteShippingAssignment).properties = [
    (
        'shipping',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteShipping,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        QuoteCartItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-shipping-interface
serial.meta.writable(QuoteShipping).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'method',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-negotiable-quote-interface
serial.meta.writable(NegotiableQuote).properties = [
    (
        'quote_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_regular_quote',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'status',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'negotiated_price_type',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'negotiated_price_value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'shipping_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'quote_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'expiration_period',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'email_notification_status',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'has_unconfirmed_changes',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_shipping_tax_changed',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_customer_price_changed',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'notifications',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'applied_rule_ids',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_address_draft',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'deleted_sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'creator_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'creator_type',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('original_total_price', serial.properties.Number()),
    ('base_original_total_price', serial.properties.Number()),
    ('negotiated_total_price', serial.properties.Number()),
    ('base_negotiated_total_price', serial.properties.Number()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1carts~1mine/put/parameters[0]/schema
serial.meta.writable(PutCartsMine).properties = [
    (
        'quote',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCart,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1creditmemo/post/parameters[0]/schema
serial.meta.writable(PostCreditMemo).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesCreditMemo,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-creditmemo-interface
serial.meta.writable(SalesCreditMemo).properties = [
    ('adjustment', serial.properties.Number()),
    ('adjustment_negative', serial.properties.Number()),
    ('adjustment_positive', serial.properties.Number()),
    ('base_adjustment', serial.properties.Number()),
    ('base_adjustment_negative', serial.properties.Number()),
    ('base_adjustment_positive', serial.properties.Number()),
    ('base_currency_code', serial.properties.String()),
    ('base_discount_amount', serial.properties.Number()),
    ('base_grand_total', serial.properties.Number()),
    ('base_discount_tax_compensation_amount', serial.properties.Number()),
    ('base_shipping_amount', serial.properties.Number()),
    ('base_shipping_discount_tax_compensation_amnt', serial.properties.Number()),
    ('base_shipping_incl_tax', serial.properties.Number()),
    ('base_shipping_tax_amount', serial.properties.Number()),
    ('base_subtotal', serial.properties.Number()),
    ('base_subtotal_incl_tax', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    ('base_to_global_rate', serial.properties.Number()),
    ('base_to_order_rate', serial.properties.Number()),
    ('billing_address_id', serial.properties.Integer()),
    ('created_at', serial.properties.String()),
    ('creditmemo_status', serial.properties.Integer()),
    ('discount_amount', serial.properties.Number()),
    ('discount_description', serial.properties.String()),
    ('email_sent', serial.properties.Integer()),
    ('entity_id', serial.properties.Integer()),
    ('global_currency_code', serial.properties.String()),
    ('grand_total', serial.properties.Number()),
    ('discount_tax_compensation_amount', serial.properties.Number()),
    ('increment_id', serial.properties.String()),
    ('invoice_id', serial.properties.Integer()),
    ('order_currency_code', serial.properties.String()),
    (
        'order_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('shipping_address_id', serial.properties.Integer()),
    ('shipping_amount', serial.properties.Number()),
    ('shipping_discount_tax_compensation_amount', serial.properties.Number()),
    ('shipping_incl_tax', serial.properties.Number()),
    ('shipping_tax_amount', serial.properties.Number()),
    ('state', serial.properties.Integer()),
    ('store_currency_code', serial.properties.String()),
    ('store_id', serial.properties.Integer()),
    ('store_to_base_rate', serial.properties.Number()),
    ('store_to_order_rate', serial.properties.Number()),
    ('subtotal', serial.properties.Number()),
    ('subtotal_incl_tax', serial.properties.Number()),
    ('tax_amount', serial.properties.Number()),
    ('transaction_id', serial.properties.String()),
    ('updated_at', serial.properties.String()),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesCreditMemoItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'comments',
        serial.properties.Array(
            item_types=(
                SalesCreditMemoComment,
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                SalesCreditMemoExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-creditmemo-item-interface
serial.meta.writable(SalesCreditMemoItem).properties = [
    ('additional_data', serial.properties.String()),
    (
        'base_cost',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('base_discount_amount', serial.properties.Number()),
    ('base_discount_tax_compensation_amount', serial.properties.Number()),
    (
        'base_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('base_price_incl_tax', serial.properties.Number()),
    ('base_row_total', serial.properties.Number()),
    ('base_row_total_incl_tax', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    ('base_weee_tax_applied_amount', serial.properties.Number()),
    ('base_weee_tax_applied_row_amnt', serial.properties.Number()),
    ('base_weee_tax_disposition', serial.properties.Number()),
    ('base_weee_tax_row_disposition', serial.properties.Number()),
    ('description', serial.properties.String()),
    ('discount_amount', serial.properties.Number()),
    (
        'entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('discount_tax_compensation_amount', serial.properties.Number()),
    ('name', serial.properties.String()),
    (
        'order_item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('parent_id', serial.properties.Integer()),
    ('price', serial.properties.Number()),
    ('price_incl_tax', serial.properties.Number()),
    ('product_id', serial.properties.Integer()),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('row_total', serial.properties.Number()),
    ('row_total_incl_tax', serial.properties.Number()),
    ('sku', serial.properties.String()),
    ('tax_amount', serial.properties.Number()),
    ('weee_tax_applied', serial.properties.String()),
    ('weee_tax_applied_amount', serial.properties.Number()),
    ('weee_tax_applied_row_amount', serial.properties.Number()),
    ('weee_tax_disposition', serial.properties.Number()),
    ('weee_tax_row_disposition', serial.properties.Number()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-comment-interface
serial.meta.writable(SalesCreditMemoComment).properties = [
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('created_at', serial.properties.String()),
    ('entity_id', serial.properties.Integer()),
    (
        'is_customer_notified',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'parent_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-extension-interface
serial.meta.writable(SalesCreditMemoExtension).properties = [
    ('base_customer_balance_amount', serial.properties.Number()),
    ('customer_balance_amount', serial.properties.Number()),
    ('base_gift_cards_amount', serial.properties.Number()),
    ('gift_cards_amount', serial.properties.Number()),
    ('gw_base_price', serial.properties.String()),
    ('gw_price', serial.properties.String()),
    ('gw_items_base_price', serial.properties.String()),
    ('gw_items_price', serial.properties.String()),
    ('gw_card_base_price', serial.properties.String()),
    ('gw_card_price', serial.properties.String()),
    ('gw_base_tax_amount', serial.properties.String()),
    ('gw_tax_amount', serial.properties.String()),
    ('gw_items_base_tax_amount', serial.properties.String()),
    ('gw_items_tax_amount', serial.properties.String()),
    ('gw_card_base_tax_amount', serial.properties.String()),
    ('gw_card_tax_amount', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1salesRules/post/parameters[0]/schema
serial.meta.writable(PostSalesRules).properties = [
    (
        'rule',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesRule,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-rule-interface
serial.meta.writable(SalesRule).properties = [
    ('rule_id', serial.properties.Integer()),
    ('name', serial.properties.String()),
    (
        'store_labels',
        serial.properties.Array(
            item_types=(
                SalesRuleLabel,
            ),
        )
    ),
    ('description', serial.properties.String()),
    (
        'website_ids',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_group_ids',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('from_date', serial.properties.String()),
    ('to_date', serial.properties.String()),
    (
        'uses_per_customer',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_active',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'condition',
        serial.properties.Property(
            types=(
                SalesRuleCondition,
            ),
        )
    ),
    (
        'action_condition',
        serial.properties.Property(
            types=(
                SalesRuleCondition,
            ),
        )
    ),
    (
        'stop_rules_processing',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_advanced',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'product_ids',
        serial.properties.Array(
            item_types=(
                int,
            ),
        )
    ),
    (
        'sort_order',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('simple_action', serial.properties.String()),
    (
        'discount_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('discount_qty', serial.properties.Number()),
    (
        'discount_step',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'apply_to_shipping',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'times_used',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_rss',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'coupon_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_auto_generation',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'uses_per_coupon',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('simple_free_shipping', serial.properties.String()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                SalesRuleExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-rule-label-interface
serial.meta.writable(SalesRuleLabel).properties = [
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-rule-data-condition-interface
serial.meta.writable(SalesRuleCondition).properties = [
    (
        'condition_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'conditions',
        serial.properties.Array(
            item_types=(
                SalesRuleCondition,
            ),
        )
    ),
    ('aggregator_type', serial.properties.String()),
    (
        'operator',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('attribute_name', serial.properties.String()),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        serial.properties.String(),
                        serial.properties.Boolean(),
                        serial.properties.Array(
                            item_types=(
                                str,
                            ),
                        )
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-rule-data-rule-extension-interface
serial.meta.writable(SalesRuleExtension).properties = [
    ('reward_points_delta', serial.properties.Integer())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1taxClasses/post/parameters[0]/schema
serial.meta.writable(PostTaxClasses).properties = [
    (
        'tax_class',
        serial.properties.Property(
            name='taxClass',
            types=(
                serial.properties.Property(
                    types=(
                        TaxClass,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-tax-class-interface
serial.meta.writable(TaxClass).properties = [
    ('class_id', serial.properties.Integer()),
    (
        'class_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'class_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-search-result-interface
serial.meta.writable(SalesCreditMemoSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesCreditMemo,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1customers~1me/put/parameters[0]/schema
serial.meta.writable(PutMe).properties = [
    (
        'customer',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Customer,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'password_hash',
        serial.properties.String(
            name='passwordHash',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1cmsPage~1{id}/put/parameters[1]/schema
serial.meta.writable(PutCMSPage).properties = [
    (
        'page',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CMSPage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-cart-search-results-interface
serial.meta.writable(QuoteCartSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        QuoteCart,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-order-item-search-result-interface
serial.meta.writable(SalesOrderItemSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesOrderItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-transaction-search-result-interface
serial.meta.writable(SalesTransactionSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesTransaction,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/sales-data-transaction-interface
serial.meta.writable(SalesTransaction).properties = [
    (
        'transaction_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('parent_id', serial.properties.Integer()),
    (
        'order_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'payment_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'txn_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'parent_txn_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'txn_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_closed',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'additional_information',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    (
        'created_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'child_transactions',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesTransaction,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/paths/~1V1~1returns~1{id}/put/parameters[1]/schema
serial.meta.writable(PutReturns).properties = [
    (
        'rma_data_object',
        serial.properties.Property(
            name='rmaDataObject',
            types=(
                serial.properties.Property(
                    types=(
                        RMA,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1returns~1{id}/delete/parameters[1]/schema
serial.meta.writable(DeleteReturns).properties = [
    (
        'rma_data_object',
        serial.properties.Property(
            name='rmaDataObject',
            types=(
                serial.properties.Property(
                    types=(
                        RMA,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1cmsBlock~1{id}/put/parameters[1]/schema
serial.meta.writable(PutCMSBlock).properties = [
    (
        'block',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CMSBlock,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1cost/post/parameters[0]/schema
serial.meta.writable(PostProductsCost).properties = [
    (
        'prices',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Cost,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-cost-interface
serial.meta.writable(Cost).properties = [
    (
        'cost',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-price-update-result-interface
serial.meta.writable(PriceUpdateResult).properties = [
    (
        'message',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'parameters',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1orders~1create/put/parameters[0]/schema
serial.meta.writable(PutOrdersCreate).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesOrder,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/checkout-agreements-data-agreement-interface
serial.meta.writable(CheckoutAgreementsAgreement).properties = [
    (
        'agreement_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'content',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('content_height', serial.properties.String()),
    (
        'checkbox_text',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_active',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_html',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'mode',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1team~1{teamId}/put/parameters[1]/schema
serial.meta.writable(PutTeam).properties = [
    (
        'team',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CompanyTeam,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-data-role-search-results-interface
serial.meta.writable(CompanyRoleSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CompanyRole,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-role-interface
serial.meta.writable(CompanyRole).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('role_name', serial.properties.String()),
    (
        'permissions',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CompanyPermission,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('company_id', serial.properties.Integer()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-permission-interface
serial.meta.writable(CompanyPermission).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('role_id', serial.properties.Integer()),
    (
        'resource_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'permission',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1company~1role~1/post/parameters[0]/schema
serial.meta.writable(PostCompanyRole).properties = [
    (
        'role',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CompanyRole,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog/post/parameters[0]/schema
serial.meta.writable(PostSharedCatalog).properties = [
    (
        'shared_catalog',
        serial.properties.Property(
            name='sharedCatalog',
            types=(
                serial.properties.Property(
                    types=(
                        SharedCatalog,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/shared-catalog-data-shared-catalog-interface
serial.meta.writable(SharedCatalog).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'description',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_group_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'type_',
        serial.properties.Property(
            name='type',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'created_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'created_by',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'tax_class_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-website-interface
serial.meta.writable(StoreWebsite).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'default_group_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customerGroups/post/parameters[0]/schema
serial.meta.writable(PostCustomerGroups).properties = [
    (
        'group',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CustomerGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-group-interface
serial.meta.writable(CustomerGroup).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'tax_class_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('tax_class_name', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/cms-data-page-search-results-interface
serial.meta.writable(CMSPageSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CMSPage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}/put/parameters[1]/schema
serial.meta.writable(PutProducts).properties = [
    (
        'product',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Product,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'save_options',
        serial.properties.Boolean(
            name='saveOptions',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-type-interface
serial.meta.writable(ProductType).properties = [
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}/put/parameters[1]/schema
serial.meta.writable(PutCarts).properties = [
    (
        'customer_id',
        serial.properties.Property(
            name='customerId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            name='storeId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1shipment~1track/post/parameters[0]/schema
serial.meta.writable(PostShipmentTrack).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesShipmentTrack,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-rule-data-coupon-search-result-interface
serial.meta.writable(SalesRuleCouponSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesRuleCoupon,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/company-data-hierarchy-interface
serial.meta.writable(CompanyHierarchy).properties = [
    ('structure_id', serial.properties.Integer()),
    ('entity_id', serial.properties.Integer()),
    ('entity_type', serial.properties.String()),
    ('structure_parent_id', serial.properties.Integer()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/analytics-data-link-interface
serial.meta.writable(AnalyticsLink).properties = [
    (
        'url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'initialization_vector',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/shared-catalog-data-search-results-interface
serial.meta.writable(SearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SharedCatalog,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/gift-wrapping-data-wrapping-search-results-interface
serial.meta.writable(GiftWrappingSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        GiftWrapping,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/gift-wrapping-data-wrapping-interface
serial.meta.writable(GiftWrapping).properties = [
    ('wrapping_id', serial.properties.Integer()),
    (
        'design',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'status',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('image_name', serial.properties.String()),
    (
        'image_base_64_content',
        serial.properties.String(
            name='image_base64_content',
        )
    ),
    ('base_currency_code', serial.properties.String()),
    (
        'website_ids',
        serial.properties.Array(
            item_types=(
                int,
            ),
        )
    ),
    ('image_url', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1gift-wrappings/post/parameters[0]/schema
serial.meta.writable(PostGiftWrappings).properties = [
    (
        'data',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        GiftWrapping,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Integer(
            name='storeId',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/cms-data-block-search-results-interface
serial.meta.writable(CMSBlockSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CMSBlock,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1categories~1{id}/put/parameters[1]/schema
serial.meta.writable(PutCategories).properties = [
    (
        'category',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Category,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-category-search-results-interface
serial.meta.writable(CategorySearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Category,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/tax-data-tax-rate-search-results-interface
serial.meta.writable(TaxRateSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        TaxRate,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/tax-data-tax-rule-search-results-interface
serial.meta.writable(TaxRuleSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        TaxRule,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-credit-data-credit-limit-search-results-interface
serial.meta.writable(CompanyCreditLimitSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CompanyCredit,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-credit-data-credit-data-interface
serial.meta.writable(CompanyCredit).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('company_id', serial.properties.Integer()),
    ('credit_limit', serial.properties.Number()),
    ('balance', serial.properties.Number()),
    ('currency_code', serial.properties.String()),
    (
        'exceed_limit',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('available_limit', serial.properties.Number())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-store-interface
serial.meta.writable(Store).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_group_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/customer-data-customer-search-results-interface
serial.meta.writable(CustomerSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Customer,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1options/post/parameters[0]/schema
serial.meta.writable(PostProductsOptions).properties = [
    (
        'option',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductCustomOption,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1order/put/parameters[0]/schema
serial.meta.writable(PutCartsMineOrder).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                QuotePayment,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-payment-interface
serial.meta.writable(QuotePayment).properties = [
    ('po_number', serial.properties.String()),
    (
        'method',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'additional_data',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuotePaymentExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-payment-extension-interface
serial.meta.writable(QuotePaymentExtension).properties = [
    (
        'agreement_ids',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1items/post/parameters[0]/schema
serial.meta.writable(PostCartsMineItems).properties = [
    (
        'cart_item',
        serial.properties.Property(
            name='cartItem',
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCartItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1coupons~1generate/post/parameters[0]/schema
serial.meta.writable(PostCouponsGenerate).properties = [
    (
        'coupon_spec',
        serial.properties.Property(
            name='couponSpec',
            types=(
                serial.properties.Property(
                    types=(
                        SalesRuleCouponGenerationSpec,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-rule-data-coupon-generation-spec-interface
serial.meta.writable(SalesRuleCouponGenerationSpec).properties = [
    (
        'rule_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'format_',
        serial.properties.Property(
            name='format',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'quantity',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'length',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('prefix', serial.properties.String()),
    ('suffix', serial.properties.String()),
    ('delimiter_at_every', serial.properties.Integer()),
    ('delimiter', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1team~1{companyId}/post/parameters[1]/schema
serial.meta.writable(PostTeam).properties = [
    (
        'team',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CompanyTeam,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-group-interface
serial.meta.writable(StoreGroup).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'root_category_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'default_store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1confirm/post/parameters[0]/schema
serial.meta.writable(PostCustomersConfirm).properties = [
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Property(
            name='websiteId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'redirect_url',
        serial.properties.String(
            name='redirectUrl',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-totals-interface
serial.meta.writable(QuoteTotals).properties = [
    ('grand_total', serial.properties.Number()),
    ('base_grand_total', serial.properties.Number()),
    ('subtotal', serial.properties.Number()),
    ('base_subtotal', serial.properties.Number()),
    ('discount_amount', serial.properties.Number()),
    ('base_discount_amount', serial.properties.Number()),
    ('subtotal_with_discount', serial.properties.Number()),
    ('base_subtotal_with_discount', serial.properties.Number()),
    ('shipping_amount', serial.properties.Number()),
    ('base_shipping_amount', serial.properties.Number()),
    ('shipping_discount_amount', serial.properties.Number()),
    ('base_shipping_discount_amount', serial.properties.Number()),
    ('tax_amount', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    (
        'weee_tax_applied_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('shipping_tax_amount', serial.properties.Number()),
    ('base_shipping_tax_amount', serial.properties.Number()),
    ('subtotal_incl_tax', serial.properties.Number()),
    ('base_subtotal_incl_tax', serial.properties.Number()),
    ('shipping_incl_tax', serial.properties.Number()),
    ('base_shipping_incl_tax', serial.properties.Number()),
    ('base_currency_code', serial.properties.String()),
    ('quote_currency_code', serial.properties.String()),
    ('coupon_code', serial.properties.String()),
    ('items_qty', serial.properties.Integer()),
    (
        'items',
        serial.properties.Array(
            item_types=(
                QuoteTotalsItem,
            ),
        )
    ),
    (
        'total_segments',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        QuoteTotalSegment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteTotalsExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-totals-item-interface
serial.meta.writable(QuoteTotalsItem).properties = [
    (
        'item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'row_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_row_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('row_total_with_discount', serial.properties.Number()),
    ('tax_amount', serial.properties.Number()),
    ('base_tax_amount', serial.properties.Number()),
    ('tax_percent', serial.properties.Number()),
    ('discount_amount', serial.properties.Number()),
    ('base_discount_amount', serial.properties.Number()),
    ('discount_percent', serial.properties.Number()),
    ('price_incl_tax', serial.properties.Number()),
    ('base_price_incl_tax', serial.properties.Number()),
    ('row_total_incl_tax', serial.properties.Number()),
    ('base_row_total_incl_tax', serial.properties.Number()),
    (
        'options',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'weee_tax_applied_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'weee_tax_applied',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteTotalsItemExtension,
            ),
        )
    ),
    ('name', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-totals-item-extension-interface
serial.meta.writable(QuoteTotalsItemExtension).properties = [
    (
        'negotiable_quote_item_totals',
        serial.properties.Property(
            types=(
                NegotiableQuoteItemTotals,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-negotiable-quote-item-totals-interface
serial.meta.writable(NegotiableQuoteItemTotals).properties = [
    (
        'cost',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'catalog_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_catalog_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'catalog_price_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_catalog_price_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'cart_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_cart_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'cart_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_cart_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'cart_price_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_cart_price_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-total-segment-interface
serial.meta.writable(QuoteTotalSegment).properties = [
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('title', serial.properties.String()),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('area', serial.properties.String()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteTotalSegmentExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-total-segment-extension-interface
serial.meta.writable(QuoteTotalSegmentExtension).properties = [
    (
        'tax_grandtotal_details',
        serial.properties.Array(
            item_types=(
                TaxGrandTotalDetails,
            ),
        )
    ),
    ('gift_cards', serial.properties.String()),
    ('gw_order_id', serial.properties.String()),
    (
        'gw_item_ids',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    ('gw_allow_gift_receipt', serial.properties.String()),
    ('gw_add_card', serial.properties.String()),
    ('gw_price', serial.properties.String()),
    ('gw_base_price', serial.properties.String()),
    ('gw_items_price', serial.properties.String()),
    ('gw_items_base_price', serial.properties.String()),
    ('gw_card_price', serial.properties.String()),
    ('gw_card_base_price', serial.properties.String()),
    ('gw_base_tax_amount', serial.properties.String()),
    ('gw_tax_amount', serial.properties.String()),
    ('gw_items_base_tax_amount', serial.properties.String()),
    ('gw_items_tax_amount', serial.properties.String()),
    ('gw_card_base_tax_amount', serial.properties.String()),
    ('gw_card_tax_amount', serial.properties.String()),
    ('gw_price_incl_tax', serial.properties.String()),
    ('gw_base_price_incl_tax', serial.properties.String()),
    ('gw_card_price_incl_tax', serial.properties.String()),
    ('gw_card_base_price_incl_tax', serial.properties.String()),
    ('gw_items_price_incl_tax', serial.properties.String()),
    ('gw_items_base_price_incl_tax', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-grand-total-details-interface
serial.meta.writable(TaxGrandTotalDetails).properties = [
    (
        'amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'rates',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        TaxGrandTotalRates,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'group_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/tax-data-grand-total-rates-interface
serial.meta.writable(TaxGrandTotalRates).properties = [
    (
        'percent',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-totals-extension-interface
serial.meta.writable(QuoteTotalsExtension).properties = [
    ('coupon_label', serial.properties.String()),
    ('base_customer_balance_amount', serial.properties.Number()),
    ('customer_balance_amount', serial.properties.Number()),
    (
        'negotiable_quote_totals',
        serial.properties.Property(
            types=(
                NegotiableQuoteTotals,
            ),
        )
    ),
    ('reward_points_balance', serial.properties.Number()),
    ('reward_currency_amount', serial.properties.Number()),
    ('base_reward_currency_amount', serial.properties.Number())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-negotiable-quote-totals-interface
serial.meta.writable(NegotiableQuoteTotals).properties = [
    (
        'items_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'quote_status',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'created_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'updated_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_group',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_to_quote_rate',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'cost_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_cost_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'original_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_original_total',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'original_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_original_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'original_price_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_original_price_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'negotiated_price_type',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'negotiated_price_value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1requisition_lists/post/parameters[0]/schema
serial.meta.writable(PostRequisitionLists).properties = [
    (
        'requisition_list',
        serial.properties.Property(
            name='requisitionList',
            types=(
                serial.properties.Property(
                    types=(
                        RequisitionList,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/requisition-list-data-requisition-list-interface
serial.meta.writable(RequisitionList).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'updated_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'description',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        RequisitionListItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/requisition-list-data-requisition-list-item-interface
serial.meta.writable(RequisitionListItem).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'requisition_list_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'options',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'added_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1invoices~1comments/post/parameters[0]/schema
serial.meta.writable(PostInvoicesComments).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesInvoiceComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1creditmemo~1refund/post/parameters[0]/schema
serial.meta.writable(PostCreditMemoRefund).properties = [
    (
        'creditmemo',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesCreditMemo,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'offline_requested',
        serial.properties.Boolean(
            name='offlineRequested',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-rule-data-rule-search-result-interface
serial.meta.writable(SalesRuleSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesRule,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/tax-data-tax-class-search-results-interface
serial.meta.writable(TaxClassSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        TaxClass,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1company~1role~1{id}/put/parameters[1]/schema
serial.meta.writable(PutCompanyRole).properties = [
    (
        'role',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CompanyRole,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/store-data-store-config-interface
serial.meta.writable(StoreConfig).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'locale',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_currency_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'default_display_currency_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'timezone',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'weight_unit',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_link_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_static_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_media_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'secure_base_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'secure_base_link_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'secure_base_static_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'secure_base_media_url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/directory-data-currency-information-interface
serial.meta.writable(CurrencyInformation).properties = [
    (
        'base_currency_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_currency_symbol',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'default_display_currency_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'default_display_currency_symbol',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'available_currency_codes',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'exchange_rates',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        ExchangeRate,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/directory-data-exchange-rate-interface
serial.meta.writable(ExchangeRate).properties = [
    (
        'currency_to',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'rate',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1eav~1attribute-sets/post/parameters[0]/schema
serial.meta.writable(PostEAVAttributeSets).properties = [
    (
        'entity_type_code',
        serial.properties.Property(
            name='entityTypeCode',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'attribute_set',
        serial.properties.Property(
            name='attributeSet',
            types=(
                serial.properties.Property(
                    types=(
                        EAVAttributeSet,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'skeleton_id',
        serial.properties.Property(
            name='skeletonId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/eav-data-attribute-set-interface
serial.meta.writable(EAVAttributeSet).properties = [
    ('attribute_set_id', serial.properties.Integer()),
    (
        'attribute_set_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_order',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('entity_type_id', serial.properties.Integer()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1password/put/parameters[0]/schema
serial.meta.writable(PutCustomersPassword).properties = [
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'template',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Integer(
            name='websiteId',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1validate/put/parameters[0]/schema
serial.meta.writable(PutCustomersValidate).properties = [
    (
        'customer',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Customer,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/customer-data-validation-results-interface
serial.meta.writable(CustomerValidationResults).properties = [
    (
        'valid',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'messages',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1orders~1{parent_id}/put/parameters[1]/schema
serial.meta.writable(PutOrders).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesOrderAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1coupons~1{couponId}/put/parameters[1]/schema
serial.meta.writable(PutCoupons).properties = [
    (
        'coupon',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesRuleCoupon,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog~1{id}/put/parameters[1]/schema
serial.meta.writable(PutSharedCatalog).properties = [
    (
        'shared_catalog',
        serial.properties.Property(
            name='sharedCatalog',
            types=(
                serial.properties.Property(
                    types=(
                        SharedCatalog,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/directory-data-country-information-interface
serial.meta.writable(CountryInformation).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'two_letter_abbreviation',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'three_letter_abbreviation',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'full_name_locale',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'full_name_english',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'available_regions',
        serial.properties.Array(
            item_types=(
                RegionInformation,
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/directory-data-region-information-interface
serial.meta.writable(RegionInformation).properties = [
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customerGroups~1{id}/put/parameters[1]/schema
serial.meta.writable(PutCustomerGroups).properties = [
    (
        'group',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        CustomerGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-attribute-search-results-interface
serial.meta.writable(ProductAttributeSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        ProductAttribute,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-attribute-interface
serial.meta.writable(ProductAttribute).properties = [
    ('is_wysiwyg_enabled', serial.properties.Boolean()),
    ('is_html_allowed_on_front', serial.properties.Boolean()),
    ('used_for_sort_by', serial.properties.Boolean()),
    ('is_filterable', serial.properties.Boolean()),
    ('is_filterable_in_search', serial.properties.Boolean()),
    ('is_used_in_grid', serial.properties.Boolean()),
    ('is_visible_in_grid', serial.properties.Boolean()),
    ('is_filterable_in_grid', serial.properties.Boolean()),
    ('position', serial.properties.Integer()),
    (
        'apply_to',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    ('is_searchable', serial.properties.String()),
    ('is_visible_in_advanced_search', serial.properties.String()),
    ('is_comparable', serial.properties.String()),
    ('is_used_for_promo_rules', serial.properties.String()),
    ('is_visible_on_front', serial.properties.String()),
    ('used_in_product_listing', serial.properties.String()),
    ('is_visible', serial.properties.Boolean()),
    ('scope', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary()),
    ('attribute_id', serial.properties.Integer()),
    (
        'attribute_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'frontend_input',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'entity_type_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_required',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'options',
        serial.properties.Array(
            item_types=(
                EAVAttributeOption,
            ),
        )
    ),
    ('is_user_defined', serial.properties.Boolean()),
    ('default_frontend_label', serial.properties.String()),
    (
        'frontend_labels',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        EAVAttributeFrontendLabel,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('note', serial.properties.String()),
    ('backend_type', serial.properties.String()),
    ('backend_model', serial.properties.String()),
    ('source_model', serial.properties.String()),
    ('default_value', serial.properties.String()),
    ('is_unique', serial.properties.String()),
    ('frontend_class', serial.properties.String()),
    (
        'validation_rules',
        serial.properties.Array(
            item_types=(
                EAVAttributeValidationRule,
            ),
        )
    ),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/eav-data-attribute-option-interface
serial.meta.writable(EAVAttributeOption).properties = [
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('sort_order', serial.properties.Integer()),
    ('is_default', serial.properties.Boolean()),
    (
        'store_labels',
        serial.properties.Array(
            item_types=(
                EAVAttributeOptionLabel,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/eav-data-attribute-option-label-interface
serial.meta.writable(EAVAttributeOptionLabel).properties = [
    ('store_id', serial.properties.Integer()),
    ('label', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/eav-data-attribute-frontend-label-interface
serial.meta.writable(EAVAttributeFrontendLabel).properties = [
    ('store_id', serial.properties.Integer()),
    ('label', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/eav-data-attribute-validation-rule-interface
serial.meta.writable(EAVAttributeValidationRule).properties = [
    (
        'key',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attributes/post/parameters[0]/schema
serial.meta.writable(PostProductsAttributes).properties = [
    (
        'attribute',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductAttribute,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1salesRules~1{ruleId}/put/parameters[1]/schema
serial.meta.writable(PutSalesRules).properties = [
    (
        'rule',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesRule,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1coupons~1deleteByIds/post/parameters[0]/schema
serial.meta.writable(PostCouponsDeleteByIds).properties = [
    (
        'ids',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'ignore_invalid_coupons',
        serial.properties.Boolean(
            name='ignoreInvalidCoupons',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-rule-data-coupon-mass-delete-result-interface
serial.meta.writable(SalesRuleCouponMassDeleteResult).properties = [
    (
        'failed_items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'missing_items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1company~1{companyId}/put/parameters[1]/schema
serial.meta.writable(PutCompany).properties = [
    (
        'company',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Company,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1hierarchy~1move~1{id}/put/parameters[1]/schema
serial.meta.writable(PutHierarchyMove).properties = [
    (
        'new_parent_id',
        serial.properties.Property(
            name='newParentId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1company~1assignRoles/put/parameters[0]/schema
serial.meta.writable(PutCompanyAssignRoles).properties = [
    (
        'user_id',
        serial.properties.Property(
            name='userId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'roles',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CompanyRole,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1companyCredits~1{id}/put/parameters[1]/schema
serial.meta.writable(PutCompanyCredits).properties = [
    (
        'credit_limit',
        serial.properties.Property(
            name='creditLimit',
            types=(
                serial.properties.Property(
                    types=(
                        CompanyCreditLimit,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-credit-data-credit-limit-interface
serial.meta.writable(CompanyCreditLimit).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('company_id', serial.properties.Integer()),
    ('credit_limit', serial.properties.Number()),
    ('balance', serial.properties.Number()),
    ('currency_code', serial.properties.String()),
    (
        'exceed_limit',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('available_limit', serial.properties.Number()),
    ('credit_comment', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1media/post/parameters[1]/schema
serial.meta.writable(PostProductsMedia).properties = [
    (
        'entry',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductAttributeMediaGalleryEntry,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1tier-prices/put/parameters[0]/schema
serial.meta.writable(PutProductsTierPrices).properties = [
    (
        'prices',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        TierPrice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-tier-price-interface
serial.meta.writable(TierPrice).properties = [
    (
        'price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'customer_group',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'quantity',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1tier-prices/post/parameters[0]/schema
serial.meta.writable(PostProductsTierPrices).properties = [
    (
        'prices',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        TierPrice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1base-prices/post/parameters[0]/schema
serial.meta.writable(PostProductsBasePrices).properties = [
    (
        'prices',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        BasePrice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-base-price-interface
serial.meta.writable(BasePrice).properties = [
    (
        'price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1cost-delete/post/parameters[0]/schema
serial.meta.writable(PostProductsCostDelete).properties = [
    (
        'skus',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-link-type-interface
serial.meta.writable(ProductLinkType).properties = [
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1links/put/parameters[1]/schema
serial.meta.writable(PutProductsLinks).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1links/post/parameters[1]/schema
serial.meta.writable(PostProductsLinks).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        ProductLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-render-search-results-interface
serial.meta.writable(ProductRenderSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        ProductRender,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-product-render-interface
serial.meta.writable(ProductRender).properties = [
    (
        'add_to_cart_button',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductRenderButton,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'add_to_compare_button',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductRenderButton,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_info',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductRenderPriceInfo,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'images',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        ProductRenderImage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'id_',
        serial.properties.Property(
            name='id',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'type_',
        serial.properties.Property(
            name='type',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_salable',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'currency_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductRenderExtension,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-render-button-interface
serial.meta.writable(ProductRenderButton).properties = [
    (
        'post_data',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'required_options',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-render-price-info-interface
serial.meta.writable(ProductRenderPriceInfo).properties = [
    (
        'final_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'max_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'max_regular_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'minimal_regular_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'special_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'minimal_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'regular_price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'formatted_prices',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductRenderFormattedPriceInfo,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                ProductRenderPriceInfoExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-render-formatted-price-info-interface
serial.meta.writable(ProductRenderFormattedPriceInfo).properties = [
    (
        'final_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'max_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'minimal_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'max_regular_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'minimal_regular_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'special_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'regular_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-render-price-info-extension-interface
serial.meta.writable(ProductRenderPriceInfoExtension).properties = [
    (
        'msrp',
        serial.properties.Property(
            types=(
                MsrpProductRenderPriceInfo,
            ),
        )
    ),
    (
        'tax_adjustments',
        serial.properties.Property(
            types=(
                ProductRenderPriceInfo,
            ),
        )
    ),
    (
        'weee_attributes',
        serial.properties.Array(
            item_types=(
                WeeeProductRenderAdjustmentAttribute,
            ),
        )
    ),
    ('weee_adjustment', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/msrp-data-product-render-msrp-price-info-interface
serial.meta.writable(MsrpProductRenderPriceInfo).properties = [
    (
        'msrp_price',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_applicable',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_shown_price_on_gesture',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'msrp_message',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'explanation_message',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/weee-data-product-render-weee-adjustment-attribute-interface
serial.meta.writable(WeeeProductRenderAdjustmentAttribute).properties = [
    (
        'amount',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'tax_amount',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'tax_amount_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'amount_excl_tax',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'attribute_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                serial.properties.Dictionary(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-render-image-interface
serial.meta.writable(ProductRenderImage).properties = [
    (
        'url',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'height',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'width',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'resized_width',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'resized_height',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-render-extension-interface
serial.meta.writable(ProductRenderExtension).properties = [
    (
        'wishlist_button',
        serial.properties.Property(
            types=(
                ProductRenderButton,
            ),
        )
    ),
    ('review_html', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-inventory-data-stock-status-collection-interface
serial.meta.writable(InventoryStockStatusCollection).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        InventoryStockStatus,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        InventoryStockStatusCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-inventory-data-stock-status-interface
serial.meta.writable(InventoryStockStatus).properties = [
    (
        'product_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'stock_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'stock_status',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'stock_item',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        InventoryStockItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-inventory-stock-status-criteria-interface
serial.meta.writable(InventoryStockStatusCriteria).properties = [
    (
        'mapper_interface_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'criteria_list',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Criteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'filters',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'orders',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'limit',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-criteria-interface
serial.meta.writable(Criteria).properties = [
    (
        'mapper_interface_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'criteria_list',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Criteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'filters',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'orders',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'limit',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1order/put/parameters[1]/schema
serial.meta.writable(PutCartsOrder).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                QuotePayment,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}/put/parameters[1]/schema
serial.meta.writable(PutGuestCarts).properties = [
    (
        'customer_id',
        serial.properties.Property(
            name='customerId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            name='storeId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-order-status-history-search-result-interface
serial.meta.writable(SalesOrderStatusHistorySearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesOrderStatusHistory,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1orders~1{id}~1comments/post/parameters[1]/schema
serial.meta.writable(PostOrdersComments).properties = [
    (
        'status_history',
        serial.properties.Property(
            name='statusHistory',
            types=(
                serial.properties.Property(
                    types=(
                        SalesOrderStatusHistory,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1order~1{orderId}~1ship/post/parameters[1]/schema
serial.meta.writable(PostOrderShip).properties = [
    (
        'items',
        serial.properties.Array(
            item_types=(
                SalesShipmentItemCreation,
            ),
        )
    ),
    ('notify', serial.properties.Boolean()),
    (
        'append_comment',
        serial.properties.Boolean(
            name='appendComment',
        )
    ),
    (
        'comment',
        serial.properties.Property(
            types=(
                SalesShipmentCommentCreation,
            ),
        )
    ),
    (
        'tracks',
        serial.properties.Array(
            item_types=(
                SalesShipmentTrackCreation,
            ),
        )
    ),
    (
        'packages',
        serial.properties.Array(
            item_types=(
                SalesShipmentPackageCreation,
            ),
        )
    ),
    (
        'arguments',
        serial.properties.Property(
            types=(
                SalesShipmentCreationArguments,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipment-item-creation-interface
serial.meta.writable(SalesShipmentItemCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'order_item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipment-comment-creation-interface
serial.meta.writable(SalesShipmentCommentCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipment-track-creation-interface
serial.meta.writable(SalesShipmentTrackCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'track_number',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'carrier_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipment-package-creation-interface
serial.meta.writable(SalesShipmentPackageCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipment-creation-arguments-interface
serial.meta.writable(SalesShipmentCreationArguments).properties = [
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1taxClasses~1{classId}/put/parameters[1]/schema
serial.meta.writable(PutTaxClasses).properties = [
    (
        'tax_class',
        serial.properties.Property(
            name='taxClass',
            types=(
                serial.properties.Property(
                    types=(
                        TaxClass,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1giftCards/post/parameters[0]/schema
serial.meta.writable(PostCartsMineGiftCards).properties = [
    (
        'gift_card_account_data',
        serial.properties.Property(
            name='giftCardAccountData',
            types=(
                serial.properties.Property(
                    types=(
                        GiftCardAccountInterface,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/gift-card-account-data-gift-card-account-interface
serial.meta.writable(GiftCardAccountInterface).properties = [
    (
        'gift_cards',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'gift_cards_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_gift_cards_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'gift_cards_amount_used',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_gift_cards_amount_used',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/customer-data-group-search-results-interface
serial.meta.writable(CustomerGroupSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CustomerGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1me~1activate/put/parameters[0]/schema
serial.meta.writable(PutMeActivate).properties = [
    (
        'confirmation_key',
        serial.properties.Property(
            name='confirmationKey',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1me~1password/put/parameters[0]/schema
serial.meta.writable(PutMePassword).properties = [
    (
        'current_password',
        serial.properties.Property(
            name='currentPassword',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'new_password',
        serial.properties.Property(
            name='newPassword',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-category-attribute-search-results-interface
serial.meta.writable(CategoryAttributeSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CategoryAttribute,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-category-attribute-interface
serial.meta.writable(CategoryAttribute).properties = [
    ('is_wysiwyg_enabled', serial.properties.Boolean()),
    ('is_html_allowed_on_front', serial.properties.Boolean()),
    ('used_for_sort_by', serial.properties.Boolean()),
    ('is_filterable', serial.properties.Boolean()),
    ('is_filterable_in_search', serial.properties.Boolean()),
    ('is_used_in_grid', serial.properties.Boolean()),
    ('is_visible_in_grid', serial.properties.Boolean()),
    ('is_filterable_in_grid', serial.properties.Boolean()),
    ('position', serial.properties.Integer()),
    (
        'apply_to',
        serial.properties.Array(
            item_types=(
                str,
            ),
        )
    ),
    ('is_searchable', serial.properties.String()),
    ('is_visible_in_advanced_search', serial.properties.String()),
    ('is_comparable', serial.properties.String()),
    ('is_used_for_promo_rules', serial.properties.String()),
    ('is_visible_on_front', serial.properties.String()),
    ('used_in_product_listing', serial.properties.String()),
    ('is_visible', serial.properties.Boolean()),
    ('scope', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary()),
    ('attribute_id', serial.properties.Integer()),
    (
        'attribute_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'frontend_input',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'entity_type_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_required',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'options',
        serial.properties.Array(
            item_types=(
                EAVAttributeOption,
            ),
        )
    ),
    ('is_user_defined', serial.properties.Boolean()),
    ('default_frontend_label', serial.properties.String()),
    (
        'frontend_labels',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        EAVAttributeFrontendLabel,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('note', serial.properties.String()),
    ('backend_type', serial.properties.String()),
    ('backend_model', serial.properties.String()),
    ('source_model', serial.properties.String()),
    ('default_value', serial.properties.String()),
    ('is_unique', serial.properties.String()),
    ('frontend_class', serial.properties.String()),
    (
        'validation_rules',
        serial.properties.Array(
            item_types=(
                EAVAttributeValidationRule,
            ),
        )
    ),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{quoteId}~1items/post/parameters[1]/schema
serial.meta.writable(PostCartsItems).properties = [
    (
        'cart_item',
        serial.properties.Property(
            name='cartItem',
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCartItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1coupons~1deleteByCodes/post/parameters[0]/schema
serial.meta.writable(PostCouponsDeleteByCodes).properties = [
    (
        'codes',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'ignore_invalid_coupons',
        serial.properties.Boolean(
            name='ignoreInvalidCoupons',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/rma-data-comment-search-result-interface
serial.meta.writable(RMACommentSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        RMAComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1returns~1{id}~1comments/post/parameters[1]/schema
serial.meta.writable(PostReturnsComments).properties = [
    (
        'data',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        RMAComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1{customerId}/put/parameters[1]/schema
serial.meta.writable(PutCustomers).properties = [
    (
        'customer',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Customer,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'password_hash',
        serial.properties.String(
            name='passwordHash',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1special-price/post/parameters[0]/schema
serial.meta.writable(PostProductsSpecialPrice).properties = [
    (
        'prices',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SpecialPrice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/catalog-data-special-price-interface
serial.meta.writable(SpecialPrice).properties = [
    (
        'price',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_from',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_to',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-custom-option-type-interface
serial.meta.writable(ProductCustomOptionType).properties = [
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'group',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-invoice-comment-search-result-interface
serial.meta.writable(SalesInvoiceCommentSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesInvoiceComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1order~1{orderId}~1refund/post/parameters[1]/schema
serial.meta.writable(PostOrderRefund).properties = [
    (
        'items',
        serial.properties.Array(
            item_types=(
                SalesCreditMemoItemCreation,
            ),
        )
    ),
    ('notify', serial.properties.Boolean()),
    (
        'append_comment',
        serial.properties.Boolean(
            name='appendComment',
        )
    ),
    (
        'comment',
        serial.properties.Property(
            types=(
                SalesCreditMemoCommentCreation,
            ),
        )
    ),
    (
        'arguments',
        serial.properties.Property(
            types=(
                SalesCreditMemoCreationArguments,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-item-creation-interface
serial.meta.writable(SalesCreditMemoItemCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'order_item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-comment-creation-interface
serial.meta.writable(SalesCreditMemoCommentCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-creation-arguments-interface
serial.meta.writable(SalesCreditMemoCreationArguments).properties = [
    ('shipping_amount', serial.properties.Number()),
    ('adjustment_positive', serial.properties.Number()),
    ('adjustment_negative', serial.properties.Number()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                SalesCreditMemoCreationArgumentsExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-creation-arguments-extension-interface
serial.meta.writable(SalesCreditMemoCreationArgumentsExtension).properties = [
    (
        'return_to_stock_items',
        serial.properties.Array(
            item_types=(
                int,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-shipment-comment-search-result-interface
serial.meta.writable(SalesShipmentCommentSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesShipmentComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1shipment~1{id}~1comments/post/parameters[1]/schema
serial.meta.writable(PostShipmentComments).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesShipmentComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-credit-data-history-search-results-interface
serial.meta.writable(CompanyCreditHistorySearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CompanyCreditHistory,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-credit-data-history-data-interface
serial.meta.writable(CompanyCreditHistory).properties = [
    (
        'id_',
        serial.properties.Integer(
            name='id',
        )
    ),
    ('company_credit_id', serial.properties.Integer()),
    ('user_id', serial.properties.Integer()),
    ('user_type', serial.properties.Integer()),
    ('currency_credit', serial.properties.String()),
    ('currency_operation', serial.properties.String()),
    (
        'rate',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('rate_credit', serial.properties.Number()),
    (
        'amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'balance',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'credit_limit',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    ('available_limit', serial.properties.Number()),
    (
        'type_',
        serial.properties.Integer(
            name='type',
        )
    ),
    ('datetime', serial.properties.String()),
    ('purchase_order', serial.properties.String()),
    ('comment', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/eav-data-attribute-set-search-results-interface
serial.meta.writable(EAVAttributeSetSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        EAVAttributeSet,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1resetPassword/post/parameters[0]/schema
serial.meta.writable(PostCustomersResetPassword).properties = [
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'reset_token',
        serial.properties.Property(
            name='resetToken',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'new_password',
        serial.properties.Property(
            name='newPassword',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attribute-sets/post/parameters[0]/schema
serial.meta.writable(PostProductsAttributeSets).properties = [
    (
        'attribute_set',
        serial.properties.Property(
            name='attributeSet',
            types=(
                serial.properties.Property(
                    types=(
                        EAVAttributeSet,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'skeleton_id',
        serial.properties.Property(
            name='skeletonId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1websites/put/parameters[1]/schema
serial.meta.writable(PutProductsWebsites).properties = [
    (
        'product_website_link',
        serial.properties.Property(
            name='productWebsiteLink',
            types=(
                serial.properties.Property(
                    types=(
                        ProductWebsiteLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-website-link-interface
serial.meta.writable(ProductWebsiteLink).properties = [
    (
        'sku',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1websites/post/parameters[1]/schema
serial.meta.writable(PostProductsWebsites).properties = [
    (
        'product_website_link',
        serial.properties.Property(
            name='productWebsiteLink',
            types=(
                serial.properties.Property(
                    types=(
                        ProductWebsiteLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1order~1{orderId}~1invoice/post/parameters[1]/schema
serial.meta.writable(PostOrderInvoice).properties = [
    ('capture', serial.properties.Boolean()),
    (
        'items',
        serial.properties.Array(
            item_types=(
                SalesInvoiceItemCreation,
            ),
        )
    ),
    ('notify', serial.properties.Boolean()),
    (
        'append_comment',
        serial.properties.Boolean(
            name='appendComment',
        )
    ),
    (
        'comment',
        serial.properties.Property(
            types=(
                SalesInvoiceCommentCreation,
            ),
        )
    ),
    (
        'arguments',
        serial.properties.Property(
            types=(
                SalesInvoiceCreationArguments,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-invoice-item-creation-interface
serial.meta.writable(SalesInvoiceItemCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'order_item_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'qty',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-invoice-comment-creation-interface
serial.meta.writable(SalesInvoiceCommentCreation).properties = [
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_visible_on_front',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-invoice-creation-arguments-interface
serial.meta.writable(SalesInvoiceCreationArguments).properties = [
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1integration~1admin~1token/post/parameters[0]/schema
serial.meta.writable(PostAdminToken).properties = [
    (
        'username',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'password',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiableQuote~1request/post/parameters[0]/schema
serial.meta.writable(PostNegotiableQuoteRequest).properties = [
    (
        'quote_id',
        serial.properties.Property(
            name='quoteId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'quote_name',
        serial.properties.Property(
            name='quoteName',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('comment', serial.properties.String()),
    (
        'files',
        serial.properties.Array(
            item_types=(
                NegotiableQuoteAttachment,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-attachment-content-interface
serial.meta.writable(NegotiableQuoteAttachment).properties = [
    (
        'base_64_encoded_data',
        serial.properties.Property(
            name='base64_encoded_data',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'type_',
        serial.properties.Property(
            name='type',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiableQuote~1decline/post/parameters[0]/schema
serial.meta.writable(PostNegotiableQuoteDecline).properties = [
    (
        'quote_id',
        serial.properties.Property(
            name='quoteId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'reason',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1gift-message/post/parameters[0]/schema
serial.meta.writable(PostCartsMineGiftMessage).properties = [
    (
        'gift_message',
        serial.properties.Property(
            name='giftMessage',
            types=(
                serial.properties.Property(
                    types=(
                        GiftMessage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/sales-data-creditmemo-comment-search-result-interface
serial.meta.writable(SalesCreditMemoCommentSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SalesCreditMemoComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1creditmemo~1{id}~1comments/post/parameters[1]/schema
serial.meta.writable(PostCreditMemoComments).properties = [
    (
        'entity',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SalesCreditMemoComment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1giftCards/put/parameters[1]/schema
serial.meta.writable(PutCartsGiftCards).properties = [
    (
        'gift_card_account_data',
        serial.properties.Property(
            name='giftCardAccountData',
            types=(
                serial.properties.Property(
                    types=(
                        GiftCardAccountInterface,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/customer-data-attribute-metadata-interface
serial.meta.writable(CustomerAttributeMetadata).properties = [
    (
        'frontend_input',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'input_filter',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'validation_rules',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CustomerValidationRule,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'multiline_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'visible',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'required',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'data_model',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'options',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        CustomerOption,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'frontend_class',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'user_defined',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_order',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'frontend_label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'note',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'system',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    (
        'backend_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('is_used_in_grid', serial.properties.Boolean()),
    ('is_visible_in_grid', serial.properties.Boolean()),
    ('is_filterable_in_grid', serial.properties.Boolean()),
    ('is_searchable_in_grid', serial.properties.Boolean()),
    (
        'attribute_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/customer-data-validation-rule-interface
serial.meta.writable(CustomerValidationRule).properties = [
    (
        'name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/customer-data-option-interface
serial.meta.writable(CustomerOption).properties = [
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('value', serial.properties.String()),
    (
        'options',
        serial.properties.Array(
            item_types=(
                CustomerOption,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-attribute-type-interface
serial.meta.writable(ProductAttributeType).properties = [
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1cost-information/post/parameters[0]/schema
serial.meta.writable(PostProductsCostInformation).properties = [
    (
        'skus',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1items~1{itemId}/put/parameters[1]/schema
serial.meta.writable(PutCartsMineItems).properties = [
    (
        'cart_item',
        serial.properties.Property(
            name='cartItem',
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCartItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1collect-totals/put/parameters[0]/schema
serial.meta.writable(PutCartsMineCollectTotals).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'shipping_carrier_code',
        serial.properties.String(
            name='shippingCarrierCode',
        )
    ),
    (
        'shipping_method_code',
        serial.properties.String(
            name='shippingMethodCode',
        )
    ),
    (
        'additional_data',
        serial.properties.Property(
            name='additionalData',
            types=(
                QuoteTotalsAdditional,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-totals-additional-data-interface
serial.meta.writable(QuoteTotalsAdditional).properties = [
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                QuoteTotalsAdditionalExtension,
            ),
        )
    ),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/quote-data-totals-additional-data-extension-interface
serial.meta.writable(QuoteTotalsAdditionalExtension).properties = [
    (
        'gift_messages',
        serial.properties.Array(
            item_types=(
                GiftMessage,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiableQuote~1{quoteId}/put/parameters[1]/schema
serial.meta.writable(PutNegotiableQuote).properties = [
    (
        'quote',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCart,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1{email}~1activate/put/parameters[1]/schema
serial.meta.writable(PutCustomersActivate).properties = [
    (
        'confirmation_key',
        serial.properties.Property(
            name='confirmationKey',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1customers~1isEmailAvailable/post/parameters[0]/schema
serial.meta.writable(PostCustomersIsEmailAvailable).properties = [
    (
        'customer_email',
        serial.properties.Property(
            name='customerEmail',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'website_id',
        serial.properties.Integer(
            name='websiteId',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1order/put/parameters[1]/schema
serial.meta.writable(PutGuestCartsOrder).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                QuotePayment,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1items/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsItems).properties = [
    (
        'cart_item',
        serial.properties.Property(
            name='cartItem',
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCartItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-payment-method-interface
serial.meta.writable(QuotePaymentMethod).properties = [
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'title',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1billing-address/post/parameters[0]/schema
serial.meta.writable(PostCartsMineBillingAddress).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_for_shipping',
        serial.properties.Boolean(
            name='useForShipping',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1invoice~1{invoiceId}~1refund/post/parameters[1]/schema
serial.meta.writable(PostInvoiceRefund).properties = [
    (
        'items',
        serial.properties.Array(
            item_types=(
                SalesCreditMemoItemCreation,
            ),
        )
    ),
    (
        'is_online',
        serial.properties.Boolean(
            name='isOnline',
        )
    ),
    ('notify', serial.properties.Boolean()),
    (
        'append_comment',
        serial.properties.Boolean(
            name='appendComment',
        )
    ),
    (
        'comment',
        serial.properties.Property(
            types=(
                SalesCreditMemoCommentCreation,
            ),
        )
    ),
    (
        'arguments',
        serial.properties.Property(
            types=(
                SalesCreditMemoCreationArguments,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1integration~1customer~1token/post/parameters[0]/schema
serial.meta.writable(PostCustomerToken).properties = [
    (
        'username',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'password',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1tier-prices-delete/post/parameters[0]/schema
serial.meta.writable(PostProductsTierPricesDelete).properties = [
    (
        'prices',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        TierPrice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1options~1{optionId}/put/parameters[1]/schema
serial.meta.writable(PutProductsOptions).properties = [
    (
        'option',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductCustomOption,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1bundle-products~1options~1add/post/parameters[0]/schema
serial.meta.writable(PostBundleProductsOptionsAdd).properties = [
    (
        'option',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        BundleOptionInterface,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/quote-data-shipping-method-interface
serial.meta.writable(QuoteShippingMethod).properties = [
    (
        'carrier_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'method_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('carrier_title', serial.properties.String()),
    ('method_title', serial.properties.String()),
    (
        'amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'base_amount',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'available',
        serial.properties.Property(
            types=(
                serial.properties.Boolean(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'error_message',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_excl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'price_incl_tax',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1gift-message/post/parameters[1]/schema
serial.meta.writable(PostCartsGiftMessage).properties = [
    (
        'gift_message',
        serial.properties.Property(
            name='giftMessage',
            types=(
                serial.properties.Property(
                    types=(
                        GiftMessage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1gift-wrappings~1{wrappingId}/put/parameters[1]/schema
serial.meta.writable(PutGiftWrappings).properties = [
    (
        'data',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        GiftWrapping,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'store_id',
        serial.properties.Integer(
            name='storeId',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1categories~1{categoryId}~1move/put/parameters[1]/schema
serial.meta.writable(PutCategoriesMove).properties = [
    (
        'parent_id',
        serial.properties.Property(
            name='parentId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'after_id',
        serial.properties.Integer(
            name='afterId',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1special-price-delete/post/parameters[0]/schema
serial.meta.writable(PostProductsSpecialPriceDelete).properties = [
    (
        'prices',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        SpecialPrice,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/bundle-data-option-type-interface
serial.meta.writable(BundleOptionType).properties = [
    (
        'label',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1items~1{itemId}/put/parameters[2]/schema
serial.meta.writable(PutCartsItems).properties = [
    (
        'cart_item',
        serial.properties.Property(
            name='cartItem',
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCartItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1totals-information/post/parameters[0]/schema
serial.meta.writable(PostCartsMineTotalsInformation).properties = [
    (
        'address_information',
        serial.properties.Property(
            name='addressInformation',
            types=(
                serial.properties.Property(
                    types=(
                        CheckoutTotalsInformation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/checkout-data-totals-information-interface
serial.meta.writable(CheckoutTotalsInformation).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('shipping_method_code', serial.properties.String()),
    ('shipping_carrier_code', serial.properties.String()),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiableQuote~1pricesUpdated/post/parameters[0]/schema
serial.meta.writable(PostNegotiableQuotePricesUpdated).properties = [
    (
        'quote_ids',
        serial.properties.Property(
            name='quoteIds',
            types=(
                serial.properties.Array(
                    item_types=(
                        int,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/rma-data-track-search-result-interface
serial.meta.writable(RMATrackSearchResult).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        RMATrack,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1returns~1{id}~1tracking-numbers/post/parameters[1]/schema
serial.meta.writable(PostReturnsTrackingNumbers).properties = [
    (
        'track',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        RMATrack,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attribute-sets~1groups/post/parameters[0]/schema
serial.meta.writable(PostProductsAttributeSetsGroups).properties = [
    (
        'group',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        EAVAttributeGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/eav-data-attribute-group-interface
serial.meta.writable(EAVAttributeGroup).properties = [
    ('attribute_group_id', serial.properties.String()),
    ('attribute_group_name', serial.properties.String()),
    ('attribute_set_id', serial.properties.Integer()),
    (
        'extension_attributes',
        serial.properties.Property(
            types=(
                EAVAttributeGroupExtension,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/eav-data-attribute-group-extension-interface
serial.meta.writable(EAVAttributeGroupExtension).properties = [
    ('attribute_group_code', serial.properties.String()),
    ('sort_order', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1media~1{entryId}/put/parameters[2]/schema
serial.meta.writable(PutProductsMedia).properties = [
    (
        'entry',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductAttributeMediaGalleryEntry,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1billing-address/post/parameters[1]/schema
serial.meta.writable(PostCartsBillingAddress).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_for_shipping',
        serial.properties.Boolean(
            name='useForShipping',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/checkout-data-payment-details-interface
serial.meta.writable(CheckoutPaymentDetails).properties = [
    (
        'payment_methods',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        QuotePaymentMethod,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'totals',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteTotals,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1payment-information/post/parameters[0]/schema
serial.meta.writable(PostCartsMinePaymentInformation).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            name='billingAddress',
            types=(
                QuoteAddress,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1shipping-information/post/parameters[0]/schema
serial.meta.writable(PostCartsMineShippingInformation).properties = [
    (
        'address_information',
        serial.properties.Property(
            name='addressInformation',
            types=(
                serial.properties.Property(
                    types=(
                        CheckoutShippingInformation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/checkout-data-shipping-information-interface
serial.meta.writable(CheckoutShippingInformation).properties = [
    (
        'shipping_address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            types=(
                QuoteAddress,
            ),
        )
    ),
    (
        'shipping_method_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'shipping_carrier_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'custom_attributes',
        serial.properties.Array(
            item_types=(
                Attribute,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1configurable-products~1variation/put/parameters[0]/schema
serial.meta.writable(PutConfigurableProductsVariation).properties = [
    (
        'product',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        Product,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'options',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        ConfigurableProductOption,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json#/definitions/framework-metadata-object-interface
serial.meta.writable(Metadata).properties = [
    (
        'attribute_code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1tier-prices-information/post/parameters[0]/schema
serial.meta.writable(PostProductsTierPricesInformation).properties = [
    (
        'skus',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1base-prices-information/post/parameters[0]/schema
serial.meta.writable(PostProductsBasePricesInformation).properties = [
    (
        'skus',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-product-link-attribute-interface
serial.meta.writable(ProductLinkAttribute).properties = [
    (
        'code',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'type_',
        serial.properties.Property(
            name='type',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/catalog-data-category-product-link-interface
serial.meta.writable(CategoryProductLink).properties = [
    ('sku', serial.properties.String()),
    ('position', serial.properties.Integer()),
    (
        'category_id',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1categories~1{categoryId}~1products/put/parameters[1]/schema
serial.meta.writable(PutCategoriesProducts).properties = [
    (
        'product_link',
        serial.properties.Property(
            name='productLink',
            types=(
                serial.properties.Property(
                    types=(
                        CategoryProductLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1categories~1{categoryId}~1products/post/parameters[1]/schema
serial.meta.writable(PostCategoriesProducts).properties = [
    (
        'product_link',
        serial.properties.Property(
            name='productLink',
            types=(
                serial.properties.Property(
                    types=(
                        CategoryProductLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1bundle-products~1{sku}~1links~1{id}/put/parameters[2]/schema
serial.meta.writable(PutBundleProductsLinks).properties = [
    (
        'linked_product',
        serial.properties.Property(
            name='linkedProduct',
            types=(
                serial.properties.Property(
                    types=(
                        BundleLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiableQuote~1submitToCustomer/post/parameters[0]/schema
serial.meta.writable(PostNegotiableQuoteSubmitToCustomer).properties = [
    (
        'quote_id',
        serial.properties.Property(
            name='quoteId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('comment', serial.properties.String()),
    (
        'files',
        serial.properties.Array(
            item_types=(
                NegotiableQuoteAttachment,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1gift-message~1{itemId}/post/parameters[1]/schema
serial.meta.writable(GiftMessageItemRepositorySavePost).properties = [
    (
        'gift_message',
        serial.properties.Property(
            name='giftMessage',
            types=(
                serial.properties.Property(
                    types=(
                        GiftMessage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1totals-information/post/parameters[1]/schema
serial.meta.writable(PostCartsTotalsInformation).properties = [
    (
        'address_information',
        serial.properties.Property(
            name='addressInformation',
            types=(
                serial.properties.Property(
                    types=(
                        CheckoutTotalsInformation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1configurable-products~1{sku}~1child/post/parameters[1]/schema
serial.meta.writable(PostConfigurableProductsChild).properties = [
    (
        'child_sku',
        serial.properties.Property(
            name='childSku',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1downloadable-links/post/parameters[1]/schema
serial.meta.writable(PostProductsDownloadableLinks).properties = [
    (
        'link',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        DownloadableLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_global_scope_content',
        serial.properties.Boolean(
            name='isGlobalScopeContent',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog~1{id}~1assignProducts/post/parameters[1]/schema
serial.meta.writable(PostAssignProducts).properties = [
    (
        'products',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Product,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1gift-message/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsGiftMessage).properties = [
    (
        'gift_message',
        serial.properties.Property(
            name='giftMessage',
            types=(
                serial.properties.Property(
                    types=(
                        GiftMessage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attribute-sets~1attributes/post/parameters[0]/schema
serial.meta.writable(PostProductsAttributeSetsAttributes).properties = [
    (
        'attribute_set_id',
        serial.properties.Property(
            name='attributeSetId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'attribute_group_id',
        serial.properties.Property(
            name='attributeGroupId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'attribute_code',
        serial.properties.Property(
            name='attributeCode',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'sort_order',
        serial.properties.Property(
            name='sortOrder',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1special-price-information/post/parameters[0]/schema
serial.meta.writable(PostProductsSpecialPriceInformation).properties = [
    (
        'skus',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        str,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1bundle-products~1options~1{optionId}/put/parameters[1]/schema
serial.meta.writable(PutBundleProductsOptions).properties = [
    (
        'option',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        BundleOptionInterface,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1selected-payment-method/put/parameters[0]/schema
serial.meta.writable(PutCartsMineSelectedPaymentMethod).properties = [
    (
        'method',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1set-payment-information/post/parameters[0]/schema
serial.meta.writable(PostCartsMineSetPaymentInformation).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            name='billingAddress',
            types=(
                QuoteAddress,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-comment-interface
serial.meta.writable(NegotiableQuoteComment).properties = [
    (
        'entity_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'parent_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'creator_type',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_decline',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_draft',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'creator_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'comment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'created_at',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary()),
    (
        'attachments',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        NegotiableQuoteCommentAttachment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/negotiable-quote-data-comment-attachment-interface
serial.meta.writable(NegotiableQuoteCommentAttachment).properties = [
    (
        'attachment_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'comment_id',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    (
        'file_name',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'file_path',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'file_type',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    ('extension_attributes', serial.properties.Dictionary())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1companyCredits~1history~1{historyId}/put/parameters[1]/schema
serial.meta.writable(PutCompanyCreditsHistory).properties = [
    (
        'purchase_order',
        serial.properties.String(
            name='purchaseOrder',
        )
    ),
    ('comment', serial.properties.String())
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1eav~1attribute-sets~1{attributeSetId}/put/parameters[1]/schema
serial.meta.writable(PutEAVAttributeSets).properties = [
    (
        'attribute_set',
        serial.properties.Property(
            name='attributeSet',
            types=(
                serial.properties.Property(
                    types=(
                        EAVAttributeSet,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attributes~1{attributeCode}/put/parameters[1]/schema
serial.meta.writable(PutProductsAttributes).properties = [
    (
        'attribute',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ProductAttribute,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/eav-data-attribute-group-search-results-interface
serial.meta.writable(EAVAttributeGroupSearchResults).properties = [
    (
        'items',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        EAVAttributeGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'search_criteria',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        SearchCriteria,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'total_count',
        serial.properties.Property(
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1items~1{itemId}/put/parameters[2]/schema
serial.meta.writable(PutGuestCartsItems).properties = [
    (
        'cart_item',
        serial.properties.Property(
            name='cartItem',
            types=(
                serial.properties.Property(
                    types=(
                        QuoteCartItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1collect-totals/put/parameters[1]/schema
serial.meta.writable(PutGuestCartsCollectTotals).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'shipping_carrier_code',
        serial.properties.String(
            name='shippingCarrierCode',
        )
    ),
    (
        'shipping_method_code',
        serial.properties.String(
            name='shippingMethodCode',
        )
    ),
    (
        'additional_data',
        serial.properties.Property(
            name='additionalData',
            types=(
                QuoteTotalsAdditional,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1shipping-information/post/parameters[1]/schema
serial.meta.writable(PostCartsShippingInformation).properties = [
    (
        'address_information',
        serial.properties.Property(
            name='addressInformation',
            types=(
                serial.properties.Property(
                    types=(
                        CheckoutShippingInformation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1configurable-products~1{sku}~1options/post/parameters[1]/schema
serial.meta.writable(PostConfigurableProductsOptions).properties = [
    (
        'option',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ConfigurableProductOption,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiable-carts~1{cartId}~1giftCards/post/parameters[1]/schema
serial.meta.writable(PostNegotiableCartsGiftCards).properties = [
    (
        'gift_card_account_data',
        serial.properties.Property(
            name='giftCardAccountData',
            types=(
                serial.properties.Property(
                    types=(
                        GiftCardAccountInterface,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog~1{id}~1unassignProducts/post/parameters[1]/schema
serial.meta.writable(PostUnassignProducts).properties = [
    (
        'products',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Product,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog~1{id}~1assignCategories/post/parameters[1]/schema
serial.meta.writable(PostAssignCategories).properties = [
    (
        'categories',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Category,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1estimate-shipping-methods/post/parameters[0]/schema
serial.meta.writable(PostCartsMineEstimateShippingMethods).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1billing-address/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsBillingAddress).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_for_shipping',
        serial.properties.Boolean(
            name='useForShipping',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1guest-carts~1{cartId}~1giftCards/post/parameters[1]/schema
serial.meta.writable(PostCartsGuestGiftCards).properties = [
    (
        'gift_card_account_data',
        serial.properties.Property(
            name='giftCardAccountData',
            types=(
                serial.properties.Property(
                    types=(
                        GiftCardAccountInterface,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1gift-message~1{itemId}/post/parameters[2]/schema
serial.meta.writable(GiftMessageItemRepositorySavePost1).properties = [
    (
        'gift_message',
        serial.properties.Property(
            name='giftMessage',
            types=(
                serial.properties.Property(
                    types=(
                        GiftMessage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog~1{id}~1unassignCategories/post/parameters[1]/schema
serial.meta.writable(PostUnassignCategories).properties = [
    (
        'categories',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Category,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1bundle-products~1{sku}~1links~1{optionId}/post/parameters[2]/schema
serial.meta.writable(PostBundleProductsLinks).properties = [
    (
        'linked_product',
        serial.properties.Property(
            name='linkedProduct',
            types=(
                serial.properties.Property(
                    types=(
                        BundleLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1selected-payment-method/put/parameters[1]/schema
serial.meta.writable(PutCartsSelectedPaymentMethod).properties = [
    (
        'method',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1downloadable-links~1{id}/put/parameters[2]/schema
serial.meta.writable(PutProductsDownloadableLinks).properties = [
    (
        'link',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        DownloadableLink,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_global_scope_content',
        serial.properties.Boolean(
            name='isGlobalScopeContent',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1totals-information/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsTotalsInformation).properties = [
    (
        'address_information',
        serial.properties.Property(
            name='addressInformation',
            types=(
                serial.properties.Property(
                    types=(
                        CheckoutTotalsInformation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attribute-sets~1{attributeSetId}/put/parameters[1]/schema
serial.meta.writable(PutProductsAttributeSets).properties = [
    (
        'attribute_set',
        serial.properties.Property(
            name='attributeSet',
            types=(
                serial.properties.Property(
                    types=(
                        EAVAttributeSet,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
serial.meta.writable(PostCartsEstimateShippingMethods).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1payment-information/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsPaymentInformation).properties = [
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            name='billingAddress',
            types=(
                QuoteAddress,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1configurable-products~1{sku}~1options~1{id}/put/parameters[2]/schema
serial.meta.writable(PutConfigurableProductsOptions).properties = [
    (
        'option',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        ConfigurableProductOption,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiableQuote~1{quoteId}~1shippingMethod/put/parameters[1]/schema
serial.meta.writable(PutNegotiableQuoteShippingMethod).properties = [
    (
        'shipping_method',
        serial.properties.Property(
            name='shippingMethod',
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{productSku}~1stockItems~1{itemId}/put/parameters[2]/schema
serial.meta.writable(PutProductsStockItems).properties = [
    (
        'stock_item',
        serial.properties.Property(
            name='stockItem',
            types=(
                serial.properties.Property(
                    types=(
                        InventoryStockItem,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1shipping-information/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsShippingInformation).properties = [
    (
        'address_information',
        serial.properties.Property(
            name='addressInformation',
            types=(
                serial.properties.Property(
                    types=(
                        CheckoutShippingInformation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1downloadable-links~1samples/post/parameters[1]/schema
serial.meta.writable(PostProductsDownloadableLinksSamples).properties = [
    (
        'sample',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        DownloadableSample,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_global_scope_content',
        serial.properties.Boolean(
            name='isGlobalScopeContent',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiable-carts~1{cartId}~1billing-address/post/parameters[1]/schema
serial.meta.writable(PostNegotiableCartsBillingAddress).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'use_for_shipping',
        serial.properties.Boolean(
            name='useForShipping',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1companyCredits~1{creditId}~1increaseBalance/post/parameters[1]/schema
serial.meta.writable(PostCompanyCreditsIncreaseBalance).properties = [
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'currency',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'operation_type',
        serial.properties.Property(
            name='operationType',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('comment', serial.properties.String()),
    (
        'options',
        serial.properties.Property(
            types=(
                CompanyCreditBalanceOptions,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/definitions/company-credit-data-credit-balance-options-interface
serial.meta.writable(CompanyCreditBalanceOptions).properties = [
    (
        'purchase_order',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'order_increment',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'currency_display',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'currency_base',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1companyCredits~1{creditId}~1decreaseBalance/post/parameters[1]/schema
serial.meta.writable(PostCompanyCreditsDecreaseBalance).properties = [
    (
        'value',
        serial.properties.Property(
            types=(
                serial.properties.Number(),
                serial.properties.Null
            ),
        )
    ),
    (
        'currency',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'operation_type',
        serial.properties.Property(
            name='operationType',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    ),
    ('comment', serial.properties.String()),
    (
        'options',
        serial.properties.Property(
            types=(
                CompanyCreditBalanceOptions,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1gift-message~1{itemId}/post/parameters[2]/schema
serial.meta.writable(GiftMessageGuestItemRepositorySavePost).properties = [
    (
        'gift_message',
        serial.properties.Property(
            name='giftMessage',
            types=(
                serial.properties.Property(
                    types=(
                        GiftMessage,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attributes~1{attributeCode}~1options/post/parameters[1]/schema
serial.meta.writable(PostProductsAttributesOptions).properties = [
    (
        'option',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        EAVAttributeOption,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1giftregistry~1mine~1estimate-shipping-methods/post/parameters[0]/schema
serial.meta.writable(PostGiftRegistryMineEstimateShippingMethods).properties = [
    (
        'registry_id',
        serial.properties.Property(
            name='registryId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1selected-payment-method/put/parameters[1]/schema
serial.meta.writable(PutGuestCartsSelectedPaymentMethod).properties = [
    (
        'method',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1set-payment-information/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsSetPaymentInformation).properties = [
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            name='billingAddress',
            types=(
                QuoteAddress,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiable-carts~1{cartId}~1payment-information/post/parameters[1]/schema
serial.meta.writable(PostNegotiableCartsPaymentInformation).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            name='billingAddress',
            types=(
                QuoteAddress,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-carts~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
serial.meta.writable(PostGuestCartsEstimateShippingMethods).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1{sku}~1downloadable-links~1samples~1{id}/put/parameters[2]/schema
serial.meta.writable(PutProductsDownloadableLinksSamples).properties = [
    (
        'sample',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        DownloadableSample,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'is_global_scope_content',
        serial.properties.Boolean(
            name='isGlobalScopeContent',
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiable-carts~1{cartId}~1shipping-information/post/parameters[1]/schema
serial.meta.writable(PostNegotiableCartsShippingInformation).properties = [
    (
        'address_information',
        serial.properties.Property(
            name='addressInformation',
            types=(
                serial.properties.Property(
                    types=(
                        CheckoutShippingInformation,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1products~1attribute-sets~1{attributeSetId}~1groups/put/parameters[1]/schema
serial.meta.writable(PutProductsAttributeSetsGroups).properties = [
    (
        'group',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        EAVAttributeGroup,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog~1{sharedCatalogId}~1assignCompanies/post/parameters[1]/schema
serial.meta.writable(PostAssignCompanies).properties = [
    (
        'companies',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Company,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiable-carts~1{cartId}~1set-payment-information/post/parameters[1]/schema
serial.meta.writable(PostNegotiableCartsSetPaymentInformation).properties = [
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            name='billingAddress',
            types=(
                QuoteAddress,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1sharedCatalog~1{sharedCatalogId}~1unassignCompanies/post/parameters[1]/schema
serial.meta.writable(PostUnassignCompanies).properties = [
    (
        'companies',
        serial.properties.Property(
            types=(
                serial.properties.Array(
                    item_types=(
                        Company,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1worldpay-guest-carts~1{cartId}~1payment-information/post/parameters[1]/schema
serial.meta.writable(PostWorldpayGuestCartsPaymentInformation).properties = [
    (
        'email',
        serial.properties.Property(
            types=(
                serial.properties.String(),
                serial.properties.Null
            ),
        )
    ),
    (
        'payment_method',
        serial.properties.Property(
            name='paymentMethod',
            types=(
                serial.properties.Property(
                    types=(
                        QuotePayment,
                    ),
                ),
                serial.properties.Null
            ),
        )
    ),
    (
        'billing_address',
        serial.properties.Property(
            name='billingAddress',
            types=(
                QuoteAddress,
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1mine~1estimate-shipping-methods-by-address-id/post/parameters[0]/schema
serial.meta.writable(PostCartsMineEstimateShippingMethodsByAddressID).properties = [
    (
        'address_id',
        serial.properties.Property(
            name='addressId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiable-carts~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
serial.meta.writable(PostNegotiableCartsEstimateShippingMethods).properties = [
    (
        'address',
        serial.properties.Property(
            types=(
                serial.properties.Property(
                    types=(
                        QuoteAddress,
                    ),
                ),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1guest-giftregistry~1{cartId}~1estimate-shipping-methods/post/parameters[1]/schema
serial.meta.writable(PostGuestGiftRegistryEstimateShippingMethods).properties = [
    (
        'registry_id',
        serial.properties.Property(
            name='registryId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1carts~1{cartId}~1estimate-shipping-methods-by-address-id/post/parameters[1]/schema
serial.meta.writable(PostCartsEstimateShippingMethodsByAddressID).properties = [
    (
        'address_id',
        serial.properties.Property(
            name='addressId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]

# https://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json
# #/paths/~1V1~1negotiable-carts~1{cartId}~1estimate-shipping-methods-by-address-id/post/parameters[1]/schema
serial.meta.writable(PostNegotiableCartsEstimateShippingMethodsByAddressID).properties = [
    (
        'address_id',
        serial.properties.Property(
            name='addressId',
            types=(
                serial.properties.Integer(),
                serial.properties.Null
            ),
        )
    )
]
