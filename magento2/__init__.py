"""
Search Criteria:

    "GET" `search_criteria` parameters are structured as follows ("*" indicates that a property/parameter is
    *required*):

    (dict)

        * search_criteria (`magento2.model.SearchCriteria`):

                * filter_groups([`magento2.model.SearchFilterGroup`]):

                    * filters([`magento2.model.Filter`]):

                        * field (`str`)

                        * value (`str`)

                        - condition_type (`str`):

                            Values:

                                - "eq": Equals `value` (default)

                                - "neq": Not equal to `value`

                                - "gt": Greater than `value`

                                - "gteq": Greater than or equal to `value`

                                - "lt": Less than `value`

                                - "lteq": Less than or equal to `value`

                                - "like": Perform a MySQL `LIKE` pattern match against `value`

                                - "notnull": Not null

                                - "null": Null

                                - "from": Indicates that `value` is the lower limit of a value range. Use this in
                                  concert with another filter where the `condition_type` is "to".

                                - "to": Indicates that `value` is the upper limit of a value range. Use this in
                                  concert with another filter where the `condition_type` is "from".

                                - "in": Match any item in `value`, where `value` is a comma-separated list

                                - "nin": Match any item not in `value`, where `value` is a comma-separated list

                                - "moreq": More or equal to `value` (???)

                                - "finset": A value within a set of values (???)

                - sort_orders ([`magento2.model.SortOrder`]):

                    - field (`str`)

                    - direction (`str`):

                        - "ASC": Return values in ascending order.

                        - "DESC":  Return values in descending order.

                - page_size (`int`)

                - current_page (`int`)
"""

# region Backwards Compatibility
from __future__ import absolute_import, division, generators, nested_scopes, print_function, unicode_literals,\
    with_statement

import http
import logging
from copy import deepcopy, copy

from future import standard_library

import serial.abc
import serial.marshal

standard_library.install_aliases()
from builtins import *
# endregion

import collections
from urllib.parse import urlencode, quote, quote_plus

from magento2 import errors, model

import inspect
import json
import re
import ssl
import urllib
from collections import OrderedDict, Sequence, Set
from http.client import HTTPResponse
from io import BytesIO

import os

import oapi
import serial
from serial.model import Array
from serial.utilities import qualified_name, calling_function_qualified_name
from serial.abc.model import Model

try:
    from typing import Union, Dict, Any, Optional, Sequence, Set, Callable
except ImportError:
    typing = Union = Any = Sequence = Set = None


def deep_object_query(data, quote_via=None):
    # type: (Union[str, serial.model.Model], Optional[Callable]) -> str

    def parameters_values(n, d):
        # type: (str, Union[dict, Sequence, Set]) -> Dict[str, str]
        q = OrderedDict()
        if isinstance(d, dict):
            for k, v in d.items():
                pn = '%s[%s]' % (n, k)
                for kk, vv in parameters_values(pn, v).items():
                    q[kk] = vv
        elif isinstance(
            d,
            (collections.Sequence, collections.Set)
        ) and not isinstance(d, (str, bytes)):
            for i in range(len(d)):
                pn = '%s[%s]' % (n, str(i))
                for kk, vv in parameters_values(pn, d[i]).items():
                    q[kk] = vv
        elif d is not None:
            q[n] = d
        return q

    q = OrderedDict()
    if not isinstance(data, Model):
        data = serial.model.Dictionary(data)
    for k, v in serial.marshal.marshal(data).items():
        q = parameters_values(k, v)
    if quote_via is None:
        quote_via = quote_plus
    return urlencode(q, safe='[]{}":', quote_via=quote_via)


_UNVERIFIED_CONTEXT = ssl.create_default_context()
_UNVERIFIED_CONTEXT.check_hostname = False
_UNVERIFIED_CONTEXT.verify_mode = ssl.CERT_NONE


class Magento(object):
    """
    Establishes and authenticates a connection to Magento.

    Parameters:

        - server (str): The base URL of the Magento instance (including protocol). For example:
          "https://sub-domain.domain.com".

        - authentication (magento2.model.PostCustomerToken|magento2.model.PostAdminToken):
          Credentials for either a customer (`magento2.model.PostCustomerToken`) or an administrative interface
          user (`magento2.model.PostAdminToken`).

        - verify_ssl_certificate (bool): Defaults to `True`. When `True` , HTTPS requests will require a valid SSL
          certificate, registered to application's host. Only specify `False` for testing purposes--*do not* use this
          option in production applications.

    Example:

        >>> magento = Magento(\
            server=os.environ['MAGENTO_SERVER'],\
            authentication=model.PostAdminToken(\
                username=username,\
                password=password\
            ),\
            echo=True\
        )
    """

    def __init__(
        self,
        server=None, # type: Optional[str]
        authentication=None,  # type: Optional[Union[model.PostCustomerToken, model.PostAdminToken]]
        verify_ssl_certificate=True,  # type: bool
        logger=None,  # type: Optional[logging.Logger]
        echo=False
    ):
        self.logger = logger
        self.token = None
        self.server = server
        self.verify_ssl_certificate = verify_ssl_certificate
        self.echo = echo
        self._authentication = None
        self.authentication = authentication  # type: Optional[Union[model.AdminToken, model.CustomerToken]]

    @property
    def authentication(self):
        return self._authentication

    @authentication.setter
    def authentication(self, authentication):
        # type: (Union[model.PostAdminToken]) -> None
        if authentication is None:
            self._authentication = authentication
        else:
            if authentication is not self._authentication:
                self._authentication = authentication
                self.login()

    def login(self):
        if isinstance(self._authentication, model.PostAdminToken):
            response = self.request(
                'V1/integration/admin/token',
                data=self._authentication,
                _reauthenticate=False
            )
        elif isinstance(self._authentication, model.PostCustomerToken):
            response = self.request(
                'V1/integration/customer/token',
                data=self._authentication,
                _reauthenticate=False
            )
        self.token = json.loads(str(response.read(), encoding='utf-8'))

    def request(
        self,
        resource,  # type: str
        data=None,  # type: Optional[Union[str, Dict, serial.model.Object, Sequence[serial.model.Object]]]
        method='POST',  # typer: str
        timeout=None,  # type: Optional[int]
        echo=None,  # type: Optional[bool]
        _reauthenticate=True  # type: bool
    ):
        if echo is None:
            echo = self.echo
        log_request_data = None
        if isinstance(data, dict):
            data = json.dumps(data)
        elif isinstance(data, Model):
            if isinstance(data, (model.PostAdminToken, model.PostCustomerToken)):
                log_request_data = deepcopy(data)
                log_request_data.password = '*****'
                log_request_data = serial.marshal.serialize(log_request_data, 'json')
            data = serial.marshal.serialize(data, 'json')
        if isinstance(data, str):
            data = bytes(data, encoding='utf-8')
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        if self.token is not None:
            headers['Authorization'] = 'Bearer ' + self.token
        url = '%s/rest/%s' % (self.server, resource)
        request = urllib.request.Request(
            url,
            data=data,
            headers=headers,
            method=method
        )

        def get_request_text():
            return (
                ('\n    %s: %s\n\n    Headers:\n\n        ' % (request.get_method(), url)) +
                '\n        '.join(tuple(
                    '%s: %s' % (
                        k,
                        v.split(' ')[0] + ' *****' if k == 'Authorization' else v
                    )
                    for k, v in request.header_items()
                    if k != 'Proxy-authorization'
                )) + (
                    (
                        '\n\n    Body:\n\n        ' +
                        '\n        '.join(
                            re.sub(
                                r'("base64_encoded_data"\s*:\s*)"[^"]+"',
                                r'\1"..."',
                                (
                                    log_request_data or
                                    str(request.data, encoding='utf-8')
                                )
                            ).split('\n')
                        )
                    )
                    if request.data is not None
                    else ''
                ) + '\n'
            )

        try:
            kw = {}
            if (not self.verify_ssl_certificate) and ('https:' in self.server.lower()):
                kw.update(context=_UNVERIFIED_CONTEXT)
            if timeout:
                kw.update(timeout=timeout)
            if echo or (self.logger is not None):
                request_text = (
                    'Request:\n%s\n' %
                    get_request_text()
                )
                if self.logger is not None:
                    self.logger.info(request_text)
                if echo:
                    print(request_text)
            response = urllib.request.urlopen(
                request,
                **kw
            )  # type: HTTPResponse
            if echo or (self.logger is not None):
                rd = response.read()
                s = str(rd, 'utf-8')
                new_response = BytesIO(rd)
                new_response.url = response.url
                response = new_response
                response_text = (
                    'Response:\n\n    ' +
                    '\n    '.join(
                        re.sub(
                            r'("base64_encoded_data"\s*:\s*)"[^"]+"',
                            r'\1"..."',
                            s
                        ).split('\n')
                    ) +
                    '\n\n'
                )
                if self.logger is not None:
                    self.logger.info(response_text)
                if echo:
                    print(response_text)
        except urllib.error.HTTPError as e:
            if e.code == 401 and _reauthenticate:
                # Re-authenticate when an HTTP 401 error ("Unauthorized") is returned
                self.login()
                return self.request(
                    resource,
                    data=data,
                    method=method,
                    timeout=timeout,
                    echo=echo,
                    _reauthenticate=False
                )
            e = errors.MagentoError(e)
            e.msg = (
                '\n\nRequest:\n' +
                re.sub(
                    r'("base64_encoded_data"\s*:\s*)"[^"]+"',
                    r'\1"..."',
                    get_request_text()
                ) + '\n' +
                str(e)
            )
            if self.logger is not None:
                self.logger.error(str(e))
            raise e
        return response

    def schema(
        self,
        store_code='default',  # type: str
        services=None,  # type: Optional[Union[str, Sequence[str]]]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> oapi.model.OpenAPI
        """
        :param store_code: "default", "all", or a store code.
        :param services: A list of services to retrieve schema information for, or `None` to retrieve all services.
        """
        if isinstance(services, str):
            services = (services,)
        elif (
            (not isinstance(services, collections.Sequence)) or
            (
                services and
                not isinstance(services[0], str)
            )
        ):
            raise TypeError(
                'The parameter `services` for `%`, must be a sequence of `str`, not %s' % (
                    calling_function_qualified_name(),
                    repr(services)
                )
            )
        resource = store_code + '/schema'
        if services is not None:
            resource += '?' + urlencode({'services': ','.join(services)})
        response = self.request(
            method='GET',
            resource=resource,
            timeout=timeout
        )
        return oapi.model.OpenAPI(response)

    @property
    def modules(self):
        # type: () -> Sequence[str]
        response = self.request(
            resource='V1/modules',
            method='GET'
        )
        return Array(response.read())

    def orders(
        self,
        get=None,  # Optional[int]
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> model.SalesOrderSearchResult
        """
        http://devdocs.magento.com/guides/v2.2/howdoi/webapi/search-criteria.html
        """
        if get is not None:
            return model.SalesOrder(
                self.request(
                    resource='V1/orders/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif search_criteria is not None:
            return model.SalesOrderSearchResult(
                self.request(
                    resource='V1/orders' + '?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            raise ValueError(
                'Either an order ID (`get`) or search criteria (`search_criteria`) must be provided.'
            )

    def orders_create(
        self,
        put,  # type: model.SalesOrder
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> model.SalesOrder
        if not isinstance(put, model.SalesOrder):
            raise TypeError(
                'The `put` parameter must be an instance of `%s`, not `%s`.' % (
                    qualified_name(model.SalesOrder),
                    repr(put)
                )
            )
        return model.SalesOrder(
            self.request(
                resource='V1/orders/create',
                method='PUT',
                data=model.PutOrdersCreate(
                    entity=put
                ),
                timeout=timeout
            )
        )

    def order_invoice(
        self,
        order_id,  # type: int
        post=None,  # type: Optional[model.PostOrderInvoice]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> int
        if post is not None:
            if not isinstance(post, model.PostOrderInvoice):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not `%s`.' % (
                        qualified_name(model.PostOrderInvoice),
                        repr(post)
                    )
                )
        return serial.marshal.deserialize(
            self.request(
                resource='V1/order/%s/invoice' % str(order_id),
                method='POST',
                data=post,
                timeout=timeout
            ),
            'json'
        )  # type: int

    def invoices(
        self,
        get=None,  # Optional[int]
        post=None,  # Optional[SalesInvoice]
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> model.SalesInvoiceSearchResult
        """
        TODO: write tests for Magento.invoices
        http://devdocs.magento.com/guides/v2.2/howdoi/webapi/search-criteria.html
        """
        if get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not `%s`.' % repr(get)
                )
            return model.SalesInvoice(
                self.request(
                    resource='V1/invoices/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif search_criteria is not None:
            if not isinstance(search_criteria, model.SearchCriteria):
                repr_search_criteria = repr(search_criteria)
                raise TypeError(
                    'The `search_criteria` parameter must be an instance of `%s`, not%s' % (
                        qualified_name(search_criteria),
                        (
                            ': ' + repr_search_criteria
                            if '\n' in repr_search_criteria else
                            ' %s.' % repr_search_criteria
                        )
                    )
                )
            return model.SalesInvoiceSearchResult(
                self.request(
                    resource='V1/invoices' + '?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            raise ValueError(
                'Either an order ID (`get`) or search criteria (`search_criteria`) must be provided.'
            )

    def transactions(
        self,
        get=None,  # type: Optional[str]
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> serial.model.Object
        # TODO: Finish operations for `Magento.transactions`
        """
        http://devdocs.magento.com/guides/v2.2/howdoi/webapi/search-criteria.html
        """
        if get is not None:
            return model.SalesTransaction(
                self.request(
                    resource='V1/transactions/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif search_criteria is not None:
            return model.SalesTransactionSearchResult(
                self.request(
                    resource='V1/transactions' + '?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            raise ValueError(
                'Either a transaction ID (`get`) or search criteria (`search_criteria`) must be provided.'
            )

    def carts(
        self,
        get=None,  # type: Optional[int]
        put=None,  # type: Optional[int]
        customer_id=None,  # type: Optional[int]
        store_id=None,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.QuoteCart, int]
        parameter_quantity = len({id(get), id(put)} - {id(None)})
        if parameter_quantity > 1:
            raise ValueError(
                'Only *one* of the following parameters may be used for `%s`: `search_criteria`, `get`, or `put`' % (
                    calling_function_qualified_name()
                )
            )
        if get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not `%s`.' % repr(get)
                )
            return model.QuoteCart(
                self.request(
                    resource=(
                        'V1/carts/' + str(get)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )
        elif put is not None:
            if not isinstance(put, int):
                raise TypeError(
                    'The `put` parameter must be an `int`, not `%s`.' % repr(put)
                )
            if not isinstance(customer_id, int):
                raise TypeError(
                    'The `customer_id` parameter must be provided, ' +
                    'and be an `int`, not `%s`, when the `put` parameter is present' % repr(customer_id)
                )
            if not isinstance(store_id, int):
                raise TypeError(
                    'The `store_id` parameter must be provided, ' +
                    'and be an `int`, not `%s`, when the `put` parameter is present' % repr(store_id)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts/' + str(put)
                    ),
                    data=model.PutCarts(
                        customer_id=customer_id,
                        store_id=store_id
                    ),
                    method='PUT',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        else:
            return int(serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts'
                    ),
                    method='POST',
                    timeout=timeout
                ),
                'json'
            ))  # type: int

    def guest_carts(
        self,
        get=None,  # type: Optional[str]
        put=None,  # type: Optional[str]
        customer_id=None,  # type: Optional[int]
        store_id=None,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.QuoteCart, int]
        parameter_quantity = len({id(get), id(put)} - {id(None)})
        if parameter_quantity > 1:
            raise ValueError(
                'Only *one* of the following parameters may be used for `%s`: `search_criteria`, `get`, or `put`' % (
                    calling_function_qualified_name()
                )
            )
        if get is not None:
            if not isinstance(get, str):
                raise TypeError(
                    'The `get` parameter must be an `str`, not `%s`.' % repr(get)
                )
            return model.QuoteCart(
                self.request(
                    resource=(
                        'V1/guest-carts/' + get
                    ),
                    method='GET',
                    timeout=timeout
                )
            )
        elif put is not None:
            if not isinstance(put, str):
                raise TypeError(
                    'The `put` parameter must be a `str`, not `%s`.' % repr(put)
                )
            if not isinstance(customer_id, int):
                raise TypeError(
                    'The `customer_id` parameter must be provided, ' +
                    'and be an `int`, not `%s`, when the `put` parameter is present' % repr(customer_id)
                )
            if not isinstance(store_id, int):
                raise TypeError(
                    'The `store_id` parameter must be provided, ' +
                    'and be an `int`, not `%s`, when the `put` parameter is present' % repr(store_id)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts/' + put
                    ),
                    data=model.PutGuestCarts(
                        customer_id=customer_id,
                        store_id=store_id
                    ),
                    method='PUT',
                    timeout=timeout
                ),
                'json'
            )  # type: int
        else:
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts'
                    ),
                    method='POST',
                    timeout=timeout
                ),
                'json'
            )  # type: str

    def carts_search(
        self,
        search_criteria,  # type: model.SearchCriteria
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteCartSearchResults
        if not isinstance(search_criteria, model.SearchCriteria):
            raise TypeError(
                'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                    qualified_name(model.SearchCriteria),
                    repr(search_criteria)
                )
            )
        return model.QuoteCartSearchResults(
            self.request(
                resource=(
                    'V1/carts/search?' + deep_object_query({'search_criteria': search_criteria})
                ),
                method='GET',
                timeout=timeout
            )
        )

    def carts_items(
        self,
        cart_id=None,  # type: Optional[int]
        post=None,  # type: Optional[model.PostCartsItems]
        delete=None,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.QuoteCartItem, Sequence[model.QuoteCartItem]]
        parameter_quantity = len({id(cart_id), id(post)} - {id(None)})
        if parameter_quantity != 1:
            raise ValueError(
                'Please pass a value for (only) one of the following parameters: `cart_id` or `post`.'
            )
        if delete is not None:
            if not isinstance(cart_id, int):
                raise TypeError(
                    'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
                )
            if not isinstance(delete, int):
                raise TypeError(
                    'The item ID (`delete` parameter) must be an integer, not %s.' % repr(delete)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts/%s/items/%s' % (str(cart_id), str(delete))
                    ),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif cart_id is not None:
            if not isinstance(cart_id, int):
                raise TypeError(
                    'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
                )
            return serial.model.Array(
                self.request(
                    resource=(
                        'V1/carts/%s/items' % str(cart_id)
                    ),
                    method='GET',
                    timeout=timeout
                ),
                item_types=(model.QuoteCartItem,)
            )
        elif post is not None:
            if not isinstance(post, model.QuoteCartItem):
                raise TypeError(
                    'The value for `post` must be an instance of `%s`, not %s.' % (
                        qualified_name(model.QuoteCartItem),
                        repr(post)
                    )
                )
            return model.QuoteCartItem(
                self.request(
                    resource=(
                        'V1/carts/%s/items' % str(post.quote_id)
                    ),
                    data=model.PostCartsItems(
                        cart_item=post
                    ),
                    method='POST',
                    timeout=timeout
                )
            )

    def guest_carts_items(
        self,
        cart_id=None,  # type: Optional[str]
        post=None,  # type: Optional[model.QuoteCartItem]
        delete=None,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.QuoteCartItem, Sequence[model.QuoteCartItem]]
        parameter_quantity = len({id(cart_id), id(post)} - {id(None)})
        if parameter_quantity != 1:
            raise ValueError(
                'Please pass a value for (only) one of the following parameters: `cart_id` or `post`.'
            )
        if delete is not None:
            if not isinstance(cart_id, str):
                raise TypeError(
                    'The cart ID (`cart_id` parameter) must be a `str`, not %s.' % repr(cart_id)
                )
            if not isinstance(delete, int):
                raise TypeError(
                    'The item ID (`delete` parameter) must be an integer, not %s.' % repr(delete)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/items/%s' % (cart_id, str(delete))
                    ),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif cart_id is not None:
            if not isinstance(cart_id, str):
                raise TypeError(
                    'The cart ID (`cart_id` parameter) must be a `str`, not %s.' % repr(cart_id)
                )
            return serial.model.Array(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/items' % cart_id
                    ),
                    method='GET',
                    timeout=timeout
                ),
                item_types=(model.QuoteCartItem,)
            )
        elif post is not None:
            if not isinstance(post, model.QuoteCartItem):
                raise TypeError(
                    'The value for `post` must be an instance of `%s`, not %s.' % (
                        qualified_name(model.QuoteCartItem),
                        repr(post)
                    )
                )
            return model.QuoteCartItem(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/items' % post.quote_id
                    ),
                    data=model.PostGuestCartsItems(
                        cart_item=post
                    ),
                    method='POST',
                    timeout=timeout
                )
            )

    def carts_coupons(
        self,
        cart_id=None,  # type: Optional[int]
        put=None,  # type: Optional[str]
        delete=None,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.SalesRuleCoupon, bool]
        parameter_quantity = len({id(cart_id), id(put), id(delete)} - {id(None)})
        if parameter_quantity > 2:
            raise ValueError(
                'Please pass a value for no more tha  one of the following parameters: `put`, or `delete`.'
            )
        elif parameter_quantity == 0:
            raise ValueError(
                'Please pass a value for either the parameter `cart_id` or `delete`.'
            )
        if delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The item ID (`delete` parameter) must be an integer, not %s.' % repr(delete)
                )
            if not isinstance(cart_id, int):
                raise TypeError(
                    'The value for `cart_id` must be an `int`, not %s.' % repr(cart_id)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts/%s/coupons/%s' % (str(cart_id), str(delete))
                    ),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif put is not None:
            if not isinstance(put, str):
                raise TypeError(
                    'The value for `put` must be a `str`, not %s.' % repr(put)
                )
            if not isinstance(cart_id, int):
                raise TypeError(
                    'The value for `cart_id` must be an `int`, not %s.' % repr(cart_id)
                )
            put = ','.join(quote(coupon_code) for coupon_code in put.split(','))
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts/%s/coupons/%s' % (str(cart_id), put)
                    ),
                    method='PUT',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif cart_id is not None:
            if not isinstance(cart_id, int):
                raise TypeError(
                    'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts/%s/coupons' % str(cart_id)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )  # type: str

    def guest_carts_coupons(
        self,
        cart_id=None,  # type: Optional[str]
        put=None,  # type: Optional[str]
        delete=None,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.SalesRuleCoupon, bool]
        parameter_quantity = len({id(cart_id), id(put), id(delete)} - {id(None)})
        if parameter_quantity > 2:
            raise ValueError(
                'Please pass a value for no more tha  one of the following parameters: `put`, or `delete`.'
            )
        elif parameter_quantity == 0:
            raise ValueError(
                'Please pass a value for either the parameter `cart_id` or `delete`.'
            )
        if delete is not None:
            if not isinstance(cart_id, str):
                raise TypeError(
                    'The cart ID must be a `str`, not %s.' % repr(cart_id)
                )
            if not isinstance(delete, int):
                raise TypeError(
                    'The item ID (`delete` parameter) must be an `int`, not %s.' % repr(delete)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/coupons/%s' % (cart_id, str(delete))
                    ),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif put is not None:
            if not isinstance(put, str):
                raise TypeError(
                    'The value for `put` must be a `str`, not %s.' % repr(put)
                )
            if not isinstance(cart_id, str):
                raise TypeError(
                    'The value for `cart_id` must be a `str`, not %s.' % repr(cart_id)
                )
            put = ','.join(quote(coupon_code) for coupon_code in put.split(','))
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/coupons/%s' % (cart_id, put)
                    ),
                    method='PUT',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif cart_id is not None:
            if not isinstance(cart_id, int):
                raise TypeError(
                    'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/coupons' % str(cart_id)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )  # type: str

    def carts_selected_payment_methods(
        self,
        cart_id=None,  # type: Optional[int]
        put=None,  # type: Optional[model.QuotePayment]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[model.QuotePayment, bool]
        if not isinstance(cart_id, int):
            raise TypeError(
                'The value for `cart_id` must be an `int`, not %s.' % repr(cart_id)
            )
        if put is not None:
            if not isinstance(put, model.QuotePayment):
                raise TypeError(
                    'The value for `put` must be an instance of `%s`, not %s.' % (
                        qualified_name(model.QuotePayment),
                        repr(put)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts/%s/selected-payment-method' % str(cart_id)
                    ),
                    data=model.PutCartsSelectedPaymentMethod(method=put),
                    method='PUT',
                    timeout=timeout
                ),
                'json'
            )  # type: str
        else:
            return model.QuotePayment(
                self.request(
                    resource=(
                        'V1/carts/%s/selected-payment-method' % str(cart_id)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )  # type: str

    def guest_carts_selected_payment_methods(
        self,
        cart_id=None,  # type: Optional[str]
        put=None,  # type: Optional[model.QuotePayment]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[model.QuotePayment, bool]
        if not isinstance(cart_id, str):
            raise TypeError(
                'The value for `cart_id` must be a `str`, not %s.' % repr(cart_id)
            )
        if put is not None:
            if not isinstance(put, model.QuotePayment):
                raise TypeError(
                    'The value for `put` must be an instance of `%s`, not %s.' % (
                        qualified_name(model.QuotePayment),
                        repr(put)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/selected-payment-method' % cart_id
                    ),
                    data=model.PutGuestCartsSelectedPaymentMethod(method=put),
                    method='PUT',
                    timeout=timeout
                ),
                'json'
            )  # type: str
        else:
            return model.QuotePayment(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/selected-payment-method' % cart_id
                    ),
                    method='GET',
                    timeout=timeout
                )
            )  # type: str

    def carts_totals(
        self,
        get,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(get, int):
            raise TypeError(
                'The cart ID (`get` parameter) must be an integer, not %s.' % repr(get)
            )
        return model.QuoteTotals(
            self.request(
                resource=(
                    'V1/carts/%s/totals' % str(get)
                ),
                method='GET',
                timeout=timeout
            )
        )

    def carts_totals(
        self,
        get,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(get, int):
            raise TypeError(
                'The cart ID (`get` parameter) must be an integer, not %s.' % repr(get)
            )
        return model.QuoteTotals(
            self.request(
                resource=(
                    'V1/carts/%s/totals' % str(get)
                ),
                method='GET',
                timeout=timeout
            )
        )

    def negotiable_quote_request_post(
        self,
        quote_id=None,  # type: Optional[Union[int, serial.properties.Null]]
        quote_name=None,  # type: Optional[Union[str, serial.properties.Null]]
        comment=None,  # type: Optional[str]
        files=None,  # type: Optional[typing.Sequence[model.NegotiableQuoteAttachment]]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.NegotiableQuote
        # TODO: write tests for `negotiable_quote_request_post`
        if (quote_id is not None) and not isinstance(quote_id, (int, serial.properties.Null)):
            raise TypeError(
                'The parameter `quote_id` must be an `int`, not %s.' % repr(quote_id)
            )
        if (quote_name is not None) and not isinstance(quote_name, (str, serial.properties.Null)):
            raise TypeError(
                'The parameter `quote_name` must be a `str`, not %s.' % repr(quote_name)
            )
        if (comment is not None) and not isinstance(comment, str):
            raise TypeError(
                'The parameter `comment` must be a `str`, not %s.' % repr(comment)
            )
        if files is not None:
            if isinstance(files, collections.Sequence):
                for file_ in files:
                    if not isinstance(file_, model.NegotiableQuoteAttachment):
                        raise TypeError(
                            'The parameter `files` must be a sequence of `%s`, not %s.' % (
                                qualified_name(model.NegotiableQuoteAttachment),
                                repr(files)
                            )
                        )
            else:
                raise TypeError(
                    'The parameter `files` must be a sequence of `%s`, not %s.' % (
                        qualified_name(model.NegotiableQuoteAttachment),
                        repr(files)
                    )
                )
        return serial.marshal.deserialize(
            self.request(
                resource='V1/negotiableQuote/request',
                data=model.PostNegotiableQuoteRequest(
                    quote_id=quote_id,
                    quote_name=quote_name,
                    comment=comment,
                    files=files
                ),
                method='POST',
                timeout=timeout
            ),
            'json'
        )  # type: bool

    def guest_carts_totals(
        self,
        get,  # type: Optional[str]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(get, str):
            raise TypeError(
                'The cart ID (`get` parameter) must be a `str`, not %s.' % repr(get)
            )
        return model.QuoteTotals(
            self.request(
                resource=(
                    'V1/guest-carts/%s/totals' % get
                ),
                method='GET',
                timeout=timeout
            )
        )

    def carts_shipping_methods(
        self,
        cart_id,  # type: int
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, int):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
            )
        return serial.model.Array(
            self.request(
                resource=(
                    'V1/carts/%s/shipping-methods' % str(cart_id)
                ),
                method='GET',
                timeout=timeout
            ),
            item_types=(model.QuoteShippingMethod,)
        )

    def guest_carts_shipping_methods(
        self,
        cart_id,  # type: str
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, str):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be a `str`, not %s.' % repr(cart_id)
            )
        return serial.model.Array(
            self.request(
                resource=(
                    'V1/guest-carts/%s/shipping-methods' % cart_id
                ),
                method='GET',
                timeout=timeout
            ),
            item_types=(model.QuoteShippingMethod,)
        )

    def carts_estimate_shipping_methods(
        self,
        cart_id,  # type: int
        post,  # type: model.QuoteAddress
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Sequence[model.QuoteShippingMethod]
        if not isinstance(cart_id, int):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
            )
        return serial.model.Array(
            self.request(
                resource=(
                    'V1/carts/%s/estimate-shipping-methods' % str(cart_id)
                ),
                data=model.PostCartsEstimateShippingMethods(
                    address=post
                ),
                method='POST',
                timeout=timeout
            ),
            item_types=(model.QuoteShippingMethod,)
        )

    def guest_carts_estimate_shipping_methods(
        self,
        cart_id,  # type: str
        post,  # type: model.QuoteAddress
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Sequence[model.QuoteShippingMethod]
        if not isinstance(cart_id, str):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be a `str`, not %s.' % repr(cart_id)
            )
        return serial.model.Array(
            self.request(
                resource=(
                    'V1/guest-carts/%s/estimate-shipping-methods' % cart_id
                ),
                data=model.PostGuestCartsEstimateShippingMethods(
                    address=post
                ),
                method='POST',
                timeout=timeout
            ),
            item_types=(model.QuoteShippingMethod,)
        )

    def carts_shipping_information(
        self,
        cart_id,  # type: int
        post,  # type: model.CheckoutShippingInformation
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, int):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
            )
        if not isinstance(post, model.CheckoutShippingInformation):
            raise TypeError(
                'The `post` parameter must be of type `%s`, not %s.' % (
                    qualified_name(model.CheckoutShippingInformation),
                    repr(post)
                )
            )
        return model.CheckoutPaymentDetails(
            self.request(
                resource=(
                    'V1/carts/%s/shipping-information' % str(cart_id)
                ),
                data=model.PostCartsShippingInformation(
                    address_information=post
                ),
                method='POST',
                timeout=timeout
            )
        )

    def guest_carts_shipping_information(
        self,
        cart_id,  # type: str
        post,  # type: model.CheckoutShippingInformation
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, str):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be a `str`, not %s.' % repr(cart_id)
            )
        if not isinstance(post, model.CheckoutShippingInformation):
            raise TypeError(
                'The `post` parameter must be of type `%s`, not %s.' % (
                    qualified_name(model.CheckoutShippingInformation),
                    repr(post)
                )
            )
        return model.CheckoutPaymentDetails(
            self.request(
                resource=(
                    'V1/guest-carts/%s/shipping-information' % cart_id
                ),
                data=model.PostCartsShippingInformation(
                    address_information=post
                ),
                method='POST',
                timeout=timeout
            )
        )

    def carts_billing_address(
        self,
        cart_id,  # type: int
        post,  # type: model.PostCartsBillingAddress
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, int):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
            )
        if post is None:
            return model.QuoteAddress(
                self.request(
                    resource=(
                        'V1/carts/%s/billing-address' % str(cart_id)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            if not isinstance(post, model.PostCartsBillingAddress):
                raise TypeError(
                    'The `post` parameter must be of type `%s`, not %s.' % (
                        qualified_name(model.PostCartsBillingAddress),
                        repr(post)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/carts/%s/billing-address' % str(cart_id)
                    ),
                    data=post,
                    method='POST',
                    timeout=timeout
                ),
                'json'
            )  # type: int

    def guest_carts_billing_address(
        self,
        cart_id,  # type: str
        post,  # type: model.PostGuestCartsBillingAddress
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, str):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be a `str`, not %s.' % repr(cart_id)
            )
        if post is None:
            return model.QuoteAddress(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/billing-address' % cart_id
                    ),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            if not isinstance(post, model.PostGuestCartsBillingAddress):
                raise TypeError(
                    'The `post` parameter must be of type `%s`, not %s.' % (
                        qualified_name(model.PostGuestCartsBillingAddress),
                        repr(post)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/guest-carts/%s/billing-address' % cart_id
                    ),
                    data=post,
                    method='POST',
                    timeout=timeout
                ),
                'json'
            )  # type: int

    def carts_order(
        self,
        cart_id,  # type: int
        payment,  # type: model.QuotePayment
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, int):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
            )
        if not isinstance(payment, model.QuotePayment):
            raise TypeError(
                'The `post` parameter must be an instance of type `%s`, not %s.' % (
                    qualified_name(model.QuotePayment),
                    repr(payment)
                )
            )
        return serial.marshal.deserialize(
            self.request(
                resource=(
                    'V1/carts/%s/order' % str(cart_id)
                ),
                data=model.PutCartsOrder(payment_method=payment),
                method='PUT',
                timeout=timeout
            ),
            'json'
        )  # type: int

    def guest_carts_order(
        self,
        cart_id,  # type: str
        payment,  # type: model.QuotePayment
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, str):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be a `str`, not %s.' % cart_id
            )
        if not isinstance(payment, model.QuotePayment):
            raise TypeError(
                'The `post` parameter must be an instance of type `%s`, not %s.' % (
                    qualified_name(model.QuotePayment),
                    repr(payment)
                )
            )
        return serial.marshal.deserialize(
            self.request(
                resource=(
                    'V1/guest-carts/%s/order' % cart_id
                ),
                data=model.PutGuestCartsOrder(payment_method=payment),
                method='PUT',
                timeout=timeout
            ),
            'json'
        )  # type: int

    def carts_payment_methods(
        self,
        cart_id,  # type: int
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, int):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be an integer, not %s.' % repr(cart_id)
            )
        return serial.model.Array(
            self.request(
                resource=(
                    'V1/carts/%s/payment-methods' % str(cart_id)
                ),
                method='GET',
                timeout=timeout
            ),
            item_types=(model.QuotePaymentMethod,)
        )

    def guest_carts_payment_methods(
        self,
        cart_id,  # type: str
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> model.QuoteTotals
        if not isinstance(cart_id, str):
            raise TypeError(
                'The cart ID (`cart_id` parameter) must be a string, not %s.' % repr(cart_id)
            )
        return serial.model.Array(
            self.request(
                resource=(
                    'V1/guest-carts/%s/payment-methods' % str(cart_id)
                ),
                method='GET',
                timeout=timeout
            ),
            item_types=(model.QuotePaymentMethod,)
        )

    def categories_move(
        self,
        category_id,  # type: int
        put,  # type: model.PutCategoriesMove
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> bool
        if not isinstance(category_id, int):
            raise TypeError(
                'The category ID (`category_id` parameter) must be an integer, not %s.' % repr(category_id)
            )
        if not isinstance(put, model.PutCategoriesMove):
            raise TypeError(
                'The `put` parameter must be an instance of `%s`, not %s.' % (
                    qualified_name(model.PutCategoriesMove),
                    repr(put)
                )
            )
        serial.marshal.validate(put)
        return serial.marshal.deserialize(
            self.request(
                resource='V1/categories/%s/move' % str(category_id),
                method='PUT',
                data=put,
                timeout=timeout
            ),
            'json'
        )  # type: bool

    def categories(
        self,
        get=None,  # type: Optional[int]
        root_category_id=None,  # type: Optional[int]
        put=None,  # type: Optional[model.Category]
        post=None,  # type: Optional[model.Category]
        delete=None,  # type: Optional[int]
        timeout=None,  # type: Optional[int]
    ):
        # type: (str) -> Union[model.Category, bool, model.CategoryTree, model.Category]
        """
        Get, post, or delete categories.

        Parameters:

            - get (`int`): The ID of a category to be retrieved.

            - root_category_id (`int`): A root category ID--when provided this method returns a category tree
              (hierarchy).

            - post (`magento2.model.Category`): Create or update a category.

              Valid values for `custom_attributes` property:

                all_children
                children
                children_count
                custom_apply_to_products
                custom_design
                custom_design_from
                custom_design_to
                custom_layout_update
                custom_use_parent_settings
                default_sort_by
                description
                display_mode
                filter_price_range
                image
                is_anchor
                landing_page
                meta_description
                meta_keywords
                meta_title
                page_layout
                path
                path_in_store
                url_key
                url_path

            - delete (`int`): The ID of a category to be deleted.
        """
        if len({id(root_category_id), id(post), id(delete), id(get)} - {id(None)}) > 1:
            raise ValueError(
                'Only one parameter may be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        if get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The category ID (`get` parameter) must be an integer, not %s.' % repr(get)
                )
            return model.Category(
                self.request(
                    resource=(
                        'V1/categories/' + str(get)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )
        # type: (str) -> model.Category
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The ID of the category to be deleted (`delete` parameter) must be an integer, not %s.' %
                    repr(delete)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/categories/' + str(delete)
                    ),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )
        elif root_category_id is not None:
            if not isinstance(root_category_id, int):
                raise TypeError(
                    'The root category ID (`root_category_id` parameter) must be an integer, not %s.' % repr(get)
                )
            return model.CategoryTree(
                self.request(
                    resource=(
                        'V1/categories?rootCategoryId=' + str(root_category_id)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )
        elif post:
            if not isinstance(post, model.Category):
                raise TypeError('`post` must be an instance of `%s`' % qualified_name(model.Category))
            return model.Category(
                self.request(
                    resource='V1/categories',
                    method='POST',
                    data=model.PostCategories(
                        category=post
                    ),
                    timeout=timeout
                )
            )
        elif put:
            if not isinstance(put, model.Category):
                raise TypeError('`put` must be an instance of `%s`' % qualified_name(model.Category))
            if put.id_ is None:
                raise TypeError('The `id_` property of the Category being "put" must be present.')
            category_id = put.id_
            category = copy(put)
            del category.id_
            return model.Category(
                self.request(
                    resource='V1/categories/' + str(category_id),
                    method='PUT',
                    data=model.PutCategories(
                        category=category
                    ),
                    timeout=timeout
                )
            )
        else:
            return model.CategoryTree(
                self.request(
                    resource='V1/categories',
                    method='GET',
                    timeout=timeout
                )
            )

    def categories_list(
        self,
        search_criteria,  # type: model.SearchCriteria
        timeout=None  # type: Optional[int]
    ):
        # type: (model.SearchCriteria) -> model.CategorySearchResults
        return model.CategorySearchResults(
            self.request(
                resource='V1/categories/list?' + deep_object_query({'search_criteria': search_criteria}),
                method='GET',
                timeout=timeout
            )
        )

    def customers(
        self,
        get=None,  # type: Optional[int],
        delete=None,  # type: Optional[int],
        post=None,  # type: Optional[model.PostCustomers]
        put=None,  # type: Optional[model.PutCustomers]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[bool, Sequence[model.CategoryProductLink]]
        if len({id(get), id(delete), id(put), id(post)} - {id(None)}) != 1:
            raise ValueError(
                'Exactly one parameter (`get` or `post`) may be passed to `%s` at once.' % (
                    calling_function_qualified_name()
                )
            )
        if post is not None:
            if not isinstance(post, model.PostCustomers):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        qualified_name(model.PostCustomers),
                        repr(post)
                    )
                )
            serial.marshal.validate(post)
            return model.Customer(
                self.request(
                    resource='V1/customers',
                    data=post,
                    method='POST',
                    timeout=timeout
                )
            )
        elif put is not None:
            if not isinstance(put, model.PutCustomers):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        qualified_name(model.PutCustomers),
                        repr(put)
                    )
                )
            serial.marshal.validate(put)
            return model.Customer(
                self.request(
                    resource='V1/customers/' + str(put.customer.id_),
                    data=put,
                    method='PUT',
                    timeout=timeout
                )
            )
        elif get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not %s.' % (
                        repr(get)
                    )
                )
            return model.Customer(
                self.request(
                    resource='V1/customers/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The `delete` parameter must be an `int`, not %s.' % (
                        repr(delete)
                    )
                )
            return model.Customer(
                self.request(
                    resource='V1/customers/' + str(delete),
                    method='DELETE',
                    timeout=timeout
                )
            )

    def customers_search(
        self,
        search_criteria,  # type: model.SearchCriteria
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[model.SalesRuleSearchResult, model.SalesRule, bool]
        if not isinstance(search_criteria, model.SearchCriteria):
            raise TypeError(
                'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                    qualified_name(model.SearchCriteria),
                    repr(search_criteria)
                )
            )
        return model.CustomerSearchResults(
            self.request(
                resource='V1/customers/search?' + deep_object_query({'search_criteria': search_criteria}),
                method='GET',
                timeout=timeout
            )
        )

    def categories_products(
        self,
        category_id=None,  # type: Optional[int]
        post=None,  # type: Optional[model.CategoryProductLink]
        put=None,  # type: Optional[model.CategoryProductLink]
        delete=None,  # type: Optional[str]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[bool, Sequence[model.CategoryProductLink]]
        if len({id(post), id(delete), id(put), id(delete)} - {id(None)}) > 1:
            raise ValueError(
                'Only `category_id` and one additional parameter may be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        if post is not None:
            if not isinstance(post, model.CategoryProductLink):
                raise TypeError(
                    'The `post` parameter must be an instance of `magento2.model.CategoryProductLink`, not %s.' %
                    repr(post)
                )
            serial.marshal.validate(post)
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/categories/%s/products' % str(post.category_id)
                    ),
                    data=model.PostCategoriesProducts(
                        product_link=post
                    ),
                    method='POST',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif put is not None:
            if not isinstance(put, model.CategoryProductLink):
                raise TypeError(
                    'The `put` parameter must be an instance of `magento2.model.CategoryProductLink`, not %s.' %
                    repr(put)
                )
            serial.marshal.validate(put)
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/categories/%s/products' % str(put.category_id)
                    ),
                    data=model.PutCategoriesProducts(
                        product_link=put
                    ),
                    method='PUT',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif delete is not None:
            if not isinstance(delete, str):
                raise TypeError(
                    'The `delete` parameter must be a `str`, not %s.' %
                    repr(delete)
                )
            if not isinstance(category_id, int):
                raise TypeError(
                    'The `category_id` parameter must be an `int`, not %s.' %
                    repr(category_id)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/categories/%s/products/%s' % (str(category_id), delete)
                    ),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        else:
            if not isinstance(category_id, int):
                raise TypeError(
                    'The `category_id` parameter must be an `int`, not %s.' %
                    repr(category_id)
                )
            return serial.model.Array(
                self.request(
                    resource=(
                        'V1/categories/%s/products' % str(category_id)
                    ),
                    method='GET',
                    timeout=timeout
                ),
                item_types=(model.CategoryProductLink,)
            )  # type: Sequence[model.CategoryProductLink]

    def eav_attribute_sets(
        self,
        get=None,  # type: Optional[int]
        delete=None,  # type: Optional[int]
        put=None,  # type: Optional[model.EAVAttributeSet]
        post=None,  # type: Optional[model.PostEAVAttributeSets]
        timeout=None  # type: Optional[int]
    ):
        if len({id(get), id(post), id(delete), id(put)} - {id(None)}) > 1:
            raise ValueError(
                'Only one parameter may be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        if get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The attribute set ID (`get` parameter) must be an integer, not %s.' % repr(get)
                )
            return model.EAVAttributeSet(
                self.request(
                    resource=(
                        'V1/eav/attribute-sets/' + str(get)
                    ),
                    method='GET',
                    timeout=timeout
                )
            )
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The attribute set ID (`delete` parameter) must be an integer, not %s.' % repr(get)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource=(
                        'V1/eav/attribute-sets/' + str(delete)
                    ),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )
        elif put is not None:
            if not isinstance(put, model.EAVAttributeSet):
                raise TypeError(
                    'The attribute set (`put` parameter) must be an instance of `%`, not %s.' % (
                        qualified_name(model.EAVAttributeSet),
                        repr(put)
                    )
                )
            serial.marshal.validate(put)
            return model.EAVAttributeSet(
                self.request(
                    resource=(
                        'V1/eav/attribute-sets/' + str(put.attribute_set_id)
                    ),
                    data=model.PutEAVAttributeSet(
                        attribute_set=put
                    ),
                    method='PUT',
                    timeout=timeout
                )
            )
        elif post is not None:
            if not isinstance(post, model.PostEAVAttributeSets):
                raise TypeError(
                    'The attribute set (`post` parameter) must be an instance of `%s`, not %s.' % (
                        qualified_name(model.PostEAVAttributeSets),
                        repr(post)
                    )
                )
            serial.marshal.validate(post)
            return model.EAVAttributeSet(
                self.request(
                    resource=(
                        'V1/eav/attribute-sets'
                    ),
                    data=post,
                    method='POST',
                    timeout=timeout
                )
            )
        else:
            raise ValueError(
                'At least one parameter must be provided.'
            )

    def eav_attribute_sets_list(
        self,
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        timeout=None  # type: Optional[int]
    ):
        # type: (model.SearchCriteria) -> Union[model.EAVAttributeSetSearchResults, ]
        """
        Lookup EAV attribute sets.

        Search Criteria Fields:

            - "entity_type_id"

              Values:

                - 3: catalog_category
                - 4: catalog_product
                - 7: creditmemo
                - 1: customer
                - 2: customer_address
                - 6: invoice
                - 5: order
                - 8: shipment
        """
        if search_criteria is not None:
            return model.EAVAttributeSetSearchResults(
                self.request(
                    resource='V1/eav/attribute-sets/list?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            pass

    def products_attribute_set_groups_list(
        self,
        search_criteria,  # type: model.SearchCriteria
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> model.EAVAttributeGroupSearchResults
        if not isinstance(search_criteria, model.SearchCriteria):
            raise TypeError(
                'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                    repr(model.SearchCriteria),
                    repr(search_criteria)
                )
            )
        return model.EAVAttributeGroupSearchResults(
            self.request(
                resource='V1/products/attribute-sets/groups/list?' + deep_object_query(
                    {'search_criteria': search_criteria}
                ),
                method='GET',
                timeout=timeout
            )
        )

    def products_attribute_set_groups(
        self
    ):
        # TODO: Magento.products_attribute_set_groups
        pass

    def products_attribute_set_attributes(
        self,
        attribute_set_id=None,  # Optional[int]
        post=None,  # type: Optional[model.PostProductsAttributeSetsAttributes]
        delete=None,  # type: Optional[int]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[Sequence[model.ProductAttribute], int, bool]
        if post is not None:
            if not isinstance(post, model.PostProductsAttributeSetsAttributes):
                raise ValueError(
                    'The `post` parameter must be an instance of `%s`, not %s' % (
                        qualified_name(model.PostProductsAttributeSetsAttributes),
                        repr(post)
                    )
                )
            serial.marshal.validate(post)
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/attribute-sets/attributes',
                    method='POST',
                    data=post,
                    timeout=timeout
                ),
                'json'
            )  # type: int
        else:
            if not isinstance(attribute_set_id, int):
                raise ValueError(
                    'The `attribute_set_id` must be an `int`, not %s' % repr(attribute_set_id)
                )
            if delete is not None:
                if not isinstance(delete, int):
                    raise ValueError(
                        'The `delete` parameter must be an `int`, not %s' % repr(delete)
                    )
                return serial.marshal.deserialize(
                    self.request(
                        resource='V1/products/attribute-sets/%s/attributes/%s' % (
                            str(attribute_set_id),
                            str(delete)
                        ),
                        method='DELETE',
                        timeout=timeout
                    ),
                    'json'
                )  # type: bool
            else:
                return serial.model.Array(
                    self.request(
                        resource='V1/products/attribute-sets/%s/attributes' % str(attribute_set_id),
                        method='GET',
                        timeout=timeout
                    ),
                    item_types=(model.ProductAttribute,)
                )

    def products(
        self,
        get=None,  # type: Optional[str]
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        delete=None,  # type: Optional[str]
        put=None,  # type: Optional[model.PutProducts]
        post=None,  # type: Optional[model.PostProducts]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.ProductSearchResults, model.Product, bool]
        if len({id(search_criteria), id(get), id(delete), id(put), id(post)} - {id(None)}) > 1:
            raise ValueError(
                'Only one parameter may be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        if search_criteria is not None:
            if not isinstance(search_criteria, model.SearchCriteria):
                raise TypeError(
                    'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SearchCriteria),
                        repr(search_criteria)
                    )
                )
            return model.ProductSearchResults(
                self.request(
                    resource='V1/products?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )
        elif put is not None:
            if not isinstance(put, model.PutProducts):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.PutProducts),
                        repr(put)
                    )
                )
            serial.test.json(put)
            return model.Product(
                self.request(
                    resource='V1/products/' + str(put.product.sku),
                    method='PUT',
                    data=put,
                    timeout=timeout
                )
            )
        elif post is not None:
            if not isinstance(post, model.PostProducts):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.PostProducts),
                        repr(post)
                    )
                )
            serial.test.json(post)
            return model.Product(
                self.request(
                    resource='V1/products',
                    method='POST',
                    data=post,
                    timeout=timeout
                )
            )
        elif get is not None:
            if not isinstance(get, str):
                raise TypeError(
                    'The `get` parameter must be a `str`, not %s.' % (
                        repr(get)
                    )
                )
            return model.Product(
                self.request(
                    resource='V1/products/' + quote(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif delete is not None:
            if not isinstance(delete, str):
                raise TypeError(
                    'The `delete` parameter must be a `str`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/' + delete,
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool

    def products_special_price(
        self,
        post=None,  # type: Sequence[model.SpecialPrice]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> Union[model.PriceUpdateResult,Sequence]
        if not isinstance(post, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(post)
                )
            )
        post = serial.model.Array(
            post,
            item_types=(model.SpecialPrice,)
        )
        serial.marshal.validate(post)
        return serial.marshal.unmarshal(
            serial.marshal.deserialize(
                self.request(
                    resource='V1/products/special-price',
                    method='POST',
                    data=model.PostProductsSpecialPrice(prices=post),
                    timeout=timeout
                ),
                'json'
            ),
            (model.PriceUpdateResult, serial.model.Array)
        )  # type: Union[model.PriceUpdateResult,Sequence]

    def products_special_price_delete(
        self,
        post=None,  # type: Sequence[str]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> Union[model.PriceUpdateResult,Sequence]
        if not isinstance(post, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(post)
                )
            )
        post = serial.model.Array(
            post,
            item_types=(model.SpecialPrice,)
        )
        serial.marshal.validate(post)
        return serial.marshal.unmarshal(
            serial.marshal.deserialize(
                self.request(
                    resource='V1/products/special-price-delete',
                    method='POST',
                    data=model.PostProductsSpecialPriceDelete(prices=post),
                    timeout=timeout
                ),
                'json'
            ),
            (model.PriceUpdateResult, serial.model.Array)
        )  # type: Union[model.PriceUpdateResult,Sequence]

    def products_special_price_information(
        self,
        post=None,  # type: Sequence[str]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> model.PriceUpdateResult
        if not isinstance(post, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(post)
                )
            )
        post = serial.model.Array(
            post,
            item_types=(str,)
        )
        serial.marshal.validate(post)
        return serial.model.Array(
            self.request(
                resource='V1/products/special-price-information',
                method='POST',
                data=model.PostProductsSpecialPriceInformation(skus=post),
                timeout=timeout
            ),
            item_types=(model.SpecialPrice,)
        )

    def products_base_prices(
        self,
        post=None,  # type: Sequence[model.BasePrice]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> Union[model.PriceUpdateResult,Sequence]
        if not isinstance(post, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(post)
                )
            )
        post = serial.model.Array(
            post,
            item_types=(model.BasePrice,)
        )
        serial.marshal.validate(post)
        return serial.marshal.unmarshal(
            serial.marshal.deserialize(
                self.request(
                    resource='V1/products/base-prices',
                    method='POST',
                    data=model.PostProductsBasePrices(prices=post),
                    timeout=timeout
                ),
                'json'
            ),
            (model.PriceUpdateResult, serial.model.Array)
        )  # type: Union[model.PriceUpdateResult,Sequence]

    def products_base_prices_information(
        self,
        post=None,  # type: Sequence[str]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> model.PriceUpdateResult
        if not isinstance(post, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(post)
                )
            )
        post = serial.model.Array(
            post,
            item_types=(str,)
        )
        serial.marshal.validate(post)
        return serial.model.Array(
            self.request(
                resource='V1/products/base-prices-information',
                method='POST',
                data=model.PostProductsBasePricesInformation(skus=post),
                timeout=timeout
            ),
            item_types=(model.BasePrice,)
        )
    
    def products_cost(
        self,
        post=None,  # type: Sequence[model.Cost]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> Union[model.PriceUpdateResult,Sequence]
        if not isinstance(post, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(post)
                )
            )
        post = serial.model.Array(
            post,
            item_types=(model.Cost,)
        )
        serial.marshal.validate(post)
        return serial.marshal.unmarshal(
            serial.marshal.deserialize(
                self.request(
                    resource='V1/products/cost',
                    method='POST',
                    data=model.PostProductsCost(prices=post),
                    timeout=timeout
                ),
                'json'
            ),
            (model.PriceUpdateResult, serial.model.Array)
        )  # type: Union[model.PriceUpdateResult,Sequence]

    def products_cost_delete(
        self,
        skus=None,  # type: Sequence[str]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> bool
        if not isinstance(skus, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(skus)
                )
            )
        skus = serial.model.Array(
            skus,
            item_types=(str,)
        )
        serial.marshal.validate(skus)
        return serial.marshal.deserialize(
            self.request(
                resource='V1/products/cost-delete',
                method='POST',
                data=model.PostProductsCostDelete(skus=skus),
                timeout=timeout
            ),
            'json'
        )  # type: bool

    def products_cost_information(
        self,
        post,  # type: Sequence[str]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> model.PriceUpdateResult
        if not isinstance(post, collections.Sequence):
            raise ValueError(
                'The parameter `post` must be a sequence, not `%s`' % (
                    repr(post)
                )
            )
        post = serial.model.Array(
            post,
            item_types=(str,)
        )
        serial.marshal.validate(post)
        return serial.model.Array(
            self.request(
                resource='V1/products/cost-information',
                method='POST',
                data=model.PostProductsCostInformation(skus=post),
                timeout=timeout
            ),
            item_types=(model.Cost,)
        )

    def stock_items(
        self,
        sku,  # type: str
        scope_id=None,  # type: Optional[int]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> model.InventoryStockItem
        if not isinstance(sku, str):
            raise ValueError(
                'The parameter `sku` must be a `str`'
            )
        query = ''
        if scope_id is not None:
            if not isinstance(query, int):
                raise ValueError(
                    'The parameter `scope_id` must be am `int`'
                )
            query = '?scopeId=' + str(scope_id)
        return model.InventoryStockItem(
            self.request(
                resource='V1/stockItems/' + sku + query,
                method='GET',
                timeout=timeout
            )
        )

    def stock_statuses(
        self,
        sku,  # type: str
        scope_id=None,  # type: Optional[int]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> model.InventoryStockItem
        if not isinstance(sku, str):
            raise ValueError(
                'The parameter `sku` must be a `str`'
            )
        query = ''
        if scope_id is not None:
            if not isinstance(query, int):
                raise ValueError(
                    'The parameter `scope_id` must be am `int`'
                )
            query = '?scopeId=' + str(scope_id)
        return model.InventoryStockStatus(
            self.request(
                resource='V1/stockStatuses/' + sku + query,
                method='GET',
                timeout=timeout
            )
        )

    def products_stock_items(
        self,
        sku,  # type: str
        item_id=None,  # type: Optional[int]
        put=None,  # type: Optional[model.InventoryStockItem]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> int
        if not isinstance(sku, str):
            raise ValueError(
                'The parameter `sku` must be a `str`'
            )
        if item_id is None:
            item_id = put.item_id
        else:
            if not isinstance(item_id, int):
                raise ValueError(
                    'The parameter `item_id` must be an `int`'
                )
        if not isinstance(put, model.InventoryStockItem):
            raise ValueError(
                'The parameter `post` must be an instance of `%s`, not `%s`' % (
                    repr(model.InventoryStockItem),
                    repr(put)
                )
            )
        serial.marshal.validate(put)
        return serial.marshal.deserialize(
            self.request(
                resource='V1/products/%s/stockItems/%s' % (sku, str(item_id)),
                method='PUT',
                data=model.PutProductsStockItems(stock_item=put),
                timeout=timeout
            ),
            'json'
        )  # int

    @property
    def products_attributes_types(self):
        # type: (...) -> Sequence[model.ProductAttributeType]
        return serial.model.Array(
            self.request(
                resource='V1/products/attributes/types',
                method='GET'
            ),
            item_types=(model.ProductAttributeType,)
        )

    def sales_rules(
        self,
        get=None,  # type: Optional[int]
        search_criteria=None, # type: Option[model.SearchCriteria]
        delete=None,  # type: Optional[int]
        put=None,  # type: Optional[model.SalesRule]
        post=None,  # type: Optional[model.SalesRules]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> Union[model.SalesRuleSearchResult, model.SalesRule, bool]
        if len({id(search_criteria), id(get), id(delete), id(put), id(post)} - {id(None)}) > 1:
            raise ValueError(   
                'Only one parameter may be specified for `%s`' % (
                    calling_function_qualified_name()
                )
            )
        if search_criteria is not None:
            if not isinstance(search_criteria, model.SearchCriteria):
                raise TypeError(
                    'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SearchCriteria),
                        repr(search_criteria)
                    )
                )
            return model.SalesRuleSearchResult(
                self.request(
                    resource='V1/salesRules/search?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )
        elif put is not None:
            if not isinstance(put, model.SalesRule):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SalesRule),
                        repr(put)
                    )
                )
            serial.marshal.validate(put)
            return model.SalesRule(
                    self.request(
                        resource='V1/salesRules/' + str(put.rule_id),
                        data=model.PutSalesRules(
                            rule=put
                        ),
                        method='PUT',
                        timeout=timeout
                    )
                )
        elif post is not None:
            if not isinstance(post, model.SalesRule):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SalesRule),
                        repr(post)
                    )
                )
                
            serial.marshal.validate(post)
            return model.SalesRule(
                self.request(
                    resource='V1/salesRules',
                    data=model.PostSalesRules(
                        rule=post
                    ),
                    timeout=timeout
                )
            )
        elif get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not %s.' % (
                        repr(get)
                    )
                )
            return model.SalesRule(
                self.request(
                    resource='V1/salesRules/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The `delete` parameter must be an `int`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/salesRules/' + str(delete),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool

    def coupons(
        self,
        get=None,  # type: Optional[int]
        search_criteria=None, # type: Option[model.SearchCriteria]
        put=None,  # type: Optional[model.SalesRuleCoupon]
        post=None,  # type: Optional[model.SalesRuleCoupon]
        delete=None,  # type: Optional[int]
        generate=None,  # type: Optional[model.SalesRuleCouponGenerationSpec]
        delete_by_ids=None,  # type: Optional[model.PostCouponsDeleteByIds]
        delete_by_codes=None,  # type: Optional[model.PostCouponsDeleteByCodes]
        timeout=None # type: Optional[int]
    ):
        # type: (...) -> Union[model.SalesRuleCouponSearchResult, model.SalesRuleCoupon, bool, Sequence[str], model.SalesRuleCouponMassDeleteResult]
        parameter_quantity = len(
            {
                id(search_criteria),
                id(get),
                id(delete),
                id(put),
                id(post),
                id(delete_by_ids),
                id(delete_by_codes)
            } - {
                id(None)
            }
        )
        if parameter_quantity > 1:
            raise ValueError(
                'Only one parameter may be specified for `%s`' % (
                    calling_function_qualified_name()
                )
            )
        elif parameter_quantity < 1:
            raise ValueError(
                'Exactly one parameter must be specified for `%s`' % (
                    calling_function_qualified_name()
                )
            )
        if search_criteria is not None:
            if not isinstance(search_criteria, model.SearchCriteria):
                raise TypeError(
                    'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SearchCriteria),
                        repr(search_criteria)
                    )
                )
            return model.SalesRuleCouponSearchResult(
                self.request(
                    resource='V1/coupons/search?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )
        elif put is not None:
            if not isinstance(put, model.SalesRuleCoupon):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SalesRuleCoupon),
                        repr(put)
                    )
                )
            serial.marshal.validate(put)
            return model.SalesRuleCoupon(
                self.request(
                    resource='V1/coupons/' + str(put.coupon_id),
                    data=model.PutCoupons(
                        coupon=put
                    ),
                    method='PUT',
                    timeout=timeout
                )
            )
        elif post is not None:
            if not isinstance(post, model.SalesRuleCoupon):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SalesRuleCoupon),
                        repr(post)
                    )
                )
            serial.marshal.validate(post)
            return model.SalesRuleCoupon(
                self.request(
                    resource='V1/coupons',
                    data=model.PostCoupons(
                        coupon=post
                    ),
                    timeout=timeout
                )
            )
        elif get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not %s.' % (
                        repr(get)
                    )
                )
            return model.SalesRuleCoupon(
                self.request(
                    resource='V1/coupons/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The `delete` parameter must be an `int`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/coupons/' + str(delete),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif generate is not None:
            if not isinstance(generate, model.SalesRuleCouponGenerationSpec):
                raise TypeError(
                    'The `generate` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.SalesRuleCouponGenerationSpec),
                        repr(generate)
                    )
                )
            serial.marshal.validate(generate)
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/coupons/generate',
                    method='POST',
                    data=model.PostCouponsGenerate(
                        coupon_spec=generate
                    ),
                    timeout=timeout
                ),
                'json'
            )  # type: Sequence[str]
        elif delete_by_ids is not None:
            if not isinstance(generate, model.PostCouponsDeleteByIds):
                raise TypeError(
                    'The `delete_by_ids` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.PostCouponsDeleteByIds),
                        repr(delete_by_ids)
                    )
                )
            serial.marshal.validate(delete_by_ids)
            return model.SalesRuleCouponMassDeleteResult(
                self.request(
                    resource='V1/coupons/deleteByIds',
                    method='POST',
                    data=delete_by_ids,
                    timeout=timeout
                )
            )
        elif delete_by_codes is not None:
            if not isinstance(generate, model.PostCouponsDeleteByCodes):
                raise TypeError(
                    'The `delete_by_codes` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.PostCouponsDeleteByCodes),
                        repr(delete_by_codes)
                    )
                )
            serial.marshal.validate(delete_by_codes)
            return model.SalesRuleCouponMassDeleteResult(
                self.request(
                    resource='V1/coupons/deleteByCodes',
                    method='POST',
                    data=model.PostCouponsDeleteByCodes(
                        ids=delete_by_codes
                    ),
                    timeout=timeout
                )
            )
        
    def products_attributes(
        self,
        get=None,  # type: Optional[str]
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        put=None,  # type: Optional[model.ProductAttribute]
        post=None,  # type: Optional[model.ProductAttribute]
        delete=None,  # type: Optional[str]
        timeout=None,  # type: Optional[int]
    ):
        # type: (...) -> Union[model.ProductAttributeSearchResults, model.ProductAttribute]
        parameter_quantity = len(set((id(search_criteria), id(get), id(put), id(post), id(delete))) - set((id(None),)))
        if parameter_quantity < 1:
            raise ValueError(
                'A parameter must be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        elif parameter_quantity > 1:
            raise ValueError(
                'Only one parameter may be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        if put is not None:
            if not isinstance(put, model.ProductAttribute):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.ProductAttribute),
                        repr(put)
                    )
                )
            serial.marshal.validate(put)
            return model.ProductAttribute(
                self.request(
                    resource='V1/products/attributes/' + put.attribute_code,
                    method='PUT',
                    data=model.PutProductsAttributes(
                        attribute=put
                    ),
                    timeout=timeout
                )
            )
        elif post is not None:
            if not isinstance(post, model.ProductAttribute):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.ProductAttribute),
                        repr(post)
                    )
                )
            serial.marshal.validate(post)
            return model.ProductAttribute(
                self.request(
                    resource='V1/products/attributes',
                    method='POST',
                    data=model.PostProductsAttributes(
                        attribute=post
                    ),
                    timeout=timeout
                )
            )
        elif delete is not None:
            if not isinstance(delete, str):
                raise TypeError(
                    'The `delete` parameter must be a `str`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/attributes/' + delete,
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )
        elif get is not None:
            if not isinstance(get, str):
                raise TypeError(
                    'The `get` parameter must be a `str`, not %s.' % (
                        repr(get)
                    )
                )
            return model.ProductAttribute(
                self.request(
                    resource='V1/products/attributes/' + quote(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif search_criteria is not None:
            if not isinstance(search_criteria, model.SearchCriteria):
                raise TypeError(
                    'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                        qualified_name(model.SearchCriteria),
                        repr(search_criteria)
                    )
                )
            return model.ProductAttributeSearchResults(
                self.request(
                    resource='V1/products/attributes?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )

    def products_attribute_options(
        self,
        attribute_code,  # type: str
        delete=None,  # type: Optional[str]
        post=None,  # type: Optional[model.EAVAttributeOption]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[Sequence[model.EAVAttributeOption], bool]
        if len(set((id(post), id(delete))) - set((id(None),))) > 1:
            raise ValueError(
                'Only `attribute_code`, and one additional parameter may be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        if not isinstance(attribute_code, str):
            raise TypeError(
                'The `attribute_code` parameter must be a `str`, not %s' % repr(attribute_code)
            )
        if delete is not None:
            if not isinstance(delete, str):
                raise TypeError(
                    'The `delete` parameter must be a `str`, not %s' % repr(delete)
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/attributes/%s/options/%s' % (attribute_code, delete),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif post is not None:
            if not isinstance(post, model.EAVAttributeOption):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s' % (
                        qualified_name(model.EAVAttributeOption),
                        repr(post)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/attributes/%s/options' % attribute_code,
                    method='POST',
                    data=model.PostProductsAttributesOptions(
                        option=post
                    )
                ),
                'json'
            )  # type: bool
        else:
            return serial.model.Array(
                self.request(
                    resource='V1/products/attributes/%s/options' % attribute_code,
                    method='GET',
                    timeout=timeout
                ),
                item_types=(model.EAVAttributeOption,)
            )

    def products_media(
        self,
        sku,  # type: str
        get=None,  # type: Optional[int]
        put=None,  # type: Optional[model.ProductAttributeMediaGalleryEntry]
        post=None,  # type: Optional[model.ProductAttributeMediaGalleryEntry]
        delete=None,  # type: Optional[int]
        timeout=None  # type: Optional[int]
    ):
        if len(set((id(get), id(delete), id(put), id(post)),) - set((id(None),))) > 1:
            raise ValueError(
                'Only `sku` and one other parameter may be specified for `%s().%s`' % (
                    qualified_name(type(self)),
                    inspect.getframeinfo(inspect.currentframe()).function
                )
            )
        if put is not None:
            if not isinstance(put, model.ProductAttributeMediaGalleryEntry):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.PutProduct),
                        repr(put)
                    )
                )
            if put.id_ is None:
                raise ValueError(
                    'A media entry ID is required for any `put` operation.'
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/%s/media/%s' % (str(sku), str(put.id_)),
                    method='PUT',
                    data=model.PutProductsMedia(
                        entry=put
                    ),
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif post is not None:
            if not isinstance(post, model.ProductAttributeMediaGalleryEntry):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.ProductAttributeMediaGalleryEntry),
                        repr(post)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/%s/media' % str(sku),
                    method='POST',
                    data=model.PostProductsMedia(
                        entry=post
                    ),
                    timeout=timeout
                ),
                'json'
            )  # type: int
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The `delete` parameter must be an `int`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/products/%s/media/%s' % (sku, str(delete)),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )
        elif get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not %s.' % (
                        repr(get)
                    )
                )
            return model.ProductAttributeMediaGalleryEntry(
                self.request(
                    resource='V1/products/%s/media/%s' % (sku, str(get)),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            return serial.model.Array(
                self.request(
                    resource='V1/products/%s/media' % quote(sku),
                    method='GET',
                    timeout=timeout
                ),
                item_types=(model.ProductAttributeMediaGalleryEntry,)
            )

    def configurable_products_child(
        self,
        sku,  # type: str
        child_sku,  # type: str
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> bool
        if not isinstance(child_sku, str):
            raise TypeError(
                'The `child_sku` parameter must be an instance of `str`, not %s.' % (
                    repr(child_sku)
                )
            )
        return serial.marshal.deserialize(
            self.request(
                resource='V1/configurable-products/%s/child' % sku,
                method='POST',
                data=model.PostConfigurableProductsChild(
                    child_sku=child_sku
                ),
                timeout=timeout
            ),
            'json'
        )

    def configurable_products_children(
        self,
        sku,  # type: str
        delete=None,  # Optional[str]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[serial.model.Array, bool]
        """
        Parameters:

            - sku (str): The SKU of the a configurable (parent) product. Without any other arguments, this method
              returns a list of this product's children.

            - delete (str): A child SKU. When specified, the relationship between the parent product indicated by the
              `sku` parameter is severed.
        """
        if delete is None:
            return serial.model.Array(
                self.request(
                    resource='V1/configurable-products/%s/children' % quote(sku),
                    method='GET',
                    timeout=timeout
                ),
                item_types=(model.Product,)
            )
        else:
            if not isinstance(delete, str):
                raise TypeError(
                    'The `delete` parameter must be an instance of `str`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/configurable-products/%s/children/%s' % (sku, delete),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )

    def configurable_product_variation(
        self,
        product,  # type: model.Product
        options,  # type: Sequence[model.ConfigurableProductOption]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> model.Product
        if not isinstance(product, model.Product):
            raise TypeError(
                'The `product` parameter must be an instance of `%s`, not %s.' % (
                    qualified_name(model.Product),
                    repr(product)
                )
            )
        if not isinstance(options, Sequence):
            raise TypeError(
                'The `options` parameter must be a sequence of `%s`, not %s.' % (
                    qualified_name(model.ConfigurableProductOption),
                    repr(options)
                )
            )
        for o in options:
            if not isinstance(o, model.ConfigurableProductOption):
                raise TypeError(
                    'The `options` parameter must be a sequence containing instances of `%s`, not %s.' % (
                        qualified_name(model.ConfigurableProductOption),
                        repr(o)
                    )
                )
        return model.Product(
            self.request(
                resource='V1/configurable-products/variation',
                method='PUT',
                data=model.PutConfigurableProductsVariation(
                    product=product,
                    options=options
                ),
                timeout=timeout
            )
        )

    def configurable_products_options(
        self,
        sku,  # type: str
        get=None,  # Optional[int]
        put=None,  # Optional[model.ConfigurableProductOption]
        post=None,  # Optional[model.ConfigurableProductOption]
        delete=None,  # Optional[int]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Union[Sequence[model.ConfigurableProductOption], model.ConfigurableProductOption, bool, int]
        if delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The `delete` parameter must be an instance of `str`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/configurable-products/%s/options/%s' % (sku, str(delete)),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )  # type: bool
        elif put is not None:
            if not isinstance(put, model.ConfigurableProductOption):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        qualified_name(model.ConfigurableProductOption),
                        repr(put)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/configurable-products/%s/options/%s' % (sku, str(put.id_)),
                    method='PUT',
                    data=model.PutConfigurableProductsOptions(
                        option=put
                    ),
                    timeout=timeout
                ),
                'json'
            )  # type: int
        elif post is not None:
            if not isinstance(post, model.ConfigurableProductOption):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        qualified_name(model.ConfigurableProductOption),
                        repr(post)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/configurable-products/%s/options' % sku,
                    method='POST',
                    data=model.PostConfigurableProductsOptions(
                        option=post
                    ),
                    timeout=timeout
                ),
                'json'
            )  # type: int
        elif get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an instance of `int`, not %s.' % (
                        repr(get)
                    )
                )
            return model.ProductOption(
                self.request(
                    resource='V1/configurable-products/%s/options/%s' % (sku, quote(get)),
                    method='GET',
                    timeout=timeout
                )
            )
        else:
            return serial.model.Array(
                self.request(
                    resource='V1/configurable-products/%s/options/all' % quote(sku),
                    method='GET',
                    timeout=timeout
                ),
                item_types=(model.ConfigurableProductOption,)
            )

    @property
    def products_links_types(self):
        # type: (...) -> Sequence[model.ProductLinkType]
        return serial.model.Array(
            self.request(
                resource='V1/products/links/types',
                method='GET'
            ),
            item_types=(model.ProductLinkType,)
        )  # type: Sequence[model.ProductLinkType]

    def tax_classes(
        self,
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        get=None,  # Optional[int]
        put=None,  # Optional[model.TaxClass]
        post=None,  # Optional[model.TaxClass]
        delete=None,  # Optional[int]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Optional[Union[int, bool, model.TaxClassSearchResults, model.TaxClass]]
        parameter_quantity = len(set((id(search_criteria), id(get), id(put), id(post), id(delete))) - set((id(None),)))
        if parameter_quantity < 1:
            raise ValueError(
                'A parameter must be specified for `%s`' % (
                    calling_function_qualified_name()
                )
            )
        elif parameter_quantity > 1:
            raise ValueError(
                'Only one parameter may be specified for `%s`' % (
                    calling_function_qualified_name()
                )
            )
        if put is not None:
            if not isinstance(put, model.TaxClass):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.TaxClass),
                        repr(put)
                    )
                )
            serial.marshal.validate(put)
            return int(serial.marshal.deserialize(
                self.request(
                    resource='V1/taxClasses/' + str(put.class_id),
                    method='PUT',
                    data=model.PutTaxClasses(
                        tax_class=put
                    ),
                    timeout=timeout
                ),
                'json'
            ))
        elif post is not None:
            if not isinstance(post, model.TaxClass):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.TaxClass),
                        repr(post)
                    )
                )
            serial.marshal.validate(post)
            return int(serial.marshal.deserialize(
                self.request(
                    resource='V1/taxClasses',
                    method='POST',
                    data=model.PostTaxClasses(
                        tax_class=post
                    ),
                    timeout=timeout
                ),
                'json'
            ))
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The `delete` parameter must be an `int`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/taxClasses/' + str(delete),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )
        elif get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not %s.' % (
                        repr(get)
                    )
                )
            return model.TaxClass(
                self.request(
                    resource='V1/taxClasses/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif search_criteria is not None:
            if not isinstance(search_criteria, model.SearchCriteria):
                raise TypeError(
                    'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                        qualified_name(model.SearchCriteria),
                        repr(search_criteria)
                    )
                )
            return model.TaxClassSearchResults(
                self.request(
                    resource='V1/taxClasses/search?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )

    def customer_groups(
        self,
        get=None,  # type: Optional[int]
        search_criteria=None,  # type: Optional[model.SearchCriteria]
        put=None,  # type: Optional[model.CustomerGroup]
        post=None,  # type: Optional[model.CustomerGroup]
        delete=None,  # type: Optional[int]
        timeout=None  # type: Optional[int]
    ):
        # type: (...) -> Optional[Union[int, bool, model.CustomerGroupSearchResults, model.CustomerGroup]]
        parameter_quantity = len(set((id(search_criteria), id(get), id(put), id(post), id(delete))) - set((id(None),)))
        if parameter_quantity < 1:
            raise ValueError(
                'A parameter must be specified for `%s`' % (
                    calling_function_qualified_name()
                )
            )
        elif parameter_quantity > 1:
            raise ValueError(
                'Only one parameter may be specified for `%s`' % (
                    calling_function_qualified_name()
                )
            )
        if put is not None:
            if not isinstance(put, model.CustomerGroup):
                raise TypeError(
                    'The `put` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.CustomerGroup),
                        repr(put)
                    )
                )
            serial.marshal.validate(put)
            return int(serial.marshal.deserialize(
                self.request(
                    resource='V1/customerGroups/' + str(put.id_),
                    method='PUT',
                    data=model.PutCustomerGroups(
                        group=put
                    ),
                    timeout=timeout
                ),
                'json'
            ))
        elif post is not None:
            if not isinstance(post, model.CustomerGroup):
                raise TypeError(
                    'The `post` parameter must be an instance of `%s`, not %s.' % (
                        repr(model.CustomerGroup),
                        repr(post)
                    )
                )
            serial.marshal.validate(post)
            return int(serial.marshal.deserialize(
                self.request(
                    resource='V1/customerGroups',
                    method='POST',
                    data=model.PostCustomerGroups(
                        group=post
                    ),
                    timeout=timeout
                ),
                'json'
            ))
        elif delete is not None:
            if not isinstance(delete, int):
                raise TypeError(
                    'The `delete` parameter must be an `int`, not %s.' % (
                        repr(delete)
                    )
                )
            return serial.marshal.deserialize(
                self.request(
                    resource='V1/customerGroups/' + str(delete),
                    method='DELETE',
                    timeout=timeout
                ),
                'json'
            )
        elif get is not None:
            if not isinstance(get, int):
                raise TypeError(
                    'The `get` parameter must be an `int`, not %s.' % (
                        repr(get)
                    )
                )
            return model.TaxClass(
                self.request(
                    resource='V1/customerGroups/' + str(get),
                    method='GET',
                    timeout=timeout
                )
            )
        elif search_criteria is not None:
            if not isinstance(search_criteria, model.SearchCriteria):
                raise TypeError(
                    'The `search_criteria` parameter must be an instance of `%s`, not %s.' % (
                        qualified_name(model.SearchCriteria),
                        repr(search_criteria)
                    )
                )
            return model.CustomerGroupSearchResults(
                self.request(
                    resource='V1/customerGroups/search?' + deep_object_query({'search_criteria': search_criteria}),
                    method='GET',
                    timeout=timeout
                )
            )

    def customer_groups_default(
        self,
        store_id=None,  # type: Optional[int]
        timeout=None  # type: Optional[int]
    ):
        if store_id is not None:
            if not isinstance(store_id, int):
                raise TypeError(
                    'The `store_id` parameter must be an `int`, not `%s`.' % (
                        repr(store_id)
                    )
                )
        return model.CustomerGroup(
            self.request(
                resource='V1/customerGroups/default' + ('' if store_id is None else '/' + str(store_id)),
                method='GET',
                timeout=timeout
            )
        )

    @property
    def store_websites(self):
        # type: () -> Sequence[model.StoreWebsite]
        return serial.model.Array(
            self.request(
                resource='V1/store/websites',
                method='GET'
            ),
            item_types=(model.StoreWebsite,)
        )

    @property
    def store_configs(self):
        # type: () -> Sequence[model.StoreConfig]
        return serial.model.Array(
            self.request(
                resource='V1/store/storeConfigs',
                method='GET'
            ),
            item_types=(model.StoreConfig,)
        )

    @property
    def store_views(self):
        # type: () -> Sequence[model.Store]
        return serial.model.Array(
            self.request(
                resource='V1/store/storeViews',
                method='GET'
            ),
            item_types=(model.Store,)
        )

    @property
    def store_groups(self):
        # type: () -> Sequence[model.StoreGroup]
        return serial.model.Array(
            self.request(
                resource='V1/store/storeGroups',
                method='GET'
            ),
            item_types=(model.StoreGroup,)
        )
