# region Backwards Compatibility
from __future__ import nested_scopes, generators, division, absolute_import, with_statement,\
print_function, unicode_literals
from future import standard_library
standard_library.install_aliases()
from builtins import *
# endregion

from urllib.error import HTTPError

from io import BytesIO

import magento2


class MagentoError(Exception):

    def __init__(self, http_error):
        # type: (HTTPError) -> None
        response_data = None
        response_str = ''
        if hasattr(http_error, 'file'):
            response_data = http_error.file.read()
        elif hasattr(http_error, 'fp'):
            response_data = http_error.fp.read()
        if response_data is not None:
            response_str = str(response_data, encoding='utf-8')
            http_error.file = BytesIO(response_data)
        self.http_error = http_error  # type: HTTPError
        if response_str:
            try:
                self.error_response = magento2.model.ErrorResponse(response_str)  # type: str
            except (TypeError, AttributeError) as e:
                self.error_response = None
        else:
            self.error_response = None
        self.msg = (
            'Response:\n\n    %s: %s' % (str(http_error.code), http_error.msg) + '\n\n    ' + response_str
        )  # type: str

    def __str__(self):
        return self.msg

    @property
    def args(self):
        return (self.msg,)