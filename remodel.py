import json
import os

import re
from collections import OrderedDict
from urllib.request import urlopen

import oapi
import serial
from oapi import resolve_references


def remodel(
    schema_url='http://devdocs.magento.com/swagger/schemas/latest-2.2.schema.json',
    model_path='./magento2/model.py',
    schema_path='./schema.json'
):
    # type: (str, str) -> None
    """
    This function re-generates `magento2.model` based on the swagger schema.
    """
    schema_path = os.path.abspath(schema_path)
    with urlopen(schema_url) as http_response:
        json_response_data = http_response.read()
        if not isinstance(json_response_data, str):
            json_response_data = str(
                json_response_data,
                encoding = 'utf-8'
            )
        json_response_data = json.dumps(
            json.loads(
                json_response_data,
                object_hook=OrderedDict,
                object_pairs_hook=OrderedDict
            ),
            indent=4
        )
        with open(schema_path, 'r') as schema_file:
            existing_json_data = schema_file.read()
            if not isinstance(existing_json_data, str):
                existing_json_data = str(
                    existing_json_data,
                    encoding='utf-8'
                )
        if existing_json_data != json_response_data:
            with open(schema_path, 'w') as schema_file:
                schema_file.write(
                    json_response_data
                )
    model_path = os.path.abspath(model_path)

    def rename(name, names):
        # text: (str) -> str
        for a, b in (
                ('V1(?![0-9])', ''),
                ('v1(?![0-9])', ''),
                ('Id(?![a-z])', 'ID'),
                ('Rma(?![a-z])', 'RMA'),
                ('Eav(?![a-z])', 'EAV'),
                ('Creditmemo(?![a-z])', 'CreditMemo'),
                ('Cms(?![a-z])', 'CMS'),
                ('Giftregistry', 'GiftRegistry'),
        ):
            name = re.sub(a, b, name)
        for a, b in (
                ('Object(?![a-z])', ''),
                ('Content(?![a-z])', ''),
                ('Integration(?![a-z]|$)', ''),
                ('SharedCatalog(?![a-z]|$|Interface)', ''),
                ('Catalog(?![a-z]|$|Interface)', ''),
                ('Directory(?![a-z]|$|Interface)', ''),
                ('Data(?![a-z])', ''),
                ('sItem$', 's'),
                ('Interface(?![a-z])', ''),
                ('^Framework(?![a-z])', ''),
                ('CustomersMe(?![a-z])', 'Me'),
        ):
            n = re.sub(a, b, name)
            if n not in names:
                name = n
        return name
    open_api = oapi.model.OpenAPI(urlopen(schema_url))
    # This compensates for incorrect property definitions in the default Magento Schema
    open_api.definitions[
        'framework-attribute-interface'
    ].properties[
        'value'
    ].any_of = [
        oapi.model.Schema(
            type_='string'
        ),
        oapi.model.Schema(
            type_='array',
            items=oapi.model.Schema(
                any_of=[
                    oapi.model.Schema(type_='string'),
                    oapi.model.Schema(type_='boolean'),
                    oapi.model.Schema(type_='integer')
                ]
            )
        )
    ]
    open_api.definitions[
        'error-response'
    ].properties[
        'parameters'
    ] = oapi.model.Schema(
        any_of=[
            open_api.definitions[
                'error-response'
            ].properties[
                'parameters'
            ],
            oapi.model.Schema(
                type_='array',
                items=oapi.model.Schema(
                    type_='string'
                )
            )
        ]
    )
    open_api.definitions[
        'error-response'
    ].properties[
        'parameters'
    ] = oapi.model.Schema(
        any_of=[
            open_api.definitions[
                'error-response'
            ].properties[
                'parameters'
            ],
            oapi.model.Reference(
                ref='#/definitions/error-parameters-item'
            ),
        ]
    )
    open_api.definitions[
        'error-response'
    ].properties[
        'messages'
    ] = oapi.model.Schema(
        type_='object'
    )
    open_api.definitions[
        'sales-rule-data-condition-interface'
    ].properties[
        'value'
    ] = oapi.model.Schema(
        any_of=[
            open_api.definitions[
                'sales-rule-data-condition-interface'
            ].properties[
                'value'
            ],
            oapi.model.Schema(
                type_='boolean'
            ),
            oapi.model.Schema(
                type_='array',
                items=oapi.model.Schema(
                    type_='string'
                )
            )
        ]
    )
    open_api_model = oapi.Model(
        open_api,
        rename=rename
    )
    temp_path = '/'.join(model_path.split('/')[:-1]) + '/_model.py'
    if os.path.exists(model_path):
        os.rename(model_path, temp_path)
    with open(model_path, 'w') as model_file:
        model_file.write(str(open_api_model))
    if os.path.exists(temp_path):
        os.remove(temp_path)
    return open_api_model


if __name__ == '__main__':
    remodel()